 
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="x_min-width" style="table-layout:fixed">
    <tbody>
        <tr>
            <td align="center" class="x_section-padding">
                <table border="0" cellpadding="0" cellspacing="0" width="600" class="x_responsive-table">
                <tbody>
                <tr>
                <td style="vertical-align:top; border-collapse:collapse">
                <table border="0" cellpadding="0" cellspacing="0" width="600" class="x_responsive-table">
                <tbody>
                <tr>
                <td style="vertical-align:top; border-collapse:collapse">

                <!-- #f6f6f6   Color del Header-->  
                <table bgcolor="#f6f6f6" width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top-left-radius:5px; border-top-right-radius:5px;    box-shadow: 2px 3px 10px rgba(0, 0, 0, 0.25);">
                    <tbody>
                         <tr>
                            <td style="border-top-left-radius: 5px;border-top-right-radius: 5px;vertical-align: top;border-collapse: collapse;padding-top: 15px;
                            padding-right: 7%;padding-left: 7%;padding-bottom: 15px;background-color: #c27d0e;font-size: 22px;font-family: Proxima Nova,'proxima_nova_regular',Helvetica,Arial,sans-serif;
                            color: white;text-align: center;text-transform: uppercase;">
                            Unidad Educativa General Eloy Alfaro
                            </td>
                        </tr>
                         
                        <tr>

                        <td style="padding:0px 35px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                        <td class="x_padding2">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top-left-radius:5px; border-top-right-radius:5px">
                            <tbody>
                                <tr>
                                    <td align="center" style="padding:25px 0 10px 0">
                                        <img src="http://www.fixsystem.com.ec/runa/noticias.png" width="66" height="66" border="0" alt="" class="x_img-max" style="display:block; padding:0; color:#777777; text-decoration:none"> 
                                        <p style="    font-size: 16px;font-family: Proxima Nova,'proxima_nova_regular',Helvetica,Arial,sans-serif; text-decoration: none;color: #777777;text-align: center;font-weight: 700;">
                                                    NOTICIAS  </p> 
                                     
                                    </td>

                                </tr>
                                </tbody>
                            </table>

                            </td>
                        </tr>
                             
                         
                              @foreach($articulos as $articulo)
                                <tr>
                                <td align="center" class="x_padding2" style="padding:25px 0 0 0">
                                    <table border="0" cellspacing="0" cellpadding="0" class="x_responsive-button">
                                        <tbody>
                                        <tr>
                                        <td align="center" style="padding:0">
                                        <table border="0" cellspacing="0" cellpadding="0" class="x_responsive-button">
                                            <tbody>
                                            <tr>
                                                <td align="left" bgcolor="#ffffff" style="-webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; font-size:16px; font-family:Proxima Nova,'proxima_nova_regular',Helvetica,Arial,sans-serif; color:#777777; text-decoration:none; color:#777777; text-decoration:none; -webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; padding:12px 18px; border:1px solid #ffffff; display:block; margin-bottom:10px;border: 1px dashed rgb(197, 197, 197)">
                                                   <b>{{$articulo->titulo}}</b> 
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                                <tr>
                                    <td align="left" class="x_padding2" style="-webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; font-size:16px; font-family:Proxima Nova,'proxima_nova_regular',Helvetica,Arial,sans-serif; color:#777777; text-decoration:none; color:#777777; text-decoration:none; -webkit-border-radius:5px; -moz-border-radius:5px; border-radius:5px; padding:12px 18px; border:1px solid #ffffff; display:block; margin-bottom:10px)">
                                         <?php
                                                                $ser1 = substr($articulo->texto,0,400);
                                                                echo strip_tags($ser1); 
                                         ?>
                                    </td>
                                </tr>
                                   <tr>
                                        <td align="center" class="x_padding2" style="padding:25px 0px 0px 0px">
                                        <table border="0" cellspacing="0" cellpadding="0" class="x_responsive-button">
                                            <tbody>
                                                <tr>
                                                <td align="center" style="padding:0px">
                                                <table border="0" cellspacing="0" cellpadding="0" class="x_responsive-button">
                                                    <tbody>
                                                        <tr>
                                                            <!-- #925b03   Color del BOTON -->  
                                                            
                                                        <td align="center" bgcolor="#925b03" style="-webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px">
                                                        <a href="#" target="_blank" style="font-size:16px; font-family:Proxima Nova,'proxima_nova_regular',Helvetica,Arial,sans-serif; color:#ffffff; text-decoration:none; color:#ffffff; text-decoration:none; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; padding:12px 18px;  display:block">
                                                            <b>Leer</b> más </a>
                                                        </td>
                                                        </tr>
                                                        
                                                    </tbody>
                                                </table>
                                                </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                        </tr>
                                        <tr> <td> <hr style="border: 1px dashed #cecece;"> </td> </tr>

                                 @endforeach

                             
                         
                    </tbody>
                    </table>
                    </td>
                    </tr>
                   
                    </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
                </td>
                </tr>
                </tbody>
                </table>
        </td>
    </tr>
    </tbody>
</table>

 