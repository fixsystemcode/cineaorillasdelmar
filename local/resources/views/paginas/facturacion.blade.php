<!DOCTYPE html>
<html lang="en">
<head>
    <title>Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Favicon -->
    
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="canonical" href="webpage" />
    <meta name="msapplication-TileColor" content="#403cba">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 190 instituciones educativas y alrededor de 200000 usuarios usan RUNACHAY." />
    <!-- en si es ingles, es si es español -->
    <meta property="og:locale" content="es">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Runachay - Formulario de contacto">
    <meta property="og:description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 190 instituciones educativas y alrededor de 200000 usuarios usan RUNACHAY.">
    <meta property="og:url" content="http://runachay.com.ec/contratarunachay">
    <meta property="og:site_name" content="Runachay">
    <meta property="article:publisher" content="https://www.facebook.com/Runachay-322670881553067/">
    <meta property="og:image" content="{{asset('assets/images/logo128.png')}}">
    <meta name="theme-color" content="#40609e">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">





    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Font Awesome 4.7.0 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('assets/css/alertify.css')}}">
    <!-- Main style -->

    <link rel="stylesheet" href="{{asset('assets/css/contratar.css')}}">

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">

</head>
<body>

    <section class="contratar_section">
        <div class="cs_container">
            <div class="cs_lateral">
                <div class="csl_box">
                    <div class="csl_container">
                        <div class="csl_top" style="">
                            <img src="{{asset('assets/images/logo2.png')}}" alt="">
                            <h2>Formulario de datos para facturación</h2>
                        </div>
                        <div class="csl_bottom" style="">
                            <img src="{{asset('assets/images/macbook.png')}}" alt="">
                            <h4>Transformando la educación del Ecuador.</h4>
                        </div>
                    </div>
                </div>
                    
            </div>
            <div class="cs_main">
                <div class="csm_box">
                    <div class="csm_title">
                        <h2>Runachay.</h2>
                        <p>Datos para Facturación.</p>
                        <h4 style="color: red;"><strong>Formulario no disponible temporalmente.</strong></h4>
                    </div>
                    <div class="csm_form">
                        {!! Form::open(['route'=>'registrarfacturacion.store','method'=>'POST','class'=>'form-registro','data-formtype'=>'contact']) !!}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="cmsf_item">
                                    <label for="ue">Unidad Educativa:</label>
                                    <input type="text" name="ue" aria-required="true" aria-invalid="false" placeholder="" id="ue" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="cmsf_item">
                                    <label for="ruc">Cédula o Ruc:</label>
                                    <input type="text" name="ruc" aria-required="true" aria-invalid="false" placeholder="" id="ruc" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="cmsf_item">
                                    <label for="razon">Nombre o Razón social:</label>
                                    <input type="text" name="razon" aria-required="true" aria-invalid="false" placeholder="" id="razon" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="cmsf_item">
                                    <label for="direccion">Dirección:</label>
                                    <input type="text" name="direccion" aria-required="true" aria-invalid="false" placeholder="" id="direccion" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="cmsf_item">
                                    <label for="telefono">Teléfono:</label>
                                    <input type="text" name="telefono" aria-required="true" aria-invalid="false" placeholder="" id="telefono" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="cmsf_item">
                                    <label for="valor">Valor:</label>
                                    <input type="number" name="valor" aria-required="true" aria-invalid="false" placeholder="" id="valor" required>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-7 col-sm-12">
                                <div class="g-recaptcha" data-sitekey="6Lf6f_YZAAAAAApN1-3aQCrLUpbnL9UYRF6AbiJP"></div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="csmf_button">
                                    <button class="" type="submit">Enviar</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="cs_separador"></div>
                    <div class="cs_contacto">
                        <p><span>*</span> Si necesitas ayuda personalizada puedes contactarnos por otros medios:</p>
                        <div class="csc_buttons">
                            <a href="#" class="csc_wide" onclick="contactos()">Información de contacto</a>
                            <a href="https://www.facebook.com/Runachay-322670881553067" class="csc_social" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://www.instagram.com/runachayecuador/" class="csc_social" target="_blank"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    
    <!-- JQuery v2.2.4 -->
    <script src="{{asset('assets/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="{{asset('assets/js/jquery.counterup.js')}}"></script>

    <script src="{{asset('assets/js/alertify.js')}}"></script>


    <script src="{{asset('assets/js/bootstrap.js')}}"></script>
    <!-- Superfish v1.7.9 -->
    <script src="{{asset('assets/js/plugins/jquery.superfish.min.js')}}"></script>


    <script>
        $
        function contactos(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'Información de contacto';

            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>Puede ponerse en contacto directamente con nosotros enviándonos un mensaje vía WhatsApp al número 0980761961 o vía correo electrónico:</h4><ul style='color: #20A4F0;margin-bottom: 20px;'><li>ventas@runachay.com.ec</li><li>gerencia@runachay.com.ec</li></ul><p style='color: #98A5B5;font-size: 13px;'>Nuestros horarios de atención son de Lunes a Viernes de 8:00 a 18:00 y Sábados de 8:00 a 12:00.</p></div>");
        }
    </script>

    <script>
        jQuery('form.form-registro').on('submit', function( e ){
            e.preventDefault();
            var $form = jQuery(this);
            if ($form.find('.result').hasClass('sc_infobox_style_error')) {
                $form.find('.result').removeClass('sc_infobox_style_success');
                return;
            };
            $form.find('.result').removeClass('sc_infobox_style_error');
            $form.find('.result').css('display', "block");
            var request = $form.serialize();
            console.log(request);
            var ajax = jQuery.post( "registrarfacturacion", request )
                .done(function( data ) {
                        if (data==0){
                            jQuery(function($){
                                alertify.success('Registro guardado correctamente.');
                                $('form.form-registro')[0].reset();
                                grecaptcha.reset();
                            });
                        }
                        if (data==1){
                            jQuery(function($){
                                alertify.error('Al parecer tuvimos problemas al enviar tu solicitud, por favor vuelve a intentarlo.');
                            });
                        }
                        if (data==2) {
                            jQuery(function($){
                                alertify.warning('El CAPTCHA es obligatorio.');
                            });
                        }
                        console.log(data);
            })
                .fail(function( data ) {
                    jQuery(function($){
                        alertify.error('Al parecer tuvimos problemas al enviar tu solicitud, por favor vuelve a intentarlo.x');
                        console.log(data);
                    });
            })
        });
    </script>

</body>
</html>