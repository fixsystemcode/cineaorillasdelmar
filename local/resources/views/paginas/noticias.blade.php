<!DOCTYPE html>
<html lang="en">
<head>
    <title>Runachay | Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="favicon.ico" />
    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Font Awesome 4.7.0 -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <!-- Main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.mb.YTPlayer.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">


    <style>
        .lado1,
        .imag-ce,
        .lado2,
        .lado3,
        .lado4,
        .lado5,
        .lado6,
        .lado7,
        .lado8,
        .lado9,
        .modulo-dots{
            visibility: hidden;
        }

        /*iframe#iframe_P1{
            width: 100% !important;
            height: 100% !important;
            margin: 0 !important;
            top: -5px !important;
        }*/

        /*.lado1,
        .imag-ce,
        .lado2,
        .lado3,
        .lado4,
        .lado5,
        .lado6,
        .lado7,
        .lado8,
        .lado9,
        .modulo-dots
        {
          opacity: 1;
          transition: opacity .7s ease-in, margin-top .7s ease-out;
        }*/


        /*.js-reveal {
          opacity: 1;
          transition: opacity .7s ease-in, margin-top .7s ease-out;
        }*/

        .js-reveal--hidden {
          opacity: 0;
          margin-top: 5px;
        }

        .cont-cont{
            position: relative;
            /*height: 455.063px;*/
            width: 55%;
            right: 50%;
            left: 50%;
            transform: translate(-50%, 0%);
            margin: 30px 0;
        }
        .foot-video h3{
            font-size: 38px;
            font-weight: 400;
            /*color: purple;*/
            color: #484696;
            padding: 0 20%;
        }
        .foot-video strong{
            color: #00ADDD;
            font-weight: 600;
        }

        
    </style>


</head>
<body class="home-01">
    <div class="wrapp-content">
        <!-- Header -->
        <header class="wrapp-header" style="padding-top: 60px; min-height: auto;">
            <div class="main-nav">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="./" class="logo">
                                <img src="{{asset('assets/img/logoprincipal2.png')}}" alt="">
                            </a>
                            <div class="main-nav__btn">
                                <div class="icon-left"></div>
                                <div class="icon-right"></div>
                            </div>
                            <ul class="main-nav__list">
                                <li>
                                    <a href="#">Inicio</a>
                                </li>
                                <li>
                                    <a href="#">Módulos</a>
                                    <ul>
                                        <li>
                                            <a href="#comunicacion">Comunicación</a>
                                        </li>
                                        <li>
                                            <a href="#matriculacion">Matriculación</a>
                                        </li>
                                        <li>
                                            <a href="#calificaciones">Calificaciones</a>
                                        </li>
                                        <li>
                                            <a href="#planificaciones">Planificación</a>
                                        </li>
                                        <li>
                                            <a href="#contabilidad">Contabilidad</a>
                                        </li>
                                        <li>
                                            <a href="#app">APP movil</a>
                                        </li>
                                        <li>
                                            <a href="#dece">DECE</a>
                                        </li>
                                        <li>
                                            <a href="#cobranzas">Cobranzas</a>
                                        </li>
                                        <li>
                                            <a href="#evaluacion">Evaluación</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                    </div>
                </div><!-- /.container-fluid -->
            </div>
        </header>
        <!-- Content -->
        <main class="content-row">
            <div class="page-title-wrapp">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-title-01">Blog</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="breadcrumbs">
                                <li class="active">
                                    <a href="index.html">Home</a>
                                </li>
                                <li>Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-box-01 padding-top-33 padding-bottom-36">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="blog-listing right">
                                @foreach($noticias as $noti)
                                <article class="blog-post">
                                    <figure class="blog-post__img">
                                        <a href="{{URL::to('/blog')}}/{{$noti->slug}}">
                                            <img src="{{asset('local/public/images')}}/{{$noti->path}}" alt="">
                                        </a>
                                    </figure>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">
                                                    <?php echo date('d', strtotime($noti->created_at)); ?>
                                                    <?php echo date('M Y', strtotime($noti->created_at)); ?>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Admin</a>
                                                </p>
                                            </li>
<!--                                             <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li> -->
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="{{URL::to('/blog')}}/{{$noti->slug}}">{{$noti->titulo}}</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>
                                            <?php $ser1 = substr($noti->texto,0,400);echo strip_tags($ser1); ?>...
                                        </p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="{{URL::to('/blog')}}/{{$noti->slug}}" class="blog-post__btn">Leer Más</a>
                                        <!-- <a href="#" class="blog-post__likes">255 Likes</a> -->
                                    </div>
                                </article>
                                @endforeach

                                <!-- <article class="blog-post">
                                    <blockquote>
                                        <p>Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma - which is living with the results of other people's thinking. </p>
                                        <span class="blockquote-author">&mdash; Steve Jobs</span>
                                    </blockquote>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">24 September, 2017</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Katty Simon</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="blog_post_right_sidebar.html">A Virtual High School Experience</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>To be an enthusiast had become her social vocation and, sometimes even when she did not feel like it, she became enthusiastic in order not to disappoint the expectations of those who knew her. </p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="blog_post_right_sidebar.html" class="blog-post__btn">Read More</a>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                    </div>
                                </article> -->

                                <!-- <article class="blog-post">
                                    <div class="nivoSlider">
                                        <img src="{{asset('assets/img/blog_post/blog_post_02.jpg')}}" alt='' />
                                        <img src="{{asset('assets/img/blog_post/blog_post_03.jpg')}}" alt='' />
                                        <img src="{{asset('assets/img/blog_post/blog_post_04.jpg')}}" alt='' />
                                    </div>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">24 September, 2017</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Katty Simon</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="blog_post_right_sidebar.html">The First Twelve Years</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>To be an enthusiast had become her social vocation and, sometimes even when she did not feel like it, she became enthusiastic in order not to disappoint the expectations of those who knew her. The subdued smile which, though it did not suit
                                            her faded features, always playedround her lips expressed, as in a spoiled child.</p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="blog_post_right_sidebar.html" class="blog-post__btn">Read More</a>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                    </div>
                                </article> -->

                                <!-- <article class="blog-post">
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">24 September, 2017</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Katty Simon</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="blog_post_right_sidebar.html">2017-18 Student Application</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>To be an enthusiast had become her social vocation and, sometimes even when she did not feel like it, she became enthusiastic in order not to disappoint the expectations of those who knew her. The subdued smile which, though it did not suit
                                            her faded features, always playedround her lips expressed, as in a spoiled child.</p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="blog_post_right_sidebar.html" class="blog-post__btn">Read More</a>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                    </div>
                                </article> -->

                                <!-- <article class="blog-post">
                                    <div class="blog_post_image">
                                        <div class="video_bg">
                                            <div class="video_img-01"></div>
                                            <a class="play-video" href="#" data-video-url="https://www.youtube.com/embed/jK4a5m7u_-Q?rel=0">Play Video</a>
                                            <iframe class="video_frame frame16x10" src="https://www.youtube.com/embed/jK4a5m7u_-Q?rel=0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">24 September, 2017</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Katty Simon</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="blog_post_right_sidebar.html">Student Meetings And Games</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>To be an enthusiast had become her social vocation and, sometimes even when she did not feel like it, she became enthusiastic in order not to disappoint the expectations of those who knew her. The subdued smile which, though it did not suit
                                            her faded features, always playedround her lips expressed, as in a spoiled child.</p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="blog_post_right_sidebar.html" class="blog-post__btn">Read More</a>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                    </div>
                                </article> -->

                                <!-- <article class="blog-post">
                                    <div class="link-format">
                                        <h3 class="link-format__title">Smart Education are opennig new courses</h3>
                                        <p class="link-format__subtitle">http://gt3themes.com/</p>
                                    </div>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">24 September, 2017</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Katty Simon</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="blog_post_right_sidebar.html">Your Future with Smart Education</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>To be an enthusiast had become her social vocation and, sometimes even when she did not feel like it, she became enthusiastic in order not to disappoint the expectations of those who knew her. The subdued smile which, though it did not suit
                                            her faded features, always playedround her lips expressed, as in a spoiled child.</p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="blog_post_right_sidebar.html" class="blog-post__btn">Read More</a>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                    </div>
                                </article> -->

                                <!-- <article class="blog-post">
                                    <div class="music-format">
                                        <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/136949481&amp;color=a0ce4e&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                                    </div>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">24 September, 2017</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Katty Simon</a>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        <a href="blog_post_right_sidebar.html">The Best Music for You Relax</a>
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>To be an enthusiast had become her social vocation and, sometimes even when she did not feel like it, she became enthusiastic in order not to disappoint the expectations of those who knew her. </p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <a href="blog_post_right_sidebar.html" class="blog-post__btn">Read More</a>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                    </div>
                                </article> -->

                                <div class="blog-pagination">
                                    <ul class="pagination-list">
                                        <li class="active">
                                            <a href="#">1</a>
                                        </li>
                                        <li>
                                            <a href="#">2</a>
                                        </li>
                                        <li>
                                            <a href="#">3</a>
                                        </li>
                                        <li>
                                            <a href="#">next</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <aside class="blog-listing-sidebar right">
                                <div class="widget sidebar-search-block">
                                    <form class="sidebar-search" action="./">
                                        <div class="sidebar-search__label">
                                            <input class="sidebar-search__inp-text" placeholder="Search..." type="text" name="inp-text">
                                            <button class="sidebar-search__inp-btn" type="button">Search</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="widget categories">
                                    <h3 class="widget-title">Categories</h3>
                                    <ul class="categories-list">
                                        <li>
                                            <a href="#">Student Life (15)</a>
                                            <ul>
                                                <li>
                                                    <a href="#">Academics (08)</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Campus Life (17)</a>
                                        </li>
                                        <li>
                                            <a href="#">Research (09)</a>
                                        </li>
                                        <li>
                                            <a href="#">Alumni (25)</a>
                                        </li>
                                        <li>
                                            <a href="#">Admissions (03)</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget related-posts">
                                    <h3 class="widget-title">Related Posts</h3>
                                    <ul class="related-posts__list">
                                        <li>
                                            <figure class="related-posts__img">
                                                <a href="#">
                                                    <img src="img/sidebar_img/related_posts/related_posts_01.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="related-posts__data">
                                                24 September, 2017
                                            </div>
                                            <h3 class="related-posts__title">
                                                <a href="#">New Courses for 2018</a>
                                            </h3>
                                        </li>
                                        <li>
                                            <figure class="related-posts__img">
                                                <a href="#">
                                                    <img src="img/sidebar_img/related_posts/related_posts_02.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="related-posts__data">
                                                24 September, 2017
                                            </div>
                                            <h3 class="related-posts__title">
                                                <a href="#">The Best Campus Tour</a>
                                            </h3>
                                        </li>
                                        <li>
                                            <figure class="related-posts__img">
                                                <a href="#">
                                                    <img src="img/sidebar_img/related_posts/related_posts_03.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="related-posts__data">
                                                24 September, 2017
                                            </div>
                                            <h3 class="related-posts__title">
                                                <a href="#">One Day with Us</a>
                                            </h3>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget tags-cloud">
                                    <h3 class="widget-title">Tags</h3>
                                    <ul class="tags-cloud__list">
                                        <li>
                                            <a href="#">Courses</a>
                                        </li>
                                        <li>
                                            <a href="#">Research</a>
                                        </li>
                                        <li>
                                            <a href="#">Alumni</a>
                                        </li>
                                        <li>
                                            <a href="#">Student Life</a>
                                        </li>
                                        <li>
                                            <a href="#">Learning</a>
                                        </li>
                                        <li>
                                            <a href="#">Study</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Admission</a>
                                        </li>
                                        <li>
                                            <a href="#">Campus</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget instagram">
                                    <h3 class="widget-title">Instagram</h3>
                                    <ul class="instagram__list">
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_01.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_02.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_03.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_04.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_05.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_06.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_07.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_08.jpg" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget text-widget">
                                    <h3 class="widget-title">Text Widget</h3>
                                    <p>Our campus is beautiful, complete with towering green trees and brick buildings. There are coffee shops and sports, restaurants and a multitude of great study spots. </p>
                                </div>
                                <div class="widget text-widget">
                                    <img src="img/sidebar_img/banner_img.jpg" alt="">
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<!-- Footer -->
        <footer class="wrapp-footer">
            <div class="footer-box-01">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <a href="./" class="footer-logo">
                                <img src="{{asset('assets/img/logo_white.png')}}" alt="">
                            </a>
                            <ul class="widget-contact">
                                <li>
                                    <h4 class="widget-contact__title">Ubicación:</h4>
                                    <p class="widget-contact__text">Calle Oliva Miranda y Alberto Oblete, Manta - Ecuador</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Horarios de atención:</h4>
                                    <p class="widget-contact__text">9:00am - 5:00pm</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Teléfono:</h4>
                                    <p class="widget-contact__text">+593 98 076 19610</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Email:</h4>
                                    <p class="widget-contact__text">
                                        <a class="widget-contact__text-email" href="mailto:thesmartedu@edu.com">soporte@runachay.com.ec</a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <!--
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="widget-link">
                                <h3 class="widget-title">Explore</h3>
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">History &amp; Mission</a>
                                    </li>
                                    <li>
                                        <a href="#">Administration</a>
                                    </li>
                                    <li>
                                        <a href="#">Community</a>
                                    </li>
                                    <li>
                                        <a href="#">Around the World</a>
                                    </li>
                                    <li>
                                        <a href="#">News Network</a>
                                    </li>
                                    <li>
                                        <a href="#">Visitor Information</a>
                                    </li>
                                    <li>
                                        <a href="#">Social Media</a>
                                    </li>
                                    <li>
                                        <a href="#">Our Campuses</a>
                                    </li>
                                    <li>
                                        <a href="#">Campus Directories</a>
                                    </li>
                                    <li>
                                        <a href="#">Employment</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        -->
                        <!--
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="widget-link">
                                <h3 class="widget-title">Admissions</h3>
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">Undergraduate</a>
                                    </li>
                                    <li>
                                        <a href="#">By School</a>
                                    </li>
                                    <li>
                                        <a href="#">Process</a>
                                    </li>
                                    <li>
                                        <a href="#">Visitor Information</a>
                                    </li>
                                    <li>
                                        <a href="#">For Prospective Parents</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        -->
                        <!-- <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="widget-link">
                                <h3 class="widget-title">Research</h3>
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">Applied Physics Laboratory</a>
                                    </li>
                                    <li>
                                        <a href="#">Research Projects Administration</a>
                                    </li>
                                    <li>
                                        <a href="#">Funding Opportunities</a>
                                    </li>
                                    <li>
                                        <a href="#">Undergraduate Research</a>
                                    </li>
                                    <li>
                                        <a href="#">Resources</a>
                                    </li>
                                    <li>
                                        <a href="#">Health &amp; Medicine</a>
                                    </li>
                                    <li>
                                        <a href="#">Social Sciences, Humanities &amp; Arts</a>
                                    </li>
                                    <li>
                                        <a href="#">Natural Sciences, Engineering &amp; Tech</a>
                                    </li>
                                    <li>
                                        <a href="#">Global Research</a>
                                    </li>
                                    <li>
                                        <a href="#">Technology Transfer</a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="footer-box-02">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <p class="copy-info">&copy; 2018 Runachay. Todos los derechos reservados</p>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 text-center">
                            <ul class="social-list-01">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-info">
                                <a class="footer-info__01" href="#">Política de privacidad</a>
                                <a class="footer-info__02" href="#">Térnimos de uso</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="back2top" title="Back to Top">Ir arriba</a>
        </footer>
    </div>
    <!-- JQuery v2.2.4 -->
    <script src="{{asset('assets/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
    <script src="{{asset('assets/js/jquery.mb.YTPlayer.js')}}"></script>
    
    <script>
        $(document).ready(function(){
            var anchoi=$(".cont-cont").width();
               var radioi=0.5625;
               var resuli = anchoi*radioi;
               $(".cont-cont").css('height', resuli);

            $(window).resize(function(){
               var alto=$(".cont-cont").height();
               var ancho=$(".cont-cont").width();
               var radio=0.5625;
               var resul = ancho*radio;
               $(".cont-cont").css('height', resul);
            });
        })
    </script>


    <script src="{{asset('assets/js/scrollreveal.js')}}"></script>
    <script>
        window.sr = ScrollReveal();

        $(document).ready(function(){
            $(window).scroll(function(event) {
                var dist = 600;
                var scrollTop = $(window).scrollTop();
                // alert(scrollTop==dist);
                if (scrollTop > dist) {
                    sr.reveal('.imag-ce', {duration: 1000, delay: 0, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado1', {duration: 1000, delay: 500, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado2', {duration: 1000, delay: 650, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado3', {duration: 1000, delay: 800, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado4', {duration: 1000, delay: 950, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado8', {duration: 1000, delay: 1100, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado7', {duration: 1000, delay: 1250, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado6', {duration: 1000, delay: 1400, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado5', {duration: 1000, delay: 1550, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado9', {duration: 1000, delay: 1700, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.modulo-dots', {duration: 1000, delay: 1850, distance: '0px', scale: 1, opacity:0});
                }
            });
        });




        // window.sr = ScrollReveal();

        // sr.reveal('.iconocentral', { viewFactor: 1,});
        // sr.reveal('.lado1', {viewFactor: 1, duration: 1000, delay: 500, distance: '0px', scale: 0.8});
        // sr.reveal('.lado2', {viewFactor: 1, duration: 1000, delay: 650, distance: '0px', scale: 0.8});
        // sr.reveal('.lado3', {viewFactor: 1, duration: 1000, delay: 800, distance: '0px', scale: 0.8});
        // sr.reveal('.lado4', {viewFactor: 1, duration: 1000, delay: 950, distance: '0px', scale: 0.8});
        // sr.reveal('.lado8', {viewFactor: 1, duration: 1000, delay: 1100, distance: '0px', scale: 0.8});
        // sr.reveal('.lado7', {viewFactor: 1, duration: 1000, delay: 1250, distance: '0px', scale: 0.8});
        // sr.reveal('.lado6', {viewFactor: 1, duration: 1000, delay: 1400, distance: '0px', scale: 0.8});
        // sr.reveal('.lado5', {viewFactor: 1, duration: 1000, delay: 1550, distance: '0px', scale: 0.8});
        // sr.reveal('.lado9', {viewFactor: 1, duration: 1000, delay: 1700, distance: '0px', scale: 0.8});
        // sr.reveal('.dots', {viewFactor: 1, duration: 1000, delay: 1850, distance: '0px', scale:0.8});
        
        // sr.reveal('.table-03__box-01', {container: '.fooContainer', duration: 1000, delay: 50, distance: '0px', scale:0.8});
        
    </script>

    <script src="{{asset('assets/js/bootstrap.js')}}"></script>
    <!-- Superfish v1.7.9 -->
    <script src="{{asset('assets/js/plugins/jquery.superfish.min.js')}}"></script>

    <script src="{{asset('assets/js/plugins/jquery.nivo.min.js')}}"></script>
    <!-- Owl carousel v2.2.1 -->
    <!-- Waypoint -->
    <!-- Progress circle -->

    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/jquery.scroll-reveal.js"></script> -->
    <!-- Main script -->
    <script src="{{asset('assets/js/main.js')}}"></script>

    <script>
        $(document).ready(function(){
           $(window).bind('scroll', function() {
           var distance = 50;
                 if ($(window).scrollTop() > distance) {
                     $('.main-nav').addClass('scrolled');
                    $('.main-nav > div').addClass('paddingtop');
                    $('.navbar-brand').addClass('brand-scrolled');
                 }
                 else {
                     $('.main-nav').removeClass('scrolled');
                    $('.main-nav > div').removeClass('paddingtop');
                    $('.navbar-brand').removeClass('brand-scrolled');
                 }
            });
        });
    </script>
</body>
</html>