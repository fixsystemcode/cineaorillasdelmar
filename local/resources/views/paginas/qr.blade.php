<html lang="es">
<head>
<style type="text/css">
	@page { margin: 0px; } 
	body {
		margin: 1px;
		background-size: 100%;
        background-position: center center;
		background-attachment: fixed;
        margin: 0px;
	}
	.divnombres{
		position: fixed;
		top: 36%;
	}

	.divnombres h1{
		font-size: 1.8em;
		font-family: 'Encode Sans Semi Expanded', sans-serif;
		font-weight: 45;
		text-align: center;
		text-transform: uppercase;	
	}

	.divnombres h2{
		font-size: 1.8em;
		font-family: 'Encode Sans Semi Expanded', sans-serif;
		font-weight: 100;
		text-align: center;
		text-transform: uppercase;	
	}

</style>      
	
</head>
<body>
	<?php
    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }
    ?>
	<div class="divnombres">
		<h2><strong>RESERVA VÁLIDA PARA {{$infowebi->tema}}!</strong></h2><br>
		<h1><strong>Nombres: </strong>
		{{$inforeserva->nombres}}
		</h1><br>
		<h1><strong>Apellidos: </strong>
		{{$inforeserva->apellidos}}
		</h1><br>
		<h1><strong>Correo: </strong>
		{{$inforeserva->email}}
		</h1><br>
		<h1><strong>Fecha y Hora de registro: </strong>
		<?php echo date('d', strtotime($inforeserva->created_at))." de ".mesES($inforeserva->created_at)." del ".date('Y', strtotime($inforeserva->created_at)); ?></span> a las <?php echo date( 'H:i', strtotime($inforeserva->created_at));  ?>
		</h1><br>
	</div>
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<g id="qrcode"/>
	</svg>
    
</body>
</html>
<script type="text/javascript">
   	var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    var ruta = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));

    var id = $inforeserva->id;

    var linkqr = ruta+'reserva/'+id;

  var qrcode = new QRCode(document.getElementById("qrcode"), {
      width : 100,
      height : 100,
      useSVG: true
  });

  function makeCode () {		
      qrcode.makeCode(linkqr);
  }

  makeCode();

  $("#text").
      on("blur", function () {
          makeCode();
      }).
      on("keydown", function (e) {
          if (e.keyCode == 13) {
              makeCode();
          }
      });
    </script>