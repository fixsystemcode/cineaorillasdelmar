<!DOCTYPE html>
<html lang="en">
<?php
    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }
    ?>
<head>
	<title>Registro Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 280 instituciones educativas y alrededor de 500000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">

	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/alertify.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/webinar/style.css')}}">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/e8d895419f.js" crossorigin="anonymous"></script>

	<meta property="og:url"           content="{{URL::current()}}" />
	<meta property="og:type"          content="Certificación" />
	<meta property="og:title"         content="Certificación de {{$infowebi->tema}}" />
	<meta property="og:description"   content="#docenciaconexcelencia. Siempre innovándome, siempre capacitándome. He asistido al evento {{$infowebi->tema}}" />
	<meta property="og:image"         content="http://runachay.com.ec/img/repo/runachay/certificaciones/cert.jpg" />


	<meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="#docenciaconexcelencia. Siempre innovándome, siempre capacitándome. He asistido al evento {{$infowebi->tema}}">
    <meta name="twitter:title" content="Certificación de {{$infowebi->tema}}">
    <meta name="twitter:site" content="{{URL::current()}}">
    <meta name="twitter:image" content="http://runachay.com.ec/img/repo/runachay/certificaciones/cert.jpg">
    <meta name="twitter:creator" content="@runachayec">



	<style>
		.certificaci span{
			padding: 4px 20px;
			border-radius: 8px;
			background-color: #b6d9ff;
			color: #1361d1;
			font-weight: 700;
			font-size: 16px;
			margin-bottom: 10px;
			display: inline-block;
		}
	</style>
	
</head>
<body>

	@if($infowebi == null)

	El webinar no fue encontrado, por de click <a>aqui</a> para regresar 

	@else

	   @if($infowebi->estado == 1)

		<section class="form_page">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="row">
							<div class="col-lg-12">
								<div class="fp_head mb40"><img src="{{asset('assets/img/logocuadrado.png')}}" alt=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-xxl-5 col-xl-6 col-lg-6 order-last order-lg-first">
								<div class="fp_data">
									<div class="fp_title mb40">
										<h5 class="certificaci"><span>Certificación</span></h5>
										<h2 class="color_title"><strong>{{$infowebi->tema}}</strong></h2>
										<p style="display: none;">
											@if($dias_evento == 1)
												<strong>FECHA : </strong><?php echo date('d', strtotime($infowebi->fecha))." de ".mesES($infowebi->fecha)." del ".date('Y', strtotime($infowebi->fecha)); ?></span>.
											@else
												<strong>DESDE EL : </strong><?php echo date('d', strtotime($infowebi->fecha))." de ".mesES($infowebi->fecha)." del ".date('Y', strtotime($infowebi->fecha)); ?></span>. <strong>HASTA EL:</strong> <?php echo date('d', strtotime($ultimo_dia->fecha))." de ".mesES($ultimo_dia->fecha)." del ".date('Y', strtotime($ultimo_dia->fecha)); ?>
											@endif
										</p>
									</div>
									<div class="fp_anuncio mb20">
										<img src="{{asset('local/public/images')}}/{{$infowebi->anuncio}}" alt="" style="width: 100%;">
									</div>
									
								</div>
							</div>
							<div class="col-xxl-6 col-xl-6 col-lg-6 offset-xxl-1 order-first order-lg-last">
								@if(Session::has('message'))
								<div class="fp_form" id="box_confirm">
										<div class="row">
											<div class="col-lg-12">
												<div class="fpf_title mb20">
													<h3><strong>Registro exitoso</strong></h3>
													<br>
													<p>Tu registro se ha realizado con éxito, la información de acceso ha sido enviada a tu correo electrónico.</p>
													<p>Podrás acceder al evento desde el siguiente enlace:</p>
													<div style="padding: 12px 20px;background: #f5f5f5;border-radius: 0;display: flex;align-items: center;">
														<a href="{{ session::get('enlace') }}" style="color: #0A70CE;text-decoration: none;font-weight: 600;" id="enlace"> {{ session::get('enlace') }}</a>
														<a onclick="copiarAlPortapapeles('enlace')" class="copi">Copiar enlace</a>
													</div>
												</div>
											</div>
										</div>
									<br>
									<div class="fpf_foot">
										<p>Tienes problemas con el registro, no dudes en contactar a nuestro <a href="#" onclick="contactos()">soporte técnico.</a></p>
									</div>
								</div>
								@else
								
								<div class="fp_form">
								{!! Form::open(['route' => ['WebinarRegistro.certificado'], 'method' => 'POST']) !!}
									<form action="">
										<div class="row">
											<div class="col-lg-12">
												<div class="fpf_title mb20">
													<h3><strong>Generar certificado.</strong></h3>
													@if(Session::has('message-error'))
													<div class="alert alert-danger alert-dismissible" role="alert">
														<strong>{{ session::get('message-error') }}</strong>
													</div>
													@endif
													<!-- <p>Completa este formulario y en breve te llegara un  correo con las indicaciones para poder ingresar al Webinar en la fecha establecida</p> -->
												</div>
											</div>
											<div class="col-lg-12 col-sm-6 col-xs-6">
												<div class="form-group">
	                                    			{!! Form::text('id_webinar',$infowebi->id,['class' =>'form-control', 'placeholder' =>'','required','style'=>'display: none'])!!}
													{!! Form::label('cedula','Cédula o número de documento:') !!}
	                                    			{!! Form::text('cedula',null,['class' =>'form-control', 'placeholder' =>'','required'])!!}
												</div>
											</div>
											<!--
											<div class="col-lg-6 col-md-6 col-sm-6">
	                                			<div class="form-group"><div class="g-recaptcha" data-sitekey="6LcrxKgaAAAAAF90MDs6tgHjf7DgylRdn3pq3ajf"></div></div>
	                            			</div>
	                            			-->
											<div class="col-lg-12">
												<div class="form-group">
													<button type="submit">GENERAR</button>
												</div>
											</div>
										</div>
									</form>
									<br>
									<div class="fpf_foot">
										<p>Tienes problemas con el registro, no dudes en contactar a nuestro <a href="#" onclick="contactos()">soporte técnico.</a></p>
									</div>
								{!! Form::close() !!}
								</div>
								@endif
								<!--  -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	  @else
	     <h3 align="center">EL REGISTRO PARA ESTE WEBINAR ESTÁ INACTIVO</h3>
	  @endif

	 @endif

</body>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/alertify.js')}}"></script>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!--<script src="{{asset('runastore/js/bootstrap.js')}}"></script>
<script src="{{asset('runastore/js/jquery.js')}}"></script>
<script src="{{asset('runastore/js/owl.carousel.js')}}"></script>

<script src="{{asset('js/alertify.js')}}"></script>-->



<script>
        $
        function contactos(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'Información de contacto';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>Puede ponerse en contacto directamente con nosotros enviándonos un mensaje vía WhatsApp al número 0980761961 o vía correo electrónico:</h4><ul style='color: #20A4F0;margin-bottom: 20px;'><li>soporte@runachay.com.ec</li></ul><p style='color: #98A5B5;font-size: 13px;'>Nuestros horarios de atención son de Lunes a Viernes de 8:00 a 18:00 y Sábados de 8:00 a 12:00.</p></div>");
        }
    </script>

    <script>
        $
        function registrado(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'CONFIRMACION DE REGISTRO';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>REGISTRADO CORRECTAMENTE</h4><p style='color: #98A5B5;font-size: 13px;'>LINK DE ACCESO.</p></div>");
        }
    </script>

    <script>
		function copiarAlPortapapeles(id_elemento) {
		  var aux = document.createElement("input");
		  aux.setAttribute("value", document.getElementById(id_elemento).innerHTML);
		  document.body.appendChild(aux);
		  aux.select();
		  document.execCommand("copy");
		  document.body.removeChild(aux);

		  try {
				document.execCommand('copy');
				var aviso = document.createElement('div');
				aviso.setAttribute('id', 'aviso');
				aviso.style.cssText = 'position:fixed; z-index: 9999999; top: 50%;left:50%;margin-left: -70px;padding: 20px; background: gold;border-radius: 8px;font-family: sans-serif;';
				aviso.innerHTML = 'URL copiada';
				document.body.appendChild(aviso);
				document.load = setTimeout('document.body.removeChild(aviso)', 2000);
				document.load = setTimeout('boton.id = "getlink"',2500);
				boton.id = '';
			} catch (e) {
				// alert('Tu navegador no soporta la función de copiar');
			}
			document.body.removeChild(aux);
		}
	</script>

</html>