<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registro Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 280 instituciones educativas y alrededor de 500000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">

	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/alertify.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/webinar/style.css')}}">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/e8d895419f.js" crossorigin="anonymous"></script>

	<style>
		iframe{width: 100%;min-height: 480px;}
		.tr_control{
			position: relative;
			margin-bottom: 40px;
			text-align: center;
		}
		.tr_control h5{
			margin-bottom: 20px;
		}
		.tr_control .tr_btns{
			display: flex;
			align-items: center;
			justify-content: center;
		}
		.tr_control .tr_btns div{
			margin-right: 20px;
		}
		.tr_control .tr_btns a{
			background: var(--colorAzul);
			color: #fff;
			padding: 10px 30px;
			border-radius: 8px;
			text-decoration: none;
			display: inline-block;

		}
	</style>
	
</head>
<body>

		<section class="form_page">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="row">
							<div class="col-lg-12">
								<div class="fp_head mb40"><img src="{{asset('assets/img/logocuadrado.png')}}" alt=""></div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xxl-10 col-xl-12 offset-xxl-1 order-first order-lg-last">
								<div class="fp_form">
									<div style="margin-bottom: 40px;" id=cargariframe>

									</div>
									<div class="tr_control">
										<h5><strong>Sigue esta transmisión por facebook live: <i class="fab fa-facebook-square"></i></strong></h5>
										<div class="tr_btns">
											<div><a href="#">Ver transmisión en Facebook.com</a></div>
											<div><a href="#" id="verfacebook">Ver transmisión en esta página</a></div>
										</div>
									</div>
									<div class="tr_control">
										<h5><strong>Sigue esta transmisión por Youtube: <i class="fab fa-youtube"></i></strong></h5>
										<div class="tr_btns">
											<div><a href="#">Ver transmisión en youtube.com</a></div>
											<div><a href="#" id="veryoutube">Ver transmisión en esta página</a></div>
										</div>
									</div>
									<br>
									<div class="fpf_foot">
										<p>Tienes problemas con el registro, no dudes en contactar a nuestro <a href="#" onclick="contactos()">soporte técnico.</a></p>
									</div>
								</div>
								<!--  -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

</body>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/alertify.js')}}"></script>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<script>
        $
        function contactos(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'Información de contacto';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>Puede ponerse en contacto directamente con nosotros enviándonos un mensaje vía WhatsApp al número 0980761961 o vía correo electrónico:</h4><ul style='color: #20A4F0;margin-bottom: 20px;'><li>ventas@runachay.com.ec</li><li>gerencia@runachay.com.ec</li></ul><p style='color: #98A5B5;font-size: 13px;'>Nuestros horarios de atención son de Lunes a Viernes de 8:00 a 18:00 y Sábados de 8:00 a 12:00.</p></div>");
        }
    </script>


    <script>
    	$(document).ready(function(){
	        popup = {
	            init: function(){
	                $('#verfacebook').click(function(){
	                    popup.openfb();
	                });
	                $('#veryoutube').click(function(){
	                    popup.openyt();
	                });

	                $("span, .img-sho").click(function(){
	                    popup.close();
	                });
	                $(".overlex").click(function(){
	                    popup.close();
	                });
	            },
	            openfb: function(){
	            	$('#cargariframe').empty().append('<div class="row"><div class="col-xxl-10 offset-xxl-1"><div><iframe src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fwww.facebook.com%2FFoxNews%2Fvideos%2F459587281778163%2F&show_text=false&width=560&mute=0" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe></div></div></div>');
	            },
	            openyt: function(){
	            	$('#cargariframe').empty().append('<div class="row"><div class="col-xxl-8"><div><iframe src="https://www.youtube.com/embed/I83XWCSBgSc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div><div class="col-xxl-4"><div><iframe width="761" height="428" src="https://www.youtube.com/live_chat?v=I83XWCSBgSc&embed_domain=localhost" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div></div>');
	            },
	            close: function(){
	                $(".visho").fadeOut();
	                $(".vi-sho iframe").attr("src",src);
	            }
	        }
	        popup.init()
	    })
    </script>

</html>