<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
	<link rel="stylesheet" href="{{asset('assets/presentacion/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/presentacion/bootstrap.js')}}">

	<link rel="stylesheet" href="{{asset('assets/presentacion/presentacion.css')}}">

	<link rel="stylesheet" href="{{asset('assets/presentacion/reveal.css')}}">
	<link rel="stylesheet" href="{{asset('assets/presentacion/black.css')}}">

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/400d589a91.js" crossorigin="anonymous"></script>
</head>
<body>
	<div class="reveal" style="background-image: url({{asset('assets/presentacion/back.png')}});background-size: cover;background-position: center;">
      <div class="slides">
      	<!-- Uno -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/tp.png')}}" data-background-size="540px" data-background-repeat="no-repeat" data-background-position="0px center" data-transition="fade" id="0">
      		<div style="text-align: right;">
      			<img src="{{asset('assets/presentacion/logo.png')}}" alt="" style="width: 148px;">
      		</div>
      	</section>

      	<!-- Dos -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/tp.png')}}" data-background-size="540px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="1">
      		<div class="sl_dos">
      			<!-- fragment -->
      			<div class="sld_center">
      				<img src="{{asset('assets/presentacion/objetos/reloj.png')}}" alt="">
      			</div>
      			<div class="sld_item sldi_uno">
      				<p><strong>5 min</strong> presentación de la empresa.</p>
      			</div>
      			<div class="sld_item sldi_dos">
      				<p><strong>10 min</strong> Presentación global de la herramienta.</p>
      			</div>
      			<div class="sld_item sldi_tres">
      				<p><strong>10 min</strong> Demo general de Runachay.</p>
      			</div>
      			<div class="sld_item sldi_cuatro">
      				<p><strong>15 min</strong> Presentación de nuestras Aulas Virtuales.</p>
      			</div>
      			<div class="sld_item sldi_cinco">
      				<p><strong>15 min</strong> Preguntas y respuestas.</p>
      			</div>
      			<div class="sld_item sldi_seis">
      				<p><strong>5 min</strong> Despedida.</p>
      			</div>
      		</div>
      	</section>


	    <!-- Tres -->
	    <section data-background-image="{{asset('assets/presentacion/titulos/t3.png')}}" data-background-size="540px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="2">
      		<div class="sl_tres" style="height: 440px;width: 800px;left: 50%;position: relative;transform: translateX(-50%);">
      			<div class="slt_item slt_center">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div style="width: 100%;height: 100%;position: absolute;top: 0;left: 0; padding: 10px 20px;">
	      					<img src="{{asset('assets/presentacion/logo.png')}}" alt="" style="width: 80px">
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_uno">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p>CALIDAD DE SERVICIO</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_dos">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p>NACIMOS  DE LA INDUSTRIA</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_tres">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p><strong>+100mil</strong> ESTUDIANTES</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_cuatro">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p><strong>+300mil</strong> Usuarios</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_cinco">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p><strong>+200</strong> UNIDADES  EDUCATIVAS</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_seis">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p><strong style="font-size: 48px">+5</strong>Años</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_right">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p>EDUCANDO PERSONAS</p>
	      				</div>
      				</div>
      			</div>
      			<div class="slt_item slt_left">
      				<div class="slti_box">
      					<div class="hexaitem hexa_uno"></div>
	      				<div class="hexaitem hexa_dos"></div>
	      				<div class="hexaitem hexa_tres"></div>
	      				<div class="slti_text">
	      					<p>CRECIMIENTO DEL 150% ANUAL</p>
	      				</div>
      				</div>
      			</div>
      			
      		</div>
      	</section>

      	<!-- Cuatro -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t4.png')}}" data-background-size="420px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="3">
      		<div class="sl_cuatro">
      			<div class="slc_content">
      				<div class="slc_center">
      					<img src="{{asset('assets/presentacion/logo.png')}}" alt="">
      				</div>
      				<div class="slc_box">
      					<div class="slcb_item slcb_tl">
      						<div class="slcbi_box">
      							<img src="{{asset('assets/presentacion/objetos/41.png')}}" alt="">
      							<p><strong>EVOLUCIÓN <br> CONTINUA</strong></p>
      						</div>
      					</div>
      					<div class="slcb_item slcb_tr">
      						<div class="slcbi_box">
      							<img src="{{asset('assets/presentacion/objetos/42.png')}}" alt="">
      							<p><strong>PERSONALIZACIÓN</strong></p>
      						</div>
      					</div>
      					<div class="slcb_item slcb_bl">
      						<div class="slcbi_box">
      							<img src="{{asset('assets/presentacion/objetos/43.png')}}" alt="">
      							<p><strong>SOPORTE DE <br>ALTA CALIDAD</strong></p>
      						</div>
      					</div>
      					<div class="slcb_item slcb_br">
      						<div class="slcbi_box">
      							<img src="{{asset('assets/presentacion/objetos/44.png')}}" alt="">
      							<p><strong>TODO <br>EN UNO</strong></p>
      						</div>
      					</div>
      				</div>
      			</div>
      			
      		</div>
      	</section>

      	<!-- Cinco -->
        <section data-background-image="{{asset('assets/presentacion/titulos/t5.png')}}" data-background-size="480px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="4">
        	<div class="sl_cinco">
      			<div class="slf_circle" style="">
      				<div class="slf_dots" style=""></div>
      				<div class="slf_img">
      					<img class="" src="{{asset('assets/presentacion/logo.png')}}" alt="">
      				</div>
      				<div class="slf_items">
      					<div class="slfi_item slfi_uno color1">
      						<a href="#/5">
      							<img src="{{asset('assets/presentacion/objetos/51.png')}}" alt="">
      							<p>Comunicación</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_dos color2">
      						<a href="#/7">
      							<img src="{{asset('assets/presentacion/objetos/52.png')}}" alt="">
      							<p>matriculación</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_tres color3">
      						<a href="#/8">
      							<img src="{{asset('assets/presentacion/objetos/53.png')}}" alt="">
      							<p>Calificaciones</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_cuatro color4">
      						<a href="#/10">
      							<img src="{{asset('assets/presentacion/objetos/54.png')}}" alt="">
      							<p>Planificación</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_cinco color5">
      						<a href="#/11">
      							<img src="{{asset('assets/presentacion/objetos/55.png')}}" alt="">
      							<p>Finanzas</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_seis color6">
      						<a href="#/12">
      							<img src="{{asset('assets/presentacion/objetos/56.png')}}" alt="">
      							<p>Convivencia</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_siete color7">
      						<a href="#/13">
      							<img src="{{asset('assets/presentacion/objetos/57.png')}}" alt="">
      							<p>Aula Virtual</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_ocho color8">
      						<a href="#/14">
      							<img src="{{asset('assets/presentacion/objetos/58.png')}}" alt="">
      							<p>APP Móvil</p>
      						</a>
      					</div>
      					<div class="slfi_item slfi_nueve color9">
      						<a href="#/16">
      							<img src="{{asset('assets/presentacion/objetos/59.png')}}" alt="">
      							<p>Contabilidad</p>
      						</a>
      					</div>
      				</div>
      			</div>
      		</div>
        </section>

        <!--  -->
        <section data-background-image="{{asset('assets/presentacion/titulos/t6.png')}}" data-background-size="480px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="5">
      		<div class="sl_seis">
      			<div class="slp_div">
      				<div class="slpd_g">
      					<div class="slpdg_img">
      						<img src="{{asset('assets/presentacion/objetos/6t.png')}}" alt="">
      						<img class="slpdgi_border" src="{{asset('assets/presentacion/objetos/6i.png')}}" alt="">
      					</div>
      				</div>
      				<div class="slpd_p">
      					<div class="slpdp_list">
      						<div class="slpdpl_item"><p>DISEÑO AMIGABLE</p></div>
      						<div class="slpdpl_item"><p>FECHA Y HORA CUANDO EL MENSAJE ES LEIDO</p></div>
      						<div class="slpdpl_item"><p>IMPRESIÓN DE EVIDENCIAS</p></div>
      						<div class="slpdpl_item"><p>ESTADOS DE LOS COMUNICADOS</p></div>
      						<div class="slpdpl_item"><p>BUSCADOR AVANZADO</p></div>
      						<div class="slpdpl_item"><p>MENSAJES AUTOMATICOS Y MANUALES</p></div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>


      	<!-- Siete -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t7.png')}}" data-background-size="480px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="6">
      		<div class="sl_siete">
      			<div class="slp_div">
      				<div class="slpd_g">
      					<div class="slpdg_img">
      						<img class="slpdgi_border slpdgi_p" src="{{asset('assets/presentacion/objetos/7i.png')}}" alt="">
      					</div>
      				</div>
      				<div class="slpd_p">
      					<div class="slpdp_list">
      						<div class="slpdpl_item"><p>Mensaje para  grupales o individuales</p></div>
      						<div class="slpdpl_item"><p>Puede estar atada al código de convivencia.</p></div>
      						<div class="slpdpl_item"><p>Puede hacer uso de plantillas según el tipo de comunicados</p></div>
      						<div class="slpdpl_item"><p>Comunicaciones abiertas o cerradas</p></div>
      						<div class="slpdpl_item"><p>Copias automáticas al tutor según el tipo de comunicado</p></div>
      					</div>
      				</div>
      			</div>
      			<div style="text-align: right;">
      				<img style="width: 50%;" src="{{asset('assets/presentacion/objetos/7t.png')}}" alt="">
      			</div>
      		</div>
      	</section>

      	<!-- Ocho -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t8.png')}}" data-background-size="480px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="7">
      		<div class="sl_ocho">
      			<div class="slo_box">
      				<div class="slo_flex">
      					<!-- <div class="slo_item slo_liner">
      						<div class="sloi_box">
      							<img src="{{asset('css/presentacion/objetos/81.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slo_item slo_liner">
      						<div class="sloi_box">
      							<img src="{{asset('css/presentacion/objetos/82.png')}}" alt="">
      							<p>Transferencia bancaria de valores</p>
      						</div>
      					</div>
      					<div class="slo_item slo_liner slo_linerd">
      						<div class="sloi_box">
      							<img src="{{asset('css/presentacion/objetos/83.png')}}" alt="">
      							<p>El representatante registrará los datos del estudiante</p>
      						</div>
      					</div>
      					<div class="slo_item slo_liner">
      						<div class="sloi_box">
      							<img src="{{asset('css/presentacion/objetos/84.png')}}" alt="">
      							<p>La Unidad Educativa legalizará la matrícula</p>
      						</div>
      					</div>
      					<div class="slo_item slo_liner">
      						<div class="sloi_box">
      							<img src="{{asset('css/presentacion/objetos/85.png')}}" alt="">
      							<p>Se confirma la legalización de la matrícula</p>
      						</div>
      					</div>
      					<div class="slo_item">
      						<div class="sloi_box">
      							<img src="{{asset('css/presentacion/objetos/82.png')}}" alt="">
      							<p>Acceso a RUNACHAY(Clases Virtuales)</p>
      						</div>
      					</div> -->
      					<img src="{{asset('assets/presentacion/objetos/8f.png')}}" alt="">
      				</div>
      			</div>
      			<div class="slo_float">
      				<div class="slpdp_list">
  						<div class="slpdpl_item"><p><strong>+100</strong> mil estudiantes matriculados en Costa</p></div>
  						<div class="slpdpl_item"><p>Modulo de admisión</p></div>
  						<div class="slpdpl_item"><p>Pre matricula: Fichas Ministeriales</p></div>
  						<div class="slpdpl_item"><p>Fichas y documentos personalizados</p></div>
  						<div class="slpdpl_item"><p>Pago en línea</p></div>
  					</div>
      			</div>
      		</div>
      	</section>


      	<!-- Nueve -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t9.png')}}" data-background-size="580px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="8">
      		<div class="sl_nueve">
      			<div class="slp_div">
      				<div class="slpd_g">
      					<div class="slpdg_img">
      						<img class="" src="{{asset('assets/presentacion/objetos/9i.png')}}" alt="">
      					</div>
      				</div>
      				<div class="slpd_p">
      					<div class="slpdp_list">
      						<div class="slpdpl_item"><p>Mensaje para  grupales o individuales</p></div>
      						<div class="slpdpl_item"><p>Puede estar atada al código de convivencia.</p></div>
      						<div class="slpdpl_item"><p>Puede hacer uso de plantillas según el tipo de comunicados</p></div>
      						<div class="slpdpl_item"><p>Comunicaciones abiertas o cerradas</p></div>
      						<div class="slpdpl_item"><p>Copias automáticas al tutor según el tipo de comunicado</p></div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>



      	<!-- Diez -->
      	<section data-background-image="{{asset('assets/presentacion/bgs.png')}}" data-transition="fade" id="9">
      		<div class="sl_diez">
      			<p><strong>+80 REPORTES</strong></p>
      			<p>DE CALIFICACIONES</p>
      			<img src="{{asset('assets/presentacion/objetos/10i.png')}}" alt="">
      		</div>
      	</section>


      	<!-- Once -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t11.png')}}" data-background-size="480px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="10">
      		<div class="sl_once">
      			<div class="slp_div">
      				<div class="slpd_g">
      					<div class="slpdg_img">
      						<img class="" src="{{asset('assets/presentacion/objetos/11i.png')}}" alt="">
      					</div>
      				</div>
      				<div class="slpd_p">
      					<div class="slpdp_list">
      						<div class="slpdpl_item"><p>Diseño familiar</p></div>
      						<div class="slpdpl_item"><p>Opciones ministeriales precargadas</p></div>
      						<div class="slpdpl_item"><p>70% de ahorro en tiempo</p></div>
      						<div class="slpdpl_item"><p>Supervisión y aprobación</p></div>
      						<div class="slpdpl_item"><p>95% de implementación</p></div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>


      	<!-- Doce -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t12.png')}}" data-background-size="480px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="11">
      		<div class="sl_doce">
      			<div class="slp_div">
      				<div class="slpd_g">
      					<div class="slpdg_img">
      						<img class="slpdgi_border" src="{{asset('assets/presentacion/objetos/12i.png')}}" alt="">
      						<img class="slpdgi_sp" src="{{asset('assets/presentacion/objetos/12f.png')}}" alt="">
      					</div>
      				</div>
      				<div class="slpd_p">
      					<div class="slpdp_list">
      						<div class="slpdpl_item"><p>Valores y descuentos precargados</p></div>
      						<div class="slpdpl_item"><p>Notificaciones y bloqueos al cliente por morosidad</p></div>
      						<div class="slpdpl_item"><p>Monitoreo de cobranza</p></div>
      						<div class="slpdpl_item"><p>Tratamiento para rubros de terceros</p></div>
      						<div class="slpdpl_item"><p>Pago con TC, CH, CASH, Empleados/proveedores</p></div>
      						<div class="slpdpl_item"><p>Emisión de Estados de Cuenta</p></div>
      						<div class="slpdpl_item"><p>Monitoreo de la Gestión</p></div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>


      	<!-- Trece -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t13.png')}}" data-background-size="580px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="12">
      		<div class="sl_trece">
      			<div class="slp_div">
      				<div class="slpd_g">
      					<div class="slpdg_img">
      						<img class="slpdgi_p" src="{{asset('assets/presentacion/objetos/13i.png')}}" alt="">
      					</div>
      				</div>
      				<div class="slpd_p">
      					<div class="slpdp_list">
      						<div class="slpdpl_item"><p>DECE</p></div>
      						<div class="slpdpl_item"><p>Inspección</p></div>
      						<div class="slpdpl_item"><p>Asistencias</p></div>
      						<div class="slpdpl_item"><p>Médico</p></div>
      						<div class="slpdpl_item"><p>Encuestas</p></div>
      						<div class="slpdpl_item"><p>Democrácia Estudiantil</p></div>
      						<div class="slpdpl_item"><p>Configuración Fléxible</p></div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<!-- Catorce -->

      	<section data-background-image="{{asset('assets/presentacion/titulos/t14.png')}}" data-background-size="580px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="13">
      		<div class="sl_catorce">
      			<div class="slc_circle" style="">
      				<div class="slc_dots" style=""></div>
      				<div class="slc_img">
      					<img class="" src="{{asset('assets/presentacion/objetos/14i.png')}}" alt="">
      				</div>
      				<div class="slc_items">
      					<div class="slci_item slci_uno">
      						<div class="slci_content">
      							<p>Clases Virtuales</p>
      							<img src="{{asset('assets/presentacion/objetos/141.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slci_item slci_dos">
      						<div class="slci_content">
      							<p>Portafolio</p>
      							<img src="{{asset('assets/presentacion/objetos/142.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slci_item slci_tres">
      						<div class="slci_content">
      							<p>Biblioteca Virtual</p>
      							<img src="{{asset('assets/presentacion/objetos/143.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slci_item slci_cuatro">
      						<div class="slci_content">
      							<p>Foros</p>
      							<img src="{{asset('assets/presentacion/objetos/144.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slci_item slci_cinco">
      						<div class="slci_content">
      							<p>Video conferencias</p>
      							<img src="{{asset('assets/presentacion/objetos/145.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slci_item slci_seis">
      						<div class="slci_content">
      							<p>Evaluaciones en línea</p>
      							<img src="{{asset('assets/presentacion/objetos/146.png')}}" alt="">
      						</div>
      					</div>
      					<div class="slci_item slci_siete">
      						<div class="slci_content">
      							<p>Tareas</p>
      							<img src="{{asset('assets/presentacion/objetos/147.png')}}" alt="">
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>


      	<!-- Quince -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t15.png')}}" data-background-size="580px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="14">
      		<div class="sl_quince">
      			<div class="slq_circle" style="">
      				<div class="slq_dots" style=""></div>
      				<div class="slq_img">
      					<img class="" src="{{asset('assets/presentacion/objetos/15i.png')}}" alt="">
      				</div>
      				<div class="slq_items">
      					<div class="slqi_item slqi_uno">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/151.png')}}" alt="">
      							<p>ALERTAS</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_dos">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/152.png')}}" alt="">
      							<p>EVENTOS</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_tres">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/153.png')}}" alt="">
      							<p>TAREAS</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_cuatro">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/154.png')}}" alt="">
      							<p>INFORMACIÓN DEL ESTUDIANTE</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_cinco">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/155.png')}}" alt="">
      							<p>CLASE VIRTUAL</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_seis">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/156.png')}}" alt="">
      							<p>HORARIOS</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_siete">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/157.png')}}" alt="">
      							<p>EVALUACIÓN EN LÍNEA</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_ocho">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/158.png')}}" alt="">
      							<p>FOROS</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_nueve">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/159.png')}}" alt="">
      							<p>CALIFICACIONES</p>
      						</div>
      					</div>
      					<div class="slqi_item slqi_diez">
      						<div class="slqi_content">
      							<img src="{{asset('assets/presentacion/objetos/160.png')}}" alt="">
      							<p>COMUNICADOS</p>
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>


      	<!-- dieciseis -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t16.png')}}" data-background-size="580px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="15">
      		<div class="sl_dieciseis">
      			<div class="sld_circle" style="">
      				<div class="sld_dots" style=""></div>
      				<div class="sld_img">
      					<img class="" src="{{asset('assets/presentacion/objetos/15i.png')}}" alt="">
      				</div>
      				<div class="sld_items">
      					<div class="sldi_item sldi_uno">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/151.png')}}" alt="">
      							<p>Calificaciones</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_dos">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/152.png')}}" alt="">
      							<p>tareas</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_tres">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/153.png')}}" alt="">
      							<p>Clase Virtual</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_cuatro">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/154.png')}}" alt="">
      							<p>Tutorias</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_cinco">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/155.png')}}" alt="">
      							<p>Horarios</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_seis">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/156.png')}}" alt="">
      							<p>Evaluación en Línea</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_siete">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/157.png')}}" alt="">
      							<p>Foros</p>
      						</div>
      					</div>
      					<div class="sldi_item sldi_ocho">
      						<div class="sldi_content">
      							<img src="{{asset('assets/presentacion/objetos/158.png')}}" alt="">
      							<p>Comunicados</p>
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>



      	<!-- diecisiete -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t17.png')}}" data-background-size="580px" data-background-repeat="no-repeat" data-background-position="0px 40px" data-transition="fade" id="16">
      		<div class="sl_dieciete">
      			<div class="slu_bar">
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/171.png')}}" alt="">
      						<p>Integración de Bancos</p>
      					</div>
      				</div>
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/172.png')}}" alt="">
      						<p>Nómina</p>
      					</div>
      				</div>
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/173.png')}}" alt="">
      						<p>Presupuesto</p>
      					</div>
      				</div>
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/174.png')}}" alt="">
      						<p>Ctas. por pagar y cobrar</p>
      					</div>
      				</div>
      			</div>
      			<div class="slu_center">
      				<div class="sluc">
      					<img src="{{asset('assets/presentacion/logo.png')}}" alt="">
      				</div>
      			</div>
      			<div class="slu_bar">
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/175.png')}}" alt="">
      						<p>Contabilidad</p>
      					</div>
      				</div>
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/176.png')}}" alt="">
      						<p>Compras</p>
      					</div>
      				</div>
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/177.png')}}" alt="">
      						<p>Factura Electrónica</p>
      					</div>
      				</div>
      				<div class="slu_item">
      					<div class="slui_box">
      						<img src="{{asset('assets/presentacion/objetos/178.png')}}" alt="">
      						<p>Gastos Ministeriales</p>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>






      	<!-- Dieciocho -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t18.png')}}" data-background-size="540px" data-background-repeat="no-repeat" data-background-position="0px center" data-transition="fade" id="17">
      		<!-- <div style="text-align: right;">
      			<img src="{{asset('css/presentacion/logo.png')}}" alt="" style="width: 148px;">
      		</div> -->
      	</section>
      	<!-- Diecinueve -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t19.png')}}" data-background-size="540px" data-background-repeat="no-repeat" data-background-position="0px center" data-transition="fade" id="18">
      		<!-- <div style="text-align: right;">
      			<img src="{{asset('css/presentacion/logo.png')}}" alt="" style="width: 148px;">
      		</div> -->
      	</section>
      	<!-- Veinte -->
      	<section data-background-image="{{asset('assets/presentacion/titulos/t20.png')}}" data-background-size="540px" data-background-repeat="no-repeat" data-background-position="0px center" data-transition="fade" id="19">
      		<!-- <div style="text-align: right;">
      			<img src="{{asset('css/presentacion/logo.png')}}" alt="" style="width: 148px;">
      		</div> -->
      	</section>


      </div>
    </div>
</body>

<script src="{{asset('assets/presentacion/jquery.js')}}"></script>
<script src="{{asset('assets/presentacion/bootstrap.js')}}"></script>
<script src="{{asset('assets/presentacion/reveal.js')}}"></script>

<script>

	Reveal.initialize({
		backgroundTransition: 'fade',
		slideNumber: 'c/t',
		disableLayout: false
	});

</script>
</html>