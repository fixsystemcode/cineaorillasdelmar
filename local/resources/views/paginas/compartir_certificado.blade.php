
<html lang="en">
<?php
    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }
    ?>
<head>
	<title>Registro Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 280 instituciones educativas y alrededor de 500000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">

	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/alertify.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/webinar/style.css')}}">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/e8d895419f.js" crossorigin="anonymous"></script>


	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">


	<meta property="og:url"           content="{{URL::current()}}" />
	<meta property="og:type"          content="Certificación" />
	<meta property="og:title"         content="Certificación de {{$infowebi->tema}}" />
	<meta property="og:description"   content="#docenciaconexcelencia. Siempre innovándome, siempre capacitándome. He asistido al evento {{$infowebi->tema}}" />
	<meta property="og:image"         content="http://runachay.com.ec/img/repo/runachay/certificaciones/cert.jpg" />


	<meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:description" content="#docenciaconexcelencia. Siempre innovándome, siempre capacitándome. He asistido al evento {{$infowebi->tema}}">
    <meta name="twitter:title" content="Certificación de {{$infowebi->tema}}">
    <meta name="twitter:site" content="{{URL::current()}}">
    <meta name="twitter:image" content="http://runachay.com.ec/img/repo/runachay/certificaciones/cert.jpg">
    <meta name="twitter:creator" content="@runachayec">



	<style>
		.form_page{
			font-family: 'Roboto', sans-serif;
		}
		.cert_name{
			width: 90%;
		    height: 28px;
		    overflow: hidden;
		    top: 40%;
		    left: 50%;
		    transform: translate(-50%, -50%);
		    position: absolute;
		}
		.asistente{
			font-size: 1.4em;
		    text-align: center;
		    align-items: center;
			justify-content: center;
		    
		    font-family: "Times New Roman", Times, serif;
		    font-weight: 600;
		    /*text-overflow: ellipsis;*/
			  overflow: hidden;
			  /*white-space: pre;*/
			  text-transform: uppercase;
		}
		.cert_box{
			position: relative;
			width: 100%;
			height: auto;
			box-shadow: 0px 12px 28px rgba(0,0,0,0.2);
		}
		.btn_compartir{
			padding: 10px 20px;
			border: none;
			background-color: #105bcc;
			color: #fff;
			font-weight: 600;
			border-radius: 12px;
			display: flex;
			align-items: center;
			justify-content: center;
			height: 48px;
			min-width: 240px;
			width: 100%;
			font-size: 14px;
		}
		.btn_compartir i{
			margin-left: 10px;
			padding-top: 2px;
		}
		.cert_detalles{
			font-size: 16px;
			font-family: 'Roboto', sans-serif;
			color: #4e5760;
		}
		.cert_detalles h5{
			font-size: 22px;
			margin-bottom: 15px;
			color: #282e33;
		}
		.cert_detalles span{
			color: #3194ff;
		}
		.cert_g{
			padding-left: 30px;
			padding-top: 20px;
		}
		.cert_header span{
			padding: 4px 20px;
			border-radius: 8px;
			background-color: #b6d9ff;
			color: #1361d1;
			font-weight: 700;
			font-size: 16px;
			margin-bottom: 10px;
			display: inline-block;
		}
	</style>

	
</head>
<body>

	@if($infowebi == null)

	El webinar no existe

	@else

	@if($infoasistente == null)

	El asistente no existe

	@else

	   @if($infowebi->estado == 1)

		<section class="form_page">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="row">
							<div class="col-lg-12">
								<div class="fp_head mb40"><img src="{{asset('assets/img/logocuadrado.png')}}" alt=""></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="cert_header">
							<span>Certificación</span>
							<h3><strong>{{$infowebi->tema}}</strong></h3>
							<br><hr><br>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="fp_data">
							<div class="cert_box">
								<div class="cert_name">
									<div class="asistente"><?php
											$longitud=strlen($infoasistente->nombres);
										 	$posicion_espacio=strpos($infoasistente->nombres, " ");
											$nombre1=substr($infoasistente->nombres,0,$posicion_espacio);
											if($longitud < 12){
												echo $infoasistente->nombres." ".$infoasistente->apellidos;
											}
											else{
												echo $nombre1." ".$infoasistente->apellidos;
											}
											
										?>
									    
									</div>
								</div>
								<img src="{{asset('local/public/images')}}/{{$infowebi->logo}}" alt="" style="width: 100%;">
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="cert_g">
							<div class="cert_detalles">
								<h5><strong>Felicitaciones por tu asistencia.</strong></h5>
								<p>La educación es un área que está en constante evolución y más aún en estos tiempos de cambios. Y por eso nos agrada mucho que estés siempre capacitándote e innovándote.</p>
								<p>Te sugerimos que compartas con tus colegas, amigos y comunidad, este esfuerzo que haces por estar siempre a la vanguardia, con el objetivo de animar a más personas a seguir mejorando y transformando la educación de nuestros estudiantes. <span>#docenciaconexcelencia</span></p>
								<hr>
								<br>
							</div>
							<div class="">
							{!! Form::open(['route' => ['WebinarRegistro.descargar'], 'method' => 'POST']) !!}
								<form action="">
									<div class="form-group">
										{!! Form::text('id_evento',$infowebi->id,['class' =>'form-control', 'placeholder' =>'','required','style'=>'display: none'])!!}
										{!! Form::text('id_asistente',$infoasistente->id,['class' =>'form-control', 'placeholder' =>'','required','style'=>'display: none'])!!}
										<div class="row">
											<div class="col-lg-5">
												<div class="form-group">
													<button type="submit" class="btn_compartir">Descargar certificado <i class="fas fa-cloud-download-alt"></i></button>
												</div>
											</div>
											<div class="col-lg-7">
												<div class="btncompartircert">
													<p style="margin-bottom: 4px;text-align: right;"><strong>Comparte tu certificación en tus redes favoritas:</strong></p>
													<ul>
														<li><a class="apis_fb" href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('/')}}/buscarcertificado/{{$infowebi->slug}}/{{$infoasistente->id}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
														<li><a class="apis_tt" href="https://twitter.com/intent/tweet?text=He%20asistido%20a%20{{$infowebi->tema}}%20%0ASiempre%20innov%C3%A1ndome,%20siempre%20capacit%C3%A1ndome%0A%23docenciaconexcelencia" target="_blank"><i class="fab fa-twitter"></i></a></li>
														<li><a class="apis_li" href="https://www.linkedin.com/shareArticle?mini=true&url={{URL::current()}}&title=He%20asistido%20a%20{{$infowebi->tema}}&summary=He%20asistido%20a%20{{$infowebi->tema}}%20%0ASiempre%20innov%C3%A1ndome,%20siempre%20capacit%C3%A1ndome%0A%23docenciaconexcelencia%0Aenlace&source={{URL::current()}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
													</ul>
												</div>
											</div>
										</div>
										
									</div>
								</form>
								<br>
							{!! Form::close() !!}
							</div>
						</div>
						
						<!--  -->
					</div>
				</div>
			</div>
		</section>

	  @else
	     <h3 align="center">EL REGISTRO PARA ESTE WEBINAR ESTÁ INACTIVO</h3>
	  @endif
	  @endif
	 @endif

</body>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/alertify.js')}}"></script>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!--<script src="{{asset('runastore/js/bootstrap.js')}}"></script>
<script src="{{asset('runastore/js/jquery.js')}}"></script>
<script src="{{asset('runastore/js/owl.carousel.js')}}"></script>

<script src="{{asset('js/alertify.js')}}"></script>-->

    
    

</html>