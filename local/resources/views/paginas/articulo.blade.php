<!DOCTYPE html>
<html lang="en">
<head>
    <title>Runachay | Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="favicon.ico" />
    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Font Awesome 4.7.0 -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <!-- Main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.mb.YTPlayer.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">


    <style>
        .lado1,
        .imag-ce,
        .lado2,
        .lado3,
        .lado4,
        .lado5,
        .lado6,
        .lado7,
        .lado8,
        .lado9,
        .modulo-dots{
            visibility: hidden;
        }
        .js-reveal--hidden {
          opacity: 0;
          margin-top: 5px;
        }

        .cont-cont{
            position: relative;
            /*height: 455.063px;*/
            width: 55%;
            right: 50%;
            left: 50%;
            transform: translate(-50%, 0%);
            margin: 30px 0;
        }
        .foot-video h3{
            font-size: 38px;
            font-weight: 400;
            /*color: purple;*/
            color: #484696;
            padding: 0 20%;
        }
        .foot-video strong{
            color: #00ADDD;
            font-weight: 600;
        }

        
    </style>


</head>
<body class="home-01">
    <div class="wrapp-content">
        <!-- Header -->
        <header class="wrapp-header" style="padding-top: 60px; min-height: auto;">
            <div class="main-nav">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="./" class="logo">
                                <img src="{{asset('assets/img/logoprincipal2.png')}}" alt="">
                            </a>
                            <div class="main-nav__btn">
                                <div class="icon-left"></div>
                                <div class="icon-right"></div>
                            </div>
                            <ul class="main-nav__list">
                                <li>
                                    <a href="#">Inicio</a>
                                </li>
                                <li>
                                    <a href="#">Módulos</a>
                                    <ul>
                                        <li>
                                            <a href="#comunicacion">Comunicación</a>
                                        </li>
                                        <li>
                                            <a href="#matriculacion">Matriculación</a>
                                        </li>
                                        <li>
                                            <a href="#calificaciones">Calificaciones</a>
                                        </li>
                                        <li>
                                            <a href="#planificaciones">Planificación</a>
                                        </li>
                                        <li>
                                            <a href="#contabilidad">Contabilidad</a>
                                        </li>
                                        <li>
                                            <a href="#app">APP movil</a>
                                        </li>
                                        <li>
                                            <a href="#dece">DECE</a>
                                        </li>
                                        <li>
                                            <a href="#cobranzas">Cobranzas</a>
                                        </li>
                                        <li>
                                            <a href="#evaluacion">Evaluación</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                    </div>
                </div><!-- /.container-fluid -->
            </div>
        </header>
        <!-- Content -->
        <main class="content-row">
            <div class="content-box-01 single-post">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="blog-listing right">
                                <article class="blog-post single">
                                    <div class="nivoSlider">
                                        <img src="{{asset('assets/img/blog_post/blog_post_02.jpg')}}" alt='' />
                                        @foreach($images as $img)
                                        <img src="{{asset('local/public/galeria')}}/{{$img->path}}" alt='' />
                                        @endforeach
                                    </div>
                                    <div class="blog-post__meta">
                                        <ul class="blog-post__meta-list">
                                            <li>
                                                <p class="blog-post__meta-date">
                                                    <?php echo date('d', strtotime($articulo->created_at)); ?>
                                                    <?php echo date('M Y', strtotime($articulo->created_at)); ?>
                                                </p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-author">by
                                                    <a href="#">Admin</a>
                                                </p>
                                            </li>
                                            <!-- <li>
                                                <p class="blog-post__meta-category">Student Life</p>
                                            </li>
                                            <li>
                                                <p class="blog-post__meta-comments">3 Comments</p>
                                            </li> -->
                                        </ul>
                                    </div>
                                    <h3 class="blog-post__title">
                                        {{$articulo->titulo}}
                                    </h3>
                                    <div class="blog-post__text">
                                        <p>
                                            <span class="dropcap"><?php $ser1 = substr($articulo->texto,3,1);echo strip_tags($ser1); ?></span>
                                            <?php echo $articulo->texto; ?>
                                        </p>
                                    </div>
                                    <div class="blog-post__btn-wrapp">
                                        <ul class="post-tag-list">
                                            <li>
                                                <a href="#">Courses</a>
                                            </li>
                                            <li>
                                                <a href="#">Research</a>
                                            </li>
                                            <li>
                                                <a href="#">Alumni</a>
                                            </li>
                                        </ul>
                                        <a href="#" class="blog-post__likes">255 Likes</a>
                                        <div class="preview_share_wrapper">
                                            <a href="#" class="preview_share_toggler">
                                                <i class="fa fa fa-share-alt"></i>
                                            </a>
                                            <div class="preview_share_block">
                                                <a class="share_facebook" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                                <a class="share_twitter" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                                <a class="share_gplus" href="#">
                                                    <i class="fa fa-google-plus"></i>
                                                </a>
                                                <a class="share_pinterest" href="#">
                                                    <i class="fa fa-pinterest-p"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <div class="single-post-nav">
                                    <a class="single-post-nav__prew" href="#">prew</a>
                                    <a class="single-post-nav__next" href="#">next</a>
                                </div>
                                <div class="single-related-posts">
                                    <h3 class="single-related__title">Related Posts</h3>
                                    <ul class="single-related-posts__list">
                                        <li>
                                            <figure class="single-related-posts__img">
                                                <a href="#">
                                                    <img src="img/related_posts/related_posts_img_01.jpg" alt="">
                                                </a>
                                            </figure>
                                            <p class="single-related-posts__data">24 September, 2017</p>
                                            <h3 class="single-related-posts__title">
                                                <a href="#">Our Students Love Studying</a>
                                            </h3>
                                            <a class="single-related-posts__btn" href="#">Read More</a>
                                        </li>
                                        <li>
                                            <figure class="single-related-posts__img">
                                                <a href="#">
                                                    <img src="img/related_posts/related_posts_img_02.jpg" alt="">
                                                </a>
                                            </figure>
                                            <p class="single-related-posts__data">24 September, 2017</p>
                                            <h3 class="single-related-posts__title">
                                                <a href="#">New Courses for 2018</a>
                                            </h3>
                                            <a class="single-related-posts__btn" href="#">Read More</a>
                                        </li>
                                        <li>
                                            <div class="nivoSlider">
                                                <img src="img/related_posts/related_posts_img_03.jpg" alt='' />
                                                <img src="img/related_posts/related_posts_img_04.jpg" alt='' />
                                                <img src="img/related_posts/related_posts_img_05.jpg" alt='' />
                                            </div>
                                            <p class="single-related-posts__data">24 September, 2017</p>
                                            <h3 class="single-related-posts__title">
                                                <a href="#">Our Students Love Studying</a>
                                            </h3>
                                            <a class="single-related-posts__btn" href="#">Read More</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="comments">
                                    <h3 class="comments-title">Comments (2)</h3>
                                    <ul class="comments__list">
                                        <li>
                                            <div class="comments__user-wrapp">
                                                <figure class="comments__user-img">
                                                    <img src="img/blog_post/comment_user_01.jpg" alt="">
                                                </figure>
                                            </div>
                                            <div class="comments__content">
                                                <h4 class="comments__user-name">Mark Wilson</h4>
                                                <p class="comments__data">24 September, 2017</p>
                                                <a class="comments__reply" href="#">Reply</a>
                                                <div class="comments__text">
                                                    <p>The Smart Education has a first rate academic program, a fully qualified and very well educated teaching staff, bright and comfortable classrooms, continuous student counseling.</p>
                                                </div>
                                            </div>
                                            <ul class="comments__child-list">
                                                <li>
                                                    <div class="comments__user-wrapp">
                                                        <figure class="comments__user-img">
                                                            <img src="img/blog_post/comment_user_02.jpg" alt="">
                                                        </figure>
                                                    </div>
                                                    <div class="comments__content">
                                                        <h4 class="comments__user-name">Amanda Smith</h4>
                                                        <p class="comments__data">24 September, 2017</p>
                                                        <a class="comments__reply" href="#">Reply</a>
                                                        <div class="comments__text">
                                                            <p>Our campus is beautiful, complete with towering green trees and brick buildings. There are coffee shops, sports, restaurants and a multitude of great study spots. </p>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="reply-form">
                                    <h3 class="reply-form__title">Leave a Reply</h3>
                                    <form action="./" class="reply-form__form">
                                        <div class="reply-form__box-01">
                                            <input class="reply-form__name" type="text" name="login" placeholder="Name *">
                                        </div>
                                        <div class="reply-form__box-02">
                                            <input class="reply-form__email" type="email" name="login" placeholder="Email *">
                                        </div>
                                        <div class="reply-form__box-03">
                                            <input class="reply-form__url" type="url" name="website" placeholder="Website">
                                        </div>
                                        <div class="reply-form__box-04">
                                            <textarea class="reply-form__message" name="message" cols="30" rows="10" placeholder="Message..."></textarea>
                                        </div>
                                        <div class="reply-form__box-05">
                                            <button class="btn-01" type="submit">Post comment</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <aside class="blog-listing-sidebar right">
                                <div class="widget sidebar-search-block">
                                    <form class="sidebar-search" action="./">
                                        <div class="sidebar-search__label">
                                            <input class="sidebar-search__inp-text" placeholder="Search..." type="text" name="inp-text">
                                            <button class="sidebar-search__inp-btn" type="button">Search</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="widget categories">
                                    <h3 class="widget-title">Categories</h3>
                                    <ul class="categories-list">
                                        <li>
                                            <a href="#">Student Life (15)</a>
                                            <ul>
                                                <li>
                                                    <a href="#">Academics (08)</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Campus Life (17)</a>
                                        </li>
                                        <li>
                                            <a href="#">Research (09)</a>
                                        </li>
                                        <li>
                                            <a href="#">Alumni (25)</a>
                                        </li>
                                        <li>
                                            <a href="#">Admissions (03)</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget related-posts">
                                    <h3 class="widget-title">Related Posts</h3>
                                    <ul class="related-posts__list">
                                        <li>
                                            <figure class="related-posts__img">
                                                <a href="#">
                                                    <img src="img/sidebar_img/related_posts/related_posts_01.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="related-posts__data">
                                                24 September, 2017
                                            </div>
                                            <h3 class="related-posts__title">
                                                <a href="#">New Courses for 2018</a>
                                            </h3>
                                        </li>
                                        <li>
                                            <figure class="related-posts__img">
                                                <a href="#">
                                                    <img src="img/sidebar_img/related_posts/related_posts_02.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="related-posts__data">
                                                24 September, 2017
                                            </div>
                                            <h3 class="related-posts__title">
                                                <a href="#">The Best Campus Tour</a>
                                            </h3>
                                        </li>
                                        <li>
                                            <figure class="related-posts__img">
                                                <a href="#">
                                                    <img src="img/sidebar_img/related_posts/related_posts_03.jpg" alt="">
                                                </a>
                                            </figure>
                                            <div class="related-posts__data">
                                                24 September, 2017
                                            </div>
                                            <h3 class="related-posts__title">
                                                <a href="#">One Day with Us</a>
                                            </h3>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget tags-cloud">
                                    <h3 class="widget-title">Tags</h3>
                                    <ul class="tags-cloud__list">
                                        <li>
                                            <a href="#">Courses</a>
                                        </li>
                                        <li>
                                            <a href="#">Research</a>
                                        </li>
                                        <li>
                                            <a href="#">Alumni</a>
                                        </li>
                                        <li>
                                            <a href="#">Student Life</a>
                                        </li>
                                        <li>
                                            <a href="#">Learning</a>
                                        </li>
                                        <li>
                                            <a href="#">Study</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Admission</a>
                                        </li>
                                        <li>
                                            <a href="#">Campus</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget instagram">
                                    <h3 class="widget-title">Instagram</h3>
                                    <ul class="instagram__list">
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_01.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_02.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_03.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_04.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_05.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_06.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_07.jpg" alt="">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="img/sidebar_img/instagram/instagram_img_08.jpg" alt="">
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="widget text-widget">
                                    <h3 class="widget-title">Text Widget</h3>
                                    <p>Our campus is beautiful, complete with towering green trees and brick buildings. There are coffee shops and sports, restaurants and a multitude of great study spots. </p>
                                </div>
                                <div class="widget text-widget">
                                    <img src="img/sidebar_img/banner_img.jpg" alt="">
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </main>
<!-- Footer -->
        <footer class="wrapp-footer">
            <div class="footer-box-01">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <a href="./" class="footer-logo">
                                <img src="{{asset('assets/img/logo_white.png')}}" alt="">
                            </a>
                            <ul class="widget-contact">
                                <li>
                                    <h4 class="widget-contact__title">Ubicación:</h4>
                                    <p class="widget-contact__text">Calle Oliva Miranda y Alberto Oblete, Manta - Ecuador</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Horarios de atención:</h4>
                                    <p class="widget-contact__text">9:00am - 5:00pm</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Teléfono:</h4>
                                    <p class="widget-contact__text">+593 98 076 19610</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Email:</h4>
                                    <p class="widget-contact__text">
                                        <a class="widget-contact__text-email" href="mailto:thesmartedu@edu.com">soporte@runachay.com.ec</a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <!--
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="widget-link">
                                <h3 class="widget-title">Explore</h3>
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">History &amp; Mission</a>
                                    </li>
                                    <li>
                                        <a href="#">Administration</a>
                                    </li>
                                    <li>
                                        <a href="#">Community</a>
                                    </li>
                                    <li>
                                        <a href="#">Around the World</a>
                                    </li>
                                    <li>
                                        <a href="#">News Network</a>
                                    </li>
                                    <li>
                                        <a href="#">Visitor Information</a>
                                    </li>
                                    <li>
                                        <a href="#">Social Media</a>
                                    </li>
                                    <li>
                                        <a href="#">Our Campuses</a>
                                    </li>
                                    <li>
                                        <a href="#">Campus Directories</a>
                                    </li>
                                    <li>
                                        <a href="#">Employment</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        -->
                        <!--
                        <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="widget-link">
                                <h3 class="widget-title">Admissions</h3>
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">Undergraduate</a>
                                    </li>
                                    <li>
                                        <a href="#">By School</a>
                                    </li>
                                    <li>
                                        <a href="#">Process</a>
                                    </li>
                                    <li>
                                        <a href="#">Visitor Information</a>
                                    </li>
                                    <li>
                                        <a href="#">For Prospective Parents</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        -->
                        <!-- <div class="col-sm-3 col-md-3 col-lg-3">
                            <div class="widget-link">
                                <h3 class="widget-title">Research</h3>
                                <ul class="widget-list">
                                    <li>
                                        <a href="#">Applied Physics Laboratory</a>
                                    </li>
                                    <li>
                                        <a href="#">Research Projects Administration</a>
                                    </li>
                                    <li>
                                        <a href="#">Funding Opportunities</a>
                                    </li>
                                    <li>
                                        <a href="#">Undergraduate Research</a>
                                    </li>
                                    <li>
                                        <a href="#">Resources</a>
                                    </li>
                                    <li>
                                        <a href="#">Health &amp; Medicine</a>
                                    </li>
                                    <li>
                                        <a href="#">Social Sciences, Humanities &amp; Arts</a>
                                    </li>
                                    <li>
                                        <a href="#">Natural Sciences, Engineering &amp; Tech</a>
                                    </li>
                                    <li>
                                        <a href="#">Global Research</a>
                                    </li>
                                    <li>
                                        <a href="#">Technology Transfer</a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="footer-box-02">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <p class="copy-info">&copy; 2018 Runachay. Todos los derechos reservados</p>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 text-center">
                            <ul class="social-list-01">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-info">
                                <a class="footer-info__01" href="#">Política de privacidad</a>
                                <a class="footer-info__02" href="#">Térnimos de uso</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="back2top" title="Back to Top">Ir arriba</a>
        </footer>
    </div>
    <!-- JQuery v2.2.4 -->
    <script src="{{asset('assets/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

    <script src="{{asset('assets/js/scrollreveal.js')}}"></script>

    <script src="{{asset('assets/js/bootstrap.js')}}"></script>
    <!-- Superfish v1.7.9 -->
    <script src="{{asset('assets/js/plugins/jquery.superfish.min.js')}}"></script>

    <script src="{{asset('assets/js/plugins/jquery.nivo.min.js')}}"></script>
    <!-- Owl carousel v2.2.1 -->
    <!-- Waypoint -->
    <!-- Progress circle -->

    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/jquery.scroll-reveal.js"></script> -->
    <!-- Main script -->
    <script src="{{asset('assets/js/main.js')}}"></script>

    <script>
        $(document).ready(function(){
           $(window).bind('scroll', function() {
           var distance = 50;
                 if ($(window).scrollTop() > distance) {
                     $('.main-nav').addClass('scrolled');
                    $('.main-nav > div').addClass('paddingtop');
                    $('.navbar-brand').addClass('brand-scrolled');
                 }
                 else {
                     $('.main-nav').removeClass('scrolled');
                    $('.main-nav > div').removeClass('paddingtop');
                    $('.navbar-brand').removeClass('brand-scrolled');
                 }
            });
        });
    </script>
</body>
</html>