<!DOCTYPE html>
<html lang="en">
<?php
    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }
    ?>
<head>
	<title>Registro Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 280 instituciones educativas y alrededor de 500000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">

	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/alertify.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/webinar/style.css')}}">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/e8d895419f.js" crossorigin="anonymous"></script>
	
</head>
<body>

		<section class="form_page">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="row">
							<div class="col-lg-12">
								<div class="fp_head mb40"><img src="{{asset('assets/img/logocuadrado.png')}}" alt=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-xxl-6 col-xl-8 col-lg-6 offset-xxl-3 offset-xl-2 order-last order-lg-first">

								<div class="fp_data">
									<div class="fp_title mb40">
										<h2 class="color_title"><strong>{{$infowebi->tema}}</strong> - <strong>{{$infowebi->fecha}}</strong></h2>
									</div>
									
										<div class="fp_form">
											<h5><strong>Seleccione el día para registrar su asistencia:</strong></h5>
											<br><br>
											<div class="listadias">
												<?php $contador = 1; ?>
												@foreach ($dias_evento as $item)
												<div class="item_ld">
													<p><strong>Dia {{$contador}}</strong> - <?php echo date('d', strtotime($item->fecha)); ?>
                                            		<?php echo date('M Y', strtotime($item->fecha)); ?></p>
													<a href="{{URL::to('/asistencia')}}/{{$item->slug}}">Registrar asistencia</a>
												</div>
												<?php $contador ++; ?>
												@endforeach
											</div>
										</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</section>


</body>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/alertify.js')}}"></script>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<!--<script src="{{asset('runastore/js/bootstrap.js')}}"></script>
<script src="{{asset('runastore/js/jquery.js')}}"></script>
<script src="{{asset('runastore/js/owl.carousel.js')}}"></script>

<script src="{{asset('js/alertify.js')}}"></script>-->



<script>
        $
        function contactos(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'Información de contacto';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>Puede ponerse en contacto directamente con nosotros enviándonos un mensaje vía WhatsApp al número 0980761961 o vía correo electrónico:</h4><ul style='color: #20A4F0;margin-bottom: 20px;'><li>ventas@runachay.com.ec</li><li>gerencia@runachay.com.ec</li></ul><p style='color: #98A5B5;font-size: 13px;'>Nuestros horarios de atención son de Lunes a Viernes de 8:00 a 18:00 y Sábados de 8:00 a 12:00.</p></div>");
        }
    </script>

    <script>
        $
        function registrado(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'CONFIRMACION DE REGISTRO';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>REGISTRADO CORRECTAMENTE</h4><p style='color: #98A5B5;font-size: 13px;'>LINK DE ACCESO.</p></div>");
        }
    </script>

    <script>
		function copiarAlPortapapeles(id_elemento) {
		  var aux = document.createElement("input");
		  aux.setAttribute("value", document.getElementById(id_elemento).innerHTML);
		  document.body.appendChild(aux);
		  aux.select();
		  document.execCommand("copy");
		  document.body.removeChild(aux);

		  try {
				document.execCommand('copy');
				var aviso = document.createElement('div');
				aviso.setAttribute('id', 'aviso');
				aviso.style.cssText = 'position:fixed; z-index: 9999999; top: 50%;left:50%;margin-left: -70px;padding: 20px; background: gold;border-radius: 8px;font-family: sans-serif;';
				aviso.innerHTML = 'URL copiada';
				document.body.appendChild(aviso);
				document.load = setTimeout('document.body.removeChild(aviso)', 2000);
				document.load = setTimeout('boton.id = "getlink"',2500);
				boton.id = '';
			} catch (e) {
				// alert('Tu navegador no soporta la función de copiar');
			}
			document.body.removeChild(aux);
		}
	</script>
	<script>


	</script>

</html>