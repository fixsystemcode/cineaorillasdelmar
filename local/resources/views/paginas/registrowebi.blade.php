<!DOCTYPE html>
<html lang="en">
<?php
    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }
    ?>
<head>
	<title>Cine a Orillas del Mar - Manta | Sistema web para resrevación online</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 280 instituciones educativas y alrededor de 500000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">

	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/alertify.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/webinar/style.css')}}">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/e8d895419f.js" crossorigin="anonymous"></script>
	
</head>
<body>

	@if($infowebi == null)

	El webinar no fue encontrado, por de click <a>aqui</a> para regresar 

	@else

	   @if($infowebi->estado == 1)

		<section class="form_page">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<div class="row">
							<div class="col-lg-12">
								<div class="fp_head mb40"><img src="{{asset('assets/img/logocuadrado.png')}}" alt=""></div>
							</div>
						</div>
						<div class="row">
							<div class="col-xxl-5 col-xl-6 col-lg-6 order-last order-lg-first">
								<div class="fp_data">
									<div class="fp_anuncio mb20">
										<img src="{{asset('local/public/images')}}/{{$infowebi->anuncio}}" alt="" style="width: 100%;">
									</div>
									<div class="fp_title mb40">
										<h2 class="color_title"><strong>{{$infowebi->tema}}</strong></h2>
										<p>
											<strong>FECHA: </strong><?php echo date('d', strtotime($infowebi->fecha))." de ".mesES($infowebi->fecha)." del ".date('Y', strtotime($infowebi->fecha)); ?></span><br>
											<strong>HORA: </strong><?php echo date( 'H:i', strtotime($infowebi->hora));  ?><br>
										</p>
									</div>

									

								</div>
							</div>
							<div class="col-xxl-6 col-xl-6 col-lg-6 offset-xxl-1 order-first order-lg-last">
								@if(Session::has('message-duplicado'))
								<div class="fp_form" id="box_confirm">
										<div class="row">
											<div class="col-lg-12">
												<div class="fpf_title mb20">

													<h3><strong>{{$infowebi->tema}}</strong></h3>
													<br>
													<p>{{ session::get('message-duplicado') }}.</p>
													<div style="padding: 12px 20px;background: #f5f5f5;border-radius: 0;display: flex;align-items: center;">
														<a href="{{ session::get('enlace') }}" style="color: #0A70CE;text-decoration: none;font-weight: 600;" id="enlace"> {{ session::get('enlace') }}</a>
														<a href="{{ session::get('ultimoid') }}" style="color: #0A70CE;text-decoration: none;font-weight: 600;" id="ultimoid"> {{ session::get('ultimoid') }}</a>
														<a onclick="copiarAlPortapapeles('enlace')" class="copi">Copiar enlace</a>

													</div>
												</div>
											</div>
										</div>
									<br>
									<div class="fpf_foot">
										<p>Tienes problemas con el registro, no dudes en contactar a nuestro <a href="#" onclick="contactos()">soporte técnico.</a></p>
									</div>
								</div>
								@elseif(Session::has('message'))
								<div class="fp_form" id="box_confirm">
										<div class="row">
											<div class="col-lg-12">
												<div class="fpf_title mb20">
													<h3><strong>Registro exitoso</strong></h3>
													<br>
													<?php $id_reserva = session::get('ultimoid')->id;  ?>
													<input id="text" type="text" value="{{ session::get('ultimoid')->id }}" style="display: none;" />
													<p>Tu registro se ha realizado con éxito, la confirmación de la reserva ha sido enviada a tu correo electrónico.</p>
													<p>Podrás entrar al evento presentando el siguiente codigo QR:</p>
													<div style="padding: 12px 20px;background: #f5f5f5;border-radius: 0;display: flex;align-items: center;">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
														    <g id="qrcode"/>
														</svg>
														
														<a class="copi" id="imprime">Descargar</a>
													</div>
												</div>
											</div>
										</div>
									<br>
									<div class="fpf_foot">
										<p>Tienes problemas con el registro, no dudes en contactar a nuestro <a href="#" onclick="contactos()">soporte técnico.</a></p>
									</div>
								</div>
								@else
								
								<div class="fp_form">
								{!! Form::open(['route' => ['WebinarRegistro.store', 'id_webinar' =>$infowebi->id], 'method' => 'POST', 'name' => 'formreg',
	                            'files'=>true]) !!}
									<form action="">
										<div class="row">
											<div class="col-lg-12">
												<div class="fpf_title mb20">
													<h3><strong>Formulario de Reserva.</strong></h3>
													<h5><strong>Puestos Disponibles: <?php echo (100 - $totalreservas); ?> de 100.</strong></h5>
													@if(Session::has('message-error'))
													<div class="alert alert-danger alert-dismissible" role="alert">
														<strong>{{ session::get('message-error') }}</strong>
													</div>
													@endif
													<!-- <p>Completa este formulario y en breve te llegara un  correo con las indicaciones para poder ingresar al Webinar en la fecha establecida</p> -->
												</div>
											</div>
											<div class="col-lg-12 col-sm-6 col-xs-6">
												<div class="form-group">
													{!! Form::label('cedula','Cédula:') !!}
	                                    			{!! Form::text('cedula',null,['class' =>'form-control', 'placeholder' =>'','required'])!!}
												</div>
											</div>
											<div class="col-lg-6 col-sm-6 col-xs-6">
												<div class="form-group">
													{!! Form::label('nombres','Nombres:') !!}
	                                    			{!! Form::text('nombres',null,['class' =>'form-control', 'placeholder' =>'','required'])!!}
												</div>
											</div>
											<div class="col-lg-6 col-sm-6 col-xs-6">
												<div class="form-group">
													{!! Form::label('apellidos','Apellidos:') !!}
	                                    			{!! Form::text('apellidos',null,['class' =>'form-control', 'placeholder' =>'','required'])!!}
												</div>
											</div>
											<div class="col-lg-6 col-sm-6 col-xs-6">
												<div class="form-group">
													{!! Form::label('email','Correo electrónico:') !!}
	                                    			{!! Form::email('email',null,['class' =>'form-control', 'placeholder' =>'','required'])!!}
												</div>
											</div>
											<div class="col-lg-6 col-sm-6 col-xs-6">
												<div class="form-group">
													{!! Form::label('telefono','Teléfono:') !!}
	                                    			{!! Form::text('telefono',null,['class' =>'form-control', 'placeholder' =>'','required'])!!}
												</div>
											</div>
											<div class="col-lg-12"><hr></div>
											
											<div class="col-lg-12 col-md-12 col-sm-12">
	                                			<div class="form-group"><div class="g-recaptcha" data-sitekey="6LcrxKgaAAAAAF90MDs6tgHjf7DgylRdn3pq3ajf"></div></div>
	                            			</div>
											<div class="col-lg-12">
												<div class="form-group">
													<button type="submit">Enviar</button>
												</div>
											</div>
										</div>
									</form>
									<br>
									<div class="fpf_foot">
										<p>Tienes problemas con el registro, no dudes en contactar a nuestro <a href="#" onclick="contactos()">soporte técnico.</a></p>
									</div>
								{!! Form::close() !!}
								</div>
								@endif
								<!--  -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	  @else
	     <h3 align="center">EL REGISTRO PARA ESTE WEBINAR ESTÁ INACTIVO</h3>
	  @endif

	 @endif

</body>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<script src="{{asset('assets/js/alertify.js')}}"></script>
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/html2pdf.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/jQuery.print.js')}}"></script>
<script src="{{asset('assets/js/jquery.PrintArea.js')}}"></script>
<script src="{{asset('assets/js/printThis.js')}}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="{{asset('assets/js/qrcode.js')}}"></script>
<!--<script src="{{asset('runastore/js/bootstrap.js')}}"></script>
<script src="{{asset('runastore/js/jquery.js')}}"></script>
<script src="{{asset('runastore/js/owl.carousel.js')}}"></script>

<script src="{{asset('js/alertify.js')}}"></script>-->

<script type="text/javascript">
   	var loc = window.location;
    var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
    var ruta = loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));

    var id = document.getElementById("text").value;

    var linkqr = ruta+'reserva/'+id;

  var qrcode = new QRCode(document.getElementById("qrcode"), {
      width : 100,
      height : 100,
      useSVG: true
  });

  function makeCode () {		
      var elText = "https://fb.com";

      qrcode.makeCode(linkqr);
  }

  makeCode();

  $("#text").
      on("blur", function () {
          makeCode();
      }).
      on("keydown", function (e) {
          if (e.keyCode == 13) {
              makeCode();
          }
      });

    $("#imprime").click(function (){
			//$('#ticket').printThis();
		    var mode = 'iframe';
		    var options = { mode : mode};
		    setTimeout(function () {
		      $("#box_confirm").print();
		    }, 50);
		  });
    </script>


<script>
        $
        function contactos(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'Información de contacto';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>Puede ponerse en contacto directamente con nosotros enviándonos un mensaje vía WhatsApp al número 0980761961 o vía correo electrónico:</h4><ul style='color: #20A4F0;margin-bottom: 20px;'><li>innovacion@runachay.com.ec</li></ul><p style='color: #98A5B5;font-size: 13px;'>Nuestros horarios de atención son de Lunes a Viernes de 8:00 a 18:00 y Sábados de 8:00 a 12:00.</p></div>");
        }
    </script>

    <script>
        $
        function registrado(){
            alertify.genericDialog || alertify.dialog('genericDialog',function(){
                return {
                    main:function(content){
                        this.setContent(content);
                    },
                    setup:function(){
                        return {
                            focus:{
                                element:function(){
                                    return this.elements.body.querySelector(this.get('selector'));
                                },
                                select:true
                            },
                            options:{
                                basic:true,
                                maximizable:false,
                                resizable:false,
                                padding:false
                            }
                        };
                    },
                    settings:{
                        selector:undefined
                    }
                };
            });
            alertify.defaults.glossary.title = 'CONFIRMACION DE REGISTRO';
            alertify.genericDialog ("<div style='padding: 20px 40px;text-align: justify;'><h4 style='color: #333846;font-size: 16px;font-weight: 600;margin: 0 0 20px;line-height: 1.4;'>REGISTRADO CORRECTAMENTE</h4><p style='color: #98A5B5;font-size: 13px;'>LINK DE ACCESO.</p></div>");
        }
    </script>

    <script>
		function copiarAlPortapapeles(id_elemento) {
		  var aux = document.createElement("input");
		  aux.setAttribute("value", document.getElementById(id_elemento).innerHTML);
		  document.body.appendChild(aux);
		  aux.select();
		  document.execCommand("copy");
		  document.body.removeChild(aux);

		  try {
				document.execCommand('copy');
				var aviso = document.createElement('div');
				aviso.setAttribute('id', 'aviso');
				aviso.style.cssText = 'position:fixed; z-index: 9999999; top: 50%;left:50%;margin-left: -70px;padding: 20px; background: gold;border-radius: 8px;font-family: sans-serif;';
				aviso.innerHTML = 'URL copiada';
				document.body.appendChild(aviso);
				document.load = setTimeout('document.body.removeChild(aviso)', 2000);
				document.load = setTimeout('boton.id = "getlink"',2500);
				boton.id = '';
			} catch (e) {
				// alert('Tu navegador no soporta la función de copiar');
			}
			document.body.removeChild(aux);
		}
	</script>
	<script type="text/javascript">
		
		$("#pais").change(function(){
		  
		  if($("#pais").val()==="Ecuador"){

		    var combo = document.getElementById('provincia');
		    var texto = document.getElementById('provincia2');
			combo.style.display = 'inline';
		  	texto.style.display = 'none';
		  }else{
		    var combo = document.getElementById('provincia');
		    var texto = document.getElementById('provincia2');
			combo.style.display = 'none';
		  	texto.style.display = 'inline';
		  }
		  
		});

	</script>
</html>