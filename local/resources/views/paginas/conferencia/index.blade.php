<!DOCTYPE html>
<html lang="en">
<head>
	<title>Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 190 instituciones educativas y alrededor de 200000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <link href="{{asset('assets/images/logo32.png')}}" rel="icon" type="img/ico.png" sizes="32x32">
    <link href="{{asset('assets/images/logo96.png')}}" rel="icon" type="img/ico.png" sizes="96x96">
    <link rel="apple-touch-icon" href="{{asset('assets/images/logo64.png')}}">
    <link href="{{asset('assets/images/logo64.png')}}" rel="icon" type="image/x-icon">

    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">

	

	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@200;300;400;500;700;800&display=swap" rel="stylesheet">



	<style>
		body{
			height: 100vh;
			background-size: cover;
			background-position: center;
		}
		.fs_index{
			position: relative;
			width: 100%;
			top: 50%;
			transform: translateY(-50%);
		}
		.fs_cont{
			position: relative;
			padding: 30px 40px;
			background-color: rgba(255,255,255,1);
			border-radius: 8px;
			box-shadow: 4px 12px 34px rgba(0,0,0,0.1);
			text-align: justify;
		}
		.fs_cont h5{
			color: #fff;
		}
		.fs_cont img{
			width: 48px;
			height: auto;
			margin-bottom: 40px;
		}
		.fs_cont h2{
			margin: 0 0 10px;
			color: #29303F;
		}
		.fs_cont p{
			color: #414856;
		}
	</style>
</head>
<body style="background-image: url('{{asset('assets/img/bg.jpg')}}');">
	<section class="fs_index" >
		<div class="container">
			<div class="row" style="align-items: center;">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="fs_cont">
						<img src="{{asset('assets/img/logocuadrado.png')}}" alt="">
						<!-- <h5 class="color_verde"><strong>Runachay</strong></h5> -->
						<h2 class="color_title"><strong>JORNADA DE ACTUALIZACIÓN DOCENTE</strong></h2>
						<br>
						<p>Estimado docente, reciba un saludo de la Empresa Runachay, esta vez para invitarle a ser parte de las <strong>Jornadas de Actualización Pedagógica Runachay 2021</strong>, que se darán desde el 3 al 5 de mayo, de 15h00 a 19h00. </p>
						<p>Tendremos temas de actualización docente y un espacio para compartir, fortaleciendo nuestro accionar en este evento que contará con ponentes nacionales e internacionales.</p>
						<p>Se entregará un certificado por la participación con un total de 40 horas, este evento no tiene costo alguno para la comunidad docente.</p>
						<br>
						<div style="padding: 10px 30px;background-color: #EFF3F9;border-radius: 6px;">
							<p class="color_text" style="margin: 0;">El registro a este evento estará disponible desde el día <strong>Miércoles 28 de Abril</strong> a partir de las <strong>15:00 PM</strong>.</p>
						</div>
						<br>
					</div>
				</div>
				<div class="col-xxl-6 offset-xxl-1">
					<div class="fs_img">
						<img src="{{asset('img/servicios.png')}}" alt="" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
		<div class="bar"></div>
	</section>
	
</body>

<script src="js/bootstrap.js"></script>
<script src="js/jquery.js"></script>


</html>