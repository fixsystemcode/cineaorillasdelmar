@extends('layouts.backend')

@section('titulo')
Editar
@stop


@section('title','Editar articulo: '. $informacion->title)

@section('contenido')

    {!!Form::model($informacion,['route'=>['informacion.update',$informacion],'method'=>'PUT','files' => true])!!}
<div class="form-group">
					{!! Form::label('direccion','Dirección') !!}
					{!! Form::text('direccion',null,['class' =>'form-control', 'placeholder' =>'Dirección','required'])!!}
				</div>

				<div class="form-group">
					{!! Form::label('hora','Horarios de atención') !!}
					{!! Form::text('hora',null,['class' =>'form-control', 'placeholder' =>'Horarios de atención','required'])!!}
				</div>

				<div class="form-group">
                	{!! Form::label('email', 'Email:') !!}
                	{!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa el Email']) !!}
           		 </div>

				<div class="form-group">
					{!! Form::label('telefono','Teléfonos') !!}
					{!! Form::text('telefono',null,['class' =>'form-control', 'placeholder' =>'Telefonos','required'])!!}
				</div>


		
				
				<div class="form-group">
					{!! Form::submit('Actualizar', ['class' =>'btn btn-primary']) !!}
					
				</div>
			

	
	{!!Form::close()!!}<br>

	

@endsection