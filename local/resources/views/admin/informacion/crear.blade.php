@extends('layouts.backend')


@section('titulo')
 Nuevo  
@stop

@section('contenido')


           {!! Form::open(['route' => 'informacion.store', 'method' => 'POST','files'=>true]) !!}
				
				<div class="form-group">
					{!! Form::label('direccion','Dirección') !!}
					{!! Form::text('direccion',null,['class' =>'form-control', 'placeholder' =>'Título','required'])!!}
				</div>

				<div class="form-group">
					{!! Form::label('hora','Horarios de atención') !!}
					{!! Form::text('hora',null,['class' =>'form-control', 'placeholder' =>'Días de atención','required'])!!}
				</div>

				<div class="form-group">
                	{!! Form::label('email', 'Email:') !!}
                	{!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa el Email']) !!}
           		 </div>

				<div class="form-group">
					{!! Form::label('telefono','Teléfonos') !!}
					{!! Form::text('telefono',null,['class' =>'form-control', 'placeholder' =>'Hora de atención','required'])!!}
				</div>


				
				<div class="form-group">
					{!! Form::submit('Guardar', ['class' =>'btn btn-primary']) !!}
					
				</div>
			    
			{!! Form::close() !!}

@endsection			
