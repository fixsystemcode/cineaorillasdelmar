@extends('layouts.backend')


@section('contenido')

@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>


@endif

<h1>Informacion General</h1>

<br>
@if (count($informaciones))


<br>
    <div class="row">
      <div class="col-md-12">     

          <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
              <th>Titulo</th>
             
              <th>Acciones</th>
            
            
              

            </thead>

            <tbody>
              <tr>
                @foreach ($informaciones as $informacion)
                <td>Informacion Básica del Sitio</td>
                <td><a class="btn btn-success" href="{{route('informacion.edit', $informacion->id)}}" role="button">Editar</a>
                    
                </td>

              </tr>
              @endforeach
            </tbody>
          </table>

         

         

      </div>
   
    </div>

@else
    No hay Informacion
@endif





@endsection