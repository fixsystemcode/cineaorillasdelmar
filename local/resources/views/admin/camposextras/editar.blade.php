@extends('layouts.backend')


@section('titulo')
    <i class="fa fa-save"></i> Editar
@endsection

@section('contenido')


<h1>Editar Campos Extras Modulo</h1>

@include('alerts.request')

{!! Form::model($dato, array('method' => 'PATCH', 'route' => array('camposextras.update', $dato->id))) !!}

    
   <div class="form-group">
            {!! Form::label('nombre', 'Nombre:') !!}
            {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('descripcion', 'Descripción:') !!}
            {!! Form::text('descripcion',$dato->descripcion,['class'=>'form-control','placeholder'=>'Descripcion']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('valor', 'Valor:') !!}
            {!! Form::text('valor',null,['class'=>'form-control','placeholder'=>'Valor']) !!}
    </div>
    
    <div class="form-group">
            {!! Form::label('tipo', 'Tipo:') !!}
            {!! Form::select('tipo',  array('text' => 'text',
                                            'select' => 'select',
                                            'textarea' => 'textarea',
                                            'date' => 'date',
                                            'multipleselect' => 'multipleselect'
                                           ), 'text' ,['class'=>'form-control prioridad']) !!}
    </div>
    
    <div class="form-group">
         {!! Form::label('id_campos_extras_grupo', 'Asignadar a grupo',['class'=>'control-label']) !!}
         {!! Form::select('id_campos_extras_grupo', $camposextrasgrupos, null,['required' => 'required','class'=>'form-control select2']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('icono', 'Ícono:') !!}
            {!! Form::text('icono',null,['class'=>'form-control','placeholder'=>'Ícono']) !!}
    </div>
 

      
    


            {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}


{!! Form::close() !!}



@stop