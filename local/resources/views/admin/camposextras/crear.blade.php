@extends('layouts.backend')


@section('contenido')

<h1>Creación de Campos extras Modulo</h1>

@include('alerts.request')

{!! Form::open(['route' => 'camposextras.store', 'method' => 'POST','files'=>true]) !!}

    

    <div class="form-group">
            {!! Form::label('nombre', 'Nombre:') !!}
            {!! Form::text('nombre',null,['class'=>'form-control','placeholder'=>'Nombre']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('descripcion', 'Descripcion:') !!}
            {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripcion']) !!}
    </div>

    <div class="form-group">
            {!! Form::label('valor', 'Valor:') !!}
            {!! Form::text('valor',null,['class'=>'form-control','placeholder'=>'Valor']) !!}
    </div>
   
    
     <div class="form-group">
            {!! Form::label('tipo', 'Tipo:') !!}
            {!! Form::select('tipo',  array('text' => 'text',
                                            'select' => 'select',
                                            'textarea' => 'textarea',
                                            'date' => 'date',
                                            'multipleselect' => 'multipleselect'
                                            ), 'text' ,['class'=>'form-control prioridad']) !!}
                                    
    </div>

     <div class="form-group">
         {!! Form::label('id_campos_extras_grupo', 'Asignadar a Campos Extras de Modulo',['class'=>'control-label']) !!}

         {!! Form::select('id_campos_extras_grupo', $camposextrasgrupos, null,['required' => 'required','class'=>'form-control select2']) !!}

    </div>

    <div class="form-group">
            {!! Form::label('icono', 'Ícono:') !!}
            {!! Form::text('icono',null,['class'=>'form-control','placeholder'=>'Ícono']) !!}
    </div>

    
  



   

    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}

{!! Form::close() !!}


@stop