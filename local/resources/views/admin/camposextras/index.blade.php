@extends('layouts.backend')


@section('titulo')
    <i class="fa fa-save"></i> Campos extras
@endsection

@section('contenido')


<a class="btn btn-primary btn-app" href="{{ URL::route('camposextras.create') }}"> <i class="fa fa-calendar-check-o"></i>Nuevo</a>
<br>


@if (count($datos))
 

<br>
    <div class="row">
      <div class="col-md-12">

         <div class="box">
           
            <!-- /.box-header -->
            <div class="box-body">
               <table id="example1" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                        
                            <th>Descripcion</th>
                            
                           
                            <th>Pertenece a</th>
                           
                            <th style="width:20%">Acciones</th>
                           
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php $s = 0 ; ?>
                        @foreach ($datos as $dato)
                            <tr>
                                <td> <?php $s = $s + 1; echo $s; ?></td>
                              
                                <td>{{ $dato->descripcion }}</td>
                                
                               
                             
                                <td>{{ $dato->tipodescripcion }}</td>
                              

                                 <td ><a class="btn btn-success btn-xs" href="{{route('camposextras.edit', $dato->id)}}" role="button"><i class="fa fa-pencil-square-o"></i> Editar</a>
                                      <a class="btn btn-danger btn-xs" href="{{route('camposextras.destroy', $dato->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button"><i class="fa fa-trash-o"></i> Eliminar</a>
                                 </td>


                            </tr>
                        @endforeach
                          
                    </tbody>
                  
                </table>
            </div>
            <!-- /.box-body -->
          </div>
       

      </div>
      <div class="col-md-6">

      </div>
    </div>


    
@else
       <p style="
    text-align: center;
    color: #a3a3a3;
    font-size: 24px;
    padding: 35px;
    text-transform: uppercase;"> No hay datos</p>
@endif

@stop