@extends('layouts.backend')

@section('titulo')
Tipos de Usuarios
@stop

@section('contenido')

 
<a class="btn btn-primary" href="{{ URL::route('tipos-usuarios.create') }}">Nuevo</a>
<br>
@if ($tipos->count())

<br>
    <div class="row">
      <div class="col-md-6">
        <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($tipos as $tipo)
                    <tr>
                        <td>{{ $tipo->id }}</td>
                        <td>{{ $tipo->tipo }}</td>
                        <td>{!! link_to_route('tipos-usuarios.edit', 'Editar', array($tipo->id), array('class' => 'btn btn-info')) !!}</td>
                        <td>
                          {!! Form::open(array('method' => 'DELETE', 'route' => array('tipos-usuarios.destroy', $tipo->id))) !!}                       
                             {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                          {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                  
            </tbody>
          
        </table>

      </div>
      <div class="col-md-6">

      </div>
    </div>


    
@else
    No hay usuarios
@endif

@stop