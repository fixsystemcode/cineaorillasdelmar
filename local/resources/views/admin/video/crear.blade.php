@extends('layouts.backend')

@section('titulo')
Nuevo
@stop

@section('contenido')

@include('alerts.request')



 {!! Form::open(['route' => 'video.store', 'method' => 'POST','files'=>true]) !!}


 	<div class="form-group">
		{!! Form::label('titulo','Título') !!}
		{!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
	</div>
	 <div class="form-group">
                {!! Form::label('descripcion', 'Descripcion:') !!}
                {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción del Slider','required']) !!}
            </div>



              <div class="form-group">
                {!! Form::label('categoria','Categoria') !!}
                {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control','required' , 'list' =>'listas'])!!}
              </div>

    <div class="form-group">
		{!! Form::label('path','URL del Video de YouTuBe') !!}
		{!! Form::text('path',null,['class' =>'form-control', 'placeholder' =>'Pegue aquí la URL del video','required'])!!}
	</div>




    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}





{!! Form::close() !!}



@stop