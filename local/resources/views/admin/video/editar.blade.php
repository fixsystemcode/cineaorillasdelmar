@extends('layouts.backend')

@section('titulo')
Editar
@stop

@section('contenido')

@include('alerts.request')

 


{!! Form::model($video, array('method' => 'PATCH', 'route' => array('video.update', $video->id) , 'files' => true)) !!}
    
    
  
        <div class="form-group">
          {!! Form::label('titulo','Título') !!}
          {!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
        </div>
        <div class="form-group">
                {!! Form::label('descripcion', 'Descripcion:') !!}
                {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción de la imagen']) !!}
            </div>
      
 
    
    <div class="form-group">
                {!! Form::label('categoriainterna','Categoria') !!}
                {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas'])!!}
    </div>

          <div class="form-group">
          {!! Form::label('path','URL del Video de YouTuBe') !!}
          {!! Form::text('path',null,['class' =>'form-control', 'placeholder' =>'Pegue aquí la URL del video','required'])!!}
        </div>



   

    {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}



{!! Form::close() !!}



@stop