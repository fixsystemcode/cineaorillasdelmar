@extends('layouts.backend')

@section('titulo')
Nuevo  
@stop


@section('contenido')

@include('alerts.request')

 

 {!! Form::open(['route' => 'imagen.store', 'method' => 'POST','files'=>true]) !!}
      


    <div class="form-group">
          {!! Form::label('imagen','Imagen') !!}
          {!! Form::file('path')!!}
    </div>
    <div class="form-group">
            {!! Form::label('titulo', 'Titulo del Slider:') !!}
            {!! Form::text('titulo',null,['class'=>'form-control','placeholder'=>'Ingrese Título','required']) !!}
    </div>
    
      
           <div class="form-group">
                {!! Form::label('descripcion', 'Descripcion:') !!}
                {!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Descripción del Slider','required']) !!}
            </div>
    
 
  
    

              <div class="form-group">
                {!! Form::label('categoria','Categoria') !!} 
                {!! Form::select('id_categoriainterna',$categorias, null,[ 'class' =>'form-control','required' , 'list' =>'listas'])!!}
              </div>


   

    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop