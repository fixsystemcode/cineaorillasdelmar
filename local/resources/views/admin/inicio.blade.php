@extends('layouts.backend')

@section('ruta')
Gertor CMS FIXSYSTEM
@stop

@section('titulo')
Administración Portal Web | Gestor CMS FIXSYSTEM
@stop

@section('contenido')


<div class="row">
	<div class="col-lg-3">
                <div class="widget-head-color-box navy-bg p-lg text-center">
                    <a href="{{ URL::asset('usuarios') }}" style="color: white;">
	                    <div class="m-b-md">
	                        <i class="fa fa-user fa-4x"></i>
	                        <h1 class="m-xs">{{$cusuarios}}</h1>
	                        <h3 class="font-bold no-margins">
	                            Usuarios
	                        </h3>
	                     
	                    </div>
                    </a>

                </div>
                    
    </div>
    <div class="col-lg-3">
                <div class="widget yellow-bg p-lg text-center">
                    <a href="{{ URL::asset('articulos') }}" style="color: white;">
                    	<div class="m-b-md">
	                        <i class="fa fa-newspaper-o fa-4x"></i>
	                        <h1 class="m-xs">{{$carticulo}}</h1>
	                        <h3 class="font-bold no-margins">
	                            Artículos
	                        </h3>
	                       
	                    </div>
                    </a>
                </div>
                    
    </div>
    <div class="col-lg-3">
                <div class="widget red-bg p-lg text-center">
                    <a href="{{ URL::asset('animacion') }}" style="color: white;">
                    	<div class="m-b-md">
	                        <i class="fa fa-picture-o fa-4x"></i>
	                        <h1 class="m-xs">{{$cslider}}</h1>
	                        <h3 class="font-bold no-margins">
	                            Slider
	                        </h3>
	                        
	                    </div>
                    </a>
                </div>
                    
    </div>
    <div class="col-lg-3">
                <div class="widget lazur-bg p-lg text-center">
                    <a href="{{ URL::asset('video') }}" style="color: white;">
                    	<div class="m-b-md">
	                        <i class="fa fa-youtube-play fa-4x"></i>
	                        <h1 class="m-xs">{{$cvideos}}</h1>
	                        <h3 class="font-bold no-margins">
	                            Videos
	                        </h3>
	                        
	                    </div>
                    </a>
                </div>
                    
    </div>

</div>

<div class="row">
	<div class="col-lg-3">

    </div>

    <div class="col-lg-9">
		    @if (count($articulos))

				<br>
				    <div class="row">
				        <div class="col-md-12">     


					        <div class="box">
					           
					            <!-- /.box-header -->
					            <div class="box-body">   

						          <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
						            <thead>
						              <th>ID</th>
						              <th>Título</th>
						              <th style="width:30%;">Contenido</th>
						              <th>Autor</th>
						              <th>Categoria</th>
						              
						              <th>Acciones</th>
						            </thead>

						            <tbody>
						              <tr>
						                <?php $s = 0 ; ?>
						                @foreach ($articulos as $articulo)
						                <td> <?php $s = $s + 1; echo $s; ?></td>
						                <td>{{$articulo->titulo}}</td>
						                <td><?php $ser1 = substr($articulo->texto,0,200); echo strip_tags($ser1); ?></td>
						                <td>{{$articulo->nombres}}</td>
						                <td>{{$articulo->categoria}}</td>
						                
						                <td><a class="btn btn-success" href="{{route('articulos.edit', $articulo->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
						                    <a class="btn btn-danger" href="{{route('articulos.destroy', $articulo->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button"><i class="fa fa-trash-o"></i></a>
						                </td>

						              </tr>
						              @endforeach
						            </tbody>
						          </table>
	 
					            </div>
					   
					        </div>
				        </div>
				   
				    </div>

			@else
				    No hay Artículos
			@endif
    </div>

</div>


@stop