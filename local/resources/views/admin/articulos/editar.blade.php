@extends('layouts.backend')
@section('titulo')
Editar
@stop
@section('title','Editar articulo: '. $articulo->title)
@section('contenido')
<style>
#map {
	height: 400px;
	width: 100%;
}
#elim{
	color:#fff;
	border:1px solid #ccc;
	background-color:#fb0000;
	margin-right:10px;
	padding: 0 2px;
	border-radius:4px;
	-moz-border-radius:4px;
	-webkit-border-radius:4px;
	-o-border-radius:4px;
	border-radius:4px;
	font-weight:bold;
	font-size:0.8em;
	cursor:pointer;
}
#map-canvas {
	height: 100%;
	margin: 0;
}
#map-canvas > .centerMarker {
	position: absolute;
	/*url of the marker*/
	background: url(http://maps.gstatic.com/mapfiles/markers2/marker.png) no-repeat;
	/*center the marker*/
	top: 50%;
	left: 50%;
	z-index: 1;
	/*fix offset when needed*/
	margin-left: -10px;
	margin-top: -34px;
	/*size of the image*/
	height: 34px;
	width: 20px;
	cursor: pointer;
}
/* css galeria */
#reorder-helper{margin: 18px 10px;padding: 10px;}
.light_box {
	background: #efefef;
	padding: 20px;
	margin: 10px 0;
	text-align: center;
	font-size: 1.2em;
}
.gallery{ width:100%; float:left; margin-top:10px;padding-left:5px;}
.gallery ul{ margin:0; padding:0; list-style-type:none;}
.gallery ul li{ padding:7px; border:2px solid #ccc; float:left; margin:10px 7px; background:none; width:auto; height:auto;}
/* NOTICE */
.notice, .notice a{ color: #fff !important; }
.notice { z-index: 8888; }
.notice a { font-weight: bold; }
.notice_error { background: #E46360; }
.notice_success { background: #657E3F; }
/* style 6 */
.inputimg + label {
	color: #ec9b1b;
}
.inputimg:focus + label,
.inputimg.has-focus + label,
.inputimg + label:hover {
	color: #d47e03;
}
.inputimg + label figure {
	width: 100px;
	height: 135px;
	background-color: #ec9b1b;
	display: block;
	position: relative;
	padding: 30px;
	margin: 0 auto 10px;
}
.inputimg:focus + label figure,
.inputimg.has-focus + label figure,
.inputimg + label:hover figure {
	background-color: #d47e03;
}
.inputimg + label figure::before,
.inputimg + label figure::after {
	width: 0;
	height: 0;
	content: '';
	position: absolute;
	top: 0;
	right: 0;
}
.inputimg + label figure::before {
	border-top: 20px solid #05354e;
	border-left: 20px solid transparent;
}
.inputimg + label figure::after {
	border-bottom: 20px solid #d47e03;
	border-right: 20px solid transparent;
}
.inputimg:focus + label figure::after,
.inputimg.has-focus + label figure::after,
.inputimg + label:hover figure::after {
	border-bottom-color: #ec9b1b;
}
.inputimg + label svg {
	width: 100%;
	height: 100%;
	fill: #f1e5e6;
}
</style>
<?php




?>
{!!Form::model($articulo,['route'=>['articulos.update',$articulo],'method'=>'PUT','files' => true,'id'=>'my-dropzone'])!!}
<div class="row" style="background: #f4f4f4;padding: 18px;border-radius: 8px;border: 1px solid #e7eaec;">
	<div class="col-md-12">
		{{Session::get('message')}}
	</div>
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-7">
				<div class="form-group">
					{!! Form::label('titulo','Título') !!}
					{!! Form::text('titulo',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
				</div>
			</div>
			<div class="col-md-5" style="padding-top: 16px;">
				<div class="form-group" style="padding: 6px;" >
					<input type="checkbox" data-on="Destacar" data-off="No Destacar"  data-toggle="toggle" name="destacado" id="destacado"
					@if($articulo->destacado == "1")
					checked
					@endif
					>
					<input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="activado" id="activado" type="checkbox"
					@if($articulo->activado == "1")
					checked
					@endif
					>
					<input data-toggle="toggle" data-on="Redes Sociales" data-off="Redes Sociales" name="publicar" id="publicar" type="checkbox">
				</div>
			</div>
		</div>
		<div = class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('categoria','Categoria') !!}
					{!! Form::select('id_categoria',$categorias, null,[ 'class' =>'form-control' , 'list' =>'listas','id' =>'categorias'])!!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('tags[]', 'Tags o Etiquetas') !!}
					{!! Form::select('tags[]', $tags, $mis_Tags,['class'=>'form-control selective','multiple'=>'multiple']) !!}
				</div>
			</div>

		</div>

		<div id ="calendario"
		@if ($articulo->id_categoria!=19)
		style="display:none;"
		@endif
		>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('fechainicio','Fecha y hora de Inicio') !!}
					<div class='input-group date' id='datetimeinicio'>
						{!! Form::text('fechainicio',null,['class' =>'form-control'])!!}

						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					{!! Form::label('fechafin','Fecha y hora de Finalización') !!}
					<div class='input-group date' id='datetimefin'>
						{!! Form::text('fechafin',null,['class' =>'form-control'])!!}
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('lugar','Lugar del Evento ( 50 Caracteres máximo)') !!}
					{!! Form::text('lugar',null,['class' =>'form-control', 'placeholder' =>'Lugar del Evento','maxlength'=>'50'])!!}
				</div>

			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('prioridad','Prioridad') !!}
					{!! Form::select('id_prioridad',$prioridad, null,[ 'class' =>'form-control' , 'listprop' =>'listasprio','id' =>'prioridad'])!!}
				</div>
			</div>
		</div>

	</div>

	<div class="form-group">
		{!! Form::label('descripcionredes','Comentario en Redes Sociales ( 200 caracteres máximo)') !!}
		{!! Form::text('descripcionredes',null,['class' =>'form-control', 'placeholder' =>'Comentario en Redes Sociales','maxlength'=>'200'])!!}
	</div>
	<div class="form-group">
		{!! Form::label('texto','Contenido') !!}
		{!! Form::textarea('texto',null,['class' =>'form-control summernote ckeditor', 'placeholder' =>'Contenido del artículo','required'])!!}
	</div>
	<div class="form-group" style="background: #e8e7e7;border: 1px solid #D0CBCB;padding: 6px;">
		<center>
			{!! Form::label('imagen','Galeria de Imagen') !!}
		</center>
		{!! Form::open(array('url'=>'upload','method'=>'POST', 'id'=>'frmA')) !!}
		<meta name="_token" content="{!! csrf_token() !!}"/>
		<div class="row">
			<div class="col-md-3">
				<div class="btn-group" style="width: 100%;">
					<center>
						<label title="Upload image file" for="inputImage" class="btn btn-primary">
							{!! Form::label('imagen','Imagen principal',['class'=>'control-label']) !!}
							{!! Form::file('imagenprincipal',['id'=>'imagenprincipal','accept'=>'image/*','style'=>'display:none;'])!!}


						</label>
					</center>
				</div>
				<label for="imagenprincipal">
					@if($articulo->path == NULL)
					<img id="imgprincipal" src="{{asset('/assets/images/no-imagen.jpg')}}"
					style ="width: 100%; object-fit: cover!important; border: 1px solid #1ab394;">
					@else
					<img id="imgprincipal" src="{{asset('local/public/images')}}/{{$articulo->path}}"
					style ="width: 100%; object-fit: cover!important; border: 1px solid #1ab394;">


					@endif
				</label>
			</div>
			<div class="col-md-9">
				<div class="gallery">
					<ul class="reorder_ul reorder-photos-list" id="nuevalista">
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-1" id="file-1" class="inputfile inputimg"  style='display:none;' accept="image/*" />
								<label for="file-1">
									<img id="imgfile-1" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[0]->path.'" name="'.$imgs2[0]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?> "
									><a class="btn btn-danger deleteImage" id="elim1"  onclick="eliminarElemento('imgfile-1','elim1');" style="<?php try {if ($imgs2[0]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-2" id="file-2" class="inputfile inputimg"  style='display:none;' accept="image/*" />
								<label for="file-2">
									<img id="imgfile-2" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[1]->path.'" name="'.$imgs2[1]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?> "
									><a class="btn btn-danger deleteImage" id="elim2"  onclick="eliminarElemento('imgfile-2','elim2');" style="<?php try {if ($imgs2[1]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-3" id="file-3" class="inputfile inputimg"  style='display:none;' accept="image/*" />
								<label for="file-3">
									<img id="imgfile-3" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[2]->path.'" name="'.$imgs2[2]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?> "
									><a class="btn btn-danger deleteImage" id="elim3"  onclick="eliminarElemento('imgfile-3','elim3');" style="<?php try {if ($imgs2[2]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-4" id="file-4" class="inputfile inputimg"  style='display:none;' accept="image/*" />
								<label for="file-4">
									<img id="imgfile-4" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[3]->path.'" name="'.$imgs2[3]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>" >
									<a class="btn btn-danger deleteImage" id="elim4"  onclick="eliminarElemento('imgfile-4','elim4');" style="<?php try {if ($imgs2[3]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-5" id="file-5" class="inputfile inputimg"  style='display:none;' accept="image/*" />
								<label for="file-5">
									<img id="imgfile-5" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[4]->path.'" name="'.$imgs2[4]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>" ><a class="btn btn-danger deleteImage" id="elim5"  onclick="eliminarElemento('imgfile-5','elim5');" style="<?php try {if ($imgs2[4]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-6" id="file-6" class="inputfile inputimg"  style='display:none;' accept="image/*" />
								<label for="file-6">
									<img id="imgfile-6" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[5]->path.'" name="'.$imgs2[5]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>"><a class="btn btn-danger deleteImage" id="elim6"  onclick="eliminarElemento('imgfile-6','elim6');" style="<?php try {if ($imgs2[5]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-7" id="file-7" class="inputfile inputimg"  style='display:none;'  accept="image/*" />
								<label for="file-7">
									<img id="imgfile-7" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[6]->path.'" name="'.$imgs2[6]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>"><a class="btn btn-danger deleteImage" id="elim7"  onclick="eliminarElemento('imgfile-7','elim7');" style="<?php try {if ($imgs2[6]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-8" id="file-8" class="inputfile inputimg"  style='display:none;'  accept="image/*" />
								<label for="file-8">
									<img id="imgfile-8" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[7]->path.'" name="'.$imgs2[7]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>" ><a class="btn btn-danger deleteImage" id="elim8"  onclick="eliminarElemento('imgfile-8','elim8');" style="<?php try {if ($imgs2[7]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-9" id="file-9" class="inputfile inputimg"  style='display:none;'  accept="image/*" />
								<label for="file-9">
									<img id="imgfile-9" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[8]->path.'" name="'.$imgs2[8]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>" ><a class="btn btn-danger deleteImage" id="elim9"  onclick="eliminarElemento('imgfile-9','elim9');" style="<?php try {if ($imgs2[8]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
						<li>
							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="file-10" id="file-10" class="inputfile inputimg"  style='display:none;'  accept="image/*" />
								<label for="file-10">
									<img id="imgfile-10" style="height: 100px;padding: 6px;" src="<?php
									try {
										echo(asset('local/public/galeria').'/'.$imgs2[9]->path.'" name="'.$imgs2[9]->path);
									} catch (Exception $e) {
										echo(asset('/assets/images/no-imagen.jpg'));
									}?>" ><a class="btn btn-danger deleteImage" id="elim10"  onclick="eliminarElemento('imgfile-10','elim10');" style="<?php try {if ($imgs2[9]->path != null) {}} catch (Exception $e) {echo("display:none;");}?>"><i class="fa fa-trash-o"></i></a>
								</label>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-12" id="diverror"></div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;">
				<div class="btn-group" style="width: 100%;">
					<center>
						<p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Lista de Documentos</p>
						<ul class="reorder-documentos-list reorder_ul list-group" id="listadocs" name="documentos">
							@if($docs != NULL)
							@foreach ($docs as $doc)
							<li id="{{$doc->path}}" class="list-group-item active">
								<span onclick="eliminardoc(this)" title="Eliminar Documento">
									<img  style="height: 20px;" src="{{asset('/assets/images/elidoc.png')}}">
								</span>
								&nbsp;
								<a href="javascript:void(0);" style="float:none; color: white;class="image_link" >
									{{$doc->titulo}}
								</a>
								<span title="Visualizar Documento">
									&nbsp;
								<a href="/local/public/doc/{{$doc->path}}" target="_blank" >
									<img  style="height: 20px;" src="/assets/images/verdoc.png">
								</span></a>
							</li>
							@endforeach
							@endif

						</ul>
						<div id="subiendodoc" style="display:none;">Subiendo Documento Espere...</div>

							<div id="barra"  class="progress progress-striped active" style="width: 90%;display:none;">

								<div id="porcentaje" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" role="progressbar" class="progress-bar progress-bar-success" >


								</div>
							</div>
								<button  class="btn btn-danger" type="button" id="btncancelardoc" style="display: none;">Cancelar</button>
							<label id="botonsubirdoc" title="Subir Documento" for="inputImage" class="btn btn-primary">


							<a href="javascript:void(0);" style="float:none;" class="image_link">
								<input type="file" name="doc" id="doc" class="inputfile inputimg"  style='display:none;'  />
								<label for="doc"  style="color: white;">
									<img id="imgfile-1" style="height: 30px;" src="{{asset('/assets/images/newdoc.png')}}" >
									Agregar Documento
								</label>
							</a>
						</label>
					</center>
				</div>
				<select name="rutadocs[]" id="rutadocs" multiple="multiple" style='display:none;' >
				</select>
				<select name="titulodocs[]" id="titulodocs" multiple="multiple" style='display:none;' >
				</select>
				<select name="sobrasdocs[]" id="sobrasdocs" multiple="multiple" style='display:none;' >
				</select>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group" style=" background: #e8e7e7; border: 1px solid #D0CBCB;padding: 6px;">
				<center>
					<p style="text-align: center; font-size: 15px; font-weight: 700;margin-top: 20px;">Lista de Videos</p>


					<ul class="reorder-videos-list reorder_ul list-group" id="listaDesordenada" name="videos">
						@if($mis_videos != NULL)
						@foreach ($mis_videos as $vide)



						<li id="{{$vide->path}}" class="list-group-item active">
							<span onclick="eliminar(this)" title="Eliminar Video de la Lista">
								<img  style="height: 25px;" src="{{asset('/assets/images/delvideo.png')}}" >
							</span>
							&nbsp;
								<a href="javascript:void(0);" style="float:none; color: white;" class="image_link">
									{{$vide->titulo}}
								</a>
								<span onclick="modificamodal('{{$vide->path}}')" title="Reproducir Video">
									<a href="" data-target="#modal-vervideo" data-toggle="modal">
										&nbsp;
										<img  style="height: 25px;" src="{{asset('/assets/images/play.png')}}">
									</a>
								</span>



						</li>
						@endforeach
						@endif
					</ul>

				</center>

				<div class="input-group">

					{!! Form::text('video',null,['class' =>'form-control summernote ckeditor', 'id'=>'nuevo_video' ,'placeholder' =>'URL YouTube/Facebook/Vimeo de video'])!!}<span class="input-group-btn"><button  class="btn btn-primary"  type="button" id="btnagregavideo" align="center" onclick="return add_li()" ><img  style="height: 20px;" src="{{asset('/assets/images/addvideo.png')}}" > Agregar Video</button></span>
				</div>

				<select name="videos[]" id="videos" multiple="multiple" style='display:none;'>
				</select>
				<select name="titulovideo[]" id="titulovideo" multiple="multiple" style='display:none;'>
				</select>
				<select name="sobrasvideos[]" id="sobrasvideos" multiple="multiple" style='display:none;' ></select>


				<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-vervideo"  >



					<div class="modal-dialog">
						<div class="modal-content">


							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true"></span>
								</button>
								<h4 class="modal-title">Visualizar Video</h4>
							</div>
							<div class="modal-body">

								<div id="reproductor" >

								</div>

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	{!! Form::close() !!}

	<select name="ruta[]" id="ruta" multiple="multiple" style='display:none;' >
	</select>
	<select name="sobras[]" id="sobras" multiple="multiple" style='display:none;' >
	</select>

	<div class="form-group">
		{!! Form::hidden('id_usuario', Auth::user()->id, null,['class' =>'form-control'])!!}
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('latitud','Latitud') !!}
						{!! Form::text('latitud',null,['class' =>'form-control', 'placeholder' =>'Latitud'])!!}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{!! Form::label('longitud','Longitud') !!}
						{!! Form::text('longitud',null,['class' =>'form-control', 'placeholder' =>'Longitud'])!!}


					</div>
				</div>

				<div class="col-md-12">


					<h3>Mapa</h3>
					<div id="map"></div>
					<br>

				</div>
			</div>
			<div class="col-md-6">
				<div class="col-md-12">
					<div class="form-group">
						{!! Form::label('seo_descripcion','SEO Descripción') !!}

						<textarea class="form-control" placeholder="SEO Descripción" style="resize: none" rows="5" name="seo_descripcion" cols="50" id="seo_descripcion">{{$articulo->seo_descripcion}}</textarea>

					</div>
				</div>


				<div class="col-md-12">
					<div class="form-group">
						{!! Form::label('seo_keyword','SEO KEYWORD') !!}

						<input class="form-control" placeholder="Seo Keyword" name="seo_keyword" type="text" id="seo_keyword" value="{{$articulo->seo_keyword}}">
					</div>
				</div>
			</div>
		</div>
	</div>
	<p>Campos Extras</p>

	<div class="row">

		<div class="col-lg-12">
			<div class="ibox-content">
				<div class="panel-body">
					<div class="panel-group" id="accordion">
						<?php
						foreach ($grupos as $grupo) {
							echo '<div class="panel panel-default">';

							echo '<a data-toggle="collapse" data-parent="#accordion" href="#'.$grupo->id.'">
							<div class="panel-heading">
							<h5 class="panel-title"> '
							.$grupo->descripcion.
							'</h5>
							</div> </a>';
							echo '<div id="'.$grupo->id.'" class="panel-collapse collapse">
							<div class="panel-body">';

							$totalextra = count($camposextras);
							foreach($camposextrasid as $key  => $value)
							{
								if(isset($camposextrasguardados[$value])){
									$valorcampos = $camposextrasguardados[$value];
								}else {
									$valorcampos = null;
								}
								if ($grupo->id == $camposgruposid[$key]) {
									echo '<div class="row" style="margin-bottom: 6px;">';
									echo '<div class="form-group">'.
									Form::label($camposextras[$key],$camposextrasdesc[$key].':',array("class"=>"col-lg-4 control-label"));
									echo '<div class="col-lg-8">';
									if($camposextrastipo[$key]=='date'){
										echo Form::input('date',$camposextras[$key], $fecha, ['class' => 'form-control', 'placeholder' => 'Date']);
									}elseif($camposextrastipo[$key]=='select'){
										$camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$key] . ';';
										eval($camposeleccion);
										echo Form::select($camposextras[$key],$camposeleccion,$valorcampos, array('class' => 'form-control','placeholder'=>'Ingrese '.$camposextrasdesc[$key]));

									}elseif($camposextrastipo[$key]=='textarea'){
										echo Form::textarea($camposextras[$key], $valorcampos, array('maxlength'=>'500','class' => 'form-control','placeholder'=>'Ingrese '.$camposextrasdesc[$key]));
									}elseif($camposextrastipo[$key]=='multipleselect'){
													               // echo $valorcampos;
										$camposeleccion=  '$camposeleccion='.'array'. $camposextrasvalor[$key] . ';';
										eval($camposeleccion);
										echo Form::select($camposextras[$key].'[]',$camposeleccion,$valorcampos, array('multiple class' => 'select2','style' => 'width:100%','placeholder'=>'Ingrese '.$camposextras[$key]));
									}else
									echo Form::text($camposextras[$key], $valorcampos, array('class' => 'form-control','placeholder'=>'Ingrese valor'));
									echo '</div>';
									echo '</div>';
									echo '</div>';
								}
							}
							echo '</div>
							</div>
							</div>';
						}

						?>




					</div>
				</div>
			</div>
		</div>

	</div>

	<p style="text-align: center;">
		<input type="checkbox" id="inicio" name="inicio" style="display:none;">
		<button class="btn btn-primary" type="submit" onclick="document.getElementById('inicio').checked=true;"><i class="fa fa-floppy-o"></i> Actualizar
		</button>
		<button class="btn btn-primary" type="submit" onclick="document.getElementById('inicio').checked=false;"><i class="fa fa-floppy-o"></i> Actualizar y Cerrar
		</button>
		<a class="btn btn-primary dim" href="{{ URL::asset('articulos')}}"><i class="fa fa-chevron-left"></i> Atrás
		</a>
	</p>
</div>

</div>
{!! Form::close() !!}

<br>

<script>
	function initMap() {

		if ("{{$articulo->latitud}}" == "" || "{{$articulo->longitud}}" == "")
		{
			var latitud = -0.9660858726870881;
			var longitud = -80.70762632485355;
			var zoommap = 14;
		}
		else{
			var latitud = '{{$articulo->latitud}}';
			var latitud = parseFloat(latitud);
			var longitud = '{{$articulo->longitud}}';
			var longitud = parseFloat(longitud);
			var zoommap = 20;
			$("#latitud").val('{{$articulo->latitud}}');
			$("#longitud").val('{{$articulo->longitud}}');
		}

		var uluru = {lat: latitud, lng: longitud};

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 14,
			center: uluru
		});
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
			draggable:true,
			title:"Domicilio Estudiante!"
		});
		// To add the marker to the map, call setMap();
		marker.setMap(map);
		marker.bindTo('position', map, 'center');
		google.maps.event.addListener(map, "center_changed", function() {
			var markerLatLng = marker.getPosition();
			$("#latitud").val(markerLatLng.lat());
			$("#longitud").val(markerLatLng.lng());
		});
		cargado = 1;
	}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG-pn80TS8oXf8e0deZ5jR48o6BvE1D_Y&callback=initMap">
</script>


<script>
	$('.selective').selectize({
		plugins: ['remove_button'],
		delimiter: ',',
		persist: false,
		create: function(input) {
			return {
				value: input,
				text: input
			}
		}
	});
	$( document ).ready(function() {
		$(function () {
			var ahora=moment();
			$('#datetimeinicio').datetimepicker({

				locale: 'es',
				format:'YYYY/MM/DD HH:mm',
			});
			$('#datetimefin').datetimepicker({

			useCurrent: false, //Important! See issue #1075
			locale: 'es',
			format:'YYYY/MM/DD HH:mm',

		});
			$("#datetimeinicio").on("dp.change", function (e) {
				$('#datetimefin').data("DateTimePicker").minDate(e.date);
			});
			$("#datetimefin").on("dp.change", function (e) {
				$('#datetimeinicio').data("DateTimePicker").maxDate(e.date);
			});
		});

	});
</script>
@include('scripts.articuloscript')
@endsection
