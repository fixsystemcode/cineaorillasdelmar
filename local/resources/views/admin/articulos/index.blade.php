@extends('layouts.backend')

@section('titulo')
  Artículos
@stop

@section('contenido')

@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>


@endif


<br>

<a class="btn btn-primary" href="{{ URL::route('articulos.create') }}">Nuevo</a>
<br>
@if (count($articulos))

<br>
    <div class="row">
      <div class="col-md-12">


        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">

          <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
              <th>#</th>
              <th>ID</th>
              <th>Título</th>
              <th style="width:30%;">Contenido</th>
              <th>Autor</th>
              <th>Activado</th>
              <th>Categoria</th>
              <th>Imagen</th>
              <th>Acciones</th>
            </thead>

            <tbody>
              <tr>
                <?php $s = 0 ; ?>
                @foreach ($articulos as $articulo)
                <td>{{$articulo->id}}</td>
                <td> <?php $s = $s + 1; echo $s; ?></td>
                <td>{{$articulo->titulo}}</td>
                <td><?php $ser1 = substr($articulo->texto,0,200); echo strip_tags($ser1); ?></td>
                <td>{{$articulo->nombres}}</td>
                <td>
                <?php if ($articulo->activado == true) {
                  echo "SI";
                }else{
                  echo "NO";
                }
                 ?>

                </td>
                <td>{{$articulo->categoria}}</td>
                @if(empty($articulo->path))
                  <td><img src="{{asset('assets/images')}}/no-imagen.jpg" onerror=" this.src = 'local/public/images/articulo.png';" style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                @else
                  <td>

                     <div class="lightBoxGallery">
                            <a href="{{asset('local/public/images')}}/{{$articulo->path}}" title="Image from Unsplash" data-gallery="">
                              <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/images')}}/{{$articulo->path}}" onerror="this.src = 'local/public/images/articulo.png';" >
                            </a>

                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>

                  </td>
                @endif
                <td><a class="btn btn-success" href="{{route('articulos.edit', $articulo->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="{{route('articulos.destroy', $articulo->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button"><i class="fa fa-trash-o"></i></a>
                </td>

              </tr>
              @endforeach
            </tbody>
          </table>



      </div>

    </div>
      </div>

    </div>
    <script>
      function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
        .replace( /\n/g, ' ' )
        .replace( /[áàäâ]/g, 'a' )
        .replace( /[éèëê]/g, 'e' )
        .replace( /[íìïî]/g, 'i' )
        .replace( /[óòöô]/g, 'o' )
        .replace( /[úùüû]/g, 'u' ):
        data;

        jQuery.extend( jQuery.fn.dataTableExt.oSort,
        {
          "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
          "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
          return accents_supr(data);
        }
      }


      jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
        .replace( /έ/g, 'ε' )
        .replace( /[ύϋΰ]/g, 'υ' )
        .replace( /ό/g, 'ο' )
        .replace( /ώ/g, 'ω' )
        .replace( /ά/g, 'α' )
        .replace( /[ίϊΐ]/g, 'ι' )
        .replace( /ή/g, 'η' )
        .replace( /\n/g, ' ' )
        .replace( /á/g, 'a' )
        .replace( /é/g, 'e' )
        .replace( /í/g, 'i' )
        .replace( /ó/g, 'o' )
        .replace( /ú/g, 'u' )
        .replace( /ê/g, 'e' )
        .replace( /î/g, 'i' )
        .replace( /ô/g, 'o' )
        .replace( /è/g, 'e' )
        .replace( /ï/g, 'i' )
        .replace( /ü/g, 'u' )
        .replace( /ã/g, 'a' )
        .replace( /õ/g, 'o' )
        .replace( /ç/g, 'c' )
        .replace( /ì/g, 'i' ) :
        data;
      };
    </script>

    @else
    No hay Artículos
@endif





@endsection