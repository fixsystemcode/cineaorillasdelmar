@extends('layouts.backend')



@section('titulo')
SOLICITUDES DE INGRESO AL SISTEMA
@stop

@section('contenido')

@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>


@endif


<br>
 
<a class="btn btn-primary" href="{{ URL::route('articulos.create') }}">Nuevo</a>
<br>
@if (count($articulos))

<br>
    <div class="row">
      <div class="col-md-12">     


        <div class="box">
           
            <!-- /.box-header -->
            <div class="box-body">   

          <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
              <th>#</th>
              <th>ID</th>
              <th>Título</th>
              <th style="width:30%;">Contenido</th>
              <th>Autor</th>
              <th>Categoria</th>
              <th>Imagen</th>
              <th>Acciones</th>
            </thead>

            <tbody>
              <tr>
                <?php $s = 0 ; ?>
                @foreach ($articulos as $articulo)
                <td>{{$articulo->id}}</td>
                <td> <?php $s = $s + 1; echo $s; ?></td>
                <td>{{$articulo->titulo}}</td>
                <td><?php $ser1 = substr($articulo->texto,0,200); echo strip_tags($ser1); ?></td>
                <td>{{$articulo->nombres}}</td>
                <td>{{$articulo->categoria}}</td>
                @if(empty($articulo->path))
                  <td><img src="{{asset('assets/images')}}/no-imagen.jpg"  style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                @else
                  <td>

                     <div class="lightBoxGallery">
                            <a href="{{asset('local/public/images')}}/{{$articulo->path}}" title="Image from Unsplash" data-gallery="">
                              <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/images')}}/{{$articulo->path}}">
                            </a>
                           
                 

                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>
                  
                  </td>
                @endif
                <td>

                {!! Form::open(array('method' => 'GET', 'route' => array('hab_articulo', $articulo->id))) !!}                       
                         {!! Form::submit('Activar', array('class' => 'btn btn-primary')) !!}
                       {!! Form::close() !!}
                <a class="btn btn-success" href="{{route('articulos.edit', $articulo->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="{{route('articulos.destroy', $articulo->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button"><i class="fa fa-trash-o"></i></a>
                
                </td>

              </tr>
              @endforeach
            </tbody>
          </table>

         

      </div>
   
    </div>
      </div>
   
    </div>

@else
    No hay Artículos
@endif





@endsection