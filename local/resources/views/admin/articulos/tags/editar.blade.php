@extends('layouts.backend')


@section('title','Editar articulo: '. $tags->title)

@section('contenido')

    {!!Form::model($tags,['route'=>['tags.update',$tags],'method'=>'PUT','files' => true])!!}

	  			
			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							{!! Form::label('tag','Título') !!}
							{!! Form::text('tag',null,['class' =>'form-control', 'placeholder' =>'Título del artículo','required'])!!}
						</div>
						<div class="form-group">
					          {!! Form::label('imagen','Imagen') !!}
					          {!! Form::file('path')!!}
					    </div>
					</div>

			</div>	

		
	{!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}<br>

	

@endsection