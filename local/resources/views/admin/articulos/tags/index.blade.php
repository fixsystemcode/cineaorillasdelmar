@extends('layouts.backend')


@section('contenido')

@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>


@endif

<h1>Articulos</h1>
<a class="btn btn-primary" href="{{ URL::route('tags.create') }}">Nuevo</a>
<br>
@if (count($tags))

<br>
    <div class="row">
      <div class="col-md-12">     

          <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
              <th>#</th>
              <th>Tags</th>
              <th>Imagen</th>
              <th>Acciones</th>
            </thead>

            <tbody>
              <tr>
                @foreach ($tags as $tag)
                <td>{{$tag->id}}</td>
                <td>{{$tag->tag}}</td>
                 @if(empty($tag->path))
                <td><img src="{{asset('local/public/images')}}/no-imagen.jpg"  style ="width: 100px"></td>
                @else
                <td><img src="{{asset('local/public/images')}}/{{$tag->path}}"  style ="width: 100px"></td>
                @endif
                <td><a class="btn btn-success" href="{{route('tags.edit', $tag->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="{{route('tags.destroy', $tag->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button"><i class="fa fa-trash-o"></i></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

         

      </div>
   
    </div>

@else
    No hay Artículos
@endif





@endsection