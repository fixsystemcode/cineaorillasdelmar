@extends('layouts.backend')

 

@section('titulo')
Imágenes
@stop

@section('contenido')

@include('alerts.success')

 


@if (count($asistentes))

<!--<button id="botonExcel">EXPORTAR</button>-->
<button id="botonExce" onclick="exportTableToExcel('tablaexportar')">EXPORTAR</button>

<div id="impresionExcelDiv" name="impresionExcelDiv" style="display: none;">
    <table  id="tablaexportar" class="table" >
        <thead>
            <tr>
                
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>EMAIL</th>
                <th>Ciudad</th>
                <th>Telefono</th>
                <th>Institucion</th>
                <th>Cargo</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($asistentesex as $asistentesex)
                <tr>
                <td>{{ $asistentesex->nombres }}</td>
                <td>{{ $asistentesex->apellidos }}</td>
                <td>{{ $asistentesex->email }}</td>
                <td>{{ $asistentesex->ciudad }}</td>
                <td>{{ $asistentesex->telefono }}</td>
                <td>{{ $asistentesex->institucion }}</td>
                <td>{{ $asistentesex->cargo }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
 </div>

    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>EMAIL</th>
                <th>Ciudad</th>
                <th>Telefono</th>
                <th>Institucion</th>
                <th>Cargo</th>
                <!--<th>ACCIONES</th>-->
            </tr>
        </thead>

        <tbody>
            @foreach ($asistentes as $asistentes)
                <tr>
                <td>{{ $asistentes->nombres }}</td>
                <td>{{ $asistentes->apellidos }}</td>
                <td>{{ $asistentes->email }}</td>
                <td>{{ $asistentes->ciudad }}</td>
                <td>{{ $asistentes->telefono }}</td>
                <td>{{ $asistentes->institucion }}</td>
                <td>{{ $asistentes->cargo }}</td>
                <!--<td>
                    <a class="btn btn-success" href="{{route('WebinarRegistro.edit', $asistentes->id)}}" role="button">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                    {!! Form::open(array('method' => 'DELETE', 'route' => array('WebinarRegistro.destroy', $webi->id))) !!}
                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                    {!! Form::close() !!}
                </td>-->
                </tr>
            @endforeach
              
        </tbody>
      
    </table>
    
    
    
@else
    No hay ningún asistente registrado en este Webinar
@endif

@stop
@section('script')
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };

    function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}


</script>
<script>
    

    function exportarexcelFuncion() {
        $(".exportarexcelnuevo").table2excel({
          name: "Worksheet Name",
          filename: "SomeFile.xls"
        });
      }
</script>
<script>
    
    $(document).ready(() => {
        $("#botonExcel").click(function() {
            $("#tablaexportar").table2excel({
                exclude: ".table-striped",
                name: "Worksheet name",
                filename: "Asistentes",
                fileext: ".xls"

            });
        });
    });

</script>
@endsection