@extends('layouts.backend')

@section('titulo')
Nuevo Ponente  
@stop


@section('contenido')

@include('alerts.request')

 

 {!! Form::open(['route' => 'ponentes.store', 'method' => 'POST','files'=>true]) !!}
    <div class="form-group">
          {!! Form::label('foto','Foto') !!}
          {!! Form::file('foto')!!}
    </div>  
    <div class="row">
      <div class="col-md-6">  
        <div class="form-group">
                {!! Form::label('apellidos', 'Apellidos:') !!}
                {!! Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese Apellidos','required']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('nombres', 'Nombres:') !!}
                {!! Form::text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese Nombres','required']) !!}
        </div>
      </div>
    </div>
    <div class="form-group">
            {!! Form::label('profesion', 'Cargo/Profesión:') !!}
            {!! Form::text('profesion',null,['class'=>'form-control','placeholder'=>'Ingrese la profesion o cargo del ponente','required']) !!}
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_fb', 'Link de Facebook:') !!}
                {!! Form::text('link_fb',null,['class'=>'form-control','placeholder'=>'Link de Facebook']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_tw', 'Link de Twitter:') !!}
                {!! Form::text('link_tw',null,['class'=>'form-control','placeholder'=>'Link de Twitter']) !!}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_inst', 'Link de Instagram:') !!}
                {!! Form::text('link_inst',null,['class'=>'form-control','placeholder'=>'Link de Instagram']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_linke', 'Link de Linkedin:') !!}
                {!! Form::text('link_linke',null,['class'=>'form-control','placeholder'=>'Link de Linkedin']) !!}
        </div>
      </div>
    </div>
    
    
    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}


            
        
 
{!! Form::close() !!}



@stop