@extends('layouts.backend')

@section('titulo')
Editar Ponente
@stop

@section('contenido')

@include('alerts.request')

 


{!! Form::model($ponentes, array('method' => 'PATCH', 'route' => array('ponentes.update', $ponentes->id) , 'files' => true)) !!}
    
    <div class="row">
      <div class="col-md-6">
          {!! Form::label('foto','Foto') !!}
          {!! Form::file('foto')!!}
      </div>
        <div class="col-md-6">
            <img src="{{asset('local/public/images')}}/{{$ponentes->foto}}" onerror="if (this.src != '../../local/public/images/avatar.png') this.src = '../../local/public/images/avatar.png';"  style ="width: 100px">
        </div> 
     </div>   
      
    <div class="row">
      <div class="col-md-6">  
        <div class="form-group">
                {!! Form::label('apellidos', 'Apellidos:') !!}
                {!! Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese Apellidos','required']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('nombres', 'Nombres:') !!}
                {!! Form::text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese Nombres','required']) !!}
        </div>
      </div>
    </div>
    <div class="form-group">
            {!! Form::label('profesion', 'Cargo/Profesión:') !!}
            {!! Form::text('profesion',null,['class'=>'form-control','placeholder'=>'Ingrese la profesion o cargo del ponente','required']) !!}
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_fb', 'Link de Facebook:') !!}
                {!! Form::text('link_fb',null,['class'=>'form-control','placeholder'=>'Link de Facebook','required']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_tw', 'Link de Twitter:') !!}
                {!! Form::text('link_tw',null,['class'=>'form-control','placeholder'=>'Link de Twitter','required']) !!}
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_inst', 'Link de Instagram:') !!}
                {!! Form::text('link_inst',null,['class'=>'form-control','placeholder'=>'Link de Instagram','required']) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
                {!! Form::label('link_linke', 'Link de Linkedin:') !!}
                {!! Form::text('link_linke',null,['class'=>'form-control','placeholder'=>'Link de Linkedin','required']) !!}
        </div>
      </div>
    </div>
      


   

    {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}



{!! Form::close() !!}



@stop