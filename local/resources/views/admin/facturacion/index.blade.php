@extends('layouts.backend')

@section('titulo')
  Artículos
@stop

@section('contenido')
<?php
    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }
    ?>

@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>


@endif


<br>

<br>
@if (count($registros))

<br>
    <div class="row">
      <div class="col-md-12">


        <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <div style="text-align: right;">
                <button id="Btnxls" style="padding: 10px 15px;background-color: #2287DB;border: none;margin-bottom: 15px;color: #fff">Descargar registros</button>
              </div>

          <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
            <thead>
              <th>ID</th>
              <th>Unidad Educativa</th>
              <th>RUC</th>
              <th>Telefono</th>
              <th>Dirección</th>
              <th>Razón Social</th>
              <th>Valor</th>
              <th>Fecha de  registro</th>
            </thead>

            <tbody>
              <tr>
                <?php $s = 0 ; ?>
                @foreach ($registros as $articulo)
                <td>{{$articulo->id}}</td>
                <td>{{$articulo->ue}}</td>
                <td>{{$articulo->ruc}}</td>
                <td>{{$articulo->telefono}}</td>
                <td>{{$articulo->direccion}}</td>
                <td>{{$articulo->razon}}</td>
                <td>{{$articulo->valor}}</td>
                <td><?php echo date('d', strtotime($articulo->created_at))." ".mesES($articulo->created_at).", ".date('Y', strtotime($articulo->created_at))." / ".date('G : i', strtotime($articulo->created_at)); ?></td>
              </tr>
              @endforeach
            </tbody>
          </table>



      </div>

      <div style="display: none;"><table id="datosfact"><thead><th>ID</th><th>Unidad Educativa</th><th>RUC</th><th>Telefono</th><th>Dirección</th><th>Razón Social</th><th>Valor</th><th>Fecha de  registro</th>
</thead><tbody><tr><?php $s = 0 ; ?>@foreach ($registros as $articulo)<td>{{$articulo->id}}</td><td>{{$articulo->ue}}</td><td>{{$articulo->ruc}}</td><td>{{$articulo->telefono}}</td><td>{{$articulo->direccion}}</td><td>{{$articulo->razon}}</td><td>{{$articulo->valor}}</td><td><?php echo date('d', strtotime($articulo->created_at))." ".mesES($articulo->created_at).", ".date('Y', strtotime($articulo->created_at))." / ".date('G : i', strtotime($articulo->created_at)); ?></td></tr>@endforeach</tbody></table></div>
    </div>
      </div>

    </div>
    <script>
      function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
        .replace( /\n/g, ' ' )
        .replace( /[áàäâ]/g, 'a' )
        .replace( /[éèëê]/g, 'e' )
        .replace( /[íìïî]/g, 'i' )
        .replace( /[óòöô]/g, 'o' )
        .replace( /[úùüû]/g, 'u' ):
        data;

        jQuery.extend( jQuery.fn.dataTableExt.oSort,
        {
          "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
          "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
          return accents_supr(data);
        }
      }


      jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
        .replace( /έ/g, 'ε' )
        .replace( /[ύϋΰ]/g, 'υ' )
        .replace( /ό/g, 'ο' )
        .replace( /ώ/g, 'ω' )
        .replace( /ά/g, 'α' )
        .replace( /[ίϊΐ]/g, 'ι' )
        .replace( /ή/g, 'η' )
        .replace( /\n/g, ' ' )
        .replace( /á/g, 'a' )
        .replace( /é/g, 'e' )
        .replace( /í/g, 'i' )
        .replace( /ó/g, 'o' )
        .replace( /ú/g, 'u' )
        .replace( /ê/g, 'e' )
        .replace( /î/g, 'i' )
        .replace( /ô/g, 'o' )
        .replace( /è/g, 'e' )
        .replace( /ï/g, 'i' )
        .replace( /ü/g, 'u' )
        .replace( /ã/g, 'a' )
        .replace( /õ/g, 'o' )
        .replace( /ç/g, 'c' )
        .replace( /ì/g, 'i' ) :
        data;
      };
    </script>

    @else
    No hay Artículos
@endif





@endsection