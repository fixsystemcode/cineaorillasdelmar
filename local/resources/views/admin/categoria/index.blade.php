@extends('layouts.backend')

@section('titulo')
Categorías
@stop


@section('contenido')

@if(Session::has('message'))
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  {{Session::get('message')}}
</div>


@endif
 
<a class="btn btn-primary" href="{{ URL::route('categorias.create') }}">Nuevo</a>
<br>
@if ($categorias->count())

<br>
    <div class="row">
      <div class="col-md-12">  

<table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
  <thead>
  	<th>Nombre de categorías</th>
    <th>Destacado</th>
    <th>Imagen fondo</th>
    <th>Icono</th>
    <th>Acciones</th>
     
  </thead>

  <tbody>
  	<tr>
  		@foreach ($categorias as $categoria)
  		<td>{{$categoria->categoria}}</td>
      <td>{{$categoria->cat_destacado}}</td>
      @if(empty($categoria->path))
                  <td><img src="{{asset('assets/images')}}/no-imagen.jpg"  style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                @else
                  <td>

                     <div class="lightBoxGallery">
                            <a href="{{asset('local/public/images')}}/{{$categoria->path}}" title="Image from Unsplash" data-gallery="">
                              <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/images')}}/{{$categoria->path}}">
                            </a>

                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>
                  
                  </td>
                @endif
      @if(empty($categoria->icono))
                  <td><img src="{{asset('assets/images')}}/no-imagen.jpg"  style ="width: 50px; height: 50px!important;object-fit: cover!important;"></td>
                @else
                  <td>

                     <div class="lightBoxGallery">
                            <a href="{{asset('local/public/images')}}/{{$categoria->icono}}" title="Image from Unsplash" data-gallery="">
                              <img  style="width: 50px;height: 50px!important;"  src="{{asset('local/public/images')}}/{{$categoria->icono}}">
                            </a>
                           
                 

                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>
                  
                  </td>
                @endif
  		
  		<td><a class="btn btn-success" href="{{route('categorias.edit', $categoria->id)}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
  		    <a class="btn btn-danger" href="{{route('categorias.destroy', $categoria->id)}}" onclick="return confirm('Quiere borrar el registro?')" role="button"><i class="fa fa-trash-o"></i></a>
  		</td>

  	</tr>
  	@endforeach
  </tbody>
</table>

   </div>
      <div class="col-md-6">

      </div>
    </div>

@else
    No hay categorias
@endif


{!! $categorias->render() !!}





@endsection