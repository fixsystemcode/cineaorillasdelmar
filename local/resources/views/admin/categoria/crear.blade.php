@extends('layouts.backend')

@section('titulo')
Nuevo  
@stop


@section('contenido')


           {!! Form::open(['route' => 'categorias.store', 'method' => 'POST',
           					'files'=>true]) !!}
				<div class="form-group">
					{!! Form::label('categoria','Nombre de Categoria') !!}
					{!! Form::text('categoria',null,['class' =>'form-control', 'placeholder' =>'Ingrese nombre de categoria','required'])!!}
				</div>

				<div class="form-group">
					{!! Form::label('cat_destacado','Nombre de Categoria') !!}
					{!! Form::text('cat_destacado',null,['class' =>'form-control', 'placeholder' =>'Categoria destacado ingrese 1','required'])!!}
			</div>

				<div class="form-group"  >

						{!! Form::label('path','Fondo Categoria') !!}
						{!! Form::file('path',[])!!}
					   
				</div>

				<div class="form-group"  >

						{!! Form::label('icono','Icono de categoria') !!}
						{!! Form::file('icono',[ ])!!}
					   
				</div>
				
				<div class="form-group">
					{!! Form::submit('Registrar', ['class' =>'btn btn-primary']) !!}
					
				</div>


			    
			{!! Form::close() !!}
@endsection			

			

