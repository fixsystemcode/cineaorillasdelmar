@extends('layouts.backend')

@section('titulo')
Editar
@stop


@section('contenido')

@if(count($errors) >0)
<div class="alert alert-danger" role="alert">
  <ul>
  	@foreach($errors->all() as $error)
  	<li>{{$error}}</li>
  	@endforeach

  </ul>

</div>


@endif

{!!Form::model($categoria,['route'=>['categorias.update',$categoria],'method'=>'PUT','files' => true])!!}


          	<div class="form-group">
          			{!! Form::label('categoria', 'Nombre:') !!}
                    {!! Form::text('categoria',null,['class'=>'form-control','placeholder'=>'Ingrese tipo de usuario']) !!}

	       </div>
	       <div class="form-group">
					{!! Form::label('cat_destacado','Nombre de Categoria') !!}
					{!! Form::text('cat_destacado',null,['class' =>'form-control', 'placeholder' =>'Categoria destacado ingrese 1','required'])!!}
			</div>

	       <div class="form-group"  >

						{!! Form::label('path','Fondo Categoria') !!}
						{!! Form::file('path',[])!!}
					   
				</div>

				<div class="form-group"  >

						{!! Form::label('icono','Icono de categoria') !!}
						{!! Form::file('icono',[ ])!!}
					   
				</div>

				

				<div class="form-group">
					{!! Form::submit('Actualizar', ['class' =>'btn btn-primary']) !!}
					
				</div>


			    
			{!! Form::close() !!}

			


			
@endsection			

			

