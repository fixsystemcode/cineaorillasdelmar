@extends('layouts.backend')



@section('titulo')
SOLICITUDES DE INGRESO AL SISTEMA
@stop

@section('contenido')
@include('alerts.success')
@if (count($solicitudes))
    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
           <thead>
            <tr>
                <th>Cedula:</th>
                
                <th>Nombres:</th>
                <th>Apellidos:</th>
                <th>Correo:</th>
                <th>Habilitar</th>                
                <th>Rechazar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($solicitudes as $solicitud)
                <tr>                 
                	<td>{{$solicitud->ci}}</td>
                   
                    <td>{{$solicitud->nombres}}</td>
                    <td>{{$solicitud->apellidos}}</td>    
                    <td>{{$solicitud->email}}</td>   
                                  <td>
                      {!! Form::open(array('method' => 'GET', 'route' => array('hab_user', $solicitud->id))) !!}                       
                         {!! Form::submit('Habilitar', array('class' => 'btn btn-primary')) !!}
                       {!! Form::close() !!}

                    </td> 
                    <td>
                          {!! Form::submit('Rechazar', array('class' => 'btn btn-danger')) !!}</td>             
                </tr>
            @endforeach              
        </tbody>      
      
        </table>  

    
@else
No hay SOLICITUDES
@endif

@stop