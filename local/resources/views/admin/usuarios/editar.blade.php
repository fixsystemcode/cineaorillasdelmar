@extends('layouts.backend')

@section('titulo')
Editar
@stop


@section('contenido')

@include('alerts.request')
 


{!! Form::model($usuario, array('method' => 'PATCH', 'route' => array('usuarios.update', $usuario->id) , 'files' => true)) !!}
    
    
    <div class="row">
        <div class="col-md-6">
          {!! Form::label('imagen','Imagen') !!}
          {!! Form::file('path')!!}
        </div>

        <div class="col-md-6">

                <img src="{{asset('local/public/images')}}/{{$usuario->path}}" onerror="if (this.src != '../../local/public/images/avatar.png') this.src = '../../local/public/images/avatar.png';"  style ="width: 100px">

        </div>  

    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('nombres', 'Nombres:') !!}
            {!! Form::text('nombres',null,['class'=>'form-control','placeholder'=>'Ingrese Nombres']) !!}
    </div>


      </div>


      <div class="col-md-6">

      <div class="form-group" >
            {!! Form::label('apellidos', 'Apellidos:') !!}
            {!! Form::text('apellidos',null,['class'=>'form-control','placeholder'=>'Ingrese Apellidos']) !!}
        </div>
    </div>

    </div>



   

            <div class="row">
      <div class="col-md-6">
           <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa el Email']) !!}
            </div>
      </div>
      <div class="col-md-6">
            <div class="form-group">
                    {!! Form::label('telefono', 'Telefono:') !!}
                    {!! Form::text('telefono',null,['class'=>'form-control','placeholder'=>'Ingresa el teléfono']) !!}
            </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
            <div class="form-group">
                    {!! Form::label('login', 'Usuario:') !!}
                    {!! Form::text('login',null,['class'=>'form-control','placeholder'=>'Ingresa el Nombre del usuario']) !!}
            </div>
      </div>
      <div class="col-md-6">
         <div class="form-group">
            {!! Form::label('password', 'Contraseña:') !!}
            {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Ingrese la contraseña']) !!}
         </div>
      </div>
    </div>
  
     <div class="row">
      <div class="col-md-6">
            <div class="form-group">

                    {!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento:') !!}
                    {!!  Form::date('fecha_nacimiento',
                    \Carbon\Carbon::parse ($usuario->fecha_nacimiento),['class'=>'form-control']) !!}
            </div>
      </div>
      <div class="col-md-6">
            <div class="form-group">
                    {!! Form::label('id_tipo_usuario', 'Tipo de Usuario:') !!}
                    {!! Form::select('id_tipo_usuario', $tipos, null,['class'=>'form-control']) !!}
                    
            </div>
      </div>
    </div>
            {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}


{!! Form::close() !!}



@stop