@extends('layouts.paginas')

@section('ruta')
Ingreso al sistema
@stop

@section('titulo')
Ingreso al sistema
@stop

@section('script')
 
<script src='https://www.google.com/recaptcha/api.js'></script>

@stop

@section('contenido')
 
         
<section class="pageTitle" >
      <div class="container">
        <div class="row">
          
        </div>
      </div>
</section>
 

 <section class="mainContentSection">
      <div class="container">
        <div class="row">
          
          <div class="col-sm-8 col-xs-12">
            @include('alerts.errors')
            @include('alerts.success') 

          

               {!! Form::open(['route'=>'registro.store', 'method' => 'POST']) !!}
    
    <p style="font-size: 22px;color: #0086c7;">Complete sus datos para el registro.</p>

   
    <div class="form-group">
                        
        {!! Form::hidden('id_tipo_usuario', 5, null,['class' =>'form-control'])!!}
     </div>


    <div class="row">
        <div class="col-md-6">
         <br>
            {!! Form::text('nombres',null,['title'=>'Escriba sus dos nombres','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Nombres', 'onkeypress'=>'return val2(event)', 'maxlength'=>'100', 'id'=>'txtnombre']) !!}

        </div>

        <div class="col-md-6">
        <br>
           {!! Form::text('apellidos',null,['title'=>'Escriba sus dos apellidos','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Apellidos', 'onkeypress'=>'return val2(event)', 'maxlength'=>'100', 'id'=>'txtapellido']) !!}
        </div>

    </div>

     <div class="row">
        
        <div class="col-md-6">
 
            <label for="ci:name"><span style="color: red;margin-bottom: 0px;font-size: 11px;font-style: italic;text-align: right;margin-top: 0px;" id="mensaje"> </span> </label>
            {!! Form::text('ci',null,['title'=>'Sin guión, Ejemplo: 1303497801','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Cédula de identidad', 'id'=>'txtcedula', 'onkeypress'=>'return val(event)', 'maxlength'=>'10']) !!}
        </div>

        <div class="col-md-6">
        <br>
        
            {!! Form::text('telefono',null,['title'=>'Teléfono fijo o celular','data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control','placeholder'=>'Número de teléfono', 'onkeypress'=>'return val(event)', 'maxlength'=>'10']) !!}
        </div>
        
        
        
 
    </div>


    <div class="row">
        

        <div class="col-md-12">
        <br>
           {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Correo electrónico', 'onkeypress'=>'return val3(event)', 'maxlength'=>'50']) !!}

        </div>

    </div>     
 
    <div class="row">
        <div class="col-md-12">
         <br>
            {!! Form::password('password',['class' => 'form-control awesome','placeholder'=>'Escriba una contraseña']) !!}
        </div>

         
    </div>    
    <br>
    
      <div class="form-group">
            {!!Form::submit('Registrar',['class'=>'btn buttonTransparent'])!!}
     </div>        
        
 
{!! Form::close() !!}


         
          </div>

          <div class="col-sm-4 col-xs-12">
            <div class="contactInfo">
               <p style="font-size: 22px;color: #0086c7;">Ingreso al Sistema.</p>
               <form action="login" method="post">

                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 

                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Ingrese email" style="text-align: center;"  >
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password"  placeholder="Ingrese su contraseña" style="text-align: center;">
                       
                </div>

                <button type="submit" class="btn buttonTransparent" style="background: #0086c7;color: white;">Ingresar</button>

           

                <a  style="background: #0086c7;color: white;" class="btn buttonTransparent"  href="{{url('contrasena')}}"><small>¿Olvidé mi contraseña?</small></a>
                
              
            </form>
               
              
            </div>
            <br>
              
                <a href="{{ URL::asset('auth') }}">Continuar con Facebook</a>
                 <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>

               
          </div>

          

        </div>
      </div>
    </section>

@stop







 