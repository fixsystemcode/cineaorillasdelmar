@extends('layouts.backend')

@section('ruta')
Listado de Usuarios
@stop

@section('titulo')
Usuarios
@stop

@section('contenido')

@include('alerts.success')

 

<a class="btn btn-primary" href="{{ URL::route('usuarios.create') }}">Nuevo</a>
@if (count($usuarios))
    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                <th>Número</th>
                <th>Imagen</th>
                <th>Nombres</th>
		        <th>Email</th>
		        <th>Teléfono</th>
                <th>Usuario</th>
               
                <th>Fecha de Nacimiento</th>
                <th>Tipo de Usuario</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>

        <tbody>
            <?php $s = 0 ; ?>
            @foreach ($usuarios as $usuario)

                <tr>
                <td> <?php $s = $s + 1; echo $s; ?></td>
                @if(empty($usuario->path))
                <td><img src="{{asset('local/public/images')}}/no-imagen.jpg" onerror="this.src = 'local/public/images/avatar.png';" style ="width: 50px"></td>
                @else
                <td><img src="{{asset('local/public/images')}}/{{$usuario->path}}" onerror="this.src = 'local/public/images/avatar.png';"  style ="width: 50px"></td>
                @endif
                	
                	<td>{{ $usuario->nombres }}</td>
                    <td>{{ $usuario->email }}</td>
                    <td>{{ $usuario->telefono }}</td>
                    <td>{{ $usuario->login }}</td>
                  

                   
                    <td>  
                        @if($usuario->fecha_nacimiento != "")
                        {{\Carbon\Carbon::parse ($usuario->fecha_nacimiento)->format('d/m/Y')}}
                        @endif
                     </td>

                    <td>{{ $usuario->tipo }}</td>

			        <td>{!! link_to_route('usuarios.edit', 'Editar', array($usuario->id), array('class' => 'btn btn-info')) !!}</td>
                    <td>
         	          {!! Form::open(array('method' => 'DELETE', 'route' => array('usuarios.destroy', $usuario->id))) !!}                       
                         {!! Form::submit('Eliminar', array('class' => 'btn btn-danger')) !!}
                       {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
              
        </tbody>
      
    </table>
    
    
    
@else
    No hay usuarios
@endif

@stop