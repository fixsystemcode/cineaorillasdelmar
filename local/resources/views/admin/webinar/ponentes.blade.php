@extends('layouts.backend')

 

@section('titulo')
PONENTES
@stop

@section('contenido')

@include('alerts.success')



<div id="AñadirPonente" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h3 style="text-align: center;">PONENTES REGISTRADOS</h3>
                </div>
                <div class="modal-body">
                    
                        {!!Form::open(['route' => ['asignarponentes.store' , 'id_dia_evento' => $dia_evento->id], 'method' => 'POST'])!!} 
                         
                        {!! Form::label('ponente','Ponente')!!}
                        {!! Form::select('id_ponente',$nomponentes, null,[ 'class' =>'form-control' , 'list' =>'listas','id' =>'ponentes'])!!}
             
                         {!! Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
                         {!! Form::close()!!}                     
                    
                </div>
            </div>
        </div>
</div> 

<a data-target="#AñadirPonente" class="btn btn-primary" data-toggle="modal" >AGREGAR PONENTE</a>
@if (count($ponentes))

<!--<button id="botonExcel">EXPORTAR</button>-->


    <table  id="tbbuzon" class="col-lg-6 table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                
                <th>Apellidos</th>
                <th>Nombres</th>
                <th>foto</th>
                <th>profesion</th>
                <th>ACCIONES</th>
                <!--<th>ACCIONES</th>-->
            </tr>
        </thead>

        <tbody>
        @foreach ($inf as $inf)
            
                    <tr>
                    <td>{{ $inf->apellidos }}</td>
                    <td>{{ $inf->nombres }}</td>
                    @if(empty($inf->foto))
                    <td><img src="{{asset('local/public/images')}}/no-imagen.jpg"  style ="width: 100px"></td>
                    @else
                    <td><img src="{{asset('local/public/images')}}/{{$inf->foto}}"  style ="width: 100px"></td>
                    @endif
                    <td>{{ $inf->profesion }}</td>
                    <td>
                        {!! Form::open(array('method' => 'DELETE', 'route' => array('asignarponentes.destroy', $inf->id))) !!}
                            <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                        {!! Form::close() !!}
                    <a class="btn btn-success" href="{{route('asignarponentes.mostrar', array( 'id_dia_evento'=>$dia_evento->id, 'id_ponente'=>$inf->id_ponente) )}}" role="button">
                            CALIFICACIONES
                        </a>
                    </td>
                    </tr>
                
        @endforeach
              
        </tbody>
      
    </table>


     
    
@else
    No hay ningún ponente registrado en este día
@endif

@stop
@section('script')
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };



</script>
@endsection