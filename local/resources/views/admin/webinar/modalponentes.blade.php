

<div id="AñadirPonente" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    PONENTES REGISTRADOS
                </div>
                <div class="modal-body">
                    <table  id="tbbuzon" class="table" >
                        <thead>
                            <tr>
                                
                                <th>Apellidos</th>
                                <th>Nombres</th>
                                <th>foto</th>
                                <th>profesion</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach ($infoponentes2 as $infoponentes2)
                                    <tr>
                                    <td>{{ $infoponentes2->apellidos }}</td>
                                    <td>{{ $infoponentes2->nombres }}</td>
                                    @if(empty($infoponentes2->foto))
                                    <td><img src="{{asset('local/public/images')}}/no-imagen.jpg"  style ="width: 100px"></td>
                                    @else
                                    <td><img src="{{asset('local/public/images')}}/{{$infoponentes2->foto}}"  style ="width: 100px"></td>
                                    @endif
                                    <td>{{ $infoponentes2->profesion }}</td>
                                    <td>
                                        {!! Form::open(array('method' => 'DELETE', 'route' => array('webinar.destroyponente', $infoponentes2->id))) !!}
                                            <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                                        {!! Form::close() !!}
                                    </td>
                                    </tr>
                        @endforeach
                              
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@section('script')
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };



</script>
@endsection