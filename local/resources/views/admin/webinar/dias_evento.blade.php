@extends('layouts.backend')

 

@section('titulo')
DIAS DEL EVENTO
@stop

@section('contenido')

@include('alerts.success')



<div id="AñadirPonente" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-head">
                    <h3 style="text-align: center;">DIAS REGISTRADOS</h3>
                </div>
                <div class="modal-body">
                    
                        {!!Form::open(['route' => ['asignardias.store' , 'id_webinar' => $webi->id], 'method' => 'POST'])!!} 
                         
                         <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::label('fecha', 'Fecha:') !!}
                                {!! Form::date('fecha',null,['class'=>'form-control','placeholder'=>'Ingrese fecha','required']) !!}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('hora_inicio','Hora de inicio:') !!}
                                {!! Form::time('hora_inicio',null,['class'=>'form-control','required']) !!}
                                
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {!! Form::label('hora_fin','Hora de finalización:') !!}
                                {!! Form::time('hora_fin',null,['class'=>'form-control','required']) !!}
                            </div>
                        </div>
                        
             
                         {!! Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
                         {!! Form::close()!!}                     
                    
                </div>
            </div>
        </div>
</div> 

<a data-target="#AñadirPonente" class="btn btn-primary" data-toggle="modal" >AGREGAR DIA</a>
@if (count($dias_evento))

<!--<button id="botonExcel">EXPORTAR</button>-->


    <table  id="tbbuzon" class="col-lg-6 table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                
                <th>Fecha</th>
                <th>Hora inicio</th>
                <th>Hora Fin</th>
                <th>link de asistencia</th>
                <th>ACCIONES LINK</th>
                <th>ACCIONES</th>
                <!--<th>ACCIONES</th>-->
            </tr>
        </thead>

        <tbody>
        @foreach ($dias_evento as $dias_evento)
            
                    <tr>
                    <td>{{ $dias_evento->fecha }}</td>
                    <td>{{ $dias_evento->hora_inicio }}</td>
                    <td>{{ $dias_evento->hora_fin }}</td>
                    <td>/asistencia/{{ $dias_evento->slug }}</td>
                    <td><?php if ($dias_evento->estado_enlace == '1') {
                          
                            ?>
                            {!! Form::open(array('method' => 'PUT', 'route' => array('asignardias.cambiarestado', $dias_evento->id))) !!}
                            <button type="submit" onclick="return confirm('Quiere cambiar el estado del enlace de asistencia?')" class="btn btn-success"><span>DESACTIVAR</span></button>
                            {!! Form::close() !!}

                        <?php
                        }else{
                            ?>
                            {!! Form::open(array('method' => 'PUT', 'route' => array('asignardias.cambiarestado', $dias_evento->id))) !!}
                            <button type="submit" onclick="return confirm('Quiere cambiar el estado del enlace de asistencia?')" class="btn btn-success"><span>ACTIVAR</span></button>
                            {!! Form::close() !!}

                            <?php
                        }
                        ?>
                    </td>
                    <td>
                        <a class="btn btn-success" href="{{route('webinar.add', $dias_evento->id)}}" role="button">
                            PONENTES
                        </a>
                        <a class="btn btn-success" href="{{route('asignardias.asis', $dias_evento->id)}}" role="button">
                            ASISTENCIA
                        </a>
                        {!! Form::open(array('method' => 'DELETE', 'route' => array('asignardias.destroy', $dias_evento->id))) !!}
                            <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                        {!! Form::close() !!}
                    </td>
                    </tr>
                
        @endforeach
              
        </tbody>
      
    </table>


     
    
@else
    No hay ningún Dia registrado en este Webinar
@endif

@stop
@section('script')
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };



</script>
@endsection