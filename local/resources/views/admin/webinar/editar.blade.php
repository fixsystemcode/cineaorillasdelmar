@extends('layouts.backend')

@section('titulo')
Editar Webinar
@stop

@section('contenido')

@include('alerts.request')

 


{!! Form::model($webi, array('method' => 'PATCH', 'route' => array('webinar.update', $webi->id) , 'files' => true)) !!}
    
    
    <div class="row">
        <div class="col-md-3">
            <div class="form-gorup">
                {!! Form::label('anuncio','Anuncio') !!}
                {!! Form::file('anuncio')!!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <img src="{{asset('local/public/images')}}/{{$webi->anuncio}}"  style ="width: 100px">
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('tema', 'Tema:') !!}
                {!! Form::text('tema',null,['class'=>'form-control','placeholder'=>'Ingrese Nombres']) !!}
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('slug', 'Slug(como se mostrara el enlace):') !!}
                {!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'','required']) !!}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                {!! Form::label('fecha','Fecha del Webinar') !!}
                <div class='input-group date' id='fecha'>
                    {!! Form::date('fecha',null,['class' =>'form-control'])!!}
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                {!! Form::label('hora','Hora del Webinar') !!}
                <div class='input-group date' id='hora'>
                    {!! Form::time('hora',null,['class' =>'form-control'])!!}
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                {!! Form::label('hora_fin','Hora final del evento') !!}
                <div class='input-group date' id='fecha'>
                    <input type='time' name="hora_fin" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group" >
                {!! Form::label('estado', 'Estado:') !!}
                <div>
                    <input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="estado" id="estado" type="checkbox" @if($webi->estado == true)
                            checked
                            @endif
                            >
                </div>
            </div>
        </div>
    </div>
    
    
    
      
    

    
      


   

    {!!Form::submit('Actualizar',['class'=>'btn btn-primary'])!!}



{!! Form::close() !!}



@stop