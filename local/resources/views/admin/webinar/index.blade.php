@extends('layouts.backend')

 

@section('titulo')
Eventos
@stop

@section('contenido')

@include('alerts.success')

 

<a class="btn btn-primary" href="{{ URL::route('webinar.create') }}">Nuevo Evento</a>
@if (count($webi))
    <table  id="tbbuzon" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                
                <th>Tema</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Anuncio</th>
                <th>REGISTRO ACTIVO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($webi as $webi)
                <tr>
                <td>{{ $webi->tema }}</td>
                <td>{{ $webi->fecha }}</td>
                <td>{{ $webi->hora }}</td>
                @if(empty($webi->anuncio))
                <td><img src="{{asset('local/public/images')}}/no-imagen.jpg"  style ="width: 100px"></td>
                @else
                <td><img src="{{asset('local/public/images')}}/{{$webi->anuncio}}"  style ="width: 100px"></td>
                @endif
                <td><?php if ($webi->estado == '1') {
                  echo "SI";
                }else{
                  echo "NO";
                }
                 ?></td>
                <td>
                    <a class="btn btn-success" href="{{route('webinar.edit', $webi->id)}}" role="button">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                    {!! Form::open(array('method' => 'DELETE', 'route' => array('webinar.destroy', $webi->id))) !!}
                        <button type="submit" onclick="return confirm('Quiere borrar el registro?')" class="btn btn-danger"><span class="fa fa-trash-o"></span></button>
                    {!! Form::close() !!}
                    <a class="btn btn-success" href="{{route('webinar.show', $webi->id)}}" role="button">
                        REGISTRADOS
                    </a>
                    <!--<a class="btn btn-success" href="{{route('webinar.asis', $webi->id)}}" role="button">
                        ASISTENCIA
                    </a>
                    <a class="btn btn-success" href="{{route('webinar.aprob', $webi->id)}}" role="button">
                        APROBADOS
                    </a>
                    <a class="btn btn-success" href="{{route('webinar.dates', $webi->id)}}" role="button">
                        CONFIGURAR
                    </a>-->
                </td>
                </tr>
            @endforeach
              
        </tbody>
      
    </table>
    
    
    
@else
    No hay ningún Webinar registrado
@endif

@stop
@section('script')
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };
</script>
@endsection