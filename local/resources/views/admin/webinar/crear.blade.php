@extends('layouts.backend')

@section('titulo')
Nuevo Webinar  
@stop


@section('contenido')

@include('alerts.request')

 

 {!! Form::open(['route' => 'webinar.store', 'method' => 'POST','files'=>true]) !!}
      

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                {!! Form::label('anuncio','Anuncio') !!}
                {!! Form::file('anuncio')!!}
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('tema', 'Tema:') !!}
                {!! Form::text('tema',null,['class'=>'form-control','placeholder'=>'Ingrese Tema','required']) !!}
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group">
                {!! Form::label('slug', 'Slug(como se mostrara el enlace):') !!}
                {!! Form::text('slug',null,['class'=>'form-control','placeholder'=>'','required']) !!}
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                {!! Form::label('fecha','Fecha del evento') !!}
                <div class='input-group date' id='fecha'>
                    <input type='date' name="fecha" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                {!! Form::label('hora','Hora de inicio del evento') !!}
                <div class='input-group date' id='fecha'>
                    <input type='time' name="hora" class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                {!! Form::label('hora_fin','Hora final del evento (Primer día)') !!}
                <div class='input-group date' id='fecha'>
                    <input type='time' name="hora_fin" class="form-control" />
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group" >
                {!! Form::label('estado', 'Estado:') !!}
                <div>
                    <input data-toggle="toggle" data-on="Activado" data-off="Desactivado" name="estado" id="estado" type="checkbox">
                </div>
            </div>
        </div>
    </div>

    
    
    
    
    
      
           
    
    
  
    {!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
{!! Form::close() !!}



@stop