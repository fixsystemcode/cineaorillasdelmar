@extends('layouts.backend')

 

@section('titulo')
ASISTENCIA AL DIA DEL EVENTO
@stop

@section('contenido')
<?php
function diaES($fecha){
    $dia = date('l', strtotime($fecha));
    $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    $nombredia = str_replace($dias_EN, $dias_ES, $dia);
    return $nombredia;
}
function mesES($fecha){
    $mes = date('M', strtotime($fecha));
    $meses_ES = array("Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic");
    $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
    return $nombreMes;
}
?>

@include('alerts.success')

 


@if (count($asistencia))

<!--<button id="botonExcel">EXPORTAR</button>-->
<button class="btn btn-primary" id="botonExce" onclick="exportTableToExcel('tablaexportar')">EXPORTAR</button>

<div id="impresionExcelDiv" name="impresionExcelDiv" style="display: none;">
    <table  id="tablaexportar" class="table" >
        <thead>
            <tr>
                
                <th>Cedula</th>
                <th>Comentario</th>
                <th>Registro Asistencia</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($asistenciaex as $asistenciaex)
            <tr>
                <td>{{ $asistenciaex->cedula }}</td>
                <td>{{ $asistenciaex->comentario }}</td>
                <td><?php echo date('d', strtotime($asistenciaex->created_at))." ".mesES($asistenciaex->created_at).", ".date('Y', strtotime($asistenciaex->created_at))." / ".date('G : i', strtotime($asistenciaex->created_at)); ?></td>
            </tr>
            @endforeach
        </tbody>
    </table>
 </div>

    <table  id="tbasis" class="table table-striped table-bordered table-hover display" >
        <thead>
            <tr>
                
                <th>Cedula</th>
                <th>Comentario</th>
                <th>Registro Asistencia</th>
                <!--<th>ACCIONES</th>-->
            </tr>
        </thead>

        <tbody>
            @foreach ($asistencia as $asistencia)
                <tr>
                <td>{{ $asistencia->cedula }}</td>
                <td>{{ $asistencia->comentario }}</td>
                <td><?php echo date('d', strtotime($asistencia->created_at))." ".mesES($asistencia->created_at).", ".date('Y', strtotime($asistencia->created_at))." / ".date('G : i', strtotime($asistencia->created_at)); ?></td>
            
                </tr>
            @endforeach
              
        </tbody>
      
    </table>
    
    
    
@else
    No hay ninguna asistencia registrada en este día
@endif

@stop
@section('script')
<script>
    function accents_supr(data){
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /\n/g, ' ' )
            .replace( /[áàäâ]/g, 'a' )
            .replace( /[éèëê]/g, 'e' )
            .replace( /[íìïî]/g, 'i' )
            .replace( /[óòöô]/g, 'o' )
            .replace( /[úùüû]/g, 'u' ):
        data;
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
            "spanish-string-asc"  : function (s1, s2) { return s1.localeCompare(s2); },
            "spanish-string-desc" : function (s1, s2) { return s2.localeCompare(s1); }
        });

        jQuery.fn.DataTable.ext.type.search['spanish-string'] = function ( data ) {
            return accents_supr(data);
        }
    }

    jQuery.fn.DataTable.ext.type.search.string = function ( data ) {
        return ! data ?
        '' :
        typeof data === 'string' ?
        data
            .replace( /έ/g, 'ε' )
            .replace( /[ύϋΰ]/g, 'υ' )
            .replace( /ό/g, 'ο' )
            .replace( /ώ/g, 'ω' )
            .replace( /ά/g, 'α' )
            .replace( /[ίϊΐ]/g, 'ι' )
            .replace( /ή/g, 'η' )
            .replace( /\n/g, ' ' )
            .replace( /á/g, 'a' )
            .replace( /é/g, 'e' )
            .replace( /í/g, 'i' )
            .replace( /ó/g, 'o' )
            .replace( /ú/g, 'u' )
            .replace( /ê/g, 'e' )
            .replace( /î/g, 'i' )
            .replace( /ô/g, 'o' )
            .replace( /è/g, 'e' )
            .replace( /ï/g, 'i' )
            .replace( /ü/g, 'u' )
            .replace( /ã/g, 'a' )
            .replace( /õ/g, 'o' )
            .replace( /ç/g, 'c' )
            .replace( /ì/g, 'i' ) :
        data;
    };

    function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}


</script>
<script>
    

    function exportarexcelFuncion() {
        $(".exportarexcelnuevo").table2excel({
          name: "Worksheet Name",
          filename: "SomeFile.xls"
        });
      }
</script>
<script>
    
    $(document).ready(() => {
        $("#botonExcel").click(function() {
            $("#tablaexportar").table2excel({
                exclude: ".table-striped",
                name: "Worksheet name",
                filename: "Asistentes",
                fileext: ".xls"

            });
        });
    });

</script>
@endsection