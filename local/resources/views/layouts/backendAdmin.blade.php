<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema Asignación de tareas | Panel Control</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    {!! Html::style('/admin/bootstrap/css/bootstrap.min.css') !!}
  
    {!! Html::style('/admin/css/bootstrap-datetimepicker.min.css') !!}

    
    <!-- Font Awesome -->
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') !!}
    <!-- Ionicons -->
     {!! Html::style('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') !!}
    <!-- Theme style -->
     {!! Html::style('/admin/dist/css/AdminLTE.min.css') !!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {!! Html::style('/admin/dist/css/skins/_all-skins.min.css') !!}
    <!-- iCheck -->
    {!! Html::style('/admin/plugins/iCheck/flat/blue.css') !!}
    <!-- Morris chart 
    {!! Html::style('/admin/plugins/morris/morris.css') !!} -->
   
    <!-- Select2 -->
    {!! Html::style('/admin/plugins/select2/select2.min.css') !!}
       <!-- jvectormap -->
    {!! Html::style('/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <!-- Date Picker -->
    {!! Html::style('/admin/plugins/datepicker/datepicker3.css') !!}
    <!-- Daterange picker -->
    {!! Html::style('/admin/plugins/daterangepicker/daterangepicker-bs3.css') !!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!! Html::style('/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
    {!! Html::style('/admin/css/sistemalaravel.css') !!}

   {!! Html::style('/css/normalize.css') !!}
   
   {!! Html::style('/css/component.css') !!}

   {!! Html::style('/admin/css/bootstrap-datetimepicker.min.css') !!}


   {!! Html::style('https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css') !!}

   {!! Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') !!}


   {!! Html::style('https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css') !!}


    {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css') !!}





     {!! Html::script('/js/modernizr.custom.js') !!}

   <script src="{{ asset('local/public/vendors/ckeditor/ckeditor.js') }}"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>F</b>XS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <span class="hidden-xs"><?php $nombre = Auth::user()->nombres; echo $nombre; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
            
                <p>
                  <?php $nombre = Auth::user()->nombres; echo $nombre; ?>
                
                </p>
              </li>
           
              <!-- Menu Footer-->
              <li class="user-footer">
                
                <div class="pull-right">
                  <a href="{{ URL::asset('logout')}}" class="btn btn-default btn-flat">Cerrar Session</a>
                  <a target="_blank" href="{{ URL::asset('/')}}" class="btn btn-default btn-flat">Ir a página web</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            
        </div>
     
      </div>
      <!-- search form -->
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      
      @include('layouts.menuback')
      
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistema
        <small>Asignación de Tareas</small>
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="box box-primary">

                <div class="box-header">
                    <h3 class="box-title">@yield('titulo')</h3>
                </div>

                <div class="box-body">
                      @yield('contenido')
                </div>
                     
             
            </div><!-- /.content-wrapper -->

   
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; 2016 <a href="http://fixsystem.com.ec">FIXSYSTEM</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
    <!-- jQuery 2.1.4 -->
    {!! Html::script('/admin/plugins/jQuery/jQuery-2.1.4.min.js') !!}
    <!-- jQuery UI 1.11.4 -->
    {!! Html::script('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->


    <script>  
      $(function () {
        $('#datetimepicker9').datepicker({
          viewMode: 'years'
        });
      });
      


   $(document).ready(function() {
     

    $('#example1').DataTable( {
         "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },

        "order": [[ 0, "desc" ]],

        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5'
        ]
    } 

    );
} );






    $("#content-wrapper").css("min-height","2000px"); 
   
    $(function () {
      //Initialize Select2 Elements
      $(".select2").select2();
    });

  

    [].slice.call( document.querySelectorAll( '.progress-button' ) ).forEach( function( bttn, pos ) {
        new UIProgressButton( bttn, {
          callback : function( instance ) {
            var progress = 0,
              interval = setInterval( function() {
                progress = Math.min( progress + Math.random() * 0.1, 1 );
                instance.setProgress( progress );

                if( progress === 1 ) {
                  instance.stop( pos === 1 || pos === 3 ? -1 : 1 );
                  clearInterval( interval );
                }
              }, 150 );
          }
        } );
      } );

    </script>

    {!! Html::script('admin/plugins/datatables/jquery.dataTables.min.js') !!}
     {!! Html::script('admin/plugins/datatables/dataTables.bootstrap.min.js') !!}


    {!! Html::script('/admin/plugins/colorpicker/bootstrap-colorpicker.min.js') !!}

   
   <!-- Bootstrap 3.3.5 -->


    {!! Html::script('/admin/plugins/select2/select2.full.min.js') !!}
    <!-- Bootstrap 3.3.5 -->
    {!! Html::script('/admin/bootstrap/js/bootstrap.min.js') !!}
    <!-- Morris.js charts 
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    {!! Html::script('/admin/plugins/morris/morris.min.js') !!} -->
    <!-- Sparkline -->
    {!! Html::script('/admin/plugins/sparkline/jquery.sparkline.min.js') !!}
    <!-- jvectormap -->
    {!! Html::script('/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! Html::script('/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    <!-- jQuery Knob Chart -->
    {!! Html::script('/admin/plugins/knob/jquery.knob.js') !!}
    <!-- daterangepicker -->
    {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js') !!}
    {!! Html::script('/admin/plugins/daterangepicker/daterangepicker.js') !!}
    <!-- datepicker -->
    {!! Html::script('/admin/plugins/datepicker/bootstrap-datepicker.js') !!}
    <!-- Bootstrap WYSIHTML5 -->
    {!! Html::script('/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
    <!-- Slimscroll -->
    {!! Html::script('/admin/plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <!-- FastClick -->
    {!! Html::script('/admin/plugins/fastclick/fastclick.min.js') !!}
    <!-- AdminLTE App -->
    {!! Html::script('/admin/dist/js/app.min.js') !!}
    <!-- AdminLTE dashboard demo (This is only for demo purposes) 
    {!! Html::script('/admin/dist/js/pages/dashboard.js') !!} -->
    <!-- AdminLTE for demo purposes 
    {!! Html::script('/dist/js/demo.js') !!}-->
 
 <!-- javascript del sistema laravel -->

   {!! Html::script('admin/js/myjava.js') !!}
   {!! Html::script('admin/js/sistemalaravel.js') !!}
   {!! Html::script('admin/js/graficas.js') !!}
   {!! Html::script('admin/js/highcharts.js') !!}


   {!! Html::script('/js/classie.js') !!}
  
   {!! Html::script('/js/uiProgressButton.js') !!}

   <!--  JS de datapicker -->

    {!! Html::script('admin/js/bootstrap-datetimepicker.js') !!}
     {!! Html::script('admin/js/locales/bootstrap-datetimepicker.fr.js') !!}





    {!! Html::script('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') !!}
    
    {!! Html::script('https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js') !!}

    {!! Html::script('https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') !!}

    {!! Html::script('https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js') !!}

    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') !!}


    {!! Html::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') !!}
    {!! Html::script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') !!}

    {!! Html::script('//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') !!}

    {!! Html::script('//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') !!}

    {!! Html::script('//cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js') !!}

    <script src="http://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://highcharts.github.io/export-csv/export-csv.js"></script>

   @yield('script')

</body>
</html>
