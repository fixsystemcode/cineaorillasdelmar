<!DOCTYPE html>
<html lang="en">
<head>
    <title>Runachay | Plataforma de gestión educativa y administrativa - Ecuador</title>
    <meta charset="UTF-8">
    <meta name="description" content="La plataforma de gestión educativa y administrativa más completa y versátil del país. Mas de 190 instituciones educativas y alrededor de 200000 usuarios usan RUNACHAY.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Favicon -->
    
    <link rel="icon" type="image/vnd.microsoft.icon" href="{{asset('assets/img/ico.png')}}">
    <!-- Bootstrap v3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Font Awesome 4.7.0 -->
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <!-- Main style -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jquery.mb.YTPlayer.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/loader.css')}}">

    <script src='https://www.google.com/recaptcha/api.js'></script>


    <style>
        p::selection {
            background: #603089;
            color: #fff;
        }
        .lado1,
        .imag-ce,
        .lado2,
        .lado3,
        .lado4,
        .lado5,
        .lado6,
        .lado7,
        .lado8,
        .lado9,
        .modulo-dots{
            visibility: hidden;
        }

        .js-reveal--hidden {
          opacity: 0;
          margin-top: 5px;
        }
        .counter{
            text-shadow: 1px 1px 2px rgba(0,0,0,0.2);
        }
        .ytp-pause-overlay-controls-hidden .ytp-pause-overlay{
            display: none; 
        }
        #iframe_P1{pointer-events: none;}
    </style>
    <style>
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
            /*background: url({{asset('assets/img/loader.svg')}}) center no-repeat #fff;*/
        }
    </style>
</head>
<body class="home-01" id="style-5">

    <div class="se-pre-con">
        <div style="display: flex;text-align: center; justify-content: center;align-items: center; width: 100%;height: 100%;">
            <div>
                <img src="{{asset('assets/img/logox.png')}}" alt="" style="width: 80px; height: auto;display: block;transform: translate(0,15px);">
                <img src="{{asset('assets/img/loader3.gif')}}" alt="" style="width: 50px; height: auto; transform: translate(2px,-12px);">
            </div>
        </div>
    </div>
    <!-- <div class="covel" id="covel">
        <div class="cssload-loader">
            <div class="cssload-inner cssload-one"></div>
            <div class="cssload-inner cssload-two"></div>
            <div class="cssload-inner cssload-three"></div>
        </div>
    </div> -->
    
    <div class="wrapp-content">
        <!-- Header -->
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="./" class="logo">
                            <img src="{{asset('assets/img/logoprincipal2.png')}}" alt="">
                        </a>
                        <div class="main-nav__btn">
                            <div class="icon-left"></div>
                            <div class="icon-right"></div>
                        </div>
                        <ul class="main-nav__list">
                            <li><a href="#">Inicio</a></li>
                            <li>
                                <a href="#">Módulos</a>
                                <ul>
                                    <li><a href="#comunicacion">Comunicación</a></li>
                                    <li><a href="#matriculacion">Matriculación</a></li>
                                    <li><a href="#calificaciones">Calificaciones</a></li>
                                    <li><a href="#planificacion">Planificación</a></li>
                                    <li><a href="#contabilidad">Contabilidad</a></li>
                                    <li><a href="#app">APP movil</a></li>
                                    <li><a href="#dece">DECE</a></li>
                                    <li><a href="#cobranzas">Cobranzas</a></li>
                                    <li><a href="#encuestas">Evaluación</a></li>
                                    <li><a href="#inscripcion">Inscripción Webinar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <header class="wrapp-header move">
            <div class="media-cont">
                <div id="P1" class="player main-nav" data-property="{videoURL:'https://www.youtube.com/watch?v=b-bf3nQzrGE',containment:'.cont-video',autoPlay:true, startAt:0, opacity:1, showControls: false, optimizeDisplay: false, showYTLogo:false, loop: true, abundance: 0.1, rel: 0}">
                </div>
                <div style="height: auto; width: auto;" class="4545">
                </div>
                <div class="cont-cont">
                    <div class="video-controles">
                        <a onclick="jQuery('#P1').YTPToggleVolume()"><i class="fa fa-volume-up un" aria-hidden="true"></i></a>
                        <a onclick="jQuery('#P1').YTPPause()"><i class="fa fa-pause-circle-o do" aria-hidden="true"></i></a>
                        <a onclick="jQuery('#P1').YTPPlay()"><i class="fa fa-play-circle-o tr" aria-hidden="true"></i></a>
                    </div>
                    <div class="cont-video">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div class="foot-video">
                    <h3>La plataforma de <span>gestión educativa y administrativa</span> más <span>completa</span> y versátil del país.</h3>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="clear: both;"></div>
        </header>
        <!-- Content -->
        <main class="content-row" id="solicita-informacion">
            <div class="content-box-02">
                <div class="table-03" >
                    <div class="table-03__row" >
                        <div class="table-03__box-02">
                            <div class="table-03__content">
                                <h3 class="title-03"><span>¿Qué es</span> RUNACHAY?</h3>
                                <div class="content-box-03__text">
                                    <p>Es una herramienta tecnológica  que automatiza los principales procesos que manejan las instituciones educativas.
                                        <br> <br>
                                    Y lo más importante, está basada en las <strong>normas y leyes ecuatorianas.</strong></p>
                                </div>
                                <p class="author-info">
                                    - Runachay es una marca registrada por
                                    <span>Runachay Ecuador S.A.</span>
                                </p>
                                <div class="author-img">
                                    <img src="{{asset('assets/img/runaicon_hor.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="table-03__box-02">
                            <div class="text-center containerfix">
                                <div class="containerfix">
                                    <div class="row" style="position: relative; display: flex; justify-content: center;">
                                        <div style="" class="modulos-contenedor animated">
                                            <div class="derecho-modulos">
                                                <div class="xy">
                                                    <div class="item-modulo lado1">
                                                        <div>
                                                            <a data-easing="easeOutQuad" href="#comunicacion" class="owl-option-02__box-01">
                                                                <h3 class="owl-option-02__title tcolor1">
                                                                    Comunicación
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color1"></div>
                                                                    <div style="" class="hexa-lado2 color1"></div>
                                                                    <div style="" class="hexa-lado3 color1"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-01"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="item-modulo right lado2">
                                                        <div>
                                                            <a href="#matriculacion" class="owl-option-02__box-01">
                                                                <h3 class="owl-option-02__title tcolor2">
                                                                    Matriculación
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color2"></div>
                                                                    <div style="" class="hexa-lado2 color2"></div>
                                                                    <div style="" class="hexa-lado3 color2"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-02"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="xnoy">
                                                    <div class="item-modulo right lado3">
                                                        <div>
                                                            <a href="#calificaciones" class="owl-option-02__box-01">
                                                                <h3 class="owl-option-02__title tcolor3">
                                                                    Calificaciones
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color3"></div>
                                                                    <div style="" class="hexa-lado2 color3"></div>
                                                                    <div style="" class="hexa-lado3 color3"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-03"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="item-modulo lado4">
                                                        <div>
                                                            <a href="#planificacion" class="owl-option-02__box-01">
                                                                <h3 class="owl-option-02__title tcolor4">
                                                                    Planificación
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color4"></div>
                                                                    <div style="" class="hexa-lado2 color4"></div>
                                                                    <div style="" class="hexa-lado3 color4"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-04"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>                                          
                                            </div>
                                            <div class="izquierdo-modulos">
                                                <div class="noxy">
                                                    <div class="item-modulo lado5">
                                                        <div>
                                                            <a href="#cobranzas" class="owl-option-02__box-01">
                                                                <h3 class="owl-option-02__title tcolor5">
                                                                    Cobranzas
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color5"></div>
                                                                    <div style="" class="hexa-lado2 color5"></div>
                                                                    <div style="" class="hexa-lado3 color5"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-05"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="item-modulo left lado6">
                                                        <div>
                                                            <a href="#dece" class="owl-option-02__box-01">
                                                                <h3 class="owl-option-02__title tcolor6">
                                                                    DECE
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color6"></div>
                                                                    <div style="" class="hexa-lado2 color6"></div>
                                                                    <div style="" class="hexa-lado3 color6"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-06"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="noxnoy">
                                                    <div class="item-modulo left lado7">
                                                        <div>
                                                            <a href="#app" class="owl-option-02__box-01 color7">
                                                                <h3 class="owl-option-02__title tcolor7">
                                                                    App Móvil
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color7"></div>
                                                                    <div style="" class="hexa-lado2 color7"></div>
                                                                    <div style="" class="hexa-lado3 color7"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-07"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="item-modulo lado8">
                                                        <div>
                                                            <a href="#contabilidad" class="owl-option-02__box-01 color8">
                                                                <h3 class="owl-option-02__title tcolor8">
                                                                    Contabilidad
                                                                </h3>
                                                                <div style="" class="hexagono-item">
                                                                    <div style="" class="hexa-lado1 color8"></div>
                                                                    <div style="" class="hexa-lado2 color8"></div>
                                                                    <div style="" class="hexa-lado3 color8"></div>
                                                                    <div class="owl-option-02__img-wrapp hov-img-08"></div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>                                          
                                            </div>
                                            <div class="centro-modulo ">
                                                <div class="lado9">
                                                    <a href="#encuestas" class="owl-option-02__box-01">
                                                        <h3 class="owl-option-02__title tcolor9">
                                                            Evaluación
                                                        </h3>
                                                        <div style="" class="hexagono-item">
                                                            <div style="" class="hexa-lado1 color9"></div>
                                                            <div style="" class="hexa-lado2 color9"></div>
                                                            <div style="" class="hexa-lado3 color9"></div>
                                                            <div class="owl-option-02__img-wrapp hov-img-09"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div style="left: 50%;top: 50%;transform: translate(-50%, -50%);display: block;position: absolute;z-index: 5;">
                                                <div class="iconocentral">
                                                    <div class="imag-ce">
                                                        <img src="{{asset('assets/img/logocuadrado.png')}}" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modulo-dots">
                                                <div style="" class="dots">
                                                    <div class="dost-2"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- section 1 -->
            <div class="parallax parallax_02" data-type="background" data-speed="10" style="position: relative;">
                <div style="width: 100%; height: 100%; background-color: #000; opacity: 0.3; position: absolute; top: 0; left: 0;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">
                                <h3 class="parallax-title">Ya son más de 
                                    <span>200000+</span> Usuarios</h3>
                                <div class="parallax-text">
                                    <p>Que utilizan nuestra plataforma a nivel nacional en varias instituciones educativas. </p>
                                </div>
                                <!-- <a href="#" class="parallax-btn">Ver más</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
<!-- METO PARALAX -->
            <div class="parallax parallax_01" data-type="background" data-speed="10" style="display: none">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">
                                <h3 class="parallax-title">Soluciones 
                                    <span>modulares</span> y personalizadas</h3>
                                <div class="parallax-text">
                                    <p>¡Que facilitan y agilitan los procesos de las instituciones educativas! </p>
                                </div>
                                <a href="#" class="parallax-btn">Ver módulos</a>
                                <br><br><br>
                                <div class="owl-carousel owl-option-02">
                                    <div class="item">
                                        <a href="#comunicacion" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-01"></div>
                                            <h3 class="owl-option-02__title">
                                                Comunicación
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#matriculacion" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-02"></div>
                                            <h3 class="owl-option-02__title">
                                                Matriculación
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#calificaciones" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-03"></div>
                                            <h3 class="owl-option-02__title">
                                                Calificaciones
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#planificacion" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-04"></div>
                                            <h3 class="owl-option-02__title">
                                                Planificación
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#cobranzas" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-05"></div>
                                            <h3 class="owl-option-02__title">
                                                Cobranzas
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#dece" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-06"></div>
                                            <h3 class="owl-option-02__title">
                                                DECE
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#encuestas" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-09"></div>
                                            <h3 class="owl-option-02__title">
                                                Evaluación
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#app" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-07"></div>
                                            <h3 class="owl-option-02__title">
                                                App Móvil
                                            </h3>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="#" class="owl-option-02__box-01">
                                            <div class="owl-option-02__img-wrapp hov-img-08"></div>
                                            <h3 class="owl-option-02__title">
                                                Contabilidad
                                            </h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
<!-- AQUÍ EMPIEZA SECCIONES -->
            <section id="comunicacion">
                <div class="content-box-02">
                    <div class="table-03">
                        <div class="table-03__row">
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">Comunicación
                                        <span>ágil y efectiva</span>
                                    </h3>
                                    <p>
                                        ¡<strong>Runachay</strong> integra las principales tecnologías de comunicación!
                                        <br>
                                        Permite de manera inmediata enviar comunicaciones a los padres de familia.
                                        <br>
                                        Además se puede <strong>imprimir las comunicaciones</strong> enviadas para futuras evidencias.
                                    </p>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-03__box-01">
                                <img class="table-03__img" src="{{asset('assets/img/mod_com.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div class="content-box-01 padding-top-100 padding-sm-top-50">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-02">
                                <h3 class="servises-item__title">Hna. Mirian Bernardette</h3>
                                <h3 class="servises-item__title">Rectora U.E. Stella Maris</h3>
                                <div class="servises-item__text">
                                    <p>"Me siento muy satisfecha con Runachay porque me parece una plataforma completa, nos ayuda en todos los procesos, especialmente en la comunicación con los padres, en el seguimiento de los estudiantes".</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-01">
                                <h3 class="servises-item__title">Mg. Darío Páez</h3>
                                <h3 class="servises-item__title">Rector U.E. Israel</h3>
                                <div class="servises-item__text">
                                    <p>"Runachay en la unidad educativa Israel tiene dos años y está consolidada la herramienta, esta nos ha permitido trabajar de manera eficaz en todos los procesos sustantivos, permitiéndonos conocer a mi como rector en un solo click que está pasando a nivel de diferentes áreas tanto pedagógicas como administrativas".</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-03">
                                <h3 class="servises-item__title">Sra. Josefa Bravo</h3>
                                <h3 class="servises-item__title">Padre de familia U.E. Manabí</h3>
                                <div class="servises-item__text">
                                    <p>"Antes teníamos que acercarnos al colegio, pedir citas para ver como estaban nuestros hijos, a veces no le daban toda la información, ahora con Runachay estoy informada todo el tiempo de lo que sucede en el colegio."</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- METO PARALLAX -->
<!-- CIERRO PARALLAX -->
<!--PARALLAX CON BOTÓN  
            <div class="parallax parallax_01" data-type="background" data-speed="10">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">
                                <h3 class="parallax-title">Hay que poner
                                    <span>nueva</span> frase aquí</h3>
                                <div class="parallax-text">
                                    <p>We have a fully qualified and very well educated teaching staff, continuous student counseling, and a very effective and enthusiastic student support staff. </p>
                                </div>
                                <a href="#" class="parallax-btn">get started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
FIN DE PARALLAX CON BOTÓN  -->
        <!--Sección 2 -->
            <section id="matriculacion">
                <div class="content-box-02">
                    <div class="table-03">
                        <div class="table-03__row">
                            <div class="table-03__box-03">
                                <img class="table-03__img" src="{{asset('assets/img/mod_mat.png')}}" alt="">
                            </div>
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">Matriculación en línea <br>
                                        <span>rápida y ordenada</span>
                                    </h3>
                                    <p>
                                        ¡<strong>Runachay</strong> simplifica y agiliza el proceso de matriculación!
                                        <br> <br>
                                        Permite obtener automáticamente ficha de matriculación, autorizaciones,  
                                        actas de compromisos  y todos los reportes exigidos por el <strong>Ministerio de Educación</strong>.
                                    </p>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN SECCION 2 -->
            <!-- PARALLAX 3 -->
            <div class="parallax parallax_06" style="position: relative;">
                <div style="width: 100%; height: 100%; background-color: #000; opacity: 0.3; position: absolute; top: 0; left: 0;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="module-counter counter-02">
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <!-- <p class="stat_count"><span class="counter">42,522</span></p> -->
                                                <p class="stat_count"><span class="counter">110,520</span></p>
                                                <p class="counter_title">Estudiantes matriculados</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">28,940</span></p>
                                                <p class="counter_title">Horas de espera ahorrados</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">6</span></p>
                                                <p class="counter_title">Años de uso del módulo</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <!-- <p class="stat_count"><span class="counter">65</span></p> -->
                                                <p class="stat_count"><span class="counter">192</span></p>
                                                <p class="counter_title">Instituciones educativas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN PARALLAX 3 -->
        <!--Sección 3 -->
            <section id="calificaciones">
                <div class="content-box-02">
                    <div class="table-03">
                        <div class="table-03__row">
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">Calificaciones
                                        <span>a un solo click</span>
                                    </h3>
                                    <p>
                                        ¡Bienvenidos al mundo de las libretas digitales!
                                        <br> <br>
                                        Runachay valida, calcula y genera libretas de <strong>calificaciones, reportes e informes</strong> acorde a las exigencias y normas del Ministerio de Educación.
                                        <br><br>
                                        Práctico, tecnológico y a la altura de la época. 
                                    </p>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-03__box-04">
                                <img class="table-03__img" src="{{asset('assets/img/mod_cal.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!-- FIN SECCION 3 -->
<!-- PARALLAX 3 -->
            <div class="parallax parallax_01" style="display: none;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="module-counter counter-02">
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count" data-count="28990">0</p>
                                                <p class="counter_title">Estudiantes matriculados</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count" data-count="9663">0</p>
                                                <p class="counter_title">Horas de espera ahorrados</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count" data-count="4">0</p>
                                                <p class="counter_title">Años de uso del módulo</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count" data-count="40">0</p>
                                                <p class="counter_title">Instituciones educativas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN PARALLAX 3 -->
<!--Sección 4 -->
            <section id="planificacion">
                <div class="content-box-02" style="background-color: #fff">
                    <div class="table-03">
                        <div class="table-03__row">
                            <div class="table-03__box-05">
                                <img class="table-03__img" src="{{asset('assets/img/mod_plan.png')}}" alt="">
                            </div>
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">Planificación académica <br>
                                        <span>fácil y sencilla</span>
                                    </h3>
                                    <br> <br>
                                    <p>
                                        Con <strong>Runachay</strong> gestione y de seguimiento a las planificaciones de manera fácil y amigable.
                                        <br> <br>
                                        Además imprima reportes basados en  las normas y exigencias del <strong>Ministerio de Educación</strong>.
                                        <br> <br>
                                        <br> <br>
                                        <br> <br>
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- FIN SECCION 4 -->
            <!-- PARALLAX 3 -->
            <div class="parallax parallax_01" data-type="background" data-speed="10">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">


                                <h3 class="parallax-title">Runachay ha sido diseñada para colaborar en la más noble y loable labor, <span>EDUCAR.</span></h3>
                                <!-- <div class="parallax-text">
                                    <p>We have a fully qualified and very well educated teaching staff, continuous student counseling, and a very effective and enthusiastic student support staff. </p>
                                </div> -->
                                <!-- <a href="#" class="parallax-btn">Contrátalo</a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN PARALLAX 3 -->
<!-- AQUÍ EMPIEZA SECCION 5 -->
            <section id="cobranzas">
                <div class="content-box-02">
                        <div class="table-03">
                            <div class="table-03__row">
                                
                                <div class="table-03__box-02">
                                    <div class="table-03__content">
                                        <h3 class="title-02">Cobro de pensiones <br>
                                            <span>efectivo y a tiempo</span>
                                        </h3>
                                        <p>
                                            ¿Problemas de morosidad?
                                            <br>
                                            Runachay es una herramienta potente en el proceso de cobranza.
                                            <br>
                                            Controla, organiza y comunica automáticamente al <strong>padre de familia</strong> sobre <strong>el cobro de pensiones</strong>.
                                            <br>
                                            Logre que los <strong>pagos mensuales</strong> lleguen a tiempo.
                                            <!-- ¡Runachay integra las principales tecnologías de comunicación!
                                            <br>
                                            Permite de manera inmediata enviar comunicaciones a los padres de familia.
                                            <br>
                                            Además se puede imprimir las comunicaciones enviadas para futuras evidencias. -->
                                        </p>
                                        <div class="progress-circle-wrapp" style="display: none;">
                                            <div class="progress-circle-item">
                                                <div data-value="0.75" class="progress-circle">
                                                    <div class="progress-circle__value"></div>
                                                </div>
                                                <p class="progress-circle__text">Uso a diario</p>
                                            </div>
                                            <div class="progress-circle-item">
                                                <div data-value="1" class="progress-circle">
                                                    <div class="progress-circle__value"></div>
                                                </div>
                                                <p class="progress-circle__text">Clientes satisfechos</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-03__box-06">
                                    <img class="table-03__img" src="{{asset('assets/img/mod_cob.png')}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
            </section>


            <div class="content-box-01 padding-top-100 padding-sm-top-50" style="display: none;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-01">
                                <h3 class="servises-item__title">Cloud Library</h3>
                                <div class="servises-item__text">
                                    <p>Morbi eget maximus diam, vel rutrum orci. Curabitur dictum augue sit amet laoreet dignissim. Etiam feugiat nisl est.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-02">
                                <h3 class="servises-item__title">Certifications</h3>
                                <div class="servises-item__text">
                                    <p>Nunc ut elit tempus, faucibus orci vel, consequat odio. Praesent consectetur dignissim ullamcorper.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-03">
                                <h3 class="servises-item__title">Video Lessons</h3>
                                <div class="servises-item__text">
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec vitae orci ac elit dapibus tincidunt.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-04">
                                <h3 class="servises-item__title">Train Your Brain</h3>
                                <div class="servises-item__text">
                                    <p>Morbi eget maximus diam, vel rutrum orci. Curabitur dictum augue sit amet laoreet dignissim. Etiam feugiat nisl est.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-05">
                                <h3 class="servises-item__title">Master the Skils</h3>
                                <div class="servises-item__text">
                                    <p>Nunc ut elit tempus, faucibus orci vel, consequat odio. Praesent consectetur dignissim ullamcorper. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="servises-item serv-item-06">
                                <h3 class="servises-item__title">Graduate the Best</h3>
                                <div class="servises-item__text">
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec vitae orci ac elit dapibus tincidunt. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!--PARALLAX CON BOTÓN  -->
            <div class="parallax parallax_01" data-type="background" data-speed="10">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">
                                <h3 class="parallax-title">Conoce nuestra
                                    <span>plataforma</span></h3>
                                <div class="parallax-text">
                                    <p>Te invitamos a conocer la plataforma de gestión educativa más completa y versátil del país.</p>
                                </div>
                                <!-- <a href="#" class="parallax-btn"></a> -->
                                <button type="button" class="btn parallax-btn" data-toggle="modal" data-target="#Modal">Solicitar demostración</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
<!--FIN DE PARALLAX CON BOTÓN  -->
        <!--SECCIÓN 6 -->
            <section id="dece">
                <div class="content-box-02">
                    <div class="table-03">
                        <div class="table-03__row">
                            <div class="table-03__box-07">
                                <img class="table-03__img" src="{{asset('assets/img/mod_dece.png')}}" alt="">
                            </div>
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">DECE <br>
                                        <span>Integrado</span>
                                    </h3>
                                    <p>
                                        Potente herramienta que agilita la labor de prevención, detección, abordaje y seguimiento.
                                        <br> <br>
                                        Información académica, personal y comportamental de los estudiantes.
                                        <br><br>
                                        Permite generar los informes solicitados por el <strong>Ministerio de educación.</strong>
                                    </p>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
<!-- FIN SECCION 6 -->
<!-- PARALLAX 3 -->
            <div class="parallax parallax_06" style="position: relative;">
                <div style="width: 100%; height: 100%; background-color: #000; opacity: 0.3; position: absolute; top: 0; left: 0;"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="module-counter counter-02">
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">6,517,848</span></p>
                                                <p class="counter_title">Tareas enviadas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">10,417,978</span></p>
                                                <p class="counter_title">Comunicaciones enviadas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">1,000,000</span></p>
                                                <p class="counter_title">Mensajes de cobranzas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">39,780</span></p>
                                                <p class="counter_title">Fichas DECE ingresadas en línea</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN PARALLAX 3 -->
<!--SECCIÓN 7 -->
            <section id="encuestas">
                <div class="content-box-02">
                    <div class="table-03">
                        <div class="table-03__row">
                            
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">Evaluación y encuestas <br>
                                        <span>a un solo click</span>
                                    </h3>
                                    <p>
                                        Permite generar encuestas en línea a padres de familia, estudiantes y a la comunidad educativa en general.
                                        <br> <br>
                                        <strong>Conozca en línea</strong> los niveles de satisfacción brindados por su institución, con el objetivo de prevenir y mejorar.
                                        <br><br>
                                        Le permite cumplir con las exigencias de <strong>Ministerio de Educación </strong>mediante evaluaciones a sus docentes.
                                    </p>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-03__box-10">
                                <img class="table-03__img" src="{{asset('assets/img/mod_enc.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!-- FIN SECCION 7 -->
<!-- PARALLAX 3 -->
            <div class="parallax parallax_01">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="module-counter counter-02">
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">300,000</span></p>
                                                <p class="counter_title">Evaluaciones realizadas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">10,000</span></p>
                                                <p class="counter_title">Planificaciones generadas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">10,008,989</span></p>
                                                <p class="counter_title">Reportes generados</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="shortcode_counter">
                                    <div class="counter_wrapper">
                                        <div class="counter_content">
                                            <div class="stat_count_wrapper">
                                                <p class="stat_count"><span class="counter">8,243,555</span></p>
                                                <p class="counter_title">Notificaciones enviadas</p>
                                            </div>
                                            <div class="stat_temp"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN PARALLAX 3 -->
<!--SECCIÓN 8 -->
            <section id="app">
                <div class="content-box-02 content-box-02--ak">
                    <div class="table-02">
                        <div class="table-02__row">
                            <div class="table-03__box-08">
                                <img class="table-03__img" src="{{asset('assets/img/mod_app.png')}}" alt="">
                            </div>
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">App
                                        <span>Móvil</span>
                                    </h3>
                                    <p>
                                        <!-- La aplicación Runachay integra todas las funcionalidades que puedes encontrar en la plataforma web, mejorando la experiencia de usuario.
                                        <br>
                                        Podras encontrar nuestra aplicación tanto Google play store como en App store de Apple, disponible para todos los usuarios. -->
                                        <ul class="kkkk">
                                            <li>El padre de familia recibe notificaciones inmediatas, novedades y/o eventos.</li>
                                            <li>Podrá observar calificaciones en linea.</li>
                                            <li>También se le informará sobre las tareas y el detalle de las mismas.</li>
                                            <li>Será una herramienta de comunicación altamente eficiente entre los representantes del estudiante y la iinstitucion educativa.</li>
                                        </ul>
                                    </p>
                                    <div style="display: flex; justify-content: center; align-items: center; margin-top: 30px">
                                        <img src="{{asset('assets/img/app.png')}}" alt="" style="height: 90px;width: auto;display: none;">
                                        <div style="padding: 5px 0;">
                                            <a href="https://play.google.com/store/apps/details?id=aplicacion.runachay.fixsystem" target="_blank"><img src="{{asset('assets/img/google.png')}}" alt="" style="height: 35px;width:auto;opacity: 0.8"></a>

                                            <a href="https://itunes.apple.com/ec/app/runachay/id1361331696" target="_blank"><img src="{{asset('assets/img/apple.png')}}" alt="" style="height: 35px;width:auto;opacity: 0.7"></a>
                                        </div>
                                    </div>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
<!-- FIN SECCION 8 -->
<!-- PARALLAX 3 -->
            <div class="parallax parallax_01" data-type="background" data-speed="10">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">
                                <!-- <h3 class="parallax-title">Hay que poner otra
                                    <span>nueva</span> frase aquí</h3> -->
                                <div class="parallax-text">
                                    <p>Quienes conformamos Runachay, nos complace enormemente su interés en lo que hacemos, para nosotros será un privilegio hacerles una demostración personal.
                                </div>
                                <!-- <a href="#" class="parallax-btn">Solicitar demostración</a> -->
                                <button type="button" class="btn parallax-btn" data-toggle="modal" data-target="#Modal">Solicitar demostración</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN PARALLAX 3 -->
<!--SECCIÓN 9 -->
            <section id="contabilidad">
                <div class="content-box-02 content-box-02--ak">
                    <div class="table-02">
                        <div class="table-02__row">
                            <div class="table-03__box-02">
                                <div class="table-03__content">
                                    <h3 class="title-02">Contabilidad <br>
                                        <span>integrada</span>
                                    </h3>
                                    <p>
                                        Información contable, anexos transaccionales, estados de pérdidas y ganancias y más... solo con un click.
                                    </p>
                                    <div style="display: flex;justify-content: center;align-items: center; margin-top: 60px;">
                                        <img src="{{asset('assets/img/sri.png')}}" alt="" style="height: 40px; width: auto;">
                                        <img src="{{asset('assets/img/iess.png')}}" alt="" style="height: 40px; width: auto;margin-left: 30px;">
                                    </div>
                                    <div class="progress-circle-wrapp" style="display: none;">
                                        <div class="progress-circle-item">
                                            <div data-value="0.75" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Uso a diario</p>
                                        </div>
                                        <div class="progress-circle-item">
                                            <div data-value="1" class="progress-circle">
                                                <div class="progress-circle__value"></div>
                                            </div>
                                            <p class="progress-circle__text">Clientes satisfechos</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-03__box-09">
                                <img class="table-03__img" src="{{asset('assets/img/mod_conta.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!-- FIN DE SECCIÓN 9 -->
<!-- PARALLAX 4 -->
            <section id='inscripcion'>
            <div class="parallax parallax_01" data-type="background" data-speed="10">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="parallax-content-01">
                                <!-- <h3 class="parallax-title">Hay que poner otra
                                    <span>nueva</span> frase aquí</h3> -->
                                <div class="parallax-text">
                                    <p>INSCRIBETE A NUESTROS WEBINAR</p></br>
                                    <p style="font-size: 16px">Te presentamos los cursos virtuales a los que puedes acceder y que se encuentran en etapa de registro, totalmente gratis, solo ingresa tu información y te enviaremos la informació de acceso.</p>
                                </div>
                                @foreach($webinars as $webinars)
                                    <div class="col-lg-12">
                                        
                                            <div class="gp_img"><a href="{{url('/inscripciones')}}/{{$webinars->slug}}"><img style="width:75%; height:250px;" src="{{asset('local/public/images')}}/{{$webinars->anuncio}}" alt=""></a></div>
                                        </br>
                                    </div>
                                @endforeach
                                
                                <!-- <a href="#" class="parallax-btn">Solicitar demostración</a> 
                                <button type="button" class="btn parallax-btn" data-toggle="modal" data-target="#Modal">Solicitar demostración</button>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
            <!-- FIN PARALLAX 4 -->
<!--FIN DE TODAS LAS SECCIONES -->
            <div class="content-box-01 padding-top-93 padding-bottom-63" style="display: none;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3 class="title-05">Noticias &amp;
                                <span>Eventos</span>
                            </h3>
                            <p class="subtitle-01">Suspendisse sodales arcu velit, non pretium massa accumsan non. Proin accumsan placerat mauris sit amet condimentum. Morbi luctus risus tincidunt urna hendrerit mollis.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <ul class="news-block">
                                @foreach($noticias as $noti)
                                <li class="news-item">
                                    <figure class="news-item__img">
                                        <a href="{{URL::to('/blog')}}/{{$noti->slug}}">
                                            <img src="{{asset('local/public/images')}}/{{$noti->path}}" alt="">
                                        </a>
                                    </figure>
                                    <div class="news-item__content">
                                        <p class="news-item__date">
                                            <?php echo date('d', strtotime($noti->created_at)); ?>
                                            <?php echo date('M Y', strtotime($noti->created_at)); ?>
                                            <!-- 24 September, 2017 -->
                                        </p>
                                        <h3 class="news-item__title">
                                            <a href="{{URL::to('/blog')}}/{{$noti->slug}}">{{$noti->titulo}}</a>
                                        </h3>
                                        <a href="{{URL::to('/blog')}}/{{$noti->slug}}" class="news-item__btn">Leer más</a>
                                    </div>
                                </li>
                                @endforeach
                                <!-- <li class="news-item">
                                    <figure class="news-item__img">
                                        <a href="single_event.html">
                                            <img src="{{asset('assets/img/news_img/news_img_02.jpg')}}" alt="">
                                        </a>
                                    </figure>
                                    <div class="news-item__content">
                                        <p class="news-item__date">24 September, 2017</p>
                                        <h3 class="news-item__title">
                                            <a href="single_event.html">New Courses for 2018</a>
                                        </h3>
                                        <a href="single_event.html" class="news-item__btn">Read More</a>
                                    </div>
                                </li> -->
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="news-info">
                                @foreach($eventos as $ev)
                                <div class="news-info__post">
                                    <div class="news-info__date-block">
                                        <p><?php echo date('d', strtotime($ev->fechainicio)); ?></p>
                                        <span><?php echo date('M', strtotime($ev->fechainicio)); ?></span>
                                    </div>
                                    <h4 class="news-info__title">
                                        <a href="single_event.html">
                                            {{$ev->titulo}}
                                        </a>
                                    </h4>
                                    <div class="news-info__text">
                                        <?php $text = substr($ev->texto, 0,60); echo $text; ?>...
                                    </div>
                                    <a href="single_event.html" class="news-info__btn">View Event</a>
                                </div>
                                @endforeach
                                <!-- <div class="news-info__post">
                                    <div class="news-info__date-block">
                                        <p>15</p>
                                        <span>Sep</span>
                                    </div>
                                    <h4 class="news-info__title">
                                        <a href="single_event.html">
                                            Boost Your Teamworking Skills
                                        </a>
                                    </h4>
                                    <div class="news-info__text">
                                        <p>Mauris ultricies mauris non odio facilisis convallis. </p>
                                    </div>
                                    <a href="single_event.html" class="news-info__btn">View Event</a>
                                </div>
                                <div class="news-info__post">
                                    <div class="news-info__date-block">
                                        <p>15</p>
                                        <span>Sep</span>
                                    </div>
                                    <h4 class="news-info__title">
                                        <a href="single_event.html">
                                            Campus Tour 2017
                                        </a>
                                    </h4>
                                    <div class="news-info__text">
                                        <p>Mauris ultricies mauris non odio facilisis convallis. </p>
                                    </div>
                                    <a href="single_event.html" class="news-info__btn">View Event</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- AQUÍ EMPIEZA SECCIÓN TESTIMONIOS -->
            <div class="content-box-02 padding-top-96 padding-bottom-67">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3 class="title-02">
                                <span>Testimonios</span> de uso de la plataforma
                            </h3>
                            <p class="subtitle-01 margin-bottom-34">Nuestros clientes hablan por nosotros, ellos han usado Runachay ya en algunos períodos
                                <br>y consideramos que su opinión es importante para mejorar la experiencia de nuevos usuarios.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="owl-carousel owl-option-01 owl-theme-01">
                            @foreach($testimonios as $tes)
                            <div class="owl-theme-01__item">
                                <figure class="owl-theme-01__item-img">
                                    <img src="{{asset('local/public/images')}}/{{$tes->path}}" alt="">
                                </figure>
                                <div class="owl-theme-01__item-header">
                                    <h3 class="owl-theme-01__item-title">{{$tes->titulo}}</h3>
                                    <p class="owl-theme-01__item-subtitle">{{$tes->seo_descripcion}}</p>
                                </div>
                                <div class="owl-theme-01__item-content">
                                    <?php echo $tes->texto; ?>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
            <!-- <nav>
                <ul class="cd-vertical-nav">
                    <li><a href="#0" class="cd-prev inactive">Next</a></li>
                    <li><a href="#0" class="cd-next">Prev</a></li>
                </ul>
            </nav> -->
            <!-- AQUÍ TERMINA SECCIÓN TESTIMONIOS -->
            <div class="modal fade modal-solicitud" id="Modal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" style="color: #303638; font-weight: 500;">Solicitud de demostración</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['route'=>'paginas.enviarsolicitud','method'=>'POST','class'=>'form-contacto2 contact-form2 inited','data-formtype'=>'contact']) !!}
                                            @include('alerts.success')
                                            @include('alerts.errors')
                            <!-- <form action=""> -->
                                <div class="reply-form__box-01">
                                    <label for="">Nombre: <span style="color: red;">*</span></label>
                                    <input class="reply-form-input" type="text" name="nombre" aria-required="true" aria-invalid="false" placeholder="" id="nombre">
                                </div>
                                <div class="reply-form__box-02">
                                    <label for="">Apellido: <span style="color: red;">*</span></label>
                                    <input class="reply-form-input" type="text" name="apellido" aria-required="true" aria-invalid="false" placeholder="" id="apellido">
                                </div>
                                <div class="reply-form__box-01">
                                    <label for="">Email: <span style="color: red;">*</span></label>
                                    <input class="reply-form-input" type="email" name="email" aria-required="true" aria-invalid="false" placeholder="" id="email">
                                </div>
                                <div class="reply-form__box-02">
                                    <label for="">Teléfono: <span style="color: red;">*</span></label>
                                    <input class="reply-form-input" type="text" name="phone" aria-required="true" aria-invalid="false" placeholder="" id="phone">
                                </div>
                                <div class="reply-form__box-04">
                                    <label for="">Institución educativa: <span style="color: red;">*</span></label>
                                    <input class="reply-form-input" type="text" name="institucion" aria-required="true" aria-invalid="false" placeholder="" id="institucion">
                                </div>
                                <div class="reply-form__box-04">
                                    <label for="">Cargo: <span style="color: red;">*</span></label>
                                    <select class="reply-form-select" id="cargo" name="cargo">
                                      <option value="null">-Seleccionar-</option>
                                      <option value="rector">Rector</option>
                                      <option value="profesor">Docente</option>
                                      <option value="Administración">Personal administrativo</option>
                                      <option value="estudiante">Estudiante</option>
                                    </select>
                                </div>
                                <div class="reply-form__box-04">
                                    <label for="">Estado / Provincia: <span style="color: red;">*</span></label>
                                    <select class="reply-form-select" id="provincia" name="provincia">
                                      <option value="null">-Seleccionar-</option>
                                      <option value="Azuay">Azuay</option>
                                      <option value="Bolívar">Bolívar</option>
                                      <option value="Cañar">Cañar</option>
                                      <option value="Carchi">Carchi</option>
                                      <option value="Chimborazo">Chimborazo</option>
                                      <option value="Cotopaxi">Cotopaxi</option>
                                      <option value="El Oro">El Oro</option>
                                      <option value="Esmeraldas">Esmeraldas</option>
                                      <option value="Galápagos">Galápagos</option>
                                      <option value="Guayas">Guayas</option>
                                      <option value="Imbabura">Imbabura</option>
                                      <option value="Loja">Loja</option>
                                      <option value="Los Ríos">Los Ríos</option>
                                      <option value="Manabí">Manabí</option>
                                      <option value="Morona-Santiago">Morona-Santiago</option>
                                      <option value="Napo">Napo</option>
                                      <option value="Orellana">Orellana</option>
                                      <option value="Pastaza">Pastaza</option>
                                      <option value="Pichincha">Pichincha</option>
                                      <option value="Santa Elena">Santa Elena</option>
                                      <option value="Santo Domingo de los Tsáchilas">Santo Domingo de los Tsáchilas</option>
                                      <option value="Sucumbíos">Sucumbíos</option>
                                      <option value="Tungurahua">Tungurahua</option>
                                      <option value="Zamora-Chinchipe">Zamora-Chinchipe</option>
                                    </select>
                                </div>
                                <div class="reply-form__box-04">
                                    <label for="">Ciudad: <span style="color: red;">*</span></label>
                                    <input class="reply-form-input" type="text" name="ciudad" aria-required="true" aria-invalid="false" placeholder="" id="ciudad">
                                </div>

                                <div class="reply-form__box-01">
                                    <div class="g-recaptcha" data-sitekey="6Le7g3IUAAAAAMy6s9Ffs2P8Dc8mOHtOlFJ4k7Xt"></div>
                                </div>
                                <div class="reply-form__box-02">
                                    <button class="btn-01" type="submit" style="float: right; padding-left: 20px; padding-right: 20px;">Enviar Solicitud</button>
                                    <div style="clear: both;"></div>
                                </div>
                                <div style="clear: both;"></div>
                            <!-- </form> -->
                            {!! Form::close() !!}
                            <div style="position: relative;">
                                <div class="result"></div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                    </div>
                  
                </div>
            </div>
        </main>
<!-- Footer -->
        <footer class="wrapp-footer">
            <div class="footer-box-01">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <a href="./" class="footer-logo">
                                <img src="{{asset('assets/img/logoprincipal2.png')}}" alt="">
                            </a>
                            <ul class="widget-contact">
                                <!-- <li>
                                    <h4 class="widget-contact__title">Ubicación:</h4>
                                    <p class="widget-contact__text">Calle Oliva Miranda y Alberto Oblete, Manta - Ecuador</p>
                                </li> -->
                                <li>
                                    <h4 class="widget-contact__title">Horarios de atención:</h4>
                                    <p class="widget-contact__text">8:00 - 18:00</p>
                                </li>
                                <li style="display: none;">
                                    <h4 class="widget-contact__title">Teléfono:</h4>
                                    <p class="widget-contact__text">0980761961</p>
                                </li>
                                <li>
                                    <h4 class="widget-contact__title">Email:</h4>
                                    <p class="widget-contact__text">
                                        <a class="widget-contact__text-email" href="mailto:thesmartedu@edu.com">soporte@runachay.com.ec</a>
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="col-sm-3 col-md-3 col-lg-3">
                            <p>Módulos</p>
                            <ul>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                                <li><a href="#">Modulo 1</a></li>
                            </ul>
                        </div> -->
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="reply-form">
                                <p style="font-size: 28px; color: #fff;">Póngase en <strong>contacto</strong> con nosotros</p>
                                <div role="form" class="form-in-wrapp" id="form-in-wrapp-f1053-p976-o1" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <div id="note"></div>
                                    <div id="fields">
                                        <!-- <form id="ajax-contact-form" action="javascript:void(0);" method="post" novalidate="novalidate" class="reply-form__form"> -->
                                        {!! Form::open(['route'=>'paginas.enviarmensaje','method'=>'POST','class'=>'form-contacto contact-form columns_p_5 inited','data-formtype'=>'contact']) !!}
                                            <div class="reply-form__box-01 name">
                                                <input type="text" class="form-control reply-form__name form-in-wrapp-validates-as-required" placeholder="Nombre" aria-required="true" id="name" name="name">
                                            </div>
                                            <div class="reply-form__box-02 your-email">
                                                <input type="text" class="form-control reply-form__email form-in-wrapp-validates-as-required form-in-wrapp-validates-as-email" placeholder="E-mail" aria-required="true" id="email" name="email">
                                            </div>
                                            <div class="reply-form__box-04 name">
                                                <input type="text" class="form-control reply-form__name form-in-wrapp-validates-as-required" placeholder="Ciudad" aria-required="true" id="ciudad" name="ciudad">
                                            </div>
                                            <div class="reply-form__box-04 your-message">
                                                <textarea aria-required="true" rows="10" cols="30" class="form-control reply-form__message" id="message" name="message" placeholder="Mensaje..."></textarea>
                                            </div>
                                            <div class="reply-form__box-01">
                                                <div class="g-recaptcha" data-sitekey="6Le7g3IUAAAAAMy6s9Ffs2P8Dc8mOHtOlFJ4k7Xt"></div>
                                            </div>
                                            <div class="reply-form__box-02">
                                                <button type="submit" class="theme_button btn-01" id="submit_btn" style="float: right;">Enviar mensaje</button>
                                            </div>
                                        <!-- </form> -->
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                            <div style="position: relative;">
                                <div class="result"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-box-02">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <p class="copy-info">&copy; 2020 Runachay. Todos los derechos reservados</p>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4 text-center">
                            <ul class="social-list-01">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="footer-info">
                                <a class="footer-info__01" href="#">Política de privacidad</a>
                                <a class="footer-info__02" href="#">Términos de uso</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="back2top" title="Back to Top">Ir arriba</a>
        </footer>

        
    </div>


    
    <!-- JQuery v2.2.4 -->
    <script src="{{asset('assets/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->
    <script src="{{asset('assets/js/jquery.mb.YTPlayer.js')}}"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="{{asset('assets/js/jquery.counterup.js')}}"></script>
    <script>
        jQuery(document).ready(function($) {
            $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
        });
    </script>
    <script>
        $(window).load(function() {
            // Animate loader off screen
            $(".se-pre-con").delay(800).fadeOut("slow");
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.parallax-btn').on('click', function(e) {
              e.preventDefault();
              $('.modal').toggleClass('is-visible');
            });
        })
    </script>
    <script>
        $(document).ready(function(){
            var anchoi=$(".cont-cont").width();
               var radioi=0.5625;
               var resuli = anchoi*radioi;
               $(".cont-cont").css('height', resuli);

            $(window).resize(function(){
               var alto=$(".cont-cont").height();
               var ancho=$(".cont-cont").width();
               var radio=0.5625;
               var resul = ancho*radio;
               $(".cont-cont").css('height', resul);
            });
        })
    </script>


    <script src="{{asset('assets/js/scrollreveal.js')}}"></script>
    <script>
        window.sr = ScrollReveal();

        $(document).ready(function(){
            $(window).scroll(function(event) {
                var dist = 550;
                var scrollTop = $(window).scrollTop();
                if (scrollTop > dist) {
                    sr.reveal('.imag-ce', {duration: 800, delay: 0, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado1', {duration: 800, delay: 500, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado2', {duration: 800, delay: 600, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado3', {duration: 800, delay: 700, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado4', {duration: 800, delay: 800, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado8', {duration: 800, delay: 900, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado7', {duration: 800, delay: 1000, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado6', {duration: 800, delay: 1100, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado5', {duration: 800, delay: 1200, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.lado9', {duration: 800, delay: 1300, distance: '0px', scale: 0.8, viewFactor: 0});
                    sr.reveal('.modulo-dots', {duration: 1000, delay: 1500, distance: '0px', scale: 1, opacity:0});
                }
            });
        });
        // sr.reveal('.foot-video', {duration:1000, delay: 0, distance: '0px', scale: 0.8, viewFactor: 0});

    </script>

    <script src="{{asset('assets/js/bootstrap.js')}}"></script>
    <!-- Superfish v1.7.9 -->
    <script src="{{asset('assets/js/plugins/jquery.superfish.min.js')}}"></script>
    <!-- Owl carousel v2.2.1 -->
    <script src="{{asset('assets/js/plugins/jquery.owl.carousel.min.js')}}"></script>
    <!-- Waypoint -->
    <script src="{{asset('assets/js/plugins/jquery.waypoint.min.js')}}"></script>
    <!-- Progress circle -->
    <script src="{{asset('assets/js/plugins/jquery.progress.circle.min.js')}}"></script>
    <!-- Typed v2.0.4 -->
    <script src="{{asset('assets/js/plugins/jquery.typed.min.js')}}"></script>
    <script src="{{asset('assets/js/smooth-scroll.polyfills.js')}}"></script>
    <script>
        var scroll = new SmoothScroll('a[href*="#"]');
        // scroll.animateScroll(1750);
    </script>

    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/jquery.scroll-reveal.js"></script> -->
    <!-- Main script -->
    <script src="{{asset('assets/js/main.js')}}"></script>

    <script>
        $(document).ready(function(){
            jQuery(function(){
              jQuery("#P1").YTPlayer();
            });
        })
    </script>

    <script>
        $(document).ready(function(){
           $(window).bind('scroll', function() {
           var distance = 50;
                 if ($(window).scrollTop() > distance) {
                     $('.main-nav').addClass('scrolled');
                    $('.main-nav > div').addClass('paddingtop');
                    $('.navbar-brand').addClass('brand-scrolled');
                 }
                 else {
                     $('.main-nav').removeClass('scrolled');
                    $('.main-nav > div').removeClass('paddingtop');
                    $('.navbar-brand').removeClass('brand-scrolled');
                 }
            });
        });
    </script>
<!--     <script>
        $('.ancla').on('click', function(e){
            e.preventDefault();
            var strAncla = '#' + $(this).data('ancla');
            $('html,body').animate({scrollTop: $(strAncla).offset().top}, 600);
        });
    </script> -->
    <script>
        $(document).ready(function(){
            $(window).scroll(function(){
                var barra = $(window).scrollTop();
                var posicion =  (barra * 0.20);
                $('.move').css({
                    'background-position': '0 -' + posicion + 'px'
                });
            });
        })
    </script>
    <script>
        $(document).ready(function($) {
            var Body = $('#covel');
            Body.addClass('covel');
        });
        $(window).load(function() {
            $('.covel').fadeOut();
            $('body').removeClass('covel');
        });
    </script>
    <script>
        jQuery('form.contact-form').on('submit', function( e ){
            e.preventDefault();
            var $form = jQuery(this);
            //jQuery($form).find('span.contact-form-respond').remove();/
            //if one of form fields is empty - exit
            if ($form.find('.result').hasClass('sc_infobox_style_error')) {
                $form.find('.result').removeClass('sc_infobox_style_success');
                return;
            };
            //sending form data to PHP server if fields are not empty
            $form.find('.result').removeClass('sc_infobox_style_error');
            $form.find('.result').css('display', "block");
            var request = $form.serialize();
            console.log(request);
            var ajax = jQuery.post( "enviarmensaje", request )
                .done(function( data ) {
                        if (data==0){
                            $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #fff; border-radius: 5px; padding: 5px; text-align: center;''><strong>Mensaje enviado correctamente, pronto nos pondremos en contacto con usted.</strong></div>" );
                            $('form.contact-form')[0].reset();
                            grecaptcha.reset();
                            // location.reload();
                        }
                        if (data==1){
                            $( ".result" ).append( "<strong>Robot verification failed, please try again.</strong>" );
                        }
                        if (data==2) {
                            $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #fff; border-radius: 5px; padding: 5px; text-align: center;''><strong>El CAPTCHA es obligatorio</strong></div>" );
                        }
                        console.log(data);
            })
                .fail(function( data ) {
                    $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #fff; border-radius: 5px; padding: 5px; text-align: center;''><strong>Fallo al enviar el mensaje.</strong></div>" );
                    console.log(data);
            })
            jQuery('.result').fadeIn().delay(5000).fadeOut();
            $('.result').empty();
        });
    </script>
    <script>
        jQuery('form.contact-form2').on('submit', function( e ){
            e.preventDefault();
            var $form = jQuery(this);
            //jQuery($form).find('span.contact-form-respond').remove();/
            //if one of form fields is empty - exit
            if ($form.find('.result').hasClass('sc_infobox_style_error')) {
                $form.find('.result').removeClass('sc_infobox_style_success');
                return;
            };
            //sending form data to PHP server if fields are not empty
            $form.find('.result').removeClass('sc_infobox_style_error');
            $form.find('.result').css('display', "block");
            var request = $form.serialize();
            console.log(request);
            var ajax = jQuery.post( "enviarsolicitud", request )
                .done(function( data ) {
                        if (data==0){
                            jQuery(function($){
                                $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #fff; border-radius: 5px; padding: 5px; text-align: center;''><strong>Mensaje enviado correctamente, pronto nos pondremos en contacto con usted.</strong></div>" );
                                $('form.contact-form2')[0].reset();
                                grecaptcha.reset();
                            });
                            
                            // location.reload();
                        }
                        if (data==1){
                            jQuery(function($){
                                $( ".result" ).append( "<strong>Robot verification failed, please try again.</strong>" );
                            });
                        }
                        if (data==2) {
                            jQuery(function($){
                                $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #fff; border-radius: 5px; padding: 5px; text-align: center;''><strong>El CAPTCHA es obligatorio</strong></div>" );
                            });
                        }
                        console.log(data);
            })
                .fail(function( data ) {
                    jQuery(function($){
                        $( ".result" ).append( "<div style='position: absolute; width:100%; margin: 3px; background-color: rgba(25,25,52,0.2); color: #fff; border-radius: 5px; padding: 5px; text-align: center;''><strong>Fallo al enviar el mensaje.</strong></div>" );
                        console.log(data);
                    });
            })
            jQuery('.result').fadeIn().delay(5000).fadeOut();
            jQuery(function($){
                $('.result').empty();
            })
        });
    </script>
</body>
</html>