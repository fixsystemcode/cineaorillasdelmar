<!-- <?php $acep = Auth::user(); ?> -->
@if($acep->id_tipo_usuario == 7)
  <li {{ (Request::is("formfacturacion") ? 'class=active' : '') }}>
      <a href="{{ URL::asset('formfacturacion') }}"><i class="fa fa-file-text-o"></i>
        <span class="nav-label">Registros </span>
      </a>
  </li>
  <li {{ (Request::is("formfacturacion") ? 'class=active' : '') }}>
      <a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="nav-label">UE</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
               <li {{ (Request::is("formfacturacion/48") ? 'class=active' : '') }}><a href="{{ URL::asset('formfacturacion/48')}}" ><i class="fa fa-cart-arrow-down"></i>Mercedarias</a></li>
        </ul>
  </li> 
@elseif($acep->id_tipo_usuario == 5) 
   <li {{ (Request::is("administracion") ? 'class=active' : '') }}>
      <a href="{{ URL::asset('administracion') }}"><i class="fa fa-file-text-o"></i>
        <span class="nav-label">Inicio </span>
      </a>
  </li>

  <li {{ (Request::is("articulos","categorias","tags" ) ? 'class=active' : '') }}>
      <a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="nav-label">Contenido</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
               <li {{ (Request::is("articulos") ? 'class=active' : '') }}><a href="{{ URL::asset('articulos')}}" ><i class="fa fa-cart-arrow-down"></i>Artículos</a></li>
        </ul>
  </li> 
  
@elseif($acep->id_tipo_usuario == 4)

  <li {{ (Request::is("administracion") ? 'class=active' : '') }}>
      <a href="{{ URL::asset('administracion') }}"><i class="fa fa-file-text-o"></i>
        <span class="nav-label">Inicio </span>
      </a>
  </li>
 
  
  <li {{ (Request::is("usuarios","tipos-usuarios","solicitudes") ? 'class=active' : '') }}>
      <a href="#"><i class="fa fa-users fa-1x"></i> <span class="nav-label">Gestor de Usuarios</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
               <li {{ (Request::is("usuarios") ? 'class=active' : '') }}><a href="{{ URL::asset('usuarios')}}" ><i class="fa fa-user"></i> Usuarios</a></li>
               <li {{ (Request::is("tipos-usuarios") ? 'class=active' : '') }}><a href="{{ URL::asset('tipos-usuarios') }}"><i class="fa fa-user-plus"></i>Tipos de Usuarios</a></li>
               <li {{ (Request::is("solicitudes") ? 'class=active' : '') }}><a href="{{ URL::asset('solicitudes') }}"><i class="fa fa-user-plus"></i>Solicitudes</a></li>
        </ul>
  </li>

  <li {{ (Request::is("usuarios","tipos-usuarios","solicitudes") ? 'class=active' : '') }}>
      <a href="#"><i class="fa fa-users fa-1x"></i> <span class="nav-label">Gestor Eventos</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
               <li {{ (Request::is("tags") ? 'class=active' : '') }}><a href="{{ URL::asset('ponentes')}}" ><i class="fa fa-check-circle-o"></i>Ponentes</a></li>
               <li {{ (Request::is("tags") ? 'class=active' : '') }}><a href="{{ URL::asset('webinar')}}" ><i class="fa fa-check-circle-o"></i>Eventos</a></li>
        </ul>
  </li>

  <li {{ (Request::is("articulos","categorias","tags","activaciones" ) ? 'class=active' : '') }}>
      <a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="nav-label">Contenido</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
               <li {{ (Request::is("articulos") ? 'class=active' : '') }}><a href="{{ URL::asset('articulos')}}" ><i class="fa fa-cart-arrow-down"></i>Artículos</a></li>
               <li {{ (Request::is("categorias") ? 'class=active' : '') }}><a href="{{ URL::asset('categorias')}}" ><i class="fa fa-check-circle-o"></i>Categorias</a></li>
               
               <li {{ (Request::is("tags") ? 'class=active' : '') }}><a href="{{ URL::asset('tags')}}" ><i class="fa fa-check-circle-o"></i>Etiquetas</a></li>
               <li {{ (Request::is("activaciones") ? 'class=active' : '') }}><a href="{{ URL::asset('activaciones')}}" ><i class="fa fa-check-circle-o"></i>Activar casas</a></li>
        </ul>
  </li> 

  <li {{ (Request::is("animacion","imagen","video" ) ? 'class=active' : '') }}>
      <a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="nav-label">Módulos</span> <span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
               <li {{ (Request::is("categoriainterna") ? 'class=active' : '') }}><a href="{{ URL::asset('categoriainterna')}}" ><i class="fa fa-cart-arrow-down"></i>Categorias</a></li>
               <li {{ (Request::is("animacion") ? 'class=active' : '') }}><a href="{{ URL::asset('animacion')}}" ><i class="fa fa-cart-arrow-down"></i>Slider</a></li>
               <li {{ (Request::is("imagen") ? 'class=active' : '') }}><a href="{{ URL::asset('imagen')}}" ><i class="fa fa-check-circle-o"></i>Imagenes</a></li>
               <li {{ (Request::is("video") ? 'class=active' : '') }}><a href="{{ URL::asset('video')}}" ><i class="fa fa-check-circle-o"></i>Videos</a></li>
        </ul>
  </li>


  @else
  <li {{ (Request::is("formregistro") ? 'class=active' : '') }}>
      <a href="{{ URL::asset('formregistro') }}"><i class="fa fa-file-text-o"></i>
        <span class="nav-label">Registros </span>
      </a>
  </li>
  
@endif