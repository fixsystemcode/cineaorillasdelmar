<!DOCTYPE html>
<html lang="es">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>La Ciudad | @yield('titulo')</title>
   <link rel="icon" href="/assets/images/logo.png" sizes="32x32" />
  <!-- PLUGINS CSS STYLE -->
   
{!! Html::style('/assets/plugins/jquery-ui/jquery-ui.css') !!}
{!! Html::style('/assets/plugins/bootstrap/css/bootstrap.min.css') !!}
{!! Html::style('/assets/plugins/font-awesome/css/font-awesome.min.css') !!}
{!! Html::style('/assets/plugins/rs-plugin/css/settings.css') !!}
 
{!! Html::style('/assets/plugins/datepicker/datepicker.css') !!}
{!! Html::style('/assets/plugins/isotope/jquery.fancybox.css') !!}
{!! Html::style('/assets/plugins/isotope/isotope.css') !!}
{!! HTML::style('admin/plantilla/css/plugins/selective/selectize.bootstrap3.css'); !!}
{!! HTML::style('/assets/fin/css/principal.css'); !!}
{!! HTML::style('/assets/fin/css/style.css'); !!}

 

 
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  
 
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@CasasVacacional">
    <meta name="twitter:creator" content="@CasasVacacional">
    <meta name="twitter:title" content="<?php if (isset($articulo->titulo)) { echo "$articulo->titulo";} ?> ">
    <meta name="twitter:image" content="<?php if (isset($imagen)) { echo "$imagen";} ?>">
    <meta name="twitter:description" content="<?php if (isset($texto)) { echo "$texto";} ?>">

  {!! Html::style('/assets/css/colors/default.css') !!}
  {!! Html::style('/assets/css/style.css') !!}

 
  <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>

  
  
  <script src="{{ asset('local/public/vendors/ckeditor/ckeditor.js') }}"></script>

  @yield('script')

<!-- llamamos la librería jquery  desde sus cdn Hosted-->
  {!! Html::script('admin/plantilla/js/jquery-2.1.1.js') !!}

  {!! Html::script('admin/plantilla/js/plugins/selective/selectize.js') !!}

  
 

<link rel="shortcut icon" type="image/png" href="http://laciudad.ec/assets/img/favicon.png"/>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
 
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.10&appId=644942119027203";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92258565-1', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>


 

  <div class="main-wrapper">

    <!-- HEADER -->
    <header class="gradient">
      @include('layouts.menu')
    </header>

    <!-- PAGE TITLE -->
    @yield('contenido')
   

    <!-- FOOTER -->
     <footer>
       
      <div class="copyRight clearfix">
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-push-6 col-xs-12">
              
            </div>
            <div class="col-sm-6 col-sm-pull-6 col-xs-12">
              <div class="copyRightText">
                <p>Copyright © 2017. Todos los derechos reservados por<a target="_blank" href="#">La Ciudad</a>.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  

  </div>

  <!-- INQUIRY MODAL -->
  <div class="modal fade bookingModal modalBook" id="inquiryModal" tabindex="-1">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Realiza tu reservación</h4>
        </div>
        <div class="modal-body">
          <form action="#">
            <div class="form-group">
              <input type="text" class="form-control" id="yourName" placeholder="Nombre">
            </div>
            <div class="form-group">
              <input type="email" class="form-control" id="yourEmail" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="phoneNumber" placeholder="Número">
            </div>
            <div class="form-group">
              <textarea class="form-control" rows="3" placeholder="Mensaje"></textarea>
            </div>
            <button type="submit" class="btn buttonCustomPrimary">Enviar</button>
          </form>
        </div>
      </div>
    </div>
</div>



{!! Html::script('/assets/js/blazy.min.js') !!}
    <script type="text/javascript">

    var plugin_path = '/assets/plugins/';

    var bLazy = new Blazy({
        selector: 'img'
    });

    </script>


  <!-- JAVASCRIPTS -->
 
  {!! Html::script('assets/js/jquery.min.js') !!} 
  {!! Html::script('assets/plugins/jquery-ui/jquery-ui.js') !!}

  {!! Html::script('assets/plugins/bootstrap/js/bootstrap.min.js') !!}
  {!! Html::script('assets/plugins/rs-plugin/js/jquery.themepunch.tools.min.js') !!}
  {!! Html::script('assets/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js') !!} 
  {!! Html::script('assets/plugins/selectbox/jquery.selectbox-0.1.3.min.js') !!} 
  {!! Html::script('assets/plugins/datepicker/bootstrap-datepicker.js') !!} 
  {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js') !!} 
  {!! Html::script('assets/plugins/counter-up/jquery.counterup.min.js') !!} 
  {!! Html::script('assets/plugins/isotope/isotope.min.js') !!} 
  {!! Html::script('assets/plugins/isotope/jquery.fancybox.pack.js') !!} 
  {!! Html::script('assets/plugins/isotope/isotope-triger.js') !!} 
  {!! Html::script('assets/plugins/countdown/jquery.syotimer.js') !!} 
  {!! Html::script('assets/js/custom.js') !!} 
  


 
 @yield('script2')

</body>

</html>