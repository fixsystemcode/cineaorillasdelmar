<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Administración de sitio web | FIXSYSTEM</title>

    {!! Html::style('admin/plantilla1/css/bootstrap.min.css') !!}
    {!! Html::style('admin/plantilla1/font-awesome/css/font-awesome.css') !!}


    <!-- Toastr style -->
    {!! Html::style('admin/plantilla1/css/plugins/toastr/toastr.min.css') !!}


    <!-- Gritter -->
    {!! Html::style('admin/plantilla1/js/plugins/gritter/jquery.gritter.css') !!}


    {!! Html::style('admin/plantilla1/css/animate.css') !!}
    {!! Html::style('admin/plantilla1/css/style.css') !!}

 <!-- Data Tables -->
 {!! Html::style('admin/plantilla1/css/plugins/dataTables/dataTables.bootstrap.css') !!}
 {!! Html::style('admin/plantilla1/css/plugins/dataTables/dataTables.responsive.css') !!}
 {!! Html::style('admin/plantilla1/css/plugins/dataTables/dataTables.tableTools.min.css') !!}

  {!! Html::style('admin/plantilla1/css/plugins/blueimp/css/blueimp-gallery.min.css') !!}






    <!-- Mainly scripts -->
     {!! Html::script('admin/plantilla1/js/jquery-2.1.1.js') !!}

     <script src="{{asset('assets/js/jquery.table2excel.js')}}"></script>

     
     {!! Html::script('admin/plantilla1/js/bootstrap.min.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/metisMenu/jquery.metisMenu.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/slimscroll/jquery.slimscroll.min.js') !!}

    <!-- Flot -->
     {!! Html::script('admin/plantilla1/js/plugins/flot/jquery.flot.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/flot/jquery.flot.tooltip.min.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/flot/jquery.flot.spline.js') !!}
        {!! Html::script('admin/plantilla1/js/plugins/flot/jquery.flot.resize.js') !!}
         {!! Html::script('admin/plantilla1/js/plugins/flot/jquery.flot.pie.js') !!}


     {!! Html::script('admin/plantilla1/js/plugins/flot/jquery.flot.pie.js') !!}


    <!-- Peity -->
     {!! Html::script('admin/plantilla1/js/plugins/peity/jquery.peity.min.js') !!}
     {!! Html::script('admin/plantilla1/js/demo/peity-demo.js') !!}

    <!-- Custom and plugin javascript -->
    {!! Html::script('admin/plantilla1/js/inspinia.js') !!}
    {!! Html::script('admin/plantilla1/js/plugins/pace/pace.min.js') !!}


    <!-- jQuery UI -->
    {!! Html::script('admin/plantilla1/js/plugins/jquery-ui/jquery-ui.min.js') !!}


    <!-- GITTER -->
    {!! Html::script('admin/plantilla1/js/plugins/gritter/jquery.gritter.min.js') !!}


    <!-- Sparkline -->
    {!! Html::script('admin/plantilla1/js/plugins/sparkline/jquery.sparkline.min.js') !!}


    <!-- Sparkline demo data  -->
     {!! Html::script('admin/plantilla1/js/demo/sparkline-demo.js') !!}


    <!-- ChartJS-->
    {!! Html::script('admin/plantilla1/js/plugins/chartJs/Chart.min.js') !!}


    <!-- Toastr -->
    {!! Html::script('admin/plantilla1/js/plugins/toastr/toastr.min.js') !!}

    <!-- Chosen -->
    {!! Html::script('admin/plantilla1/js/plugins/chosen/chosen.jquery.js') !!}

    {!! HTML::style('admin/plantilla1/css/plugins/chosen/chosen.css'); !!}

     <!-- Data Tables -->
     {!! Html::script('admin/plantilla1/js/plugins/dataTables/jquery.dataTables.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/dataTables/dataTables.bootstrap.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/dataTables/dataTables.responsive.js') !!}
     {!! Html::script('admin/plantilla1/js/plugins/dataTables/dataTables.tableTools.min.js') !!}
        {!! Html::script('admin/plantilla1/js/plugins/selective/selectize.js') !!}

         {!! Html::script('admin/plantilla1/js/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}
         <!--Bootstrap Toggle-->
    {!! Html::style('admin/plantilla1/css/bootstrap-toggle.min.css') !!}
    {!! Html::script('admin/plantilla1/js/bootstrap-toggle.min.js') !!}


    <script src="{{ asset('local/public/vendors/ckeditor/ckeditor.js') }}"></script>
  {!! Html::script('admin/plantilla1/js/moment-with-locales.js') !!}
{!! Html::style('admin/plantilla1/css/bootstrap-datetimepicker.css') !!}
 {!! Html::script('admin/plantilla1/js/bootstrap-datetimepicker.js') !!}








    {!! HTML::style('admin/plantilla1/css/plugins/selective/selectize.bootstrap3.css'); !!}


     @yield('script')

      <script>
        $(document).ready(function() {
            $('#tbbuzon').dataTable({
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },

                    "order": ([ 0, 'desc' ])


            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#tbbuzon5').dataTable({
                "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },

                    "order": ([ 7, 'asc' ])


            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#tbinscritos').dataTable({
                "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },

                    "order": ([ 7, 'asc' ])


            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#tbregis').dataTable({
                "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },

                    "order": ([ 4, 'asc' ])


            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#tbasis').dataTable({
                "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },

                    "order": ([ 1, 'asc' ])


            });
        });

    </script>
    <script>
        $(document).ready(function() {
            $('#tbcalificaciones').dataTable({
                "lengthMenu": [[25, 50, 75, -1], [25, 50, 75, "All"]],
                responsive : true,
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },

                    "order": ([ 3, 'asc' ])


            });
        });

    </script>

<script>




$(document).ready(function() {


    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"100%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

});

</script>

</head>

<body>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> <span>
                            {!! Html::image('/assets/images/mantenimiento.png', '', ['class' => ' img-responsive']) !!}

                             </span>


                        </div>
                        <div class="logo-element">
                            FS
                        </div>
                    </li>

                    @include('layouts.menuback')




                </ul>

            </div>
        </nav>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Administración del sitio web.</span>
                </li>

                  <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        Ajustes
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">

                        <li class="divider"></li>

                        <li>
                            <div class="text-center link-block">
                                <a target= "_blank" href="{{ URL::asset('/')}}">
                                    <strong>Ir página web</strong>

                                </a>
                            </div>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="{{ URL::asset('logout')}}">
                        <i class="fa fa-sign-out"></i> Cerrar Sesión
                    </a>
                </li>


            </ul>

        </nav>
        </div>



                <div class="wrapper wrapper-content animated fadeInRight">


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>@yield('titulo') </h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>


                                    </div>
                                </div>
                                <div class="ibox-content">
                                       @yield('contenido')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





        </div>



    </div>

  {!! Html::script('/assets/js/blazy.min.js') !!}

  <script>
        $("#Btnxls").click(function(){
            $("#datosfact").table2excel({
                exclude: ".excludeThisClass",
                name: "Worksheet Name",
                filename: "Registros" //do not include extension
            });
        })
    </script>


        <script type="text/javascript">

        var plugin_path = 'assets/plugins/';

        var bLazy = new Blazy({
            selector: 'img'
        });

        </script>

</body>
</html>
