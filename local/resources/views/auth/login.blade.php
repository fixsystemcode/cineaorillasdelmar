<!DOCTYPE html>
<html>

<head>


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <title>Administración de sitio web | FIXSYSTEM</title>

    {!! Html::style('admin/plantilla1/css/bootstrap.min.css') !!}
    {!! Html::style('admin/plantilla1/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('admin/plantilla1/css/animate.css') !!}
    {!! Html::style('admin/plantilla1/css/style.css') !!}
    <link rel="icon" href="admin/images/logo.png" sizes="32x32" />
    <script src="https://apis.google.com/js/platform.js" async defer></script>
<meta name="google-signin-client_id" content="178116179274-neq43nhh3i2mvi7e941ucmqee7c5e52u.apps.googleusercontent.com">
<meta name="google-signin-scope" content="profile email">

</head>


<body class="" style="background-color: #44a6d6;">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">
                <p>{!! Html::image('admin/images/logo.png', '', [ ]) !!}</p>

                </h1>

            </div>

            <h3 class="text-registro" style="color: white;"> Bienvenido</h3>
                         @include('alerts.errors')
                          @include('alerts.request')
                          @include('alerts.success')

            <p style="font-size: 14px;color: white;">Ingrese su email y constraseña.</p>

            <form action="{{ url('/login') }}" role="form" method="POST">

                 {!! csrf_field() !!}

                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Ingrese email" style="text-align: center;"  >
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password"  placeholder="Ingrese su contraseña" style="text-align: center;">

                </div>

                <button type="submit" class="btn btn-success block full-width m-b">Ingresar</button>


                <!-- <a style="font-size: 16px;color: white;" href="fb"><button type="button" class="btn btn-success block full-width m-b">Iniciar Con Facebook</button></a>
                <div class="g-signin2" data-onsuccess="onSignIn" align="center"></div> -->
                <br>

       <!--          <a href="#" onclick="signOut();">Sign out</a>
                <br> -->


                <a style="font-size: 16px;color: white;" href="{{url('contrasena')}}"><small>¿Olvidé mi contraseña?</small></a>



            </form>
            <p class="m-t" style="font-size: 14px;color: white;"> <small>Diseñado por Fixsystem</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    {!! Html::script('admin/plantilla1/js/jquery-2.1.1.js') !!}
    {!! Html::script('admin/plantilla1/js/bootstrap.min.js') !!}

    <script type="text/javascript">
        function val(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla==8) return true;
            var patron =/[0-9]/;
            var te = String.fromCharCode(tecla);
            return patron.test(te);
        }


    </script>

    <script>/*
        gapi.load('auth2', function() {
        gapi.auth2.init({
            client_id: '178116179274-neq43nhh3i2mvi7e941ucmqee7c5e52u.apps.googleusercontent.com',
            //This two lines are important not to get profile info from your users
            fetch_basic_profile: false,
            scope: 'email'
        });
      });



      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:

        if (GoogleAuth.isSignedIn.get()) {
            alert("Logeado");
            var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
        //window.location="http://byronsm12.com:8081/laciudad/goo";
        }else{
            alert(" no Logeado");
        }


    };
    function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
          console.log('User signed out.');
      });
    }
*/
</script>

</body>


</html>