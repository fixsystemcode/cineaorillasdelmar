
 


<section>
	<div class="container">
	  			<!-- COMMENTS SHORTCODEs -->
	  			<h2 class="with-breaker animate-me fadeInUp animated" style="text-transform: uppercase;
    font-weight: 700; text-align: center; visibility: visible; animation-name: fadeInUp;">
		  			Noticias
	  			</h2>
	</div>
				<div class="container">

					<div class="row">

						
						
						<div class="col-md-12 col-sm-12"> 

						
							<!-- THREE COLUMNS -->
							<div class="row">
								<!-- article item -->

								@if (count($noticias))
									@foreach ($noticias as $noticia)
										<div class="col-md-3 col-ms-2 col-xs-6">

											<!-- article item -->
											<div class="item-box">

												@if(empty($noticia->path))
														<a href="noticias/{{$noticia->slug}}">
									                	   <img class="wow fadeInDown"  style="width: 100%;height: 170px!important;object-fit: cover!important;"
									                	   data-src="{{asset('local/public/images/colagevarios.jpg')}}"  style ="width: 100%"> 
									                	</a>
									                @else
									              
									                    <a href="noticias/{{$noticia->slug}}">
														<img class="wow fadeInDown" style="width: 100%;height: 170px!important;object-fit: cover!important;"

														alt="{{$noticia->path}}" class="img-responsive thumbnail" data-src="{{asset('local/public/images')}}/{{$noticia->path}}"  >
												 		</a>
												  
									                @endif

											
													<div class="item-box-desc">
														<h4 style="text-align: center;">
															<a style=" color: #76260f;border-bottom: 1px solid; font-size: 14px;"
															 href="noticias/{{$noticia->slug}}">{{($noticia->titulo)}}</a>
													    </h4>
																				
												  </div>
											</div>
										</div>

									@endforeach
								@else
									<p>No hay noticias</p>
								@endif
								<!-- article item -->
							
							</div>

							</div>



					
						<!-- RIGHT -->
					

					</div>


				</div>
</section>

<section  style="padding: 10px;">
	<div class="container">
	  	
	  	<div class="row">
	  		<div class="col-md-12">
				<img class="img-responsive" data-src="assets/images/oferta-educativa.jpg" alt="Oferta-Educativa">
			</div>
	  	</div>		

	</div>			
				
</section>

<section  style="padding: 10px;">
	<div class="container">
	  			<!-- COMMENTS SHORTCODEs -->
	  			<h2 class="with-breaker animate-me fadeInUp animated" style="text-align: center; visibility: visible; animation-name: fadeInUp;">
		  			<span> </span>
	  			</h2>
	
				

					<div class="row">

						
					
						<div class="col-md-6" >
							<h2 class="with-breaker animate-me fadeInDownBig animated" style=" text-transform: uppercase;
    font-weight: 700; text-align: center; visibility: visible; animation-name: fadeInUp;">
						  			Galerías
					  		</h2>
					  		@if (count($articulos))
									@foreach ($articulos as $articulo)
									
									<div class="col-md-4 col-sm-6">
										<div class="panel panel-default" >
										  <div class="panel-body" style="padding: 4px;">
										  	<a href="galeria/{{$articulo->slug}}">
										   	 <img style=" width: 100%; height: 100px!important;object-fit: cover!important;" alt="{{$articulo->slug}}" class="img-responsive" data-src="{{asset('local/public/images')}}/{{$articulo->path}}" title="{{$articulo->titulo}}" >
											</a>
										  </div>
										  
										</div>
									</div>

							@endforeach
							@else
							   
							@endif
						</div>

						<div class="col-md-6">
							<div class="row">
								<h2 class="with-breaker animate-me fadeInUp animated" style="text-transform: uppercase;
    font-weight: 700; text-align: center; visibility: visible; animation-name: fadeInUp;">
						  			Videos
					  			</h2>
					  			@if (count($videos))
									@foreach ($videos as $video)

										<div class="col-md-4 col-sm-6" style="padding: 10px;">

											<?php 

													$textoimg = substr($video->path, -11);
													//echo $textoimg;

											 ?>

											<a class="image-hover lightbox" href="<?php echo "{$video->path}";?>" data-plugin-options='{"type":"iframe"}'>
												<span class="image-hover-icon image-hover-dark"><!-- image-hover-light | image-hover-dark -->
													<i class="fa fa-youtube-square"><!-- video icon --></i>
												</span>
												<img class="img-responsive" src="https://i.ytimg.com/vi/{{$textoimg}}/hqdefault.jpg" alt="img" />
											</a>


										</div>


									 

									@endforeach
								@else
									<p>No hay videos</p>
								@endif

								
							</div>

							


						</div>

				
 

					

					</div>

	</div>	


				
</section>

 



<!-- WELCOME -->
