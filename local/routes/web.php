<?php

Route::get('seleccionarjornada/{id}', 'EventoAsistenciaController@seleccionjornada');
Route::get('asistencia/{id}', 'EventoAsistenciaController@consultaregistro');
Route::get('asistencia/{id}/{usuario}', 'EventoAsistenciaController@registroasistencia');
Route::resource('eventoasistencia', 'EventoAsistenciaController');

Route::get('/', 'PaginasController@index')->name('casa');

Route::post('enviarmensaje',[
	'uses' =>'PaginasController@enviarmensaje',
	'as'   =>'paginas.enviarmensaje'
]);
Route::post('enviarsolicitud',[
	'uses' =>'PaginasController@enviarsolicitud',
	'as'   =>'paginas.enviarsolicitud'
	]);
Route::post('contratarunachay',[
	'uses' =>'PaginasController@contratarunachay',
	'as'   =>'paginas.contratarunachay'
	]);
Route::post('registrarpersona',[
	'uses' =>'RegistroController@guardar',
	'as'   =>'registrarpersona.guardar'
]);
Route::post('registrarfacturacion',[
	'uses' =>'FacturacionController@store',
	'as'   =>'registrarfacturacion.store'
]);
Route::get('administracion', 'BackendController@index');
Route::get('/login', 'HomeController@index');
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Route::get('/home', 'HomeController@index')->name('home');
// Route::get('logout', 'LogController@logout');
Route::get('administracion', 'BackendController@index');
Route::resource('categorias','CategoriasController');
Route::get('categorias/{id}/destroy',[
	'uses' =>'CategoriasController@destroy',
	'as'   =>'categorias.destroy'
	]);
Route::resource('articulos', 'ArticulosController');
Route::get('articulos/{id}/destroy',[
			'uses' => 'ArticulosController@destroy',
			'as'   => 'articulos.destroy'
		]);
Route::resource('animacion', 'SliderController');
Route::get('animacion/{id}/destroy',[
			'uses' => 'SliderController@destroy',
			'as'   => 'animacion.destroy'
		]);
Route::resource('informacion', 'InformacionController');
Route::get('informacion/{id}/destroy',[
			'uses' => 'InformacionController@destroy',
			'as'   => 'informacion.destroy'
		]);
Route::get('imagen/{id}/destroy',[
			'uses' => 'ImagenesController@destroy',
			'as'   => 'imagen.destroy'
		]);
Route::resource('video', 'VideoController');
Route::get('video/{id}/destroy',[
			'uses' => 'VideoController@destroy',
			'as'   => 'video.destroy'
		]);
// Route::resource('galeria', 'FotosController');
// Route::get('galeria/{id}/destroy',[
			// 			'uses' => 'FotosController@destroy',
			// 			'as'   => 'galeria.destroy'
		// 		]);
Route::resource('tags', 'TagsController');
Route::get('tags/{id}/destroy',[
			'uses' => 'TagsController@destroy',
			'as'   => 'tags.destroy'
		]);
Route::resource('tipos-usuarios','TiposController');
Route::resource('usuarios','UsuariosController');
Route::resource('camposextrasgrupos','CamposExtrasGruposController');
Route::get('camposextrasgrupos/{id}/destroy',[
			'uses' => 'CamposExtrasGruposController@destroy',
			'as'   => 'camposextrasgrupos.destroy'
		]);
Route::resource('camposextras','CamposExtrasController');
Route::resource('obtener','AjaxController');
Route::get('camposextras/{id}/destroy',[
			'uses' => 'CamposExtrasController@destroy',
			'as'   => 'camposextras.destroy'
		]);
Route::resource('registro','RegistrosController');
Route::get('autorizado','RegistrosController@autorizado');
Route::get('confirmacion/{codigo}', 'RegistrosController@confirmar');
Route::get('contrasena','RegistrosController@contrasena');
Route::post('recuperacion', ['as' => 'GuardarRecuperacion', 'uses' => 'RegistrosController@guardarrecuperacion']);
Route::post('restablecer', ['as' => 'GuardarRestablecer', 'uses' => 'RegistrosController@guardarrestablecer']);
Route::get('env/{id}','RegistrosController@envio');
Route::get('reestablecer/{codigo}','RegistrosController@busqueda');
Route::resource('solicitudes','RegistrosController@solicitudes');
Route::get('habilitarusuario/{id}', ['as' => 'hab_user', 'uses' => 'RegistrosController@habilitar_usuario']);
Route::resource('activaciones','ArticulosController@activaciones');
Route::get('activararticulo/{id}', ['as' => 'hab_articulo', 'uses' => 'RegistrosController@habilitararticulo']);
//Route::post('registro','UsuariosController@registro');
//Route::get('registro', ['as' => 'registro', 'uses' => 'UsuariosController@registro']);
//////////////////
Route::resource('categoriainterna','CategoriaInternaController');
Route::get('categoriainterna/{id}/destroy',[
	'uses' =>'CategoriaInternaController@destroy',
	'as'   =>'categoriainterna.destroy'
	]);
//Route::get('/publica', 'LogController@publicar');
//Route::get('/goo', 'LogController@iniciagoogle');
Route::post('/upload', 'ArticulosController@subirfotogaleria');
Route::post('/uploaddoc', 'ArticulosController@subirdoc');
Route::get('/infoevento', 'PaginasController@infoeventos');
Route::get('/pruebaa', 'ArticulosController@pruebaa');
//Route::get('/contratarunachay', function(){return view('paginas/contratarrunachay'); });
//Route::get('/formulario', function(){return view('paginas/formulario'); });
Route::get('formfacturacion/{id}','FacturacionController@ue');
Route::resource('formregistro','RegistroController');
Route::resource('formfacturacion','FacturacionController');
//Route::get('/formulario-facturacion', function(){return view('paginas/facturacion'); });
// Route::get('/confirmacion', function(){
	// 	return view('paginas/confirmacion');
// });
//Route::get('/formulario-facturacion-mercedarias', function(){return view('paginas/facturacion/mercedarias'); });
// Route::get('/jornada-docentes', function(){
	// 	return view('paginas/conferencia.index');
// });
// Route::get('/confirmaregistro', 'WebinarRegistroController@confirmaregistro');
Route::get('/confirmaregistro/{id}',[
	'uses' => 'WebinarRegistroController@confirmaregistro',
	'as'   => 'webinar.confirmaregistro'
]);
//
Route::resource('WebinarRegistro','WebinarRegistroController');
// Route::get('/confirmacion/', 'WebinarRegistroController@store');
Route::get('lista-asistentes-excel','WebinarController@exportExcel')->name('asistentes.excel');
Route::resource('imagen', 'ImagenesController');
Route::resource('webinar', 'WebinarController');
Route::resource('ponentes', 'PonentesController');
Route::resource('asignarponentes', 'AsignarPonentesController');
Route::resource('asignardias', 'AsignarDiaController');
Route::resource('calificacionponente', 'CalificacionPonenteController');
Route::get('calificaciones-ponente/{id_dia_ponente}/{id_ponente}/',[
			'uses' => 'AsignarPonentesController@mostrar_calificaciones',
			'as'   => 'asignarponentes.mostrar'
		]);
Route::get('ponentes_dia/{id}/',[
			'uses' => 'WebinarController@add',
			'as'   => 'webinar.add'
		]);
Route::get('asistencias/{id}/',[
			'uses' => 'WebinarController@show_asistencia',
			'as'   => 'webinar.asis'
		]);

Route::get('imprimirqr/{id}/',[
			'uses' => 'WebinarRegistroController@descargarqr',
			'as'   => 'WebinarRegistro.qr'
		]);

Route::get('asistentes_cumplidos/{id}/',[
			'uses' => 'WebinarController@show_aprobados',
			'as'   => 'webinar.aprob'
		]);

Route::put('cambiarestado/{id}/',[
			'uses' => 'AsignarDiaController@cambiarestado',
			'as'   => 'asignardias.cambiarestado'
		]);
Route::get('dias_evento/{id}/',[
			'uses' => 'WebinarController@dates',
			'as'   => 'webinar.dates'
		]);
Route::get('asistencia_evento/{id}/',[
			'uses' => 'AsignarDiaController@asis',
			'as'   => 'asignardias.asis'
		]);
Route::get('destroyponente/{id}/',[
			'uses' => 'WebinarController@destroyponente',
			'as'   => 'webinar.destroyponente'
		]);
//Route::get('/transmision', function(){return view("paginas.transmision");});
// Route::get('/enviarasistencia', function(){return view("admin.webinar.enviarasistencia");});
Route::get('enviarasistencia/{id}/{salto}/',[
			'uses' => 'WebinarController@envasistencia',
			'as'   => 'webinar.envasist'
		]);
Route::get('enviarasistenciaunico/{id}/{id_reg}/',[
			'uses' => 'WebinarController@enviarAsistenciaUnico',
			'as'   => 'webinar.envasist1'
		]);

Route::get('enviarcertificado/{id}/{salto}/',[
			'uses' => 'WebinarController@envcertificado',
			'as'   => 'webinar.envcert'
		]);
Route::get('enviarcertificadounico/{id}/{id_reg}/',[
			'uses' => 'WebinarController@envcertificadounico',
			'as'   => 'webinar.envcertunico'
		]);


Route::get('{id}', 'WebinarRegistroController@registrowebinar');

Route::get('reserva/{id}', 'WebinarRegistroController@reserva');

Route::get('{id}/buscar_certificado', 'WebinarRegistroController@generar_certificado');

Route::get('buscarcertificado/{id_evento}/{id_asistente}', 'WebinarRegistroController@pagina_compartir');

Route::post('certificado/',[
			'uses' => 'WebinarRegistroController@certificado',
			'as'   => 'WebinarRegistro.certificado'
		]);

Route::post('descargar/',[
			'uses' => 'WebinarRegistroController@descargar',
			'as'   => 'WebinarRegistro.descargar'
		]);
