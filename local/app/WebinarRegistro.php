<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebinarRegistro extends Model
{
    //
    protected $table = 'asistentes_webinar';

    protected $fillable = ['id_webinar','nombres','cedula','apellidos','email', 'conf_email','pais','provincia','ciudad','telefono','institucion','cargo','runachay_item'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}