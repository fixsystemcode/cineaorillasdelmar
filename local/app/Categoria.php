<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = "categorias";
    protected $fillable = ['categoria'];

    public function articulos(){
    	return $this->hasMany('App\Articulo');
    }

    public function camposextrasgrupos()
    {
        return $this->belongsToMany('App\camposextrasgrupo','categoriacamposextrasgrupo','id_categoria');
    }

}
