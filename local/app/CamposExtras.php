<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CamposExtras extends Model
{
    //
     protected $table = 'camposextras';

   protected $fillable = ['icono','nombre','descripcion','valor','requerido','tipo','orden','visible','valor_predefinido','id_campos_extras_grupo'];

 	protected $hidden = ['remember_token'];
}
