<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaCampoExtraGrupo extends Model
{
    //
    protected $table = 'categoriacamposextrasgrupo';

    protected $fillable = ['id_categoria','id_campos_extras_grupo'];

 	protected $hidden = ['remember_token'];

}

