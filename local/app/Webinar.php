<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webinar extends Model
{
    //
    protected $table = 'webinar';

    protected $fillable = ['id','slug', 'tema','fecha','hora', 'anuncio', 'estado'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}