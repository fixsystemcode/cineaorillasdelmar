<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facturacion extends Model
{
    //
    protected $table = 'facturacion';

    protected $fillable = ['identificador','ue','ruc', 'telefono', 'direccion', 'razon', 'valor'];

    protected $hidden = ['g-recaptcha-response'];
}
