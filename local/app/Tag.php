<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
     //
    protected $table = 'tags';

    protected $fillable = ['tag','path'];

 	  protected $hidden = ['remember_token'];


  	public function setPathAttribute($path){

        if(!empty($path)){
          $nombre = $path->getClientOriginalName();
          $this->attributes['path'] = $nombre;
          \Storage::disk('local')->put($nombre, \File::get($path));
        }

     }


 	   public function articulos()
    {
        return $this->belongsToMany('App\Articulo');
    }

    public function scopeSeachTag($query,$tag){

    	return $query->where('tag','=', $tag);
    }
}
