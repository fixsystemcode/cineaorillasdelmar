<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsuarioCrearRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombres' => 'required',
            'apellidos'=>'required',
            'login' => 'required|unique:usuarios',
            'email' => 'required|email|unique:usuarios',
            'password' => 'required|min:6',


        ];
    }
}
