<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Dia_evento;
use View;
use Session;
use Redirect;
use File;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\PonentesActualizarRequest;
use App\Http\Requests\PonentesCrearRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AsistentesExport;




use Illuminate\Support\Facades\Input;


class AsignarDiaController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        //
        
         $dias_evento = dias_evento::all();
 
        return View::make('admin.dias_evento.index', compact('dias_evento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        //return view('admin.imagen.crear');
         return View::make('admin.dias_evento.crear');
    }

    public function asis($id)
    {

        $asistencia = DB::table("asistencia_evento")
                ->select("*")
                ->where("id_dia_evento",$id)
                ->get();

        $asistenciaex = DB::table("asistencia_evento")
                ->select("*")
                ->where("id_dia_evento",$id)
                ->get();

        
        //dd($asistenciaex);
         return view('admin.webinar.asistencia', compact('asistencia','asistenciaex'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $id = $request->get('id_webinar');
        $dia_evento = new Dia_evento($request->all());
        $dia_evento->id_webinar = $id;
        $dia_evento->slug = Str::random(10);

        // dd($dia_evento);
        $dia_evento->save();

        Session::flash('message','Dia del evento agregado correctamente');
        return redirect::to('dias_evento/'.$id);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
         
    }

    public function cambiarestado($id){

        $dia_evento= Dia_evento::find($id);
        //dd($dia_evento);
        if($dia_evento->estado_enlace == 1){
            $dia_evento->update(['estado_enlace'=>0]);
        }else{
            $dia_evento->update(['estado_enlace'=>1]);
        }
        
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($request, $id)
    {
         

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dia_evento::destroy($id);
        Session::flash('message','Dia eliminado Correctamente del webinar');
        return back();
       
    }
        
}