<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalificacionPonente extends Model
{
    //
    protected $table = 'calificacion_ponente';

    protected $fillable = ['id_dia_evento','id_ponente','calificacion'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}