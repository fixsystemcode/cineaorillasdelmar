<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Foto;
use App\Articulo;
use App\User;
use App\Categoria;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use DateTime;

use Mail;
use SendGrid;
use Illuminate\Support\Facades\Input;


use Validator; 

class ApiNoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function api()
    {
     

         $articulos = Articulo::select("articulos.id","articulos.titulo","articulos.texto","articulos.slug","articulos.created_at")
         ->whereraw('created_at > DATE_SUB(CURDATE(), INTERVAL 2 WEEK)')
         ->orderBy('articulos.id','DESC')
         ->get();

        
         //dd($articulos);
         return $articulos;

    }

     public function noticia()
    {
     
         $articulos = Articulo::select("articulos.id","articulos.titulo","articulos.texto","articulos.slug","articulos.created_at")
         ->whereraw('created_at > DATE_SUB(CURDATE(), INTERVAL 2 WEEK)')
         ->orderBy('articulos.id','DESC')
         ->get();
      

         //dd($articulos);
         return view('noticia', compact('articulos'));

    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
