<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PonenteAsignado;
use View;
use Session;
use Redirect;
use File;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\PonentesActualizarRequest;
use App\Http\Requests\PonentesCrearRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AsistentesExport;




use Illuminate\Support\Facades\Input;


class AsignarPonentesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        //
        
         $ponentes = Ponente::all();
 
        return View::make('admin.ponentes.index', compact('ponentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        //return view('admin.imagen.crear');
         return View::make('admin.ponentes.crear');
    }


    public function show($id)
    {
        // 
        //return view('admin.imagen.crear');


         return View::make('admin.webinar.calificaciones');
    }
    
    public function mostrar_calificaciones($id_dia_evento, $id_ponente)
    {
        // 
        //return view('admin.imagen.crear');

        $calificaciones = DB::table("calificacion_ponente")
                ->select("*")
                ->where("id_dia_evento",$id_dia_evento)
                ->where("id_ponente",$id_ponente)
                ->get();

        $calificacionesex = DB::table("calificacion_ponente")
                ->select("*")
                ->where("id_dia_evento",$id_dia_evento)
                ->where("id_ponente",$id_ponente)
                ->get();

        $promedio =  DB::table("calificacion_ponente")
                ->select(DB::raw("SUM(calificacion_ponente.calificacion) as suma"))
                ->where("id_dia_evento",$id_dia_evento)
                ->where("id_ponente",$id_ponente)
                ->first()->suma;
        $pro = (int)$promedio;
        //dd($promedio);
         return view('admin.webinar.calificaciones', compact('calificaciones','calificacionesex','promedio','pro'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->get('id_dia_evento');
        $asignacion = new PonenteAsignado($request->all());
        $asignacion->id_dia_evento = $id;
        $asignacion->save();
        Session::flash('message','Ponente asignado correctamente');
        return redirect::to('ponentes_dia/'.$id);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PonentesActualizarRequest $request, $id)
    {
        //
         

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PonenteAsignado::destroy($id);
        Session::flash('message','Ponente eliminado Correctamente del evento');
        return back();
       
    }
}