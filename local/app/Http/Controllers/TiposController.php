<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\TipoCrearRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tipos;
use View;
use Session;
use Redirect;

class TiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $tipos = Tipos::all();

        
        return View::make('admin.tipos.index', compact('tipos'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('admin.tipos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoCrearRequest $request)
    {
        //
            \App\Tipos::create([
            'tipo' => $request['tipo'],
            
            ]);

            return Redirect::route('tipos-usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
          //
         $tipo = Tipos::find($id);
        if (is_null($tipo))
        {
            return Redirect::route('tipos-usuarios.index');
        }
        return View::make('admin.tipos.editar', compact('tipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         //
        $tipo = Tipos::find($id);
        $tipo->fill($request->all());
        $tipo->save();

        return Redirect::route('tipos-usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Tipos::find($id)->delete();
        return Redirect::route('tipos-usuarios.index');
    }
}
