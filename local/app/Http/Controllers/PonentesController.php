<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ponente;
use View;
use Session;
use Redirect;
use File;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\PonentesActualizarRequest;
use App\Http\Requests\PonentesCrearRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AsistentesExport;




use Illuminate\Support\Facades\Input;


class PonentesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        //
        
         $ponentes = Ponente::all();
 
        return View::make('admin.ponentes.index', compact('ponentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        //return view('admin.imagen.crear');
         return View::make('admin.ponentes.crear');
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PonentesCrearRequest $request)
    {
        //
        $ponentes = new Ponente($request->all());
        ////////////////////////////////// imagenprincipal
        $foto = $ponentes->foto;
       
        $dir = public_path().'/images/';
        $docs = $request->file('foto');
        //dd($docs);
        if (Input::hasFile('foto')){

            $foto = $docs->getClientOriginalName();
            $foto = $this->limpiar_caracteres_especiales($foto);
            //dd($imagenprincipal);
            $docs->move($dir, $foto);
        }
        
        ///////////////////////////////////
        $ponentes->foto = $foto;

        
         /////////////////////////////////////////// anuncio
        
        $ponentes->save();
        Session::flash('message','Ponente guardado correctamente');
        return redirect::to('ponentes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $ponentes= Ponente::find($id);
       return view('admin.ponentes.editar',compact('ponentes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PonentesActualizarRequest $request, $id)
    {
        //
         $ponentes= Ponente::find($id);
         $ponentes->fill($request->all());

        ////////////////////////////////// imagenprincipal
        $foto = $ponentes->foto;
       
        $dir = public_path().'/images/';
        $docs = $request->file('foto');
        //dd($docs);
        if (Input::hasFile('foto')){

            $foto = $docs->getClientOriginalName();
            $foto = $this->limpiar_caracteres_especiales($foto);
            //dd($imagenprincipal);
            $docs->move($dir, $foto);
        }
        
        ///////////////////////////////////
        $ponentes->foto = $foto;

        $ponentes->save();
         Session::flash('message','Registro actualizado correctamente');
        return redirect::to('ponentes');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $ponentes = Ponente::find($id);

        $dir = public_path().'/images/';
            File::delete($dir.$ponentes->foto);
        

    
        Ponente::destroy($id);

        Session::flash('message','Ponente eliminado Correctamente');
        return redirect::to('ponentes') ;
    }
}