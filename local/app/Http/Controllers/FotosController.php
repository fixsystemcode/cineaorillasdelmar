<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Foto;
use App\Articulo;
use App\User;
use App\Categoria;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;


use Illuminate\Support\Facades\Input;

class FotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $imgs2 = Foto::all();
        return View::make('admin.fotos.index', compact('imgs2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.fotos.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $img = new Foto($request->all());
        
        $img->save();
        Session::flash('message','Imagen guardada correctamente');
        return redirect::to('galeria');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       $img= Foto::find($id);
       return view('admin.fotos.editar',compact('img'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $img= Foto::find($id);
         $img->fill($request->all());
         $img->save();

         Session::flash('message','Imagen actualizada correctamente');
        return redirect::to('galeria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Foto::find($id)->delete();
        
        Session::flash('message','Imagen eliminada correctamente');
        return Redirect::back();
        //return Redirect::route('galeria.index');
    }
}
