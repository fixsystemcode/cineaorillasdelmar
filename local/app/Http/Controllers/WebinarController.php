<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Webinar;
use App\PonenteAsignado;
use App\Ponente;
use App\Dia_evento;
use View;
use Session;
use Redirect;
use File;
use SendGrid;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\WebinarActualizarRequest;
use App\Http\Requests\WebinarCrearRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AsistentesExport;
use Intervention\Image\Facades\Image;




use Illuminate\Support\Facades\Input;


class WebinarController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        //
        
         $webi = Webinar::all();
        return View::make('admin.webinar.index', compact('webi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
        //return view('admin.imagen.crear');
         return View::make('admin.webinar.crear');
    }

    

    public function consult($id)
    {
        
        $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->orderBy('webinar.created_at','ASC')
                  ->where("id_webinar",$id)
                  ->get();

         return view('admin.webinar.consult', compact('asistentes'));
    }

    public function exportExcel()
    {
        $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where("id_webinar",$id)
                  ->get();
        return Excel::download($asistentes,'asistentes-list.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   


    public function store(WebinarCrearRequest $request)
    {
        //
        $webi = new Webinar($request->all());
        $webi->estado = ($request->estado) ? 1 : 0;
        ////////////////////////////////// imagenprincipal
        $logo = $webi->logo;
       
        $dir = public_path().'/images/';
        $docs = $request->file('logo');
        //dd($docs);
        if (Input::hasFile('logo')){

            $logo = $docs->getClientOriginalName();
            $logo = $this->limpiar_caracteres_especiales($logo);
            //dd($imagenprincipal);

            
            
            $imageName = time().'.'."jpg";

            Image::make($docs)->encode('jpg', 65)->save(public_path('/images/' . $imageName));
            $webi->logo = $imageName;  
            


            //$docs->move($dir, $logo);
        }
        
        ///////////////////////////////////
        //$webi->logo = $logo;

        
         /////////////////////////////////////////// anuncio
        $anuncio = $webi->anuncio;
       
        $docs2 = $request->file('anuncio');
        //dd($docs);
        if (Input::hasFile('anuncio')){

            $anuncio = $docs2->getClientOriginalName();
            $anuncio = $this->limpiar_caracteres_especiales($anuncio);
            //dd($imagenprincipal);
            $docs2->move($dir, $anuncio);
        }
        
        ///////////////////////////////////
        $webi->anuncio = $anuncio;
        $webi->save();

        /////////////////// primer dia de evento
        $dia_evento = new Dia_evento($request->all());
        $ultimoIdWebi = Webinar::latest('id')->first()->id;
        $dia_evento->id_webinar = $ultimoIdWebi;
        $dia_evento->slug = Str::random(10);
        $dia_evento->hora_inicio = $request->hora;
        $dia_evento->save();

        Session::flash('message','Webinar registrado correctamente');
        return redirect::to('webinar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function exportar($id){
       
        $datos = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->get();
        //dd($infowebi);
        
        return view('paginas.exportarhtml', compact('datos'));
    }

    public function show($id)
    {
        $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->orderBy('created_at','ASC')
                  ->paginate(30);

        $asistentesex = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->orderBy('created_at','ASC')
                  ->get();

        $asistencia_evento = DB::table("asistencia_evento")
                  ->select("asistencia_evento.cedula")
                  ->leftjoin("dia_evento","asistencia_evento.id_dia_evento","=","dia_evento.id")
                  ->where("id_webinar",$id)
                  ->get();

        

        $webi= Webinar::find($id);
        //dd($asistencia_evento);
         return view('admin.webinar.consult', compact('asistentes','webi','asistentesex','asistencia_evento'));
    }


    public function show_asistencia($id)
    {
        $asistencia_evento = DB::table("asistentes_webinar")
                           ->select(DB::raw("asistentes_webinar.id, 
                                            asistentes_webinar.cedula, 
                                            asistentes_webinar.nombres,
                                            asistentes_webinar.apellidos,
                                            count(asistencia_evento.id_dia_evento) as numeroasistencia"))
                           ->join("asistencia_evento","asistencia_evento.cedula","=","asistentes_webinar.cedula")
                           ->join("dia_evento","dia_evento.id","=","asistencia_evento.id_dia_evento")
                           ->where("dia_evento.id_webinar",$id)
                           ->where("asistentes_webinar.id_webinar",$id)
                           ->groupBy("asistentes_webinar.id")
                           ->groupby("asistentes_webinar.cedula")
                           ->groupby("asistentes_webinar.nombres")
                           ->groupby("asistentes_webinar.apellidos")
                           ->get();

        $asistencia_eventoex = DB::table("asistentes_webinar")
                           ->select(DB::raw("asistentes_webinar.id, 
                                            asistentes_webinar.cedula, 
                                            asistentes_webinar.nombres,
                                            asistentes_webinar.apellidos,
                                            count(asistencia_evento.id_dia_evento) as numeroasistencia"))
                           ->join("asistencia_evento","asistencia_evento.cedula","=","asistentes_webinar.cedula")
                           ->join("dia_evento","dia_evento.id","=","asistencia_evento.id_dia_evento")
                           ->where("dia_evento.id_webinar",$id)
                           ->where("asistentes_webinar.id_webinar",$id)
                           ->groupBy("asistentes_webinar.id")
                           ->groupby("asistentes_webinar.cedula")
                           ->groupby("asistentes_webinar.nombres")
                           ->groupby("asistentes_webinar.apellidos")
                           ->get();
        $webi= Webinar::find($id);                   
        

        $webi= Webinar::find($id);
        //dd($asistencia_evento);
         return view('admin.webinar.consultasis', compact('asistencia_evento','webi','asistencia_eventoex'));
    }


    public function show_aprobados($id)
    {

        $asistenciasnecesarias = DB::table("dia_evento")
                               ->select(DB::raw("count(dia_evento.id) as total"))
                               ->where("id_webinar",$id)
                               ->first();

        $asistencia_evento = DB::table("asistentes_webinar")
                           ->select(DB::raw("asistentes_webinar.id, 
                                            asistentes_webinar.cedula, 
                                            asistentes_webinar.nombres,
                                            asistentes_webinar.apellidos,
                                            count(asistencia_evento.id_dia_evento) as numeroasistencia"))
                           ->join("asistencia_evento","asistencia_evento.cedula","=","asistentes_webinar.cedula")
                           ->join("dia_evento","dia_evento.id","=","asistencia_evento.id_dia_evento")
                           ->where("dia_evento.id_webinar",$id)
                           ->where("asistentes_webinar.id_webinar",$id)
                           ->groupBy("asistentes_webinar.id")
                           ->groupby("asistentes_webinar.cedula")
                           ->groupby("asistentes_webinar.nombres")
                           ->groupby("asistentes_webinar.apellidos")
                           ->get();

        $asistencia_eventoex = DB::table("asistentes_webinar")
                           ->select(DB::raw("asistentes_webinar.id, 
                                            asistentes_webinar.cedula, 
                                            asistentes_webinar.nombres,
                                            asistentes_webinar.apellidos,
                                            count(asistencia_evento.id_dia_evento) as numeroasistencia"))
                           ->join("asistencia_evento","asistencia_evento.cedula","=","asistentes_webinar.cedula")
                           ->join("dia_evento","dia_evento.id","=","asistencia_evento.id_dia_evento")
                           ->where("dia_evento.id_webinar",$id)
                           ->where("asistentes_webinar.id_webinar",$id)
                           ->groupBy("asistentes_webinar.id")
                           ->groupby("asistentes_webinar.cedula")
                           ->groupby("asistentes_webinar.nombres")
                           ->groupby("asistentes_webinar.apellidos")
                           ->get();
        $webi= Webinar::find($id);                   
        

        $webi = Webinar::find($id);
        // dd($asistencia_eventoex);
         return view('admin.webinar.consultaprobados', compact('asistencia_evento','webi','asistencia_eventoex','asistenciasnecesarias'));
    }




    public function envasistencia($id, $salto){
      set_time_limit(300);
      $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->orderBy('created_at','ASC')
                  // ->where('envio_correo', '0')
                  ->skip($salto)
                  ->take(250)
                  ->get();

      $webinar = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where("id",$id)
                  ->first();

      $contador = 0;
      foreach ($asistentes as $key => $value) {
        $name = $value->nombres;
        $apellido = $value->apellidos;
        $cedula = $value->cedula;
        $email = $value->email;
        $tema = $webinar->tema;
        $this->enviarcorreo($name, $apellido, $cedula, $email, $tema, $value->id);
      }
      return view('admin.webinar.enviarasistencia', compact('asistentes'));
    }

    public function enviarAsistenciaUnico($id, $id_reg){
      $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->where("id",$id_reg)
                  ->orderBy('created_at','ASC')
                  ->take(10)
                  ->get();

      $webinar = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where("id",$id)
                  ->first();

      $contador = 0;
      foreach ($asistentes as $key => $value) {
        $name = $value->nombres;
        $apellido = $value->apellidos;
        $cedula = $value->cedula;
        $email = $value->email;
        $tema = $webinar->tema;
        $this->enviarcorreo($name, $apellido, $cedula, $email, $tema, $value->id);
      }
      return view('admin.webinar.enviarasistencia', compact('asistentes'));
    }




    public function envcertificado($id, $salto){
      set_time_limit(300);

      $webinar = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where("id",$id)
                  ->first();

      $asistenciasnecesarias = DB::table("dia_evento")
                               ->select(DB::raw("count(dia_evento.id) as total"))
                               ->where("id_webinar",$id)
                               ->first();

      $asistencia_evento = DB::table("asistentes_webinar")
                           ->select(DB::raw("asistentes_webinar.id, 
                                            asistentes_webinar.cedula, 
                                            asistentes_webinar.nombres,
                                            asistentes_webinar.apellidos,
                                            asistentes_webinar.email,
                                            count(asistencia_evento.id_dia_evento) as numeroasistencia"))
                           ->join("asistencia_evento","asistencia_evento.cedula","=","asistentes_webinar.cedula")
                           ->join("dia_evento","dia_evento.id","=","asistencia_evento.id_dia_evento")
                           ->where("dia_evento.id_webinar",$id)
                           ->where("asistentes_webinar.id_webinar",$id)
                           ->groupBy("asistentes_webinar.id")
                           ->groupby("asistentes_webinar.cedula")
                           ->groupby("asistentes_webinar.nombres")
                           ->groupby("asistentes_webinar.apellidos")
                           ->groupby("asistentes_webinar.email")
                           ->skip($salto)
                           ->take(100)
                           ->get();
      $limite = $asistenciasnecesarias->total - 1;
      // dd($limite);
      foreach ($asistencia_evento as $key => $value) {
        if ($value->numeroasistencia >= $limite) {
            $name = $value->nombres;
            $apellido = $value->apellidos;
            $cedula = $value->cedula;
            $email = $value->email;
            $tema = $webinar->tema;
            $slug = $webinar->slug;
            // dd($name);
            $this->enviarcertificado($name, $apellido, $cedula, $email, $tema, $slug, $value->id);
        }
      }
    }
    public function envcertificadounico($id, $id_reg){
      $asistenciasnecesarias = DB::table("dia_evento")
                               ->select(DB::raw("count(dia_evento.id) as total"))
                               ->where("id_webinar",$id)
                               ->first();
      $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->where("id",$id_reg)
                  ->orderBy('created_at','ASC')
                  ->take(10)
                  ->get();

      $webinar = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where("id",$id)
                  ->first();

      $contador = 0;
      foreach ($asistentes as $key => $value) {
        $name = $value->nombres;
        $apellido = $value->apellidos;
        $cedula = $value->cedula;
        $email = $value->email;
        $tema = $webinar->tema;
        $slug = $webinar->slug;
        $this->enviarcertificado($name, $apellido, $cedula, $email, $tema, $slug, $value->id);
      }
    }
    public function enviarcertificado ($name, $apellido, $cedula, $email, $tema, $slug, $id=null){
      $link = 'https://inscripciones.runacode.com/buscarcertificado/'.$slug.'/'.$id;
      $htmlContent = "
                <div style='position: relative;padding-top: 40px;padding-right: 20px;padding-left: 20px;padding-bottom: 20px;background-color: #F8FAFC;display: flex;'>
                <div style='display: block;max-width: 820px;position: relative;margin: 0 auto;padding: 20px 40px;'>
                <div><img src='http://runachay.com.ec/img/repo/runachay/mail/horizontal.png' alt='' style='width: 160px;'></div>
                <br>
                <br>
                <h2 style='color: #444;margin: 0;font-weight: 700;padding-top: 24px;'>Certificación ".$tema."</h2>
                <br>
                      <p><strong>Hola ".$name." ".$apellido.".</strong></p>
                      <p>La educación es un área que está en constante evolución y más aún en estos tiempos de cambios. Y por eso nos agrada mucho que estés siempre capacitándote e innovándote.</p>
                      <p>Felicitaciones por tu asistencia, te comunicamos que ya puedes obtener tu certificado haciendo click en el siguiente enlace</p>
                      <p><a href='".$link."'><i>".$link."</i></a></p>
                    <br>  
                    <strong>Nota: </strong> Este enlace es de uso personal, no compartir.
                  </div>
                  </div>
                </div>
              ";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From:'.$name.' <soporte@runachay.com.ec>' . "\r\n";

        $from = new SendGrid\Email('Runachay', 'soporte@runachay.com.ec');
        $subject = $tema." Certificación Runachay";
        $to = new SendGrid\Email('Asistente',$email);

        $content = new SendGrid\Content("text/html", $htmlContent);

        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        //$apiKey = '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';
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);
        
        $response = $sg->client->mail()->send()->post($mail);
        // DB::update('update asistentes_webinar set envio_correo = 1 where id = ?', [$id]);
    }

    private function enviarcorreo($name, $apellido, $cedula, $email, $tema, $id=null){

        $link = 'https://inscripciones.runacode.com/asistencia/AEbXbfzZUQ/'.$id;

        $htmlContent = "
                <div style='position: relative;padding-top: 40px;padding-right: 20px;padding-left: 20px;padding-bottom: 20px;background-color: #F8FAFC;display: flex;'>
                <div style='display: block;max-width: 820px;position: relative;margin: 0 auto;padding: 20px 40px;'>
                <div><img src='http://runachay.com.ec/img/repo/runachay/mail/horizontal.png' alt='' style='width: 160px;'></div>
                <br>
                <br>
                <h2 style='color: #444;margin: 0;font-weight: 700;padding-top: 24px;'>".$tema."</h2>
                <br>
                      <p><strong>Hola ".$name." ".$apellido.".</strong></p>
                      <p>Te agradecemos por estar presente en nuestro día de ponencia <strong>".$tema."</strong>.</>
                      <p>Puedes registrar tu asistencia con el siguiente enlace: <a href='".$link."'><i>".$link."</i></a></p>
                      <p>Tu opinión es muy importante para nosotros, por ello agradecemos que califiques a nuestro ponentes de hoy</p>
                    <br>  
                    <strong>Nota: </strong> Este enlace es de uso personal, no compartir.
                  </div>
                  </div>
                </div>
              ";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From:'.$name.' <desarrolloweb@runachay.com.ec>' . "\r\n";

        $from = new SendGrid\Email('Runachay', 'info@runachay.com.ec');
        $subject = $tema." Confirmación";
        $to = new SendGrid\Email('Asistente',$email);

        $content = new SendGrid\Content("text/html", $htmlContent);

        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        //$apiKey = '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';
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        // DB::update('update asistentes_webinar set envio_correo = 1 where id = ?', [$id]);
    }


    public function add($id)
    {
        $ponentes = DB::table("asignacion_ponentes")
                  ->select("*")
                  ->where("id_dia_evento",$id)
                  ->get();




        $inf = DB::table("asignacion_ponentes")
              ->select("asignacion_ponentes.*","ponentes.nombres","ponentes.apellidos","ponentes.foto","ponentes.profesion")
              ->leftjoin("ponentes","asignacion_ponentes.id_ponente","=","ponentes.id")
              ->where("id_dia_evento",$id)
              ->get();


        $infoponentes = DB::table("ponentes")
                  ->select("*")
                  ->get();

        $infoponentes2 = DB::table("ponentes")
                  ->select("*")
                  ->get();

        $nomponentes = Ponente::orderBy('id','ASC')->get()->pluck('Nombre_And_Apellido','id')->all();

        $dia_evento= Dia_evento::find($id);
        $infowebi =  $dia_evento[0];
        //dd($inf);
         return view('admin.webinar.ponentes', compact('ponentes','dia_evento', 'inf','nomponentes'));
    }

    public function dates($id)
    {
        $dias_evento = DB::table("dia_evento")
                  ->select("*")
                  ->where("id_webinar",$id)
                  ->get();

        $webi= Webinar::find($id);
        $infowebi =  $webi[0];
        //dd($dias_evento);
         return view('admin.webinar.dias_evento', compact('dias_evento','webi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $webi= Webinar::find($id);
       return view('admin.webinar.editar',compact('webi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WebinarActualizarRequest $request, $id)
    {
        //
         $webi= Webinar::find($id);
         $webi->fill($request->all());

         ////////////////////////////////// imagenprincipal
        $webi->estado = ($request->estado) ? 1 : 0;
        ////////////////////////////////// imagenprincipal
        $logo = $webi->logo;
       
        $dir = public_path().'/images/';
        $docs = $request->file('logo');
        //dd($docs);
        if (Input::hasFile('logo')){

            $logo = $docs->getClientOriginalName();
            $logo = $this->limpiar_caracteres_especiales($logo);
            $imageName = time().'.'."jpg";

            Image::make($docs)->encode('jpg', 65)->save(public_path('images/' . $imageName));
            $webi->logo = $imageName;  
            //dd($imagenprincipal);
            //$docs->move($dir, $logo);
        }
        
        ///////////////////////////////////
        //$webi->logo = $logo;

        
         /////////////////////////////////////////// anuncio
        $anuncio = $webi->anuncio;
       
        $docs2 = $request->file('anuncio');
        //dd($docs);
        if (Input::hasFile('anuncio')){

            $anuncio = $docs2->getClientOriginalName();
            $anuncio = $this->limpiar_caracteres_especiales($anuncio);
            //dd($imagenprincipal);
            $docs2->move($dir, $anuncio);
        }
        
        ///////////////////////////////////
        $webi->anuncio = $anuncio;
        $webi->save();
         Session::flash('message','Registro actualizado correctamente');
        return redirect::to('webinar');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $webi = Webinar::find($id);

        $dir = public_path().'/images/';
            File::delete($dir.$webi->logo);
            File::delete($dir.$webi->anuncio);
        

    
        Webinar::destroy($id);

        Session::flash('message','Webinar eliminado Correctamente');
        return redirect::to('webinar') ;
    }

    
}