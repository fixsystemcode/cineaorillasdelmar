<?php

namespace App\Http\Controllers;

use App\facturacion;
use Illuminate\Http\Request;

use Auth;

class FacturacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
        return $s;
    }



    public function index()
    {
        //
        $acep = Auth::user();
        $registros = facturacion::orderBy('id','DESC')->get();
        
        return view('admin.facturacion.index', compact('registros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $solicitud = new facturacion($request->all());

        // $solicitud->save();

        // return 0;

        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
          $secret = '6Lf6f_YZAAAAAGH_VGJU3--IQy0R57Z9jGHnAmCl';
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);

          $identificador = !empty($_POST['identificador'])?$_POST['identificador']:'';
          $ue = !empty($_POST['ue'])?$_POST['ue']:'';
          $ruc = !empty($_POST['ruc'])?$_POST['ruc']:'';
          $telefono = !empty($_POST['telefono'])?$_POST['telefono']:'';
          $direccion = !empty($_POST['direccion'])?$_POST['direccion']:'';
          $razon = !empty($_POST['razon'])?$_POST['razon']:'';
          $valor = !empty($_POST['valor'])?$_POST['valor']:'';

          if($responseData->success):
            //
            $solicitud->save();
            return 0;
          else:
            return 1;
            $errMsg = 'Robot verification failed, please try again.';
        endif;
        else:
          return 2;
      endif;


    }

    public function ue($id) {
        $acep = Auth::user();
        $registros = facturacion::where('identificador','=',$id)->orderBy('id','DESC')->get();

        return view('admin.facturacion.index', compact('registros'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function show(facturacion $facturacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function edit(facturacion $facturacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, facturacion $facturacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\facturacion  $facturacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(facturacion $facturacion)
    {
        //
    }
}
