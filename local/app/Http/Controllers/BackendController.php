<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Foto;
use App\Articulo;
use App\User;
use App\Slider;
use App\Video;
use App\Categoria;
use App\VisitasModel;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;



use Illuminate\Support\Facades\Input;

class BackendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {

        // dd('');
        $acep = Auth::user();
        if ($acep->id_tipo_usuario == 6) {
            return Redirect::to('formregistro');
        }elseif ($acep->id_tipo_usuario == 7){
                return Redirect::to('formfacturacion');
        }
        // if ($acep->id_tipo_usuario == 7) {
        
        // }
        //
        // dd('dentro');
        $articulos2 = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria")->orderBy('created_at','ASC')->get();

        $articulos = DB::table("articulos")
                    ->join("categorias","articulos.id_categoria","=","categorias.id")
                    ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                    ->select("articulos.*","usuarios.nombres","categorias.categoria")->orderBy('articulos.created_at','DESC')->paginate(7);

        $carticulo = count($articulos2);

        $slider = Slider::all();
        $cslider = count($slider);


        $usuarios = User::all();
        $cusuarios = count($usuarios);


        $videos = Video::all();
        $cvideos = count($videos);



        return view('admin.inicio',compact('carticulo','cslider','cusuarios','cvideos','articulos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
