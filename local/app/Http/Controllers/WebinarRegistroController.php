<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\WebinarRegistro;
use View;
use Redirect;
use File;
use SendGrid;
use Session;
//use PDF;
use Dompdf\options;
use Dompdf\dompdf;
use Illuminate\Support\Facades\DB;
use App\CategoriaInterna;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\WebinarRegistroRequest;




use Illuminate\Support\Facades\Input;


class WebinarRegistroController extends Controller
{
    


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        return view('inicio.index');
    }

    public function registrowebinar($id){



        $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("slug",$id)
            ->first();


        if($infowebi == null){
            $id = str_replace('-', '', strtolower($id));
            $infowebi = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where(DB::raw("replace(lower(tema),' ', '')"),$id)
                  ->first();
        }
        $infoponentes = [];
        
        if($infowebi != null){
            $infoponentes = DB::table("asignacion_ponentes")
                ->select("asignacion_ponentes.*","ponentes.nombres","ponentes.apellidos","ponentes.foto","ponentes.profesion","ponentes.link_fb","ponentes.link_tw","ponentes.link_inst","ponentes.link_linke")
                ->leftjoin("ponentes","asignacion_ponentes.id_ponente","=","ponentes.id")
                ->leftjoin("dia_evento","asignacion_ponentes.id_dia_evento","=","dia_evento.id")
                ->where("id_webinar",$infowebi->id)
                ->get();
        }

        $totalreservas = DB::table("asistentes_webinar")
            ->select("*")
            ->where("id_webinar",$infowebi->id)
            ->count();
        //dd($totalreservas);
        
        return view('paginas.registrowebi', compact('infowebi','infoponentes','totalreservas'));
    }

     public function reserva($id){

        $inforeserva = DB::table("asistentes_webinar")
            ->select("*")
            ->where("id",$id)
            ->first();

        $infowebi = DB::table("webinar")
            ->select("*")
            ->where("id",$inforeserva->id_webinar)
            ->first();



        
       
        //dd($infowebi);
        
        return view('paginas.reserva', compact('infowebi','inforeserva'));
    }

    public function descargar(Request $request){
        $id_evento = $request->id_evento;
        $id_asistente = $request->id_asistente;

        $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("id",$id_evento)
            ->first();

        $infoasistente =  DB::table("asistentes_webinar")
                          ->select("*")
                          ->where("id",$id_asistente)
                          ->first();
        
        $pdf = \PDF::loadView('paginas.certificado', compact('infowebi','infoasistente'));
        return $pdf->setPaper('a4', 'landscape')->download('certificado.pdf');
    }

    public function descargarqr($id){
        $id_reserva = $id;

         $inforeserva = DB::table("asistentes_webinar")
            ->select("*")
            ->where("id",$id_reserva)
            ->first();


       

        $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("id",$inforeserva->id_webinar)
            ->first();

       
        
        $pdf = \PDF::loadView('paginas.qr', compact('infowebi','inforeserva'));
        return $pdf->stream('reserva.pdf');
    }


    public function generar_certificado($id){

      $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("slug",$id)
            ->first();


        if($infowebi == null){
            $id = str_replace('-', '', strtolower($id));
            $infowebi = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where(DB::raw("replace(lower(tema),' ', '')"),$id)
                  ->first();
        }
        $infoponentes = [];
        
        if($infowebi != null){
            $infoponentes = DB::table("asignacion_ponentes")
                ->select("asignacion_ponentes.*","ponentes.nombres","ponentes.apellidos","ponentes.foto","ponentes.profesion","ponentes.link_fb","ponentes.link_tw","ponentes.link_inst","ponentes.link_linke")
                ->leftjoin("ponentes","asignacion_ponentes.id_ponente","=","ponentes.id")
                ->leftjoin("dia_evento","asignacion_ponentes.id_dia_evento","=","dia_evento.id")
                ->where("id_webinar",$infowebi->id)
                ->get();
        }

        $ultimo_dia = DB::table("dia_evento")
                      ->select("*")
                      ->where("id_webinar",$infowebi->id)
                      ->orderBy("dia_evento.fecha","DESC")
                      ->first();

        $dias_evento = count(
                          DB::table("dia_evento")
                          ->select("*")
                          ->where("id_webinar",$infowebi->id)
                          ->get()
                      );
        
        
        return view('paginas.generar_certificado', compact('infowebi','infoponentes','ultimo_dia','dias_evento'));

    }

    public function pagina_compartir($id_evento, $id_asistente)
    {
        $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("slug",$id_evento)
            ->first();


        if($infowebi == null){
            $id_evento = str_replace('-', '', strtolower($id_evento));
            $infowebi = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where(DB::raw("replace(lower(tema),' ', '')"),$id_evento)
                  ->first();
        }

        $infoasistente =  DB::table("asistentes_webinar")
                          ->select("*")
                          ->where("id",$id_asistente)
                          ->first();


        $pdf = \PDF::loadView('paginas.certificado', compact('infowebi','infoasistente'));
        $pdf->setPaper('a4', 'landscape');

        // dd($infowebi);
        // return $pdf->stream("dompdf_out.pdf", array("Attachment" => false));
        // dd($pdf);

        // return $pdf->stream();

        return view('paginas.compartir_certificado', compact('infowebi','infoasistente', 'pdf'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function certificado(Request $request)
    {
        $id = $request->id_webinar;

        $dias_evento = count(
                          DB::table("dia_evento")
                          ->select("*")
                          ->where("id_webinar",$id)
                          ->get()
                      );

        $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("id",$id)
            ->first();

        $cedula = $request->cedula;

        $asistencia =  DB::table("asistentes_webinar")
                          ->select("*")
                          ->where("id_webinar",$id)
                          ->where("cedula",$cedula)
                          ->get();

        $conf_asistencia = count($asistencia);

        $infoasistente =  DB::table("asistentes_webinar")
                          ->select("*")
                          ->where("id_webinar",$id)
                          ->where("cedula",$cedula)
                          ->first();

        $ultimo_dia = DB::table("dia_evento")
                      ->select("*")
                      ->where("id_webinar",$infowebi->id)
                      ->orderBy("dia_evento.fecha","DESC")
                      ->first();

        $asistencia_evento = DB::table("asistentes_webinar")
                           ->select(DB::raw("asistentes_webinar.id, 
                                            asistentes_webinar.cedula, 
                                            asistentes_webinar.nombres,
                                            asistentes_webinar.apellidos,
                                            count(asistencia_evento.id_dia_evento) as numeroasistencia"))
                           ->join("asistencia_evento","asistencia_evento.cedula","=","asistentes_webinar.cedula")
                           ->join("dia_evento","dia_evento.id","=","asistencia_evento.id_dia_evento")
                           ->where("dia_evento.id_webinar",$id)
                           ->where("asistentes_webinar.id_webinar",$id)
                           ->where("asistencia_evento.cedula",$cedula)
                           ->groupBy("asistentes_webinar.id")
                           ->groupby("asistentes_webinar.cedula")
                           ->groupby("asistentes_webinar.nombres")
                           ->groupby("asistentes_webinar.apellidos")
                           ->first();

        if($conf_asistencia == 0):
            Session::flash('message-error', "La cedula ingresada no está registrada en este evento.");
            return redirect()->back(); 
        else:
            if($infowebi->slug != null):
              return redirect::to('buscarcertificado/'.$infowebi->slug.'/'.$infoasistente->id);
            else:
              return redirect::to('buscarcertificado/'.$str_replace('-', '', strtolower($infowebi->tema)).'/'.$infoasistente->id);
            endif;
        endif;
        }


    
    

    public function store(WebinarRegistroRequest $request)
    {



        $webinar = DB::table("webinar")
                  ->select("*")
                  ->orderBy('webinar.fecha','ASC')
                  ->where("id",$request->get('id_webinar'))
                  ->get();

        // dd($webinar);
        $infowebi =  $webinar[0];

        $reservas = DB::table("asistentes_webinar")
                  ->select("email")
                  ->where("id_webinar",$request->get('id_webinar'))
                  ->get();
        $validador =  count($reservas);
        // $validador = 0;
        
        
        
        
        //

        $asistentes = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id_webinar",$request->get('id_webinar'))
                  ->get();

        // dd(count($asistentes));
        
        
      if($validador <= 100):
        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
          $secret = '6LcrxKgaAAAAAJbo9sSovnrqFz5TCxMhTlQ1wsk9';
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);
      
          $name = $request->nombres;
          $apellido = $request->apellidos;
          $cedula = $request->cedula;
          $email = $request->email;
          $pais = $request->pais;

          if($request->provincia2 != null)
          {
           $provincia = $request->provincia2;
          }else{
            $provincia = $request->provincia;
          }

        
          $tema = $infowebi->tema;
          $fecha = $infowebi->fecha;
          $hora = $infowebi->hora;
          $anuncio = $infowebi->anuncio;
          
          if($responseData->success):


            $htmlContent = "
              <div style='position: relative;padding-top: 40px;padding-right: 20px;padding-left: 20px;padding-bottom: 20px;background-color: #F8FAFC;display: flex;'>
              <div style='display: block;max-width: 820px;position: relative;margin: 0 auto;padding: 20px 40px;'>
              <br>
              <br>
              <h2 style='color: #444;margin: 0;font-weight: 700;padding-top: 24px;'>".$tema."</h2>
              <br>
                    <p><strong>Hola ".$name." ".$apellido.".</strong></p>
                    <p>Gracias por reservar para ver <strong>".$tema."</strong> en la playa junto a nosotros.</>
                    <p><strong>FECHA: </strong>".$fecha."</span></p>
                    <p><strong>HORA:</strong>".date( 'H:i', strtotime($hora))." Horas<br>
                    </p>

                    </p>
                    <p>No olvide seguir las recomendaciones y presentar su codigo QR al ingresar.</p>
                  <br> 
                </div>
                </div>
              </div>
            ";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:'.$name.' <antonio.urbe@gmail.com>' . "\r\n";

            $from = new SendGrid\Email('Cine a Orillas del Mar', 'antonio.urbe@gmail.com');
            $subject = $tema." Confirmación";
            // $to = new SendGrid\Email('Contacto Soporte', 'desarrolloweb@runachay.com.ec');
            $to = new SendGrid\Email('Asistente',$email);
            //$to2 = new SendGrid\Email('Contacto Ventas', 'ventas@runachay.com.ec');
            //$to3 = new SendGrid\Email('Contacto Gerencia', 'gerencia@runachay.com.ec');
        

            $content = new SendGrid\Content("text/html", $htmlContent);

            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            //$mail2 = new SendGrid\Mail($from, $subject, $to2, $content);
            //$mail3 = new SendGrid\Mail($from, $subject, $to3, $content);
            $apiKey = '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';
            $sg = new \SendGrid($apiKey);
            $response = $sg->client->mail()->send()->post($mail);
            ////////////////////////////////// guardar
            $webi = new WebinarRegistro($request->all());
            $webi->id_webinar = $request->get('id_webinar');
            $webi->provincia = $provincia;
            $webi->save();
            
            $ultimoid = WebinarRegistro::latest('id')->first();

            
            // Session::flash('message', "Tu registro se ha realizado con éxito, la información de acceso a sido enviada al correo.");

            // return response()->json(['status' => 'ok','informacion' => $webinar], 200)->withCallback($request->input('callback'));;
            return redirect()->back()->with('ultimoid', $ultimoid)->with('message', 'Tu registro se ha realizado con éxito, la información de acceso ha sido enviada a tu correo electrónico.');
            
          else:
              Session::flash('message', "No hemos podido completar el registro, intenta nuevamente. Si el problema persiste contacta con nuestro soporte técnico .");
              
          endif;
      else:
        Session::flash('message-error', "El campo reCAPTCHA es obligatorio.");
        
      endif;
     else:
        Session::flash('message-error', "Ya no disponemos de cupos disponibles, te esperamos en nuestra próxima función!.");
     endif;

    return redirect()->back(); 
    }

    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}