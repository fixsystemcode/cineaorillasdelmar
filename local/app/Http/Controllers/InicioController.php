<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Articulo;
use App\Informacion;
use App\Slider;
use View;
use Session;
use Redirect;

class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       // $articulos = Articulo::all();//
        

       // $articulo = Articulo::find(12);
       // $imgs = Slider::all();



       // return view('paginas.inicio',compact('articulo','imgs'));//

        $articulo = Articulo::all();
        return view('inicio.index');
    }

    public function acercadenosotros()
    {
        //
       // $articulos = Articulo::all();//
        //,compact('articulos')//
         $informacion = Informacion::find(1);
        $articulo = Articulo::find(12);

        return view('paginas.acerca-de-nosotros',compact('articulo','informacion'));
    }

        public function especialidad()
    {
        //
       // $articulos = Articulo::all();//
        //,compact('articulos')//


        $articulo = Articulo::find(15);        
        return view('paginas.especialidad',compact('articulo'));
    }

            public function equipos()
    {
        //
       // $articulos = Articulo::all();//
        //,compact('articulos')//
        $articulo = Articulo::find(16);
        return view('paginas.equipos',compact('articulo'));
    }

     public function certificaciones()
    {
        //
       // $articulos = Articulo::all();//
        //,compact('articulos')//
        $articulo = Articulo::find(17);
        return view('paginas.certificaciones',compact('articulo'));
    }

         public function imagenes()
    {
        //
       // $articulos = Articulo::all();//
        //,compact('articulos')//
        $articulo = Articulo::find(18);
        return view('paginas.imagenes',compact('articulo'));
    }

    public function contactos()
    {
        //
       // $articulos = Articulo::all();//
         $informacion = Informacion::find(1);
        //,compact('articulos')//
        $articulo = Articulo::find(123);
        return view('paginas.contactos',compact('articulo','informacion'));
    }

    public function article($slug)
    {
        //
         $informacion = Informacion::find(1);
        $post = Articulo::where('slug','=', $slug)->firstOrFail();
  
        return View::make('noticia', compact('post','informacion'));
    }

  
}
