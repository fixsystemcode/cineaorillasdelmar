<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventoAsistencia extends Model
{
    //
    protected $table = 'asistencia_evento';

    protected $fillable = ['id_dia_evento','comentario','cedula'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}