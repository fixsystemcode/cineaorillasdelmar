<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EventoAsistencia;
use App\CalificacionPonente;
use View;
use Redirect;
use File;
use SendGrid;
use Session;
use Illuminate\Support\Facades\DB;
use App\CategoriaInterna;
use App\Http\Requests\ImagenesActualizarRequest;
use App\Http\Requests\WebinarRegistroRequest;




use Illuminate\Support\Facades\Input;


class EventoAsistenciaController extends Controller
{
    


 public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function index()
    {
        return view('inicio.index');
    }

    public function registroasistencia($id, $usuario){

        $infodia = DB::table("dia_evento")
            ->select("*")
            ->orderBy('dia_evento.fecha','ASC')
            ->where("slug",$id)
            ->first();


        $usuario = DB::table("asistentes_webinar")
                  ->select("*")
                  ->where("id",$usuario)
                  ->first();
        // dd($usuario);

        

        $infoponentes = [];
        $infowebi = [];
        
        if($infodia != null){
            $infoponentes = DB::table("asignacion_ponentes")
                ->select("asignacion_ponentes.*","ponentes.nombres","ponentes.apellidos","ponentes.foto","ponentes.profesion","ponentes.link_fb","ponentes.link_tw","ponentes.link_inst","ponentes.link_linke")
                ->leftjoin("ponentes","asignacion_ponentes.id_ponente","=","ponentes.id")
                ->where("id_dia_evento",$infodia->id)
                ->get();

            $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("id",$infodia->id_webinar)
            ->first();

            $totalponentes = count($infoponentes);
        }
        //dd($infoponentes);



        
        return view('paginas.registroasistencia', compact('infodia','infoponentes','infowebi','totalponentes', 'usuario'));
    }

    public function consultaregistro($id){

      $infodia = DB::table("dia_evento")
            ->select("*")
            ->orderBy('dia_evento.fecha','ASC')
            ->where("slug",$id)
            ->first();   

        $infoponentes = [];
        $infowebi = [];
        
        if($infodia != null){
            $infoponentes = DB::table("asignacion_ponentes")
                ->select("asignacion_ponentes.*","ponentes.nombres","ponentes.apellidos","ponentes.foto","ponentes.profesion","ponentes.link_fb","ponentes.link_tw","ponentes.link_inst","ponentes.link_linke")
                ->leftjoin("ponentes","asignacion_ponentes.id_ponente","=","ponentes.id")
                ->where("id_dia_evento",$infodia->id)
                ->get();

            $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("id",$infodia->id_webinar)
            ->first();

            $totalponentes = count($infoponentes);
        }
      return view('paginas.consultaasistencia', compact('infodia','infoponentes','infowebi','totalponentes'));
    }



    public function seleccionjornada($id){
      $infowebi = DB::table("webinar")
            ->select("*")
            ->orderBy('webinar.fecha','ASC')
            ->where("slug",$id)
            ->first();


      $dias_evento = DB::table("dia_evento")
                  ->leftjoin("webinar","dia_evento.id_webinar","=","webinar.id")
                  ->select("dia_evento.*", "webinar.slug as direccion")
                  ->where("webinar.slug",$id)
                  ->where("estado_enlace", 1)
                  ->orderBy("fecha", "ASC")
                  ->get();

      // dd($dias_evento);
      return view('paginas.listadias', compact('infowebi', 'dias_evento'));
    }



    public function traerAsistente($id){

        $infoasistente = WebinarRegistro::find($id);

        return view('paginas.traerAsistente', compact('infoasistente'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $dia_evento = DB::table("dia_evento")
                  ->select("*")
                  ->orderBy('dia_evento.fecha','ASC')
                  ->where("id",$request->get('id_dia_evento'))
                  ->first();

        // dd($webinar);

        $cedulas = DB::table("asistencia_evento")
                  ->select("cedula")
                  ->where("id_dia_evento",$request->get('id_dia_evento'))
                  ->where("cedula",$request->cedula)
                  ->get();
        $validador =  count($cedulas);

        $asistentes = DB::table("asistentes_webinar")
                  ->select("cedula")
                  ->where("id_webinar",$dia_evento->id_webinar)
                  ->where("cedula",$request->cedula)
                  ->get();
        $validadorregistro = count($asistentes);

        $infoponentes = DB::table("asignacion_ponentes")
                ->select("asignacion_ponentes.*","ponentes.nombres","ponentes.apellidos","ponentes.foto","ponentes.profesion","ponentes.link_fb","ponentes.link_tw","ponentes.link_inst","ponentes.link_linke")
                ->leftjoin("ponentes","asignacion_ponentes.id_ponente","=","ponentes.id")
                ->where("id_dia_evento",$request->get('id_dia_evento'))
                ->get();
        // $validador = 0;

        //dd($asistentes);
        //

        //dd($infoponentes);
        
  if($validadorregistro != 0):  
          if($validador == 0):
            
                ////////////////////////////////// guardar
                $asistencia = new EventoAsistencia($request->all());
                $asistencia->id_dia_evento = $request->get('id_dia_evento');
                $asistencia->save();


                $calif = $request->input('calificacion');

                foreach ($infoponentes as $info) {

                  $calificacion = new CalificacionPonente($request->all());
                  $calificacion->id_dia_evento = $request->get('id_dia_evento');
                  $calificacion->id_ponente = $info->id_ponente;
                  $calificacion->calificacion = $calif[$info->id];
                  $calificacion->save();
                }

                return redirect()->back()->with('message', 'Se ha registrado con éxito tu asistencia:');
          else:
            Session::flash('message-error', "Esta cédula ya ha registrado su asistencia para este día.");
          endif;
  else:
          Session::flash('message-error', "Esta cédula no esta registrada para este evento.");
  endif;

  
  



  return redirect()->back(); 
  }









    function diaES($fecha){
        $dia = date('l', strtotime($fecha));
        $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
        $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
        $nombredia = str_replace($dias_EN, $dias_ES, $dia);
        return $nombredia;
    }
    function mesES($fecha){
        $mes = date('M', strtotime($fecha));
        $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        $meses_EN = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
        return $nombreMes;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
}