<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PonenteAsignado extends Model
{
    //
    protected $table = 'asignacion_ponentes';

    protected $fillable = ['id', 'id_ponente','id_webinar'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}