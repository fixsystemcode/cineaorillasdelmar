<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\TipoCrearRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TiposArticulos;
use App\CamposExtrasGrupos;
use App\Categoria;
use App\CategoriaCampoExtraGrupo;
use App\VistasCamposModel;
use App\CamposExtras;
use App\Usuarios;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use Mail;

use Illuminate\Support\Facades\Input;

class CamposExtrasGruposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        //
        $datos = DB::table("camposextrasgrupo")
        ->orderBy('orden','ASC')
        ->get();
        //dd($datos);
        //$datos = CamposExtrasModulos::orderBy('id','DESC')->get();
        return View::make('admin.camposextrasgrupos.index', compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categorias = Categoria::lists('categoria','id');
        $vistacampos = VistasCamposModel::lists('nombre','id');
       
        return View::make('admin.camposextrasgrupos.crear', compact('categorias','vistacampos') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());

        $categorias = $request->categorias;

        //dd($categorias); 

        $dato = new CamposExtrasGrupos;
        $dato->descripcion = $request->descripcion;
        $dato->icono = $request->icono;
        $dato->orden = $request->orden;
        $dato->visible = "si";

        //$prioridad->prioridad = $request->prioridad;
        //dd($dato->id);
        $dato->save();

        

        foreach ($categorias as $categoria) {
            $pivote = new CategoriaCampoExtraGrupo;
            $pivote->id_categoria = $categoria;
            $pivote->id_campos_extras_grupo = $dato->id;
            $pivote->save();
        }   

         

        Session::flash('message','Campos extras tipo ingresado correctamente');
        return redirect::to('camposextrasgrupos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $dato = CamposExtrasGrupos::find($id);

        $categorias = Categoria::lists('categoria','id');
        $vistacampos = VistasCamposModel::lists('nombre','id');

        $mis_Categorias = CamposExtrasGrupos::join("categoriacamposextrasgrupo","camposextrasgrupo.id","=","categoriacamposextrasgrupo.id_campos_extras_grupo")
        ->select("camposextrasgrupo.id","categoriacamposextrasgrupo.*")
        ->where("camposextrasgrupo.id",$dato->id)
        ->lists('id_categoria')->ToArray();
       
       //dd($mis_Categorias);
       
        
        if (is_null($dato))
        {
            return Redirect::route('camposextrasmodulos.index');
        }
 

        return View::make('admin.camposextrasgrupos.editar', compact('dato','categorias','mis_Categorias','vistacampos'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $categorias = $request->categorias;

        //dd($categorias);

        $dato = CamposExtrasGrupos::find($id);
        $dato->fill($request->all());
        $dato->save();

         
        //dd($det);
        $dato->categorias()->detach();

        if ($categorias != null) {

             foreach ($categorias as $categoria) {
                    $pivote = new CategoriaCampoExtraGrupo;
                    $pivote->id_categoria = $categoria;
                    $pivote->id_campos_extras_grupo = $dato->id;
                    $pivote->save();
                } 
         } 
       

        return Redirect::to('camposextrasgrupos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        CamposExtrasGrupos::find($id)->delete();
        return Redirect::to('camposextrasgrupos');
    }
}
