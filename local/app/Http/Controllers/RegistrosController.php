<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UsuarioCrearRequest;
use App\Http\Requests\UsuarioActualizarRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Slider;
use App\Video;
use App\Articulo;
use App\Tipos;
use App\Imagen;
use Auth;
use OAuth;
use Mail;
use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;




class RegistrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function index()
    {
        //

         $acep = Auth::user(); 

         if ($acep == null) {
            
            $imgs2 = Imagen::all(); 
            return View::make('admin.usuarios.registro', compact('tipos','imgs2'));

         }else{
            
            $articulos2 = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria")
                ->where('articulos.id_usuario',$acep->id)
                ->orderBy('created_at','ASC')
                ->get();
                
            //dd($articulos2);

                $articulos = DB::table("articulos")
                            ->join("categorias","articulos.id_categoria","=","categorias.id")
                            ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                            ->select("articulos.*","usuarios.nombres","categorias.categoria")
                            ->where('articulos.id_usuario',$acep->id)
                            ->orderBy('articulos.created_at','DESC')
                            ->paginate(7);
                
                $carticulo = count($articulos2);

                $slider = Slider::all();
                $cslider = count($slider);


                $usuarios = User::all();
                $cusuarios = count($usuarios);


                $videos = Video::all();
                $cvideos = count($videos);


                $imgs2 = Imagen::all();
                return view('admin.tablero',compact('carticulo','cslider','cusuarios','cvideos','articulos','imgs2'));
         
         }
        

         

         
    }

 


     public function autorizado()
    {
        //
       // dd("holaaaaa");
       // dd(Auth::user());

       OAuth::login('facebook', function($user, $details) {
       // dd($details);
       $user->nombres = $details->full_name;
       $user->email = $details->email;
       $user->path = $details->avatar;
       $user->id_tipo_usuario = 5;
       $user->save();
         });
        
        // dd($user);


         $acep = Auth::user(); 

         if ($acep == null) {
            
            $imgs2 = Imagen::all(); 
            return View::make('admin.usuarios.registro', compact('tipos','imgs2'));

         }else{
            
            $articulos2 = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria")
                ->where('articulos.id_usuario',$acep->id)
                ->orderBy('created_at','ASC')
                ->get();
                
            //dd($articulos2);

                $articulos = DB::table("articulos")
                            ->join("categorias","articulos.id_categoria","=","categorias.id")
                            ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                            ->select("articulos.*","usuarios.nombres","categorias.categoria")
                            ->where('articulos.id_usuario',$acep->id)
                            ->orderBy('articulos.created_at','DESC')
                            ->paginate(7);
                
                $carticulo = count($articulos2);

                $slider = Slider::all();
                $cslider = count($slider);


                $usuarios = User::all();
                $cusuarios = count($usuarios);


                $videos = Video::all();
                $cvideos = count($videos);


                $imgs2 = Imagen::all();
                return view('admin.tablero',compact('carticulo','cslider','cusuarios','cvideos','articulos','imgs2'));
         
         }
        

         

         
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //die();

         //$tipos = Tipos::lists('tipo', 'id');
//dd("sdfdf");

        $imgs2 = Imagen::all(); 
         return View::make('admin.usuarios.registro', compact('tipos','imgs2'));
         

    }

     public function contrasena()
    {
        return view('layouts.recuperarcontrasena');
    }

     
      public function guardarrecuperacion(Request $request)
    { 
        

        //dd($request->all());

        $this->validate($request, [
                'cedula' => ['required','max:10','regex:/^[0-9]{10}$/']
            ]);
        $persona =User::where("ci","=",$request->cedula)->first();

        //dd($persona);
        if ($persona==null) {
            
            Session::flash('message-error','Este usuario no ha sido registrado');
            
            return Redirect::to('contrasena');

        }else{
            if (($persona->ci == $request->cedula  && $persona->email != $request->email)) {

                Session::flash('message-error','Este correo no coincide con su contraseña');
                return Redirect::to('contrasena');
            }else{
                if (($persona->confirmado == false)) {
                    Session::flash('message-error','Usted no ha confirmado su correo electrónico, comuniquese con el administrador para solucionar este problema');
                    return Redirect::to('contrasena');
                }else{
                        return Redirect::to('env/'.$persona->id);                    
                }
                
            }
        }
    }

    public function guardarrestablecer(Request $request)
    {
        $persona =User::where("ci","=",$request->cedula_ruc)->first();
        $persona->password = bcrypt($request->password);
        $persona->codigo = str_random(250);
        $persona->save();
        Session::flash('message','Su contraseña se cambio correctamente');
        return Redirect::to('login');
    }

    public function envio($id)
    {
        $persona = User::find($id);
        
        Mail::send('emailrecucontra',['datos' => $persona],  
            function($m) use ($persona){
                $email="info@laciudad.ec";
                $nombres="La ciudad";
                $m->from("info@casasvacacionales.com.ec","Casas Vacacionales");
                $m->to($persona->email,$persona->nombres)->subject('Cambio de contraseña');
        });
        Session::flash('message','Se ha enviado un mensaje a su correo para que reestablezca su contraseña, puede que aparezca como spam');
        return Redirect::to('login');
    }

    public function busqueda($codigo){
        $persona = User::where('codigo','=',$codigo)->first();
        return view('layouts.reestablecercorreo', ['datos'=>$persona]);
    }


    public function solicitudes()
    {
      $solicitudes = User::where('aceptado',0)->get();

      //dd($solicitudes);
      return View::make('admin.usuarios.solicitudes', compact('solicitudes'));
    }

    
  public function habilitar_usuario($id)
    {
     $usuario = User::find($id);
     $usuario->aceptado=true;
     $usuario2=$usuario;
    // $usuario->habilitado=1;
     $usuario->save();


    $user_mail=$usuario2->email;
    $user_nombres=$usuario2->nombres;
         // Avisar a ususario que ya puede ingresar al sistema
           Mail::send('avisoaprobacionmail',['email' => $usuario2->email,'nombres' => $user_nombres,'apellidos'=>$usuario2->apellidos],  
                    function($m) use ($user_mail,$user_nombres){
                        $m->from("info@laciudad.ec","La ciudad");
                        $m->to($user_mail,$user_nombres)->subject('Su cuenta ha sido aprobada');
                });


     return Redirect::to('solicitudes');   
    }

     public function habilitararticulo($id)
    {
     $articulo = Articulo::find($id);
     $articulo->activado=true;
     
     //dd($articulo);
    // $usuario->habilitado=1;
     $articulo->save();

    $usuario2 = User::find($articulo->id_usuario);

    $user_mail=$usuario2->email;
    $user_nombres=$usuario2->nombres;
         // Avisar a ususario que ya puede ingresar al sistema
           Mail::send('activiciones',['email' => $usuario2->email,'nombres' => $user_nombres,'apellidos'=>$usuario2->apellidos],  
                    function($m) use ($user_mail,$user_nombres){
                        $m->from("info@laciudad.ec","La ciudada");
                        $m->to($user_mail,$user_nombres)->subject('Su casa ha sido aprobada');
                });

     return Redirect::to('activaciones'); 

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
           
             //  \App\Usuarios::create([
             //   'nombres' => $request['nombres'],
             //   'email' => $request['email'],
             //   'telefono' => $request['telefono'],
             //   'login' => $request['login'],
             //   'password' => $request['password'],
             //   'fecha_nacimiento' => $request['fecha_nacimiento'],
             //   'id_tipo_usuario' => $request['id_tipo_usuario'],
             //   
             //   ]);

            //dd($request->all());

            $usuario = new User($request->all());

            //dd($usuario);
            $fileName = 'null';
            $dir = public_path().'/images/';

            $docs = $request->file('path');

            //dd($docs);
               if (Input::hasFile('path')){

                        $fileName = $docs->getClientOriginalName();
                        $docs->move($dir, $fileName);
              }
            
            $usuario->path = $fileName;

            $usuario->ci =  $request->ci;
            $usuario->password =  bcrypt($request->password);
            $usuario->codigo = str_random(250);
            $usuario->confirmado = false;
            $usuario->aceptado = false;
            $usuario->save();

            // $email2=$usuario->email;
            // $nombres2=$usuario->nombres;
            // $apellidos2=$usuario->apellidos;
            // $codigo2=$usuario->codigo;
            // Mail::send('email',['email' => $email2,'nombres' => $nombres2, 'codigo'=>$codigo2, 'apellido'=>$apellidos2],  
            //     function($m) use ($email2,$nombres2,$apellidos2,$codigo2){
            //         $email="info@laciudad.ec";
            //         $nombres="La ciudad";
            //         $m->from("info@laciudad.ec","La ciudad");
            //         $m->to($email2,$nombres2)->subject('Activación de cuenta');
            // });
            //Enviar correo a admin para que sepa que hay usuario sin confirma ingreso al sistema
            
               Session::flash('message','Se ha enviado un mensaje de confirmación a su correo, puede que aparezca como spam');
            return Redirect::to('registro');  
    
    }


    public function confirmar($codigo){

        //dd($id);

        //$idx;

        $imgs2 = Imagen::all();

            $persona =User::where("codigo","=",$codigo)->first();
            $persona2 = User::find($persona->id);
            $persona2->confirmado = true;
            $persona2->save();
            //return Redirect::to('login');
            return View('layouts.felicitacion',compact('imgs2'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $usuario = User::find($id);
        if (is_null($usuario))
        {
            return Redirect::route('usuarios.index');
        }

        $tipos = Tipos::lists('tipo', 'id');

        return View::make('admin.usuarios.editar', compact('usuario','tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuarioActualizarRequest $request, $id)
    {
        //
       
       $usuario = User::find($id);
        $password = $usuario->password;
        //dd($usuario->password);
        $usuario->fill($request->all());

        //dd($usuario);

        if (Input::hasFile('path')){

                $dir = public_path().'/images/';

                $images = $request->file('path');
                $fileName = $images->getClientOriginalName();
                $images->move($dir, $fileName);

                $usuario->path = $fileName;


                if (empty($request->password)) {
                     
                    $usuario->password = $password;
                    $usuario->save();

                    if ($usuario->tipo_usuario_id != 2) {
                        return Redirect::to('usuarios');
                    }else{
                         return Redirect::to('administracion'); 
                    }
                }else{
                    $usuario->password = bcrypt($request->password);
                    $usuario->save();

                    if ($usuario->tipo_usuario_id != 2) {
                        return Redirect::to('usuarios');
                    }else{
                         return Redirect::to('administracion'); 
                    }
                }
        }


        else{

            if (empty($request->password)) {
                     $usuario->password = $password;
                     $usuario->save();

                    if ($usuario->tipo_usuario_id != 2 ){
                        return Redirect::to('usuarios');
                    }else{
                         return Redirect::to('administracion'); 
                    }
                }else{
                    $usuario->password = bcrypt($request->password);
                    $usuario->save();

                    if ($usuario->tipo_usuario_id != 2) {
                        return Redirect::to('usuarios');
                    }else{
                         return Redirect::to('administracion'); 
                    }
                }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::find($id)->delete();
        return Redirect::route('usuarios.index');
    }
}


    