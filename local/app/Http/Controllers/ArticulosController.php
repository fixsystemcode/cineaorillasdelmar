<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Foto;
use App\Articulo;
use App\User;
use App\Imagen;
use App\Documentos;
use App\Videosar;
use App\prioridad;

use App\Categoria;
use App\VisitasModel;
use App\ArticuloTagModel;
use App\CamposExtrasGrupos;
use App\CamposExtras;
use View;
use Auth;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;

use File;
use Illuminate\Support\Facades\Input;
use Facebook;
use TwitterAPIExchange;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class ArticulosController extends Controller
{


     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



     public function limpiar_caracteres_especiales($s) {
      $s = str_replace("á","a",$s);
      $s = str_replace("Á","A",$s);
      $s = str_replace("é","e",$s);
      $s = str_replace("É","E",$s);
      $s = str_replace("í","i",$s);
      $s = str_replace("Í","I",$s);
      $s = str_replace("ó","o",$s);
      $s = str_replace("Ó","O",$s);
      $s = str_replace("ú","u",$s);
      $s = str_replace("Ú","U",$s);
      $s = str_replace(" ","-",$s);
      $s = str_replace("´","-",$s);
      $s = str_replace("ñ","n",$s);
      $s = str_replace("Ñ","N",$s);
      //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
      //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
      return $s;
    }

    public function inicio_fin_semana($fecha){

      $diaInicio="Monday";
      $diaFin="Sunday";

      $strFecha = strtotime($fecha);

      $fechaInicio = date('Y-m-d',strtotime('last '.$diaInicio,$strFecha));
      $fechaFin = date('Y-m-d',strtotime('next '.$diaFin,$strFecha));

      if(date("l",$strFecha)==$diaInicio){
        $fechaInicio= date("Y-m-d",$strFecha);
      }
      if(date("l",$strFecha)==$diaFin){
        $fechaFin= date("Y-m-d",$strFecha);
      }
      return $fechaInicio;
    }


    public function __construct()
    {
      $this->middleware('auth');
    }


    public function index()
    {

      $acep = Auth::user();
        //dd($acep);
      $acep = Auth::user();
      if ($acep->id_tipo_usuario == 6) {
          return Redirect::to('formregistro');
      }

      if ($acep->id_tipo_usuario == 5) {

        $articulos = DB::table("articulos")
        ->join("categorias","articulos.id_categoria","=","categorias.id")
        ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        ->select("articulos.*","usuarios.nombres","categorias.categoria")
        ->where('articulos.id_usuario',$acep->id)
        ->orderBy('created_at','ASC')
        ->get();

        $imgs2 = Imagen::all();
        return view('admin.tablero', compact('articulos','imgs2'));

      }else{

        $articulos = DB::table("articulos")
        ->join("categorias","articulos.id_categoria","=","categorias.id")
        ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        ->select("articulos.*","usuarios.nombres","categorias.categoria")
        ->orderBy('created_at','ASC')
        ->get();

        return view('admin.articulos.index', compact('articulos'));

      }

        //dd($articulos);



    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function activaciones()
    {

      $articulos = DB::table("articulos")
      ->join("categorias","articulos.id_categoria","=","categorias.id")
      ->join("usuarios","articulos.id_usuario","=","usuarios.id")
      ->select("articulos.*","usuarios.nombres","categorias.categoria")
      ->where("articulos.activado", false)
      ->orderBy('created_at','ASC')
      ->get();

      return view('admin.articulos.activacion', compact('articulos'));



    }

    public function create()
    {


      $acep = Auth::user();

      if ($acep->id_tipo_usuario == 5) {

        $categorias = Categoria::where('categoria','<>','LaCiudad')
        ->Where('categoria','<>','casas')
        ->pluck('categoria','id')->all();

        // $categorias = null
      }
      else{
        $categorias = Categoria::pluck('categoria','id');
      }
        // $tags = null;
      $tags = Tag::pluck('tag','id')->all();
      $prioridad = prioridad::pluck('nombre','id')->all();

      /*Campos Extras*/

      $tabla = DB::table("camposextras")
      ->join("camposextrasgrupo","camposextrasgrupo.id","=","camposextras.id_campos_extras_grupo")
      ->select("camposextras.*",
        "camposextrasgrupo.id as id_grupo",
        "camposextrasgrupo.descripcion as grupo")
      ->orderBy('id','DESC')
      ->get();
   //dd($tabla);

      $grupos= CamposExtrasGrupos::orderBy('id','ASC')->get();
  // dd($grupos);
      // $tabla= CamposExtras::orderBy('orden','ASC')->get();
            //dd($tabla);
      $camposgruposid=array();
      $camposgrupos=array();
      $camposextras=array();
      $camposextrasdesc=array();
      $camposextrastipo=array();
      $camposextrasvalor=array();

      foreach($tabla as $key  => $value)
      {
       $camposgruposid[$key]=  $value->id_grupo;
       $camposgrupos[$key]=  $value->grupo;
       $camposextras[$key]=  $value->nombre;
       $camposextrasdesc[$key]=  $value->descripcion;
       $camposextrastipo[$key]=  $value->tipo;
       $camposextrasvalor[$key]=  $value->valor;
     }
     /*Fin Campos Extras*/


         //dd($usuarios);

     if ($acep->id_tipo_usuario == 5) {

      $imgs2 = Imagen::all();
      return View::make('admin.articulos.creartablero' , compact('categorias','tags','tabla','grupos', 'camposgruposid','camposgrupos','camposextras','camposextrasdesc','camposextrastipo','camposextrasvalor','imgs2','prioridad'));

    }else{
     return View::make('admin.articulos.crear' , compact('categorias','tags','tabla','grupos', 'camposgruposid','camposgrupos','camposextras','camposextrasdesc','camposextrastipo','camposextrasvalor','prioridad'));
   }



 }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd('aqui');
        //dd($request->all());
      $articulo = new Articulo($request->all());


      $articulo->slug = $this->limpiar_caracteres_especiales($request->titulo);
      $articulo->slug = strtolower($articulo->slug);
        // dd($articulo->slug);
      $tags = $request->tags;
      $videos = $request->videos;
      $titulos=$request->titulovideo;
      $imagenes = $request->ruta;
      $sobras = $request->sobras;
      $rutadocs=$request->rutadocs;
      $titulodocs=$request->titulodocs;
      $sobrasdocs=$request->sobrasdocs;
        //dd($videos);
        //dd($tags);
        ////////////////////////////////// imagenprincipal
      $imagenprincipal = $articulo->path;
      $articulo->  fill($request->all());
      $dir = public_path().'/images/';
      $docs = $request->file('imagenprincipal');
        //dd($docs);
      if (Input::hasFile('imagenprincipal')){

        $imagenprincipal = $docs->getClientOriginalName();
        $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
        while (is_file($dir.$imagenprincipal)) {
          $imagenprincipal=rand(0, 9999).$imagenprincipal;


        }
            //dd($imagenprincipal);
        $docs->move($dir, $imagenprincipal);
      }
        ///////////////////////////////////
      /*Guardar Campos Extras*/
      $tabla= CamposExtras::orderBy('orden','ASC')->get();
                         //dd(Input::get($value->nombre));
      if (isset($tabla[0]))
      {
        foreach($tabla as $key => $value)
        {

                 //dd(Input::get($value->nombre));

         $camposextras[$value->id]=Input::get($value->nombre);
       }
       $camposextras=json_encode($camposextras);
       $articulo->camposextras = $camposextras;
     }
     /*Fin de campo de extras*/
        ///////////////////////////////////
        //dd($tags);





     if ($request->destacado){
      $articulo->destacado=1;
    }else{
      $articulo->destacado=0;
    }
    if ($request->estado){
      $articulo->estado=1;
    }else{
      $articulo->estado=0;
    }
    $articulo->latitud = $request->latitud;
    $articulo->longitud = $request->longitud;

    //$articulo->rutadoc = $fileName;
    $articulo->path = $imagenprincipal;

        //$articulo->precio = $request->precios;
        //dd($request->file('ruta'));
    $acep = Auth::user();
    if ($acep->id_tipo_usuario == 5) {
      $articulo->activado = false;
    }
    else{
      $articulo->activado = true;
    }
    $articulo->save();

    if ($tags != null) {
      foreach ($tags as $tag) {
                //dd((int)$tag);
                //die();
        $pivote = new ArticuloTagModel;
        $pivote->tag_id  = (int)$tag;
        $pivote->articulo_id = $articulo->id;
        $pivote->save();
      }
    }

    if ($videos != null) {
      $orden = 1;
      foreach ($videos as $v => $video ) {
                //dd((int)$video);
                //die();
        $videosar = new Videosar;

        $videosar->path=$video;
        $videosar->titulo=$titulos[$v];
        $videosar->orden=$orden;
        $videosar->id_articulos=$articulo->id;
        $videosar->save();
        $orden=$orden+1;

      }
    }



        //dd($articulo);
    $id_articulo = $articulo->id;
    $dir = public_path().'/galeria/';

    if ($imagenes != null) {
      $numero =0;
      foreach ($imagenes as $imagen) {
                //dd((int)$video);
                //die();

        $foto = new Foto;
        $foto->titulo = $this->limpiar_caracteres_especiales($imagen);
        $foto->path = $this->limpiar_caracteres_especiales($imagen);
        $foto->orden = $numero;
        $foto->articulo_id = $id_articulo;
        $foto->save();
        $numero=$numero+1;



      }
    }
    if ($sobras != null) {

      foreach ($sobras as $sobra) {
        File::delete($dir.$sobra);
      }
    }

    $dir = public_path().'/doc/';

    if ($rutadocs != null) {
      $numero =0;
      foreach ($rutadocs as $v =>  $rutadoc) {
                //dd((int)$video);
                //die();

        $doc = new Documentos;
        $doc->path = $this->limpiar_caracteres_especiales($rutadoc);
        $doc->orden = $numero;
        $doc->titulo=$titulodocs[$v];
        $doc->articulo_id = $id_articulo;
        $doc->save();
        $numero=$numero+1;



      }
    }
    if ($sobrasdocs != null) {

      foreach ($sobrasdocs as $sobra) {
        File::delete($dir.$sobra);
      }
    }



/*
    $docs = $request->file('rutadoc');
        //dd($docs);
    if (Input::hasFile('rutadoc')){
      $fileName = $docs->getClientOriginalName();
      $fileName = $this->limpiar_caracteres_especiales($fileName);
      $docs->move($dir, $fileName);
    }
    */

        //compartir en FB
    if ($request->publicar){

      $nomcategoria = Categoria::find($articulo->id_categoria);
      $this->publicarfb($nomcategoria->categoria, $articulo->slug,$request->descripcionredes);
      $this->creatweets($nomcategoria->categoria, $articulo->slug,$request->descripcionredes);
    }




    Session::flash('message','Artículo fue creado correctamente');
    return redirect::to('articulos');

  }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $acep = Auth::user();
      $articulo = Articulo::find($id);

        //dd($articulo);

      $imgs2 = Foto::where('articulo_id','=',$articulo->id)->orderBy('orden','asc')->get();
      $docs = Documentos::where('articulo_id','=',$articulo->id)->orderBy('orden','asc')->get();
        //dd($imgs2);

      $users = User::pluck('nombres', 'id')->all();

      $categorias =Categoria::pluck('categoria', 'id');
      $prioridad = prioridad::pluck('nombre','id')->all();

      $tags = Tag::pluck('tag','id')->all();

        //dd($tags);

      $mis_videos = Videosar::where("id_articulos",$articulo->id)
      ->orderBy('orden','asc')
      ->get();

      $mis_Tags = Articulo::join("articulo_tag","articulos.id","=","articulo_tag.articulo_id")
      ->select("articulos.id","articulo_tag.*")
      ->where("articulos.id",$articulo->id)
      ->pluck('tag_id')->ToArray();

       // dd($mis_Tags);
          //dd($mis_videos);
      /*Campos Extras*/
      $campos = DB::table("camposextras")
      ->join("camposextrasgrupo","camposextrasgrupo.id","=","camposextras.id_campos_extras_grupo")
      ->select("camposextras.*",
        "camposextrasgrupo.id as id_grupo",
        "camposextrasgrupo.descripcion as grupo")
      ->orderBy('id','DESC')
      ->get();
           //dd($tabla);


      $grupos= CamposExtrasGrupos::orderBy('id','ASC')->get();

      /*Campos del formulario*/
      $camposgruposid=array();
      $camposextras=array();
      $camposextrasid=array();
      $camposextrasdesc=array();
      $camposextrastipo=array();
      $camposextrasvalor=array();

      foreach($campos as $key  => $value)
      {
       $camposgruposid[$key]=  $value->id_grupo;
       $camposextrasid[$key]=  $value->id;
       $camposextras[$key]=  $value->nombre;
       $camposextrasdesc[$key]=  $value->descripcion;
       $camposextrastipo[$key]=  $value->tipo;
       $camposextrasvalor[$key]=  $value->valor;
     }

     /*Datos existentes*/
     $camposguardados = $articulo->camposextras;
     $camposextrasguardados=  json_decode($camposguardados, true);

               //print_r($camposextrasguardados);
               //$pilas = count($camposextrasguardados);

               //echo count($camposextrasguardados);

     /*Fin Campos Extras*/




        //$mis_Tags =  $articulo->tags->lists('id')->ToArray();

        //dd($mis_Tags);

        //return view('admin.articulos.editar',compact('users','tags','categorias','articulo','mis_Tags','imgs2'));

     if ($acep->id_tipo_usuario == 5) {

          //dd($mis_Tags);
      $imgs2 = Imagen::all();
      return View::make('admin.articulos.editartablero',compact('grupos','users','tags','categorias','articulo','mis_Tags','mis_videos','imgs2', 'camposgruposid','camposextrasid','camposextras','camposextrasdesc','camposextrastipo','camposextrasvalor','camposguardados','camposextrasguardados','docs','prioridad'));


    }else{
      return View::make('admin.articulos.editar',compact('grupos','users','tags','categorias','articulo','mis_Tags','mis_videos','imgs2', 'camposgruposid','camposextrasid','camposextras','camposextrasdesc','camposextrastipo','camposextrasvalor','camposguardados','camposextrasguardados','docs','prioridad'));

    }



  }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //dd($request->all());
      //dd($request->inicio);

      $articulo = Articulo::find($id);

        // dd($articulo->precio);
        // dd($articulo);
      /// imagenprincipal
      $imagenprincipal = $articulo->path;
      $articulo->fill($request->all());

        // $articulo->slug = $this->limpiar_caracteres_especiales($request->titulo);
        // $articulo->slug = strtolower($articulo->slug);
      $videos = $request->videos;
      $titulos=$request->titulovideo;
      $imagenes = $request->ruta;
      $sobras = $request->sobras;
      $rutadocs=$request->rutadocs;
      $titulodocs=$request->titulodocs;
      $sobrasdocs=$request->sobrasdocs;




      $dir = public_path().'/images/';

      $docs = $request->file('imagenprincipal');

        //dd($docs);

      if (Input::hasFile('imagenprincipal')){


        $imagenprincipal = $docs->getClientOriginalName();
        $imagenprincipal = $this->limpiar_caracteres_especiales($imagenprincipal);
        while (is_file($dir.$imagenprincipal)) {
          $imagenprincipal=rand(0, 9999).$imagenprincipal;


        }

            //dd($imagenprincipal);
        $docs->move($dir, $imagenprincipal);
        $articulo->path = $imagenprincipal;
      }
       ////////////////////////////////// doc
      $fileName = $articulo->rutadoc;


      $articulo->  fill($request->all());


      /*Guardar Campos Extras*/
      $tabla= CamposExtras::orderBy('orden','ASC')->get();
//                        print_r($tabla);
//                        die();

      if (isset($tabla[0])){
        foreach($tabla as $key => $value)
        {
          $camposextras[$value->id]=Input::get($value->nombre);
        }
        $camposextras=json_encode($camposextras);
        $articulo->camposextras = $camposextras;
      }
      /*Fin de campo de extras*/
      if ($request->destacado){
        $articulo->destacado=1;
      }else{
        $articulo->destacado=0;
      }
      if ($request->estado){
        $articulo->estado=1;
      }else{
        $articulo->estado=0;
      }
      $articulo->latitud = $request->latitud;
      $articulo->longitud = $request->longitud;
      $articulo->save();


        //guardar tag de mucho a mucho
      if($request->tags <> NULL) {

        $articulo->tags()->sync($request->tags);

      }else{
        $articulo->tags()->detach();
      }
      $sobrasvideos=$request->sobrasvideos;
      if ($sobrasvideos != null) {

        foreach ($sobrasvideos as $sobra) {

          Videosar::where('id_articulos', 'LIKE', $id)
          ->where('path','like',$sobra)->delete();

        }
      }


      if($request->videos <> NULL) {

       $videos = $request->videos;
       $orden=1;
       foreach ($videos as $v =>  $video) {
        try {
          $model = Videosar::where('id_articulos', 'like', $id)
          ->where('path','like',$video)
          ->firstOrFail();
          $model->orden=$orden;
          $model->save();
        } catch (ModelNotFoundException $ex) {
          $videosar = new Videosar;
          $videosar->path=$video;
          $videosar->titulo=$titulos[$v];
          $videosar->orden=$orden;
          $videosar->id_articulos=$id;
          $videosar->save();

        }

        $orden=$orden+1;

      }
    }




    $id_articulo = $articulo->id;
        //dd($id_articulo);
    $dir = public_path().'/galeria/';
        //dd($dir);
    if ($sobras != null) {

      foreach ($sobras as $sobra) {
        Foto::where('path', 'LIKE', $sobra)->delete();
        File::delete($dir.$sobra);

      }
    }
    $galeria=array();
    $fotos = Foto::where('articulo_id','=',$id_articulo)->get();
    foreach ($fotos as $foto) {
      array_push($galeria,$foto->path);

    }
    if ($imagenes != null) {
      $numero =0;
      foreach ($imagenes as $imagen) {
                //dd((int)$video);
                //die();

        if (in_array($imagen,$galeria)) {

          $foto = Foto::where('path','like',$imagen)->firstOrFail();
          $foto->orden=$numero;
          $foto->save();

        }else{
         $foto = new Foto;
         $foto->titulo = $this->limpiar_caracteres_especiales($imagen);
         $foto->path = $this->limpiar_caracteres_especiales($imagen);
         $foto->orden = $numero;
         $foto->articulo_id = $id_articulo;
         $foto->save();

       }


       $numero=$numero+1;



     }
   }


   $dir = public_path().'/doc/';
   if ($sobrasdocs != null) {

    foreach ($sobrasdocs as $sobra) {
      File::delete($dir.$sobra);
      Documentos::where('articulo_id', 'LIKE', $id)
      ->where('path','like',$sobra)->delete();

    }
  }

  if ($rutadocs != null) {
    $orden=1;
    foreach ($rutadocs as $v =>  $rutadoc) {
      $rutadoc=$this->limpiar_caracteres_especiales($rutadoc);

      try {
        $model = Documentos::where('articulo_id', 'like', $id)
        ->where('path','like',$rutadoc)
        ->firstOrFail();
        $model->orden=$orden;
        $model->save();
      } catch (ModelNotFoundException $ex) {
        $doc = new Documentos;
        $doc->path = $rutadoc;
        $doc->orden = $orden;
        $doc->titulo=$titulodocs[$v];
        $doc->orden=$orden;
        $doc->articulo_id = $id_articulo;
        $doc->save();

      }
      $orden =$orden+1;


    }
  }





  if ($request->publicar){

    $nomcategoria = Categoria::find($articulo->id_categoria);
    $this->publicarfb($nomcategoria->categoria, $articulo->slug,$request->descripcionredes);
    $this->creatweets($nomcategoria->categoria, $articulo->slug,$request->descripcionredes);
  }

  if ($request->inicio!='on') {
    Session::flash('message','Articulo editado Correctamente');
    return redirect::to('articulos') ;


  }else{

   Session::flash('message','Articulo editado Correctamente');
   return redirect()->route('articulos.edit', $id);
 }



}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //$articulo = Articulo::find($id);
       // $articulo = delete();


      //Foto::find($id)->delete();
      $fotos = Foto::where('articulo_id','=',$id)->get();
      $dir = public_path().'/galeria/';
      foreach ($fotos as $foto) {
        File::delete($dir.$foto->path);
      }
      $dir = public_path().'/images/';
      $art= Articulo::find($id);
      if ($art->path!=null) {
        File::delete($dir.$art->path);
      }
      $documentos = Documentos::where('articulo_id','=',$id)->get();
      $dir = public_path().'/doc/';
      foreach ($documentos as $doc) {
        File::delete($dir.$doc->path);
      }
      Videosar::where('id_articulos', 'LIKE', $id)->delete();
      Documentos::where('articulo_id', 'LIKE', $id)->delete();
      Foto::where('articulo_id', 'LIKE', $id)->delete();
      Articulo::destroy($id);
      Session::flash('message','Articulo eliminado Correctamente');
      return redirect::to('articulos') ;
    }


    function publicarfb($nomcategoria, $titulo,$mensaje){
    //app la ciudad credenciales
      $fb = new Facebook\Facebook([
        'app_id' => '644942119027203',
        'app_secret' => 'b182c4c6bae44eaab77ca5bf7f65ceb3',
        'default_graph_version' => 'v2.2',
      ]);
    //datos de publicacion
      $nomcategoria=$this->limpiar_caracteres_especiales($nomcategoria);
      $nomcategoria = strtolower($nomcategoria);
      $enlace='http://laciudad.ec/'.$nomcategoria.'/'.$titulo;
      $linkData = [
        'link' => $enlace,
        'message' => $mensaje,
        'value' => 'EVERYONE ',
      ];

      try {

        $helper = $fb->getRedirectLoginHelper();
        if (isset($_GET['state'])) {
          $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }
      //token generado en FB
        $accessToken = "EAAJKkknnwgMBAKCeXZBr9eIZCJ0xwzQadWdCaev9YekGIZCSOLZBbYe7lXKuSgEdvChFS3whVEKliMSMxgE2pCcydDCvAdZBYZBLmvI1yfQlUy3OisqS55blaT2qtYqfr4jXJtzHyJxSscEd5e7BHPozi9nQdVJFxwJLh46sn4NgZDZD";

      //token prueba
      //EAAIHgKAYDVcBABFLlvMZB8X1JNmjqzCQeNy6iujCQTuI2UjRgfTSDFwt53spOwWuioqlwuGEcydWZBmf4ZCLCkpbkQy9mn9KMKmZCZB3nxPaDNSQz87BRAJPUtuBsfFGGPsptK7seIHyFbApNdsZAQ8ioiESKtQ2z97OfNBkDQ3gZDZD
      //$accessToken = $helper->getAccessToken();
      // Returns a `Facebook\FacebookResponse` object
      //id de la pagina a publicar
        $response = $fb->post('/870146743130480/feed', $linkData, $accessToken);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }

      $graphNode = $response->getGraphNode();

    }


    function creatweets($nomcategoria, $titulo,$mensaje) {

      $url = "https://api.twitter.com/1.1/statuses/update.json";
      $requestMethod = 'POST';

        // configuracion de la cuenta

      $settings = array(
        'oauth_access_token' => '827526089194995712-3NdWhfdrIh8f8zA7OsZFZk670yXpXh9',
        'oauth_access_token_secret' => 'nqDfjFvU1gT7YglQ1dfw8JEIkRFKzsArGUoE9ZhhuEl3Z',
        'consumer_key' => 'tp2K5Xm0X6vdMNgfFHZA9cRYD',
        'consumer_secret' => '9wkYe7lGSL1u9LVhu0YWQj4yU918SlfY61Ko6rGZFBuRb3SKEZ',
      );

      $nomcategoria=$this->limpiar_caracteres_especiales($nomcategoria);
      $nomcategoria = strtolower($nomcategoria);
      $enlace='http://laciudad.ec/'.$nomcategoria.'/'.$titulo;
      $status= $mensaje.'->'.$enlace;

        // establecer el mensaje
      $postfields = array('status' => $status);

        // crea la coneccion con Twitter
      $twitter = new TwitterAPIExchange($settings);
    // envia el tweet
      $twitter->buildOauth($url, $requestMethod)
      ->setPostfields($postfields)
      ->performRequest();
    }

    Public function subirfotogaleria()
    {
      if (isset($_FILES["image"]))
      {
        $file = $_FILES["image"];
    //$nombre = $file["name"];
        $nombre=$_POST['nombre'];
        if (isset($_POST['idarticulo'])){
          $idarticulo=$_POST['idarticulo'];
        }
        $nombre=$this->limpiar_caracteres_especiales($nombre);
        $tipo = $file["type"];
        $ruta_provisional = $file["tmp_name"];
        $size = $file["size"];
        $dimensiones = getimagesize($ruta_provisional);
        $width = $dimensiones[0];
        $height = $dimensiones[1];

        $carpeta = public_path().'/galeria';
        if (!file_exists($carpeta)) {
          mkdir($carpeta, 0775, true);
        }
        $carpeta = public_path().'/galeria/';
        $src = $carpeta.$nombre;

        while (is_file($src)) {
          $nombre=rand(0, 9999).$nombre;
          $src =$carpeta.$nombre;

        }


    //$file= Image::make($file)->fit(500, 500);


        if ($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png' && $tipo != 'image/gif')
        {
          echo "Error, el archivo no es una imagen";
        }else{
          //$src = $carpeta.$nombre;
          move_uploaded_file($ruta_provisional, $src);
          if (isset($idarticulo)){
            $foto = new Foto;
            $foto->titulo = $this->limpiar_caracteres_especiales($nombre);
            $foto->path = $this->limpiar_caracteres_especiales($nombre);
            $foto->articulo_id = $idarticulo;
            $foto->save();

          }




        }
      }

    }

    Public function subirdoc()
    {


      $file = $_FILES["archivo"];
    //$nombre = $file["name"];
      $path=$_POST['path'];
      $path= $this->limpiar_caracteres_especiales($path);
      $ruta_provisional = $file["tmp_name"];
      $carpeta = public_path().'/doc';
      if (!file_exists($carpeta)) {
        mkdir($carpeta, 0775, true);
      }
      $carpeta = public_path().'/doc/';
      $src = $carpeta.$path;

      while (is_file($src)) {
        $path=rand(0, 9999).$path;
        $src =$carpeta.$path;

      }
      move_uploaded_file($ruta_provisional, $src);


    }


  }