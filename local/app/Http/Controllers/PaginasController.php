<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Articulo;
use App\Informacion;
use App\Slider;
use App\Imagen;
use App\Foto;
use App\VisitasModel;
use App\mapasModel;
use App\Categoria;
use App\CamposExtrasGrupos;
use App\CamposExtras;
use App\Tag;

use Carbon\Carbon;
use SendGrid;

use View;
use Session;
use Redirect;
use Illuminate\Support\Facades\DB;



use Illuminate\Support\Facades\Input;

class PaginasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


      $noticias = DB::table("articulos")
                  ->join("categorias","articulos.id_categoria","=","categorias.id")
                  ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                  ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                  ->orderBy('articulos.created_at','DESC')
                  ->where('categorias.categoria', 'NOTICIAS')
                  ->where("articulos.activado", true)
                  ->limit(2)
                  ->get();

      $eventos = DB::table("articulos")
                  ->join("categorias","articulos.id_categoria","=","categorias.id")
                  ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                  ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                  ->orderBy('articulos.fechainicio','ASC')
                  ->where('categorias.categoria', 'EVENTOS')
                  ->where("articulos.activado", true)
                  ->limit(3)
                  ->get();

      $testimonios = DB::table("articulos")
                  ->join("categorias","articulos.id_categoria","=","categorias.id")
                  ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                  ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                  ->orderBy('articulos.fechainicio','ASC')
                  ->where('categorias.categoria', 'TESTIMONIOS')
                  ->where("articulos.activado", true)
                  ->limit(5)
                  ->get();

      $webinars = DB::table("webinar")
                  ->select(
                    "*",
                    DB::raw("CASE WHEN slug is null or slug = '' THEN replace(lower(tema),' ', '') ELSE slug END as slug")
                  )
                  ->orderBy('webinar.fecha','ASC')
                  ->where("webinar.estado", 1)
                  ->get();



      //dd($webinars);


      return view('inicio.index', compact('noticias', 'eventos', 'testimonios','webinars'));

    }

    

    
       


    public function enviarmensaje(Request $request){
      if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
      //your site secret key
          $secret = '6Le7g3IUAAAAAIryHoTx2yCgqas8HZlo20wiUCKJ';
      //get verify response data
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);

          $name = !empty($_POST['name'])?$_POST['name']:'';
          $email = !empty($_POST['email'])?$_POST['email']:'';
          $ciudad = !empty($_POST['ciudad'])?$_POST['ciudad']:'';
          $subject = "Contacto desde la página web Runachay.com.ec";
          $message = !empty($_POST['message'])?$_POST['message']:'';
          if($responseData->success):
        //contact form submission code
            $htmlContent = "
              <div style='padding: 20px 40px;max-width: 580px;background: #f5f5f5;'>
                <h2><strong>Runachay Ecuador.</strong></h2>
                <h4>Tienes un nuevo mensaje desde el sitio web de Runachay.</h4>
                <br>
                <div>
                  <p><strong>Nombres: </strong> ".$name.".</p>
                  <p><strong>Email: </strong>".$email."</p>
                  <p><strong>Ciudad de contacto: </strong>".$ciudad."</p>
                  <p><strong>Asunto: </strong>".$subject."</p>
                  <p><strong>Mensaje: </strong>".$message."</p><br>
                </div>
                <br>
                <a href='http://runachay.com.ec'><i>www.runachay.com.ec/</i></a>
              </div>
            ";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:'.$name.' <'.$email.'>' . "\r\n";

            $from = new SendGrid\Email($request->name, $request->email);
            $subject = "Nuevo mensaje desde el sitio web de Runachay.";
            $to = new SendGrid\Email('Contacto Soporte', 'soporte@runachay.com.ec');
            $to2 = new SendGrid\Email('Contacto Ventas', 'ventas@runachay.com.ec');
            $to3 = new SendGrid\Email('Contacto Gerencia', 'gerencia@runachay.com.ec');

            $content = new SendGrid\Content("text/html", $htmlContent);

            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            $mail2 = new SendGrid\Mail($from, $subject, $to2, $content);
            $mail3 = new SendGrid\Mail($from, $subject, $to3, $content);

            $apiKey = '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';
            $sg = new \SendGrid($apiKey);
            $response = $sg->client->mail()->send()->post($mail);
            $response2 = $sg->client->mail()->send()->post($mail2);
            $response3 = $sg->client->mail()->send()->post($mail3);
            return 0;
          else:
            return 1;
            $errMsg = 'Robot verification failed, please try again.';
        endif;
        else:

          return 2;
          
      endif;
    }

    public function enviarsolicitud(Request $request){
      // dd($request);
   
      if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
      //your site secret key
          $secret = '6Le7g3IUAAAAAIryHoTx2yCgqas8HZlo20wiUCKJ';
      //get verify response data
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);
      
      // $url = !empty($_POST['url'])?$_POST['url']:'';
      $name = !empty($_POST['nombre'])?$_POST['nombre']:'';
      $apellido = !empty($_POST['apellido'])?$_POST['apellido']:'';
      $email = !empty($_POST['email'])?$_POST['email']:'';
      $phone = !empty($_POST['phone'])?$_POST['phone']:'';
      $institucion = !empty($_POST['institucion'])?$_POST['institucion']:'';
      $cargo = !empty($_POST['cargo'])?$_POST['cargo']:'';
      $provincia = !empty($_POST['provincia'])?$_POST['provincia']:'';
      $ciudad = !empty($_POST['ciudad'])?$_POST['ciudad']:'';
          if($responseData->success):
        //contact form submission code
        $to = 'jefferson.loorja@gmail.com';
        $subject = 'Solicitud de demostración';
        $htmlContent = "
          <div style='padding: 20px 40px;max-width: 580px;background: #f5f5f5;'>
            <h2><strong>Runachay Ecuador.</strong></h2>
            <h4>Tienes una nueva solicitud de demostración desde la página web de Runachay.</h4>
            <br>
            <div>
              <p><strong>Nombre: </strong>".$name." ".$apellido."</p>
              <p><strong>Email: </strong>".$email."</p>
              <p><strong>Telefono de contacto: </strong>".$phone."</p>
              <p><strong>Institucion educativa: </strong>".$institucion."</p>
              <p><strong>Cargo: </strong>".$cargo."</p>
              <p><strong>Provincia: </strong>".$provincia."</p>
              <p><strong>Ciudad: </strong>".$ciudad."</p>
            </div>
            <br>
            <a href='http://runachay.com.ec/'><i>www.runachay.com.ec</i></a>
          </div>
        ";
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From:'.$name.' <'.$email.'>' . "\r\n";


        $from = new SendGrid\Email($request->name, $request->email);
        $subject = "Solicitud de demostración desde la pagina web de Runachay";
        $to = new SendGrid\Email('Contacto Soporte', 'soporte@runachay.com.ec');
        //$to2 = new SendGrid\Email('Contacto Ventas', 'ventas@runachay.com.ec');
        //$to3 = new SendGrid\Email('Contacto Gerencia', 'gerencia@runachay.com.ec');
        
        //info@mansupplies.com.ec

        $content = new SendGrid\Content("text/html", $htmlContent);

        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        //$mail2 = new SendGrid\Mail($from, $subject, $to2, $content);
        //$mail3 = new SendGrid\Mail($from, $subject, $to3, $content);
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        //$response2 = $sg->client->mail()->send()->post($mail2);
        //$response3 = $sg->client->mail()->send()->post($mail3);

        // dd($response);
        // print_r($response);
        return 0;

          else:
              return 1;
              $errMsg = 'Robot verification failed, please try again.';
          endif;
      else:

        return 2;
          
      endif;
    }


    public function contratarunachay(Request $request){
      if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
          $secret = '6Le7g3IUAAAAAIryHoTx2yCgqas8HZlo20wiUCKJ';
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);
      
          $name = !empty($_POST['nombre'])?$_POST['nombre']:'';
          $apellido = !empty($_POST['apellido'])?$_POST['apellido']:'';
          $email = !empty($_POST['email'])?$_POST['email']:'';
          $phone = !empty($_POST['phone'])?$_POST['phone']:'';
          $institucion = !empty($_POST['institucion'])?$_POST['institucion']:'';
          $cargo = !empty($_POST['cargo'])?$_POST['cargo']:'';
          $provincia = !empty($_POST['provincia'])?$_POST['provincia']:'';
          $ciudad = !empty($_POST['ciudad'])?$_POST['ciudad']:'';
          if($responseData->success):
            $htmlContent = "
              <div style='padding: 20px 40px;max-width: 580px;background: #f5f5f5;'>
                <h2><strong>Runachay Ecuador.</strong></h2>
                <h4>Tienes una nueva solicitud de demostración desde la página web de Runachay.</h4>
                <br>
                <div>
                  <p><strong>Nombre: </strong>".$name." ".$apellido."</p>
                  <p><strong>Email: </strong>".$email."</p>
                  <p><strong>Telefono de contacto: </strong>".$phone."</p>
                  <p><strong>Institucion educativa: </strong>".$institucion."</p>
                  <p><strong>Cargo: </strong>".$cargo."</p>
                  <p><strong>Provincia: </strong>".$provincia."</p>
                  <p><strong>Ciudad: </strong>".$ciudad."</p>
                </div>
                <br>
                <a href='http://runachay.com.ec/'><i>www.runachay.com.ec</i></a>
              </div>
            ";
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From:'.$name.' <'.$email.'>' . "\r\n";

            $from = new SendGrid\Email($request->name, $request->email);
            $subject = "Solicitud de contrato desde Runachay.com.ec";
            // $to = new SendGrid\Email('Contacto Soporte', 'desarrolloweb@runachay.com.ec');
            $to = new SendGrid\Email('Contacto Soporte', 'soporte@runachay.com.ec');
            $to2 = new SendGrid\Email('Contacto Ventas', 'ventas@runachay.com.ec');
            $to3 = new SendGrid\Email('Contacto Gerencia', 'gerencia@runachay.com.ec');
        

            $content = new SendGrid\Content("text/html", $htmlContent);

            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            $mail2 = new SendGrid\Mail($from, $subject, $to2, $content);
            $mail3 = new SendGrid\Mail($from, $subject, $to3, $content);
            $apiKey = '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';
            $sg = new \SendGrid($apiKey);
            $response = $sg->client->mail()->send()->post($mail);
            $response2 = $sg->client->mail()->send()->post($mail2);
            $response3 = $sg->client->mail()->send()->post($mail3);

            return 0;
          else:
              return $responseData;
              $errMsg = 'Robot verification failed, please try again.';
          endif;
      else:
        return 2;
      endif;
    }


    public function noticias(){

        $noticias = DB::table("articulos")
                  ->join("categorias","articulos.id_categoria","=","categorias.id")
                  ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                  ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                  ->orderBy('articulos.like','DESC')
                  ->where('categorias.categoria', 'NOTICIAS')
                  ->where("articulos.activado", true)
                  ->paginate(2);

        return view('paginas.noticias', compact('noticias'));
    }

    public function articulo($slug){

        $articulo = DB::table("articulos")
                ->join("categorias","articulos.id_categoria","=","categorias.id")
                ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.id as id_categoria","categorias.icono")
                ->where("articulos.activado", true)
                ->where('slug','=', $slug)
                ->first();




        $images = Foto::where('articulo_id','=',$articulo->id)->get();

        $docu = DB::table("documentos")
                ->join("articulos","documentos.articulo_id","=","articulos.id")
                ->select("documentos.*")
                ->orderBy('orden', 'ASC')
                ->where("documentos.articulo_id","$articulo->id")
                ->get();

        $vide = DB::table("videosar")
                ->join("articulos","videosar.id_articulos","=","articulos.id")
                ->select("videosar.*")
                ->where("videosar.id_articulos","$articulo->id")
                ->get();
                
        // dd($vide);

        // dd($articulo);

        return view('paginas.articulo', compact('articulo', 'images', 'docu', 'vide'));
    }
         




    public function palabras($palabras)
    {
        //
        // return $palabras;
        //$articulos = Tag::where('tag', 'LIKE', '%'.$tag.'%');
        $informacion = Informacion::find(1);
        $imgs2 = Imagen::all();
         //dd($articulos);
        $ultimos  = DB::table("articulos")
                 ->join("categorias","articulos.id_categoria","=","categorias.id")
                 ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                 ->select("articulos.*","usuarios.nombres","categorias.categoria")
                 ->orderBy('created_at','DESC')
                 ->where('categoria', 'casas')
                 ->where("articulos.activado", true)
                 ->paginate(3);

        $tags = null ;
        // $tags = Tag::SeachTag($tag)->first();
        $fotos = Foto::all();
        $articulos = Articulo::where('texto', 'LIKE', '%'.$palabras.'%')
        ->join("categorias","articulos.id_categoria","=","categorias.id")
        ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
        ->get();

        // dd($articulos);

        $conta = count($articulos);

        // $articulos->each(function($articulos){
        //   $articulos->categoria;
        //  // dd($articulos->categoria);
        // });
        // $nombre =  Tag::SeachTag($tag)->first();
        $nombre =  null;



        return view('paginas.busquedacategorias',compact('nombre','articulos','informacion','ultimos','imgs2','tags','fotos','conta'));

    }

    public function busquedapalabrasinicio(Request $request)
    {
        //
        // dd($request->all());
        // return $palabras;
        //$articulos = Tag::where('tag', 'LIKE', '%'.$tag.'%');

        $palabras = $request->palabras;

        $informacion = Informacion::find(1);

        $categorias = Categoria::all();

        $tags = Tag::all();

        // $articulos = Articulo::where('texto', 'LIKE', '%'.$request->palabras.'%')
        // ->join("categorias","articulos.id_categoria","=","categorias.id")
        // ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        // ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
        // ->get();

         $articulos = DB::table("articulos")
                 ->join("categorias","articulos.id_categoria","=","categorias.id")
                 ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                 ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                 ->where('texto', 'LIKE', '%'.$request->palabras.'%')
                 ->where("articulos.activado", true)
                 ->orderBy('like','DESC')
                 ->get();

         $contador = count($articulos);



          // dd($articulos);

          $mapasss=array();
          $mapa_final=new mapasModel();
          foreach ($articulos as $key => $articulo) {
          // $campos = new mapasModel();

          $campos = $articulo->camposextras;
          // dd($campos);
          $camposextras = json_decode($campos, true);
          // dd($camposextras[31]);

          $mp_model1=new mapasModel();
          // $mp_model1->address= $camposextras[31] ;
          $mp_model1->latitude=$articulo->latitud;
          $mp_model1->longitude=$articulo->longitud;

          //////////////////////////////////
          // $arrayName = array('' => , );
          $mp_model2 = array('0' => "local/public/images/".$articulo->path,'1' => '300','2' => '215','3' => 'true','5' => "style='background-image: url(local/public/images/".$articulo->path.")'");

         $mp_Location=new mapasModel();
         $mapam=new mapasModel();
         // $mapam->x=$mp_model1;

         $mapam->location = $mp_model1;
         $mapam->title=$articulo->titulo;
         $mapam->listingType= "shop-img-4" ;
         $mapam->markerPin= "<div class='eltd-ls-cat-icon eltd-custom-icon'>
                                  <img src='local/public/images/".$articulo->icono."' alt='".$articulo->titulo."'/>
                             </div>";
         $mapam->featuredImage=$mp_model2;
         $mapam->ratingHtml= "<div class='eltd-listing-rating-holder'>
                                <meta itemprop='ratingValue' content='3.9'>

                                  <div class='eltd-listing-rating-stars-holder'>
                                    <span class='eltd-rating-stars' style='width: 85%'></span>
                                  </div>
                              </div>";

         $mapam->itemUrl= $articulo->categoria."/".$articulo->slug;

         $mapasss[$key]=$mapam;
         }
         // return $mp_model1;
         $mapa_addresses= new mapasModel();
         $mapa_addresses->addresses=$mapasss;
         $mapa_final->multiple=$mapa_addresses;

         $nombre =  Categoria::where('id', 25)->first();
         // dd($mapa_final) ;

         return view('layouts.paginascategorias2',compact('articulos','informacion','nombre','mapa_final','categorias','tags','palabras','contador'));


    }

     public function busquedapalabrascategoria(Request $request)
    {
        //
        // dd($request->all());
        // return $palabras;
        //$articulos = Tag::where('tag', 'LIKE', '%'.$tag.'%');


        $palabras;

        $informacion = Informacion::find(1);

        $categorias = Categoria::all();

        $tags = Tag::all();

        $articulos = null;

        $sinbusqueda = null;

        if ($request->etiqueta == 0 && $request->palabras == null) {
           $sinbusqueda = 1;
        }
        else{
          if ($request->etiqueta != 0) {

            $etiqueta = Tag::where("id",$request->etiqueta)->first();

            $palabras = $etiqueta->tag;
            //dd($tags);
            $articulos = $etiqueta->articulos()->join("categorias","articulos.id_categoria","=","categorias.id")
            ->join("usuarios","articulos.id_usuario","=","usuarios.id")
            ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
            ->where("articulos.activado", true)
            ->orderBy('like','DESC')
            ->get();

          }else{

            $palabras = $request->palabras;

            $articulos = DB::table("articulos")
                   ->join("categorias","articulos.id_categoria","=","categorias.id")
                   ->join("usuarios","articulos.id_usuario","=","usuarios.id")
                   ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
                   ->where('texto', 'LIKE', '%'.$request->palabras.'%')
                   ->where("articulos.activado", true)
                   ->orderBy('like','DESC')
                   ->get();
          }
        }
        // dd($sinbusqueda);
        // $articulos = Articulo::where('texto', 'LIKE', '%'.$request->palabras.'%')
        // ->join("categorias","articulos.id_categoria","=","categorias.id")
        // ->join("usuarios","articulos.id_usuario","=","usuarios.id")
        // ->select("articulos.*","usuarios.nombres","categorias.categoria","categorias.icono")
        // ->get();

        if($sinbusqueda==null){
          $contador = count($articulos);
        }else{
          $contador=0;
        }

         // dd($contador);
         // dd($articulos);
         $mapa_final = array();

         if ($contador != 0) {
           $mapasss=array();
          $mapa_final=new mapasModel();
          foreach ($articulos as $key => $articulo) {
          // $campos = new mapasModel();

          $campos = $articulo->camposextras;
          // dd($campos);
          $camposextras = json_decode($campos, true);
          // dd($camposextras[31]);

          $mp_model1=new mapasModel();
          // $mp_model1->address= $camposextras[31] ;
          $mp_model1->latitude=$articulo->latitud;
          $mp_model1->longitude=$articulo->longitud;

          //////////////////////////////////
          // $arrayName = array('' => , );
          $mp_model2 = array('0' => "local/public/images/".$articulo->path,'1' => '300','2' => '215','3' => 'true','5' => "style='background-image: url(local/public/images/".$articulo->path.")'");

         $mp_Location=new mapasModel();
         $mapam=new mapasModel();
         // $mapam->x=$mp_model1;

         $mapam->location = $mp_model1;
         $mapam->title=$articulo->titulo;
         $mapam->listingType= "shop-img-4" ;
         $mapam->markerPin= "<div class='eltd-ls-cat-icon eltd-custom-icon'>
                                  <img src='local/public/images/".$articulo->icono."' alt='".$articulo->titulo."'/>
                             </div>";
         $mapam->featuredImage=$mp_model2;
         $mapam->ratingHtml= "<div class='eltd-listing-rating-holder'>
                                <meta itemprop='ratingValue' content='3.9'>

                                  <div class='eltd-listing-rating-stars-holder'>
                                    <span class='eltd-rating-stars' style='width: 85%'></span>
                                  </div>
                              </div>";

         $mapam->itemUrl= $articulo->categoria."/".$articulo->slug;

         $mapasss[$key]=$mapam;
         }
         // return $mp_model1;
         $mapa_addresses= new mapasModel();
         $mapa_addresses->addresses=$mapasss;
         $mapa_final->multiple=$mapa_addresses;
         }


         $nombre =  Categoria::where('id', 25)->first();
         // dd($categorias) ;
         return view('layouts.paginascategorias2',compact('sinbusqueda','articulos','informacion','nombre','mapa_final','categorias','tags','palabras','contador'));


    }

     Public function infoeventos(Request $request){
      $codigo="";
      setlocale(LC_ALL,"es_ES.UTF-8");

      $articulo = DB::table("articulos")
                ->where('id','=', $request->id)
                ->first();
                if ($articulo->latitud != null and $articulo->longitud != null) {
                   $direccion= $articulo->latitud.','.$articulo->longitud;
                }else{
                  $direccion=$articulo->lugar;
                }

              $codigo='<div class="row">
                  <div class="tar_evento">
                  <div class="img-back">
                    <button type="button" class="btn btn-circle" onclick="mostrarmes();">
                      <i class="fa fa-times"></i>
                    </button>
                  </div>
                    <div class="img-head">
                      <img class="img-responsive" src='.asset('local/public/images').'/'.$articulo->path.'>
                    </div>
                  <div class="content_evento">

                    <div class="info_evento">
                      <h3>'.$articulo->titulo.'</h3>
                      <ul>
                        <li>'.$articulo->texto.'</li>
                        <li><i class="fa fa-calendar-o"></i>'.strftime("%A, %d de %B de %Y", strtotime($articulo->fechainicio)).'</li>
                        <li><i class="fa fa-clock-o"></i>'.substr($articulo->fechainicio, 11, 5).' - '.substr($articulo->fechafin, 11, 5).'</li>
                        <li><i class="fa fa-map-marker"></i>'.$articulo->lugar.'</li>
                      </ul>



                    </div>
                    <div class="btn_eventos">
                        <a  href="//maps.google.com/maps?daddr='.$direccion.'" target="_blank">
                        <button type="button" class="btn ><i class="fa fa-location-arrow"></i> Como llegar</button>
                        </a>

                      </div>

                  </div>
                </div>

              </div>';


      return $codigo;



     }





}
