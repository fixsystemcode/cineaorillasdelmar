<?php

namespace App\Http\Controllers;

use App\Registro;
use Illuminate\Http\Request;

use Auth;

class RegistroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function limpiar_caracteres_especiales($s) {
        $s = str_replace("á","a",$s);
        $s = str_replace("Á","A",$s);
        $s = str_replace("é","e",$s);
        $s = str_replace("É","E",$s);
        $s = str_replace("í","i",$s);
        $s = str_replace("Í","I",$s);
        $s = str_replace("ó","o",$s);
        $s = str_replace("Ó","O",$s);
        $s = str_replace("ú","u",$s);
        $s = str_replace("Ú","U",$s);
        $s = str_replace(" ","-",$s);
        $s = str_replace("´","-",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);
        //para ampliar los caracteres a reemplazar agregar lineas de este tipo:
        //$s = str_replace("caracter-que-queremos-cambiar","caracter-por-el-cual-lo-vamos-a-cambiar",$s);
        return $s;
    }




    public function index()
    {
        //

        $acep = Auth::user();
        $registros = Registro::orderBy('id','DESC')->paginate(50);
        return view('admin.registros.index', compact('registros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $solicitud = new Registro($request->all());

        // $sd='6Ld3BuAZAAAAAPfCPXed075BMxVFaTkkAvi1lBCc';
        dd($solicitud);
    }

    public function guardar(Request $request){

        $solicitud = new Registro($request->all());

        // $solicitud->save();

        // return 0;

        if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
          $secret = '6Ld3BuAZAAAAAPfCPXed075BMxVFaTkkAvi1lBCc';
          $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
          $responseData = json_decode($verifyResponse);

          $name = !empty($_POST['nombres'])?$_POST['nombres']:'';
          $lastname = !empty($_POST['apellidos'])?$_POST['apellidos']:'';
          $email = !empty($_POST['email'])?$_POST['email']:'';
          $phone = !empty($_POST['telefono'])?$_POST['telefono']:'';
          $institucion = !empty($_POST['institucion'])?$_POST['institucion']:'';

          if($responseData->success):
            //
            $solicitud->save();
            return 0;
          else:
            return 1;
            $errMsg = 'Robot verification failed, please try again.';
        endif;
        else:
          return 2;
          
      endif;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function show(Registro $registro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function edit(Registro $registro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registro $registro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Registro $registro)
    {
        //
    }
}
