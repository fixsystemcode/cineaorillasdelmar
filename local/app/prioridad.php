<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class prioridad extends Model
{
    //
       protected $table = 'prioridad';

    protected $fillable = ['titulo', 'nombre','orden'];

}
