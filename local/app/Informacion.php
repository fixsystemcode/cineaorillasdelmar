<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informacion extends Model
{
    //
     protected $table = 'informacion';

    protected $fillable = ['direccion','dia','hora', 'telefono','quienessomos','biografia', 'visitanos','email'];

 	protected $hidden = ['remember_token'];
}
