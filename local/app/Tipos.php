<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipos extends Model
{
    //
    protected $table = 'tipo_usuarios';

    protected $fillable = ['tipo'];

 	protected $hidden = ['remember_token'];

 	  public function usuarios(){
        return $this->hasMany('App\User');
    }
}
