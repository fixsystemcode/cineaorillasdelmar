<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponente extends Model
{


	public function getNombreAndApellidoAttribute()
	{
	    return $this->nombres . ' ' . $this->apellidos;
	}
    //
    protected $table = 'ponentes';

    protected $fillable = ['id', 'apellidos','nombres','foto', 'profesion', 'link_fb', 'link_tw', 'link_inst', 'link_linke', 'estado'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}