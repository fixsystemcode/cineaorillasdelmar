<?php

namespace App\Exports;

use App\WebinarRegistro;
use Maatwebsite\Excel\Concerns\FromCollection;

class AsistentesExport implements FromCollection
{

    public function collection(){
        return WebinarRegistro::all();

    }
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    
    
}
