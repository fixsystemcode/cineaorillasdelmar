<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dia_evento extends Model
{
    //
    protected $table = 'dia_evento';

    protected $fillable = ['id', 'id_webinar', 'fecha','hora_inicio', 'hora_fin', 'slug', 'estado_enlace'];

 	protected $hidden = [];

 	// public function setPathAttribute($path){

  //       if(!empty($path)){
  //         $nombre = $path->getClientOriginalName();
  //         $this->attributes['path'] = $nombre;
  //         \Storage::disk('local')->put($nombre, \File::get($path));
  //       }

  //    }
}