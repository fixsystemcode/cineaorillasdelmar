<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaInterna extends Model
{
    //
    protected $table = 'categoriainterna';
    protected $primarykey='id';

  protected $fillable = ['nombre','descripcion','patch','updated_at', 'created_at'];

  protected $hidden = [];


}
