<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticuloDocumentos extends Model
{
    //
    Protected $table='articulo_documentos';
    protected $primarykey='id';
    

    protected $fillable = ['id_articulos', 'id_documentos'];
}
