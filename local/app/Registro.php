<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    //
    protected $table = 'registros';

    protected $fillable = ['nombres','apellidos', 'email', 'telefono', 'institucion'];

    protected $hidden = ['g-recaptcha-response'];
}
